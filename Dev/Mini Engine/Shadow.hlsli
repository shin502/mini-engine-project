#define NUM_CASCADES 3

//Shadow
Texture2D gtxtShadowTexture : register(t74);
Texture2D gtxtCasCadeTexture[NUM_CASCADES] : register(t75);
static matrix gmtxProjectToTexture = { 0.5f, 0.0f, 0.0f, 0.0f, 0.0f, -0.5f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.5f, 0.5f, 0.0f, 1.0f };

cbuffer cbLightCamera : register(b6)
{
    matrix gmtxLightViewProjection[NUM_CASCADES];
    float4 gfCascadeLength[NUM_CASCADES];
 
    float gfShadowTexelSize;
    float gfCasacdeNum;
}

cbuffer cbLightCamera : register(b7)
{
    uint lightCameraNum;
}

float GetShadowFactor(float4 shadowPosition[3], float4 positionW)
{
    float fShadowFactor = 1.0f, fsDepth = 1.0f, fShadowOffset = 0 , bias = 0.005f; 
    int idx = -1;
    
    float toCamLen = distance(positionW.xyz, gvCameraPosition.xyz);
    
    [unroll]
    for (int i = 0; i < int(NUM_CASCADES); ++i)
    {
        if (toCamLen <= gfCascadeLength[i].x)
        {
            idx = i;
            break;
        }
    }
    if (idx == -1)
        fShadowFactor = 1.0f;
    else if ((saturate(shadowPosition[idx].x) == shadowPosition[idx].x) &&
        (saturate(shadowPosition[idx].y) == shadowPosition[idx].y) &&
        (saturate(shadowPosition[idx].z) == shadowPosition[idx].z))
    {
        fShadowOffset = idx / gfCasacdeNum;
       
        float depth = shadowPosition[idx].z;
        float2 shadowUV = shadowPosition[idx].xy;
        shadowUV.x /= gfCasacdeNum;
        shadowUV.x += fShadowOffset;//셈플링할 픽셀을 밀어준다. depth 버퍼의 크기가 더 크기 때문이다.
        
        float result = gtxtShadowTexture.SampleCmpLevelZero(gspCmpShadowClamp, shadowUV, depth);
        
       // const float Dilation = 2.0 / gfCascadeLength[idx].x / 1.5f;
       // //픽셀 5등분 
       // float d1 = Dilation * gfShadowTexelSize * 0.125;
       // float d2 = Dilation * gfShadowTexelSize * 0.875;
       // float d3 = Dilation * gfShadowTexelSize * 0.625;
       // float d4 = Dilation * gfShadowTexelSize * 0.375;
       // float result = (
       //2.0 * gtxtShadowTexture.SampleCmpLevelZero(gspCmpShadowClamp, shadowUV, depth) +
       //gtxtShadowTexture.SampleCmpLevelZero(gspCmpShadowClamp, shadowUV + float2(-d2, d1), depth   )     +
       //gtxtShadowTexture.SampleCmpLevelZero(gspCmpShadowClamp, shadowUV + float2(-d1, -d2), depth  )    +
       //gtxtShadowTexture.SampleCmpLevelZero(gspCmpShadowClamp, shadowUV + float2(d2, -d1), depth   )     +
       //gtxtShadowTexture.SampleCmpLevelZero(gspCmpShadowClamp, shadowUV + float2(d1, d2), depth    )      +
                                                            
       //gtxtShadowTexture.SampleCmpLevelZero(gspCmpShadowClamp, shadowUV + float2(-d4, d3), depth   )     +
       //gtxtShadowTexture.SampleCmpLevelZero(gspCmpShadowClamp, shadowUV + float2(-d3, -d4), depth  )    +
       //gtxtShadowTexture.SampleCmpLevelZero(gspCmpShadowClamp, shadowUV + float2(d4, -d3), depth   )     +
       //gtxtShadowTexture.SampleCmpLevelZero(gspCmpShadowClamp, shadowUV + float2(d3, d4), depth    )
       //) / 14.0f;
       
        result = result * result;
        fShadowFactor = (1 - result);
    }
    return fShadowFactor;
}