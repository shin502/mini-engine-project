#include "ParticleShader.h"

ParticleShader::~ParticleShader()
{
}

void ParticleShader::CreatebPipeLineState(ComPtr<ID3D12Device>& device, ComPtr<ID3D12RootSignature>& rootsignature , ID3D12RootSignature* computeRootSignature)
{
	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
	psoDesc.InputLayout = CreateInputLayout();
	psoDesc.pRootSignature = rootsignature.Get();
	psoDesc.VS = CreateVertexShader();
	psoDesc.GS = CreateGeoMetryShader();
	psoDesc.PS = CreatePixelShader();
	psoDesc.RasterizerState = CreateRasterizerState();
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.DepthStencilState = CreateDepthStencilState();
	psoDesc.SampleMask = UINT_MAX;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_POINT;
	psoDesc.NumRenderTargets = 1;
	psoDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	psoDesc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
	psoDesc.SampleDesc.Count = 1;

	D3D12_BLEND_DESC blendDesc;

	blendDesc.AlphaToCoverageEnable = FALSE;
	blendDesc.IndependentBlendEnable = FALSE;
	blendDesc.RenderTarget[0].BlendEnable = TRUE;
	blendDesc.RenderTarget[0].LogicOpEnable = FALSE;

	blendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
	blendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
	blendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ZERO;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ZERO;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].LogicOp = D3D12_LOGIC_OP_NOOP;
	UINT colorMask = D3D12_COLOR_WRITE_ENABLE_RED + D3D12_COLOR_WRITE_ENABLE_GREEN + D3D12_COLOR_WRITE_ENABLE_BLUE;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = colorMask;

	psoDesc.BlendState = blendDesc;
	ThrowIfFailed(device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_bpipelineState)));

	psoDesc.VS = CreateShadowVertexShader();
	psoDesc.PS = CreateShadowPixelShader();
	psoDesc.GS = CreateShadowGeoMetryShader();

	ThrowIfFailed(device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_ShadowPipeLine)));

	if (psoDesc.InputLayout.pInputElementDescs) delete[] psoDesc.InputLayout.pInputElementDescs;
	if (m_vsBlob) m_vsBlob->Release();
	if (m_psBlob) m_psBlob->Release();


	D3D12_COMPUTE_PIPELINE_STATE_DESC computePipelineStateDesc;
	::ZeroMemory(&computePipelineStateDesc, sizeof(D3D12_COMPUTE_PIPELINE_STATE_DESC));
	computePipelineStateDesc.pRootSignature = computeRootSignature;
	computePipelineStateDesc.CS = CreateComputeShader();
	ThrowIfFailed(device->CreateComputePipelineState(&computePipelineStateDesc, IID_PPV_ARGS (&m_ComputePipeline)));
	if (m_csBlob) m_csBlob->Release();
}
D3D12_INPUT_LAYOUT_DESC ParticleShader::CreateInputLayout()
{
	UINT inputElementDescsNum = 4;
	D3D12_INPUT_ELEMENT_DESC* inputElementDescs = new D3D12_INPUT_ELEMENT_DESC[inputElementDescsNum];

	inputElementDescs[0] = { "SIZE", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	inputElementDescs[1] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	inputElementDescs[2] = { "TEXID", 0, DXGI_FORMAT_R32_UINT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	inputElementDescs[3] = { "COPYIDX",  0, DXGI_FORMAT_R32_UINT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	D3D12_INPUT_LAYOUT_DESC inputLayout;
	inputLayout.NumElements = inputElementDescsNum;
	inputLayout.pInputElementDescs = inputElementDescs;
	return inputLayout;
}
void ParticleShader::AnimateAndUpdate(float timeElapsed , XMFLOAT3 cameraPos)
{
	// particle의 Age를 최댓값만 CPU에서 계산하여 최댓값을 다 지났을 경우 지워준다.
	for (int i = 0; i < particles.size(); ++i)
	{
		if (particles[i].age == INFINITE)
		{
		}
		else if (particles[i].age != -1.0f) {
			if (particles[i].age > 0.0f)
			{
				particles[i].age -= timeElapsed;
			}
			else
			{
				particles.erase(particles.begin() + i);
				--i;
			}
		}
	}

}
D3D12_DEPTH_STENCIL_DESC ParticleShader::CreateDepthStencilState()
{
	D3D12_DEPTH_STENCIL_DESC depthStencilDesc;
	::ZeroMemory(&depthStencilDesc, sizeof(D3D12_DEPTH_STENCIL_DESC));
	depthStencilDesc.DepthEnable = TRUE;
	depthStencilDesc.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ZERO;
	depthStencilDesc.DepthFunc = D3D12_COMPARISON_FUNC_LESS_EQUAL;
	depthStencilDesc.StencilEnable = FALSE;
	depthStencilDesc.StencilReadMask = 0x00;
	depthStencilDesc.StencilWriteMask = 0x00;
	depthStencilDesc.FrontFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D12_COMPARISON_FUNC_NEVER;
	depthStencilDesc.BackFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D12_COMPARISON_FUNC_NEVER;

	return(depthStencilDesc);
}
void ParticleShader::CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
	// 정점버퍼 및 포지션버퍼(인스턴스) 생성 뭉텅이의 위치와 ,파티클의 사이즈 , 텍스처 정보를 담고 있다.
	ParticleVertex* vertex = new ParticleVertex[BUFFERSIZE]();
	//파티클(입자)의 점 위치를 담을 버퍼이다. 점의 개수는 최대개수는 1000개이다. 
	vertexBuffer = ::CreateBufferResource(device, commandList, vertex, sizeof(ParticleVertex) * BUFFERSIZE, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
	vertexBuffer->Map(0, NULL, (void**)&mappedBuffer);
	vertexBufferView.BufferLocation = vertexBuffer->GetGPUVirtualAddress();
	vertexBufferView.StrideInBytes = sizeof(ParticleVertex);
	vertexBufferView.SizeInBytes = sizeof(ParticleVertex) * BUFFERSIZE;

	// 파티클 버퍼, ShaderResourceView, UnorderedResourceView 생성
	// copyBuffer를 통해 재사용한다., copyBuffer는 ComputeShader에서 재사용 데이터로도 사용된다.
	VS_SB_PARTICLE* scatterParticle = new VS_SB_PARTICLE[PARTICLESIZE];

	std::default_random_engine dre;
	std::uniform_real_distribution<float> urdPos(-1.0f, 1.0f);
	std::uniform_real_distribution<float> urdVel(-2.0f, 2.0f);
	std::uniform_real_distribution<float> explosionAge(2.0f, 100.0f);
	std::uniform_real_distribution<float> scatterAge(2.0f, 15.0f);


	for (int i = 0; i < PARTICLESIZE; ++i) {

		scatterParticle[i].position = XMFLOAT3(urdPos(dre), urdPos(dre), urdPos(dre));
		scatterParticle[i].velocity = XMFLOAT3(urdVel(dre), urdVel(dre), urdVel(dre));
		scatterParticle[i].age = scatterAge(dre);
	}

	for (int i = 0; i < BUFFERSIZE; ++i) {
		copyBuffer[i] = ::CreateBufferResource(device, commandList, NULL, sizeof(VS_SB_PARTICLE) * BUFFERSIZE, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_GENERIC_READ, NULL);
		copyBuffer[i]->Map(0, NULL, (void**)&mappedCopyBuffer[i]);

		copyBufferCS[i] = ::CreateBufferResource(device, commandList, NULL, sizeof(VS_SB_PARTICLE) * BUFFERSIZE, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, NULL, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);
		::ResourceBarrier(commandList, copyBufferCS[i].Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
	}
	UINT bytesNum = sizeof(VS_SB_PARTICLE) * PARTICLESIZE * BUFFERSIZE;
	//인스턴싱 개수만큼 파이클의 위치, 속도 , 수명을 만들어준다. 위에만든 것은 컴퓨트 쉐이더에 들어가지 않는것이고 이것들은 들어간다.
	particleBuffer = ::CreateBufferResource(device, commandList, NULL, bytesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, NULL, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);

	::ResourceBarrier(commandList, particleBuffer.Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);

	for (int j = 0; j < BUFFERSIZE; ++j) {
		for (int i = 0; i < PARTICLESIZE; ++i)
		{
			mappedCopyBuffer[j][i] = scatterParticle[i];
		}
	}

	for (int i = 0; i < BUFFERSIZE; ++i) {
		mappedBuffer[i].size = XMFLOAT2(5.0f, 5.0f);
		mappedBuffer[i].copyIdx = 0;
		mappedBuffer[i].texid = 0;
	}


	delete[] vertex;
	delete[] scatterParticle;
}

void ParticleShader::UpdateShaderVariables(int idx)
{
	//mappedBuffer[idx].position = XMFLOAT3(particles[idx].position);
}

void ParticleShader::Compute(ID3D12GraphicsCommandList* commandList, int idx, int instanceNum)
{
	//파티클 입자의 속도, 수명 , 위치를 계산한다.
	commandList->SetPipelineState(m_ComputePipeline.Get());

	commandList->SetComputeRoot32BitConstant(ComputeRootParticleType, particles[idx].type, 0);
	commandList->SetComputeRoot32BitConstant(ComputeRootParticleType, particles[idx].copyIdx, 1);
	commandList->SetComputeRootShaderResourceView(ComputeRootParticleInfoSRV, particleBuffer->GetGPUVirtualAddress());
	commandList->SetComputeRootShaderResourceView(ComputeRootParticleCopySRV, copyBufferCS[particles[idx].copyIdx]->GetGPUVirtualAddress());

	::ResourceBarrier(commandList, particleBuffer.Get(), D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_UNORDERED_ACCESS);
	commandList->SetComputeRootUnorderedAccessView(ComputeRootParticleInfoUAV, particleBuffer->GetGPUVirtualAddress());
	commandList->Dispatch(static_cast<int>(ceil(PARTICLESIZE / BLOCKSIZE)), 1, 1); //한번에 256개의 쓰레드가 작동하므로 파티클 사이즈 / 256 개의 그룹으로 편성
	::ResourceBarrier(commandList, particleBuffer.Get(), D3D12_RESOURCE_STATE_UNORDERED_ACCESS, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
}

void ParticleShader::CreateParticle(ID3D12GraphicsCommandList* commandList, int idx, int type , XMFLOAT2 size , UINT texid)
{
	static int count = 0;

	::ResourceBarrier(commandList, particleBuffer.Get(), D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_COPY_DEST);
	::ResourceBarrier(commandList, copyBufferCS[idx].Get(), D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_COPY_DEST);
	//처음에 설정한 파티클 값을 복사하여 넣어준다.
	commandList->CopyResource(copyBufferCS[idx].Get(), copyBuffer[idx].Get());

	UINT64 offsetbytes = UINT64(sizeof(VS_SB_PARTICLE) * PARTICLESIZE * idx);
	UINT64 numbytes = UINT64(sizeof(VS_SB_PARTICLE) * PARTICLESIZE);
	commandList->CopyBufferRegion(particleBuffer.Get(), offsetbytes, copyBuffer[idx].Get(), NULL, numbytes);
	::ResourceBarrier(commandList, copyBufferCS[idx].Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
	::ResourceBarrier(commandList, particleBuffer.Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);

	float age{};
	if (type == PARTICLE_TYPE_EXPLOSION)
		age = count % 2 ? 3.0f : 3.5f;
	else if (type == PARTICLE_TYPE_SCATTER)
		age = INFINITY;
	particles.emplace_back(ParticleInfoInCPU(10.0f * count++, 300.0f, 0.0f, age, type, size , texid, particles.size()));
}

void ParticleShader::CreateDynamicParticle(ID3D12GraphicsCommandList* commandList, int idx, int type, int texid, float sizeX, float sizeY , float age, XMFLOAT3 pos , ParticleBlobModify blob)
{
	particles.emplace_back(ParticleInfoInCPU(pos.x, pos.y, pos.z, age, type, XMFLOAT2(sizeX,sizeY), texid, particles.size()));
	ModifyParticle(commandList, particles.size() - 1, 0, blob);
}

void ParticleShader::ModifyParticle(ID3D12GraphicsCommandList* commandList, int idx, int copyIdx, ParticleBlobModify blob)
{
	std::default_random_engine dre;
	std::uniform_real_distribution<float> urdPosX(blob.positionMin.x, blob.positionMax.x);
	std::uniform_real_distribution<float> urdPosY(blob.positionMin.y, blob.positionMax.y);
	std::uniform_real_distribution<float> urdPosZ(blob.positionMin.z, blob.positionMax.z);

	std::uniform_real_distribution<float> urdVelX(blob.velocityMin.x, blob.velocityMax.x);
	std::uniform_real_distribution<float> urdVelY(blob.velocityMin.y, blob.velocityMax.y);
	std::uniform_real_distribution<float> urdVelZ(blob.velocityMin.z, blob.velocityMax.z);
	std::uniform_real_distribution<float> urdAge(blob.ageMin, blob.ageMax);


	for (int i = 0; i < PARTICLESIZE; ++i)
	{
		mappedCopyBuffer[particles[idx].copyIdx][i].position = XMFLOAT3(urdPosX(dre), urdPosY(dre), urdPosZ(dre));
		mappedCopyBuffer[particles[idx].copyIdx][i].velocity = XMFLOAT3(urdVelX(dre), urdVelY(dre), urdVelZ(dre));
		mappedCopyBuffer[particles[idx].copyIdx][i].age = urdAge(dre);
	}

	::ResourceBarrier(commandList, particleBuffer.Get(), D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_COPY_DEST);
	::ResourceBarrier(commandList, copyBufferCS[particles[idx].copyIdx].Get(), D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_COPY_DEST);
	commandList->CopyResource(copyBufferCS[particles[idx].copyIdx].Get(), copyBuffer[idx].Get());
	commandList->CopyBufferRegion(particleBuffer.Get(), sizeof(VS_SB_PARTICLE) * PARTICLESIZE * particles[idx].copyIdx, copyBuffer[particles[idx].copyIdx].Get(), NULL, sizeof(VS_SB_PARTICLE) * PARTICLESIZE);
	::ResourceBarrier(commandList, copyBufferCS[particles[idx].copyIdx].Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
	::ResourceBarrier(commandList, particleBuffer.Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
}

void ParticleShader::Render(ID3D12GraphicsCommandList* commandList , XMFLOAT3 cPos)
{
	//sort Particle
	SortParticle(cPos);
	//Rendering
	commandList->SetPipelineState(m_bpipelineState.Get());
	commandList->SetGraphicsRootShaderResourceView(GraphicsParticleData, particleBuffer->GetGPUVirtualAddress());
	D3D12_VERTEX_BUFFER_VIEW views[] = { vertexBufferView };
	commandList->IASetVertexBuffers(0, 1, views);
	commandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_POINTLIST);
	commandList->DrawInstanced(PARTICLESIZE, particles.size(), 0, 0);
}

void ParticleShader::SortParticle(XMFLOAT3 cPos)
{
	sort(particles.begin(), particles.end(), [cPos](ParticleInfoInCPU& a , ParticleInfoInCPU& b) {
		return Vector3::Length(Vector3::Subtract(cPos, a.position)) > Vector3::Length(Vector3::Subtract(cPos, b.position));
	});
	for (int i = 0; i < particles.size(); ++i)
	{
		mappedBuffer[i].position = particles[i].position;
		mappedBuffer[i].size = particles[i].size;
		mappedBuffer[i].texid = particles[i].texid;
		mappedBuffer[i].copyIdx = particles[i].copyIdx;
	}
}

void ParticleShader::ShadowRender(ID3D12GraphicsCommandList* commandList)
{
	commandList->SetPipelineState(m_ShadowPipeLine.Get());
	commandList->SetGraphicsRootShaderResourceView(GraphicsParticleData, particleBuffer->GetGPUVirtualAddress());

	D3D12_VERTEX_BUFFER_VIEW views[] = { vertexBufferView };
	commandList->IASetVertexBuffers(0, 1, views);
	commandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_POINTLIST);
	commandList->DrawInstanced(PARTICLESIZE, particles.size(), 0, 0);
}

void ParticleShader::BuildShaderParticle(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
	CreateShaderVariables(device, commandList);
	for (int i = 0; i < 4; ++i)
	{
		CreateParticle(commandList, i, PARTICLE_TYPE_SCATTER , XMFLOAT2(5.0f, 5.0f), E_SMOKE1PARTICLE);
	}
}

void ParticleShader::PrePareCompute(ID3D12GraphicsCommandList* commandList)
{
	for (int i = 0; i < particles.size(); ++i)
	{
		Compute(commandList, i, particles.size());	// ComputeShader를 사용하여 리소스에 값을 쓴다.	
	}
}

void ParticleShader::ReleaseUploadBuffer()
{
	if (uploadVertexBuffer) {
		uploadVertexBuffer->Release();
	}
}

D3D12_SHADER_BYTECODE ParticleShader::CreateVertexShader()
{
	return(CompileShaderFromFile(L"Particle.hlsl", "VSParticle", "vs_5_1", &m_vsBlob));
}

D3D12_SHADER_BYTECODE ParticleShader::CreatePixelShader()
{
	return(CompileShaderFromFile(L"Particle.hlsl", "PSParticle", "ps_5_1", &m_psBlob));
}

D3D12_SHADER_BYTECODE ParticleShader::CreateGeoMetryShader()
{
	return(CompileShaderFromFile(L"Particle.hlsl", "GSParticleDraw", "gs_5_1", &m_gsBlob));
}

D3D12_SHADER_BYTECODE ParticleShader::CreateShadowVertexShader()
{
	return(CompileShaderFromFile(L"Particle.hlsl", "VSParticle", "vs_5_1", &m_vsBlob));
}

D3D12_SHADER_BYTECODE ParticleShader::CreateShadowPixelShader()
{
	return(CompileShaderFromFile(L"Shadow.hlsl", "PS_SHADOW_PARTICLE", "ps_5_1", &m_psBlob));
}

D3D12_SHADER_BYTECODE ParticleShader::CreateShadowGeoMetryShader()
{
	return(CompileShaderFromFile(L"Shadow.hlsl", "GS_SHADOW_PARTICLE", "gs_5_1", &m_gsBlob));
}

D3D12_SHADER_BYTECODE ParticleShader::CreateComputeShader()
{
	return CompileShaderFromFile(L"CSParticle.hlsl", "main", "cs_5_1", &m_csBlob);
}

D3D12_RASTERIZER_DESC ParticleShader::CreateRasterizerState()
{
	D3D12_RASTERIZER_DESC rasterizerDesc;
	::ZeroMemory(&rasterizerDesc, sizeof(D3D12_RASTERIZER_DESC));
	rasterizerDesc.FillMode = D3D12_FILL_MODE_SOLID;
	rasterizerDesc.CullMode = D3D12_CULL_MODE_BACK;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;
	rasterizerDesc.DepthClipEnable = TRUE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.ForcedSampleCount = 0;
	rasterizerDesc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;
	return rasterizerDesc;
}

int ParticleShader::GetShaderNumber()
{
	return E_PARTICLESHADER;
}
