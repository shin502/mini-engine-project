
#define PARTICLESIZE 100

struct ParticleData
{
    float3 position;
    float3 velocity;
    float age;
};

StructuredBuffer<ParticleData> gsbParticleData : register(t88);