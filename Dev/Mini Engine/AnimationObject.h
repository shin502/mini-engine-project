#pragma once
#include "HierarchyObject.h"

class Scene;

class AnimationObj : public HierarchyObj {
private:
	
public:
	
	AnimationObj() {}
	AnimationObj(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, LoadedModelInfo* model, int animationTracks, int job);
	void SetInstancingTexture(Scene* scene , ID3D12Device* device, ID3D12GraphicsCommandList* commandList,LoadedModelInfo* model);
	int GetObjKinds();
	virtual ~AnimationObj();
};


