#include "Mesh.h"

Mesh::Mesh()
{

}

Mesh::~Mesh()
{
	if (m_vertices != NULL) {
		delete m_vertices;
	}
}

void Mesh::Render(ComPtr<ID3D12GraphicsCommandList>& commandList)
{
	commandList.Get()->IASetPrimitiveTopology(m_primitiveTopology);
	commandList.Get()->IASetVertexBuffers(m_Slot, 1, &m_vertexBufferView);
	if (m_indexBuffer) {
		commandList.Get()->IASetIndexBuffer(&m_indexBufferView);
		commandList.Get()->DrawIndexedInstanced(m_indicesNum, 1, 0, 0, 0);
	}
	else {
		commandList.Get()->DrawInstanced(m_verticesNum, 1, m_Offset, 0);
	}
}

void Mesh::Render(ID3D12GraphicsCommandList* commandList, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingBufferViews)
{

	D3D12_VERTEX_BUFFER_VIEW* vertexBufferViews;
	vertexBufferViews = new D3D12_VERTEX_BUFFER_VIEW[2];
	vertexBufferViews[0] = m_vertexBufferView;
	vertexBufferViews[1] = instancingBufferViews;

	commandList->IASetPrimitiveTopology(m_primitiveTopology);
	commandList->IASetVertexBuffers(m_Slot, 2, vertexBufferViews);
	if (m_indexBuffer)
	{
		commandList->IASetIndexBuffer(&m_indexBufferView);
		commandList->DrawIndexedInstanced(m_indicesNum, instancesNum, 0, 0, 0);
	}
	else
	{
		commandList->DrawInstanced(m_verticesNum, instancesNum, m_Offset, 0);
	}
	delete[] vertexBufferViews;
}

void Mesh::ReleaseUploadBuffer()
{
	if (m_vertexUploadBuffer) {
		m_vertexUploadBuffer->Release();
		m_vertexUploadBuffer = NULL;
	}
	if (m_indexUploadBuffer) {
		m_indexUploadBuffer->Release();
		m_indexUploadBuffer = NULL;
	}
}

DiffuseCubeMesh::~DiffuseCubeMesh()
{
	if (m_vertices) {
		delete[] m_vertices;
		m_vertices = NULL;
	}
	if (m_indices) {
		delete[] m_indices;
		m_indices = NULL;
	}
}

DiffuseCubeMesh::DiffuseCubeMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, float width , float height , float depth)
{
	m_verticesNum = 8;
	int stride = sizeof(DiffuseVertex);
	m_primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	float halfX = width * 0.5f; 
	float halfY = height * 0.5f;
	float halfZ = depth * 0.5f;

	m_vertices = new DiffuseVertex[8];
	m_vertices[0] = DiffuseVertex(XMFLOAT3(-halfX, +halfY, -halfZ), RANDOM_COLOR);
	m_vertices[1] = DiffuseVertex(XMFLOAT3(+halfX, +halfY, -halfZ), RANDOM_COLOR);
	m_vertices[2] = DiffuseVertex(XMFLOAT3(+halfX, +halfY, +halfZ), RANDOM_COLOR);
	m_vertices[3] = DiffuseVertex(XMFLOAT3(-halfX, +halfY, +halfZ), RANDOM_COLOR);
	m_vertices[4] = DiffuseVertex(XMFLOAT3(-halfX, -halfY, -halfZ), RANDOM_COLOR);
	m_vertices[5] = DiffuseVertex(XMFLOAT3(+halfX, -halfY, -halfZ), RANDOM_COLOR);
	m_vertices[6] = DiffuseVertex(XMFLOAT3(+halfX, -halfY, +halfZ), RANDOM_COLOR);
	m_vertices[7] = DiffuseVertex(XMFLOAT3(-halfX, -halfY, +halfZ), RANDOM_COLOR);
	m_vertexBuffer = ::CreateBufferResource(device, commandList, m_vertices, stride * m_verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_vertexUploadBuffer);
	m_vertexBufferView.BufferLocation = m_vertexBuffer.Get()->GetGPUVirtualAddress();
	m_vertexBufferView.StrideInBytes = stride;
	m_vertexBufferView.SizeInBytes = stride * m_verticesNum;

	m_indicesNum = 36;
	m_indices = new UINT[m_indicesNum];
	m_indices[0] = 3; m_indices[1] = 1; m_indices[2] = 0;
	m_indices[3] = 2; m_indices[4] = 1; m_indices[5] = 3;
	m_indices[6] = 0; m_indices[7] = 5; m_indices[8] = 4;
	m_indices[9] = 1; m_indices[10] = 5; m_indices[11] = 0;
	m_indices[12] = 3; m_indices[13] = 4; m_indices[14] = 7;
	m_indices[15] = 0; m_indices[16] = 4; m_indices[17] = 3;
	m_indices[18] = 1; m_indices[19] = 6; m_indices[20] = 5;
	m_indices[21] = 2; m_indices[22] = 6; m_indices[23] = 1;
	m_indices[24] = 2; m_indices[25] = 7; m_indices[26] = 6;
	m_indices[27] = 3; m_indices[28] = 7; m_indices[29] = 2;
	m_indices[30] = 6; m_indices[31] = 4; m_indices[32] = 5;
	m_indices[33] = 7; m_indices[34] = 4; m_indices[35] = 6;

	m_indexBuffer = ::CreateBufferResource(device, commandList, m_indices, sizeof(UINT) * m_indicesNum,
		D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_INDEX_BUFFER, &m_indexUploadBuffer);

	m_indexBufferView.BufferLocation = m_indexBuffer->GetGPUVirtualAddress();
	m_indexBufferView.Format = DXGI_FORMAT_R32_UINT;
	m_indexBufferView.SizeInBytes = sizeof(UINT) * m_indicesNum;


}

TerrainMesh::~TerrainMesh()
{
	if (m_vertices) {
		delete[] m_vertices;
		m_vertices = NULL;
	}
	if (m_indices) {
		delete[] m_indices;
		m_indices = NULL;
	}

}

TerrainMesh::TerrainMesh(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList, int width, int length, XMFLOAT3 scale)
{
	m_verticesNum = width * length;
	int stride = sizeof(TexturedVertex);
	m_primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP;
	m_vertices = new TexturedVertex[m_verticesNum];
	
	m_width = width;
	m_length = length;
	m_scale = scale;
	
	//load height map
	LoadHeightMapFile();
	{
		int i = 0;
		for (int z = 0; z < m_length; ++z) {
			for (int x = 0; x < m_width; ++x, ++i) {
				float height = GetPixelHeight(x,z);
				m_vertices[i] = TexturedVertex(XMFLOAT3(float(x) * scale.x, height, float(z) * scale.z),
					XMFLOAT2(float(x), float(z)));
			}
		}
		for (int j = 0; j < m_width * m_length; ++j) {
			m_vertices[j].SetNormal(CalNormal(j));
		}
	}
	
	m_vertexBuffer = ::CreateBufferResource(device.Get(), commandList.Get(), m_vertices, stride * m_verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_vertexUploadBuffer);
	m_vertexBufferView.BufferLocation = m_vertexBuffer.Get()->GetGPUVirtualAddress();
	m_vertexBufferView.StrideInBytes = stride;
	m_vertexBufferView.SizeInBytes = stride * m_verticesNum;

	m_indicesNum = ((width * 2) * (length - 1)) + ((length - 1) - 1);
	m_indices = new UINT[m_indicesNum];
	
	for (int j = 0, z = 0; z < length - 1; z++)
	{
		if ((z % 2) == 0)
		{
			for (int x = 0; x < width; x++)
			{
				if ((x == 0) && (z > 0)) m_indices[j++] = (UINT)(x + (z * width));
				m_indices[j++] = (UINT)(x + (z * width));
				m_indices[j++] = (UINT)((x + (z * width)) + width);
			}
		}
		else
		{
			for (int x = width - 1; x >= 0; x--)
			{
				if (x == (width - 1)) m_indices[j++] = (UINT)(x + (z * width));
				m_indices[j++] = (UINT)(x + (z * width));
				m_indices[j++] = (UINT)((x + (z * width)) + width);
			}
		}
	}

	m_indexBuffer = ::CreateBufferResource(device.Get(), commandList.Get(), m_indices, sizeof(UINT) * m_indicesNum,
		D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_INDEX_BUFFER, &m_indexUploadBuffer);

	m_indexBufferView.BufferLocation = m_indexBuffer->GetGPUVirtualAddress();
	m_indexBufferView.Format = DXGI_FORMAT_R32_UINT;
	m_indexBufferView.SizeInBytes = sizeof(UINT) * m_indicesNum;
}

void TerrainMesh::LoadHeightMapFile()
{
	BYTE* heightmapPixel = new BYTE[m_width * m_length];
	HANDLE hFile = ::CreateFile(L"terrainMap/myhome2.raw", GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_ATTRIBUTE_READONLY, NULL);
	DWORD dwBytesRead;
	::ReadFile(hFile, heightmapPixel, (m_width * m_length), &dwBytesRead, NULL);
	::CloseHandle(hFile);

	m_heightMapPixel;
	m_heightMapPixel.resize(m_width * m_length);
	for (int y = 0; y < m_length; y++)
	{
		for (int x = 0; x < m_width; x++)
		{
			m_heightMapPixel[x + ((m_length - 1 - y) * m_width)] = heightmapPixel[x + (y * m_width)]; // 맨 마지막 줄부터 채워나간다.
		}
	}

	if (heightmapPixel) delete[] heightmapPixel;
}

XMFLOAT3 TerrainMesh::CalNormal(int index)
{
	bool isleftup = false;
	bool isupright = false;
	bool isrightdown = false;
	bool isdownleft = false;

	XMFLOAT3 leftupNormal{ 0.0f , 0.0f , 0.0f };
	XMFLOAT3 uprightNormal{ 0.0f , 0.0f , 0.0f };
	XMFLOAT3 rightdownNormal{ 0.0f , 0.0f , 0.0f };
	XMFLOAT3 downleftNormal{ 0.0f , 0.0f , 0.0f };

	XMFLOAT3 leftupTangent{ 0.0f , 0.0f , 0.0f };
	XMFLOAT3 uprightTangent{ 0.0f , 0.0f , 0.0f };
	XMFLOAT3 rightdownTangent{ 0.0f , 0.0f , 0.0f };
	XMFLOAT3 downleftTangent{ 0.0f , 0.0f , 0.0f };

	XMFLOAT3 leftupbiTangent{ 0.0f , 0.0f , 0.0f };
	XMFLOAT3 uprightbiTangent{ 0.0f , 0.0f , 0.0f };
	XMFLOAT3 rightdownbiTangent{ 0.0f , 0.0f , 0.0f };
	XMFLOAT3 downleftbiTangent{ 0.0f , 0.0f , 0.0f };


	float bias = 1.0;
	//4개의 삼각형의 노멀을 구한다 
	auto& d2t = m_vertices;
	if (index % m_width != 0 && index + m_width < m_verticesNum) // 왼쪽 위 
	{
		//왼
		auto v1 = Vector3::Subtract(d2t[index - 1].GetPos(), d2t[index].GetPos());
		//위
		auto v2 = Vector3::Subtract(d2t[index + m_width].GetPos(), d2t[index].GetPos());
		leftupNormal = Vector3::CrossProduct(v1, v2);
		leftupNormal.y = (leftupNormal.y * bias);
		leftupNormal = (leftupNormal);

		CalTangent(index, index - 1, index + m_width , leftupTangent,leftupbiTangent,leftupNormal);
		isleftup = true;
	}
	if (index + m_width < m_verticesNum && (index + 1) % m_width != 0) // 위 오른쪽  
	{
		//위
		auto v1 = Vector3::Subtract(d2t[index + m_width].GetPos(), d2t[index].GetPos());
		//오른
		auto v2 = Vector3::Subtract(d2t[index + 1].GetPos(), d2t[index].GetPos());

		uprightNormal = Vector3::CrossProduct(v1, v2);
		uprightNormal.y = (uprightNormal.y * bias);
		uprightNormal = (uprightNormal);

		CalTangent(index, index + m_width, index + 1 ,uprightTangent,uprightbiTangent, uprightNormal);
		isupright = true;
	}
	if ((index + 1) % m_width != 0 && index - m_width >= 0) // 오른쪽 아래   
	{
		//오른
		auto v1 = Vector3::Subtract(d2t[index + 1].GetPos(), d2t[index].GetPos());
		//아래
		auto v2 = Vector3::Subtract(d2t[index - m_width].GetPos(), d2t[index].GetPos());

		rightdownNormal = Vector3::CrossProduct(v1, v2);
		rightdownNormal.y = (rightdownNormal.y * bias);
		rightdownNormal = (rightdownNormal);

		CalTangent(index, index + 1, index - m_width,rightdownTangent,rightdownbiTangent,rightdownNormal);
		isrightdown = true;
	}
	if (index - m_width >= 0 && index % m_width != 0) // 아래 왼쪽 
	{
		//아래
		auto v1 = Vector3::Subtract(d2t[index - m_width].GetPos(), d2t[index].GetPos());
		//왼쪽
		auto v2 = Vector3::Subtract(d2t[index - 1].GetPos(), d2t[index].GetPos());

		downleftNormal = Vector3::CrossProduct(v1, v2);
		downleftNormal.y = (downleftNormal.y * bias);
		downleftNormal = (downleftNormal);

		CalTangent(index, index - m_width, index - 1,downleftTangent,downleftbiTangent,downleftNormal);
		isdownleft = true;
	}

	XMFLOAT3 finalnormal = (Vector3::Add(Vector3::Add(Vector3::Add(leftupNormal, uprightNormal), rightdownNormal), downleftNormal));
	finalnormal.x = (finalnormal.x / (isleftup + isupright + isrightdown + isdownleft));
	finalnormal.y = (finalnormal.y / (isleftup + isupright + isrightdown + isdownleft));
	finalnormal.z = (finalnormal.z / (isleftup + isupright + isrightdown + isdownleft));

	XMFLOAT3 finaltangent = (Vector3::Add(Vector3::Add(Vector3::Add(leftupTangent, uprightTangent), rightdownTangent), downleftTangent));
	finaltangent.x = (finaltangent.x / (isleftup + isupright + isrightdown + isdownleft));
	finaltangent.y = (finaltangent.y / (isleftup + isupright + isrightdown + isdownleft));
	finaltangent.z = (finaltangent.z / (isleftup + isupright + isrightdown + isdownleft));

	XMFLOAT3 finalbitangent = (Vector3::Add(Vector3::Add(Vector3::Add(leftupbiTangent, uprightbiTangent), rightdownbiTangent), downleftbiTangent));
	finalbitangent.x = (finalbitangent.x / (isleftup + isupright + isrightdown + isdownleft));
	finalbitangent.y = (finalbitangent.y / (isleftup + isupright + isrightdown + isdownleft));
	finalbitangent.z = (finalbitangent.z / (isleftup + isupright + isrightdown + isdownleft));

	m_vertices[index].SetTangent(Vector3::Normalize(finaltangent));
	m_vertices[index].SetBiTangent(Vector3::Normalize(finalbitangent));

	return Vector3::Normalize(finalnormal);
}

XMFLOAT3 TerrainMesh::CalTangent(int index0 , int index1 , int index2 , XMFLOAT3& tangent, XMFLOAT3& biTangents, XMFLOAT3& normals)
{
	float u10, v10, u20, v20, x10, x20, y10, y20, z10, z20;
	
	u10 = m_vertices[index1].GetUV().x - m_vertices[index0].GetUV().x;
	v10 = m_vertices[index1].GetUV().y - m_vertices[index0].GetUV().y;
	u20 = m_vertices[index2].GetUV().x - m_vertices[index0].GetUV().x;
	v20 = m_vertices[index2].GetUV().y - m_vertices[index0].GetUV().y;
	x10 = m_vertices[index1].GetPos().x - m_vertices[index0].GetPos().x;
	y10 = m_vertices[index1].GetPos().y - m_vertices[index0].GetPos().y;
	z10 = m_vertices[index1].GetPos().z - m_vertices[index0].GetPos().z;
	x20 = m_vertices[index2].GetPos().x - m_vertices[index0].GetPos().x;
	y20 = m_vertices[index2].GetPos().y - m_vertices[index0].GetPos().y;
	z20 = m_vertices[index2].GetPos().z - m_vertices[index0].GetPos().z;
	float invDeterminant = 1.0f / (u10 * v20 - u20 * v10);
	 tangent = XMFLOAT3(invDeterminant * (v20 * x10 - v10 * x20), invDeterminant * (v20 * y10 - v10 * y20), invDeterminant * (v20 * z10 - v10 * z20));
	 biTangents = XMFLOAT3(invDeterminant * (-u20 * x10 + u10 * x20), invDeterminant * (-u20 * y10 + u10 * y20), invDeterminant * (-u20 * z10 + u10 * z20));
	 normals = Vector3::CrossProduct(biTangents, tangent, true);

	 return XMFLOAT3();

}

float TerrainMesh::GetPixelHeight(int x , int z)
{
	return float(m_heightMapPixel[x + (z * m_width)]) * m_scale.y;
}

float TerrainMesh::GetHeightByPos(float px, float pz, int startx, int startz, int width, int length)
{
	XMFLOAT3 tmpPos = XMFLOAT3(px, 1000000.0f, pz);
	XMFLOAT3 dir = XMFLOAT3(0, -1, 0);
	auto playerPos = XMLoadFloat3((XMFLOAT3*)&tmpPos);
	auto direction = XMLoadFloat3((XMFLOAT3*)&dir);

	float fdist = FLT_MAX;

	for (int z = startz; z <= startz + length; ++z) {
		for (int x = startx; x <= startx + width; ++x) {
			if (x + 1 == m_width || z + 1 == m_length)
				continue;
			float dist = FLT_MAX;
			auto v1 = XMLoadFloat3((XMFLOAT3*)&(m_vertices[(z * m_width) + x].GetPos()));
			auto v2 = XMLoadFloat3((XMFLOAT3*)&(m_vertices[((z + 1) * m_width) + x].GetPos()));
			auto v3 = XMLoadFloat3((XMFLOAT3*)&(m_vertices[(z * m_width) + x + 1].GetPos()));
			if (TriangleTests::Intersects(playerPos, direction, v1, v2, v3, dist)) {
				fdist = dist;
			}

			auto v4 = XMLoadFloat3((XMFLOAT3*)&(m_vertices[((z + 1) * m_width) + x].GetPos()));
			auto v5 = XMLoadFloat3((XMFLOAT3*)&(m_vertices[((z + 1) * m_width) + x + 1].GetPos()));
			auto v6 = XMLoadFloat3((XMFLOAT3*)&(m_vertices[(z * m_width) + x + 1].GetPos()));
			dist = FLT_MAX;
			if (TriangleTests::Intersects(playerPos, direction, v4, v5, v6, dist)) {
				fdist = dist;
			}
		}
	}

	if (fdist >= FLT_MAX - 1) {
		fdist = FLT_MAX;
	}

	return 1000000.0f - fdist;
}

bool TerrainMesh::GetTerrainRayPos(XMFLOAT3 rayOrgin, XMFLOAT3 dir, int startx, int startz, int width, int length, XMFLOAT3& fPos)
{
	auto rOrgin = XMLoadFloat3((XMFLOAT3*)&rayOrgin);
	auto direction = XMLoadFloat3((XMFLOAT3*)&dir);

	float fdist = FLT_MAX;

	for (int z = startz; z <= startz + length; ++z) {
		for (int x = startx; x <= startx + width; ++x) {
			if (x + 1 >= m_width || z + 1 >= m_length)
				continue;
			float dist = FLT_MAX;
			auto v1 = XMLoadFloat3((XMFLOAT3*)&(m_vertices[(z * m_width) + x].GetPos()));
			auto v2 = XMLoadFloat3((XMFLOAT3*)&(m_vertices[((z + 1) * m_width) + x].GetPos()));
			auto v3 = XMLoadFloat3((XMFLOAT3*)&(m_vertices[(z * m_width) + x + 1].GetPos()));
			if (TriangleTests::Intersects(rOrgin, direction, v1, v2, v3, dist)) {
				fdist = dist;
			}

			auto v4 = XMLoadFloat3((XMFLOAT3*)&(m_vertices[((z + 1) * m_width) + x].GetPos()));
			auto v5 = XMLoadFloat3((XMFLOAT3*)&(m_vertices[((z + 1) * m_width) + x + 1].GetPos()));
			auto v6 = XMLoadFloat3((XMFLOAT3*)&(m_vertices[(z * m_width) + x + 1].GetPos()));
			dist = FLT_MAX;
			if (TriangleTests::Intersects(rOrgin, direction, v4, v5, v6, dist)) {
				fdist = dist;
			}
		}
	}

	if (fdist >= FLT_MAX - 1) {
		return false;
	}
	XMFLOAT3 flen = Vector3::ScalarProduct(dir, fdist , true);
	fPos = Vector3::Add(rayOrgin, flen);
	return true;
}

float TerrainMesh::GetHeightBylerp(float px, float pz)
{
	px = px / m_scale.x;
	pz = pz / m_scale.z;

	if ((px < 0.0f) || (pz < 0.0f) || (px >= m_width) || (pz >= m_length)) return(0.0f);
	//idx
	int x = (int)px;
	int z = (int)pz;
	float xPersent = px - x;
	float zPersent = pz - z;

	float tileBL = m_vertices[x + (z * m_width)].GetPos().y;
	float tileBR = m_vertices[(x + 1) + (z * m_width)].GetPos().y;
	float tileUL = m_vertices[x + ((z + 1)* m_width)].GetPos().y;
	float tileUR = m_vertices[(x + 1) + ((z + 1) * m_width)].GetPos().y;

	//interpolation

	float topHeight = (tileUL * (1.0f - xPersent)) + (tileUR * xPersent);
	float bottomHeight = (tileBL * (1.0f - xPersent)) + (tileBR * xPersent);
	float height = (bottomHeight * (1.0f - zPersent)) + (topHeight * zPersent);

	return height;
}

float TerrainMesh::GetHeightByCorrectLerp(float px, float pz, float worldx , float worldz)
{
	px += worldx;
	pz += worldz;

	px = px / m_scale.x;
	pz = pz / m_scale.z;

	if ((px < 0.0f) || (pz < 0.0f) || (px >= m_width) || (pz >= m_length)) return(0.0f);
	//idx
	int x = (int)px;
	int z = (int)pz;
	float xPersent = px - x;
	float zPersent = pz - z;
	
	int nz = z + 1;
	int nx = x + 1;

	if (nz >= m_length)
		nz = m_length - 1;
	if (nx >= m_width)
		nx = m_width - 1;



	float tileBL = m_vertices[x + (z * m_width)].GetPos().y;
	float tileBR = m_vertices[(nx) + (z * m_width)].GetPos().y;
	float tileUL = m_vertices[x + ((nz) * m_width)].GetPos().y;
	float tileUR = m_vertices[(nx) + ((nz) * m_width)].GetPos().y;
	//interpolation

	float topHeight = (tileUL * (1.0f - xPersent)) + (tileUR * xPersent);
	float bottomHeight = (tileBL * (1.0f - xPersent)) + (tileBR * xPersent);
	float height = (bottomHeight * (1.0f - zPersent)) + (topHeight * zPersent);

	return height + 0.1f;
}

UIRectMesh::~UIRectMesh()
{
	if (m_vertices) {
		delete[] m_vertices;
		m_vertices = NULL;
	}

	if (m_vertexUploadBuffer) {
		m_vertexUploadBuffer->Release();
	}
}

UIRectMesh::UIRectMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList , int textureNum)
{
	m_verticesNum = 6 * textureNum;
	int stride = sizeof(TexVertex);
	int texIdx = 0;

	m_vertices = new TexVertex[m_verticesNum];

	//diffuse
	float w = (FRAME_BUFFER_WIDTH * 0.001f) / 8.0f;
	float h = (FRAME_BUFFER_HEIGHT * 0.001f) / 8.0f;
	float x = -1.0f + w;
	float y = 1.0f - h;
	int i = 0;

	m_vertices[i++] = TexVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), texIdx);
	texIdx++;

	//normal
	x = -1.0f + (3.0f * w);
	y = 1.0f - h;

	m_vertices[i++] = TexVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), texIdx);
	texIdx++;


	//depth
	x = -1.0f + (w);
	y = 1.0f - (3.0f * h);

	m_vertices[i++] = TexVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), texIdx);
	texIdx++;

	//specular
	x = -1.0f + (3.0f * w);
	y = 1.0f - (3.0f * h);

	m_vertices[i++] = TexVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), texIdx);
	texIdx++;

	/*w = (FRAME_BUFFER_WIDTH * 0.001f) / 2.0f;
	h = (FRAME_BUFFER_HEIGHT * 0.001f) / 2.0f;*/

	x = 1.0f - w;
	y = 1.0f - h;

	m_vertices[i++] = TexVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), texIdx);
	texIdx++;

	x = 1.0f - w;
	y = 1.0f - (3.0f * h);

	m_vertices[i++] = TexVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), texIdx);
	texIdx++;

	x = 1.0f - w;
	y = 1.0f - (5.0f * h);

	m_vertices[i++] = TexVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), texIdx);
	texIdx++;

	m_vertexBuffer = ::CreateBufferResource(device, commandList, m_vertices, stride * m_verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_vertexUploadBuffer);
	m_vertexBufferView.BufferLocation = m_vertexBuffer->GetGPUVirtualAddress();
	m_vertexBufferView.StrideInBytes = stride;
	m_vertexBufferView.SizeInBytes = stride * m_verticesNum;
}

UIRectMesh::UIRectMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
	m_verticesNum = 6;
	int stride = sizeof(TexVertex);
	int texIdx = 0;

	m_vertices = new TexVertex[m_verticesNum];
	float w = 1.0f;
	float h = 1.0f;
	float x = 0.0f;
	float y = 0.0f;
	int i = 0;

	m_vertices[i++] = TexVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), texIdx);
	m_vertices[i++] = TexVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), texIdx);

	m_vertexBuffer = ::CreateBufferResource(device, commandList, m_vertices, stride * m_verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_vertexUploadBuffer);
	m_vertexBufferView.BufferLocation = m_vertexBuffer->GetGPUVirtualAddress();
	m_vertexBufferView.StrideInBytes = stride;
	m_vertexBufferView.SizeInBytes = stride * m_verticesNum;
}

RealGrassMesh::~RealGrassMesh()
{
	if (m_vertices) {
		delete[] m_vertices;
		m_vertices = NULL;
	}

	if (m_vertexUploadBuffer) {
		m_vertexUploadBuffer->Release();
	}

}

RealGrassMesh::RealGrassMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, float w, float h, float z)
{
	m_verticesNum = 6 * 3;
	int stride = sizeof(UVNORMALVertex);
	int texIdx = 0;

	m_vertices = new UVNORMALVertex[m_verticesNum];
	int i = 0;

	//quad 1
	m_vertices[i++] = UVNORMALVertex(XMFLOAT3( -w,  +h , 0), XMFLOAT3(0.0f, 1.0f,  0.0f) ,XMFLOAT2(0.0f, 0.0f));
	m_vertices[i++] = UVNORMALVertex(XMFLOAT3( +w,  +h , 0), XMFLOAT3(0.0f, 1.0f,  0.0f)   ,XMFLOAT2(1.0f, 0.0f));
	m_vertices[i++] = UVNORMALVertex(XMFLOAT3( +w,  -h , 0), XMFLOAT3(0.0f, -1.0f, 0.0f),XMFLOAT2(1.0f, 1.0f));
	m_vertices[i++] = UVNORMALVertex(XMFLOAT3( -w,  +h , 0), XMFLOAT3(0.0f, 1.0f,  0.0f) ,XMFLOAT2(0.0f, 0.0f));
	m_vertices[i++] = UVNORMALVertex(XMFLOAT3( +w,  -h , 0), XMFLOAT3(0.0f, -1.0f, 0.0f),XMFLOAT2(1.0f, 1.0f));
	m_vertices[i++] = UVNORMALVertex(XMFLOAT3( -w,  -h , 0), XMFLOAT3(0.0f, -1.0f, 0.0f),XMFLOAT2(0.0f, 1.0f));


	//quad 2

	m_vertices[i++] = UVNORMALVertex(XMFLOAT3(-w, +h, +z), XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(0.0f, 0.0f));
	m_vertices[i++] = UVNORMALVertex(XMFLOAT3(+w, +h, -z), XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(1.0f, 0.0f));
	m_vertices[i++] = UVNORMALVertex(XMFLOAT3(+w, -h, -z), XMFLOAT3(0.0f, -1.0f, 0.0f), XMFLOAT2(1.0f, 1.0f));
	m_vertices[i++] = UVNORMALVertex(XMFLOAT3(-w, +h, +z), XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(0.0f, 0.0f));
	m_vertices[i++] = UVNORMALVertex(XMFLOAT3(+w, -h, -z), XMFLOAT3(0.0f, -1.0f, 0.0f), XMFLOAT2(1.0f, 1.0f));
	m_vertices[i++] = UVNORMALVertex(XMFLOAT3(-w, -h, +z), XMFLOAT3(0.0f, -1.0f, 0.0f), XMFLOAT2(0.0f, 1.0f));

	//quad 3

	m_vertices[i++] = UVNORMALVertex(XMFLOAT3(-w, +h, -z), XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(0.0f, 0.0f));
	m_vertices[i++] = UVNORMALVertex(XMFLOAT3(+w, +h, +z), XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(1.0f, 0.0f));
	m_vertices[i++] = UVNORMALVertex(XMFLOAT3(+w, -h, +z), XMFLOAT3(0.0f, -1.0f, 0.0f), XMFLOAT2(1.0f, 1.0f));
	m_vertices[i++] = UVNORMALVertex(XMFLOAT3(-w, +h, -z), XMFLOAT3(0.0f, 1.0f, 0.0f), XMFLOAT2(0.0f, 0.0f));
	m_vertices[i++] = UVNORMALVertex(XMFLOAT3(+w, -h, +z), XMFLOAT3(0.0f, -1.0f, 0.0f), XMFLOAT2(1.0f, 1.0f));
	m_vertices[i++] = UVNORMALVertex(XMFLOAT3(-w, -h, -z), XMFLOAT3(0.0f, -1.0f, 0.0f), XMFLOAT2(0.0f, 1.0f));


	m_vertexBuffer = ::CreateBufferResource(device, commandList, m_vertices, stride * m_verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_vertexUploadBuffer);
	m_vertexBufferView.BufferLocation = m_vertexBuffer->GetGPUVirtualAddress();
	m_vertexBufferView.StrideInBytes = stride;
	m_vertexBufferView.SizeInBytes = stride * m_verticesNum;
}

RealGrassMesh::RealGrassMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
	m_verticesNum = 1;
	int stride = sizeof(UVNORMALVertex);
	int texIdx = 0;
	m_primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_POINTLIST;

	m_vertices = new UVNORMALVertex[m_verticesNum];
	int i = 0;
	m_vertices[i++] = UVNORMALVertex(XMFLOAT3(0, 0, 0), XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT2(0.0f, 0.0f));

	m_vertexBuffer = ::CreateBufferResource(device, commandList, m_vertices, stride * m_verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_vertexUploadBuffer);
	m_vertexBufferView.BufferLocation = m_vertexBuffer->GetGPUVirtualAddress();
	m_vertexBufferView.StrideInBytes = stride;
	m_vertexBufferView.SizeInBytes = stride * m_verticesNum;
}

BillBoardMesh::~BillBoardMesh()
{
	delete[] m_vertices;
	m_vertexUploadBuffer->Release();
}

BillBoardMesh::BillBoardMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList , int tidx)
{
	m_verticesNum = 1;
	int stride = sizeof(BillBoardVertex);
	int texIdx = 0;
	m_primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_POINTLIST;

	m_vertices = new BillBoardVertex[m_verticesNum];
	int i = 0;
	m_vertices[i++] = BillBoardVertex(XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT2(200.0f, 200.0f), tidx);

	m_vertexBuffer = ::CreateBufferResource(device, commandList, m_vertices, stride * m_verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_vertexUploadBuffer);
	m_vertexBufferView.BufferLocation = m_vertexBuffer->GetGPUVirtualAddress();
	m_vertexBufferView.StrideInBytes = stride;
	m_vertexBufferView.SizeInBytes = stride * m_verticesNum;
}

SkyBoxMesh::~SkyBoxMesh()
{
	delete[] m_vertices;
	m_vertexUploadBuffer->Release();
}

SkyBoxMesh::SkyBoxMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, float fWidth, float fHeight, float fdepth) {
	m_verticesNum = 36;
	int stride = sizeof(SkyBoxVertex);
	m_primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	m_vertices = new SkyBoxVertex[36];
	float x = (fWidth * 0.5f), y = (fHeight * 0.5f), z = (fdepth * 0.5f);
	int i = 0;
	XMFLOAT3 position[36];
	XMFLOAT3 nomals[36];
	//뒷위
	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(-x, +y, -z), XMFLOAT2(0.0f, 0.0f));
	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(+x, +y, -z), XMFLOAT2(1.0f, 0.0f));
	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(+x, -y, -z), XMFLOAT2(1.0f, 1.0f));
	//뒷아래			  
	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(-x, +y, -z), XMFLOAT2(0.0f, 0.0f));
	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(+x, -y, -z), XMFLOAT2(1.0f, 1.0f));
	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(-x, -y, -z), XMFLOAT2(0.0f, 1.0f));
	//
	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(-x, +y, +z), XMFLOAT2(0.0f, 0.0f));
	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(+x, +y, +z), XMFLOAT2(1.0f, 0.0f));
	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(+x, +y, -z), XMFLOAT2(1.0f, 1.0f));

	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(-x, +y, +z), XMFLOAT2(0.0f, 0.0f));
	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(+x, +y, -z), XMFLOAT2(1.0f, 1.0f));
	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(-x, +y, -z), XMFLOAT2(0.0f, 1.0f));

	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(-x, -y, +z), XMFLOAT2(0.0f, 0.0f));
	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(+x, -y, +z), XMFLOAT2(1.0f, 0.0f));
	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(+x, +y, +z), XMFLOAT2(1.0f, 1.0f));

	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(-x, -y, +z), XMFLOAT2(0.0f, 0.0f));
	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(+x, +y, +z), XMFLOAT2(1.0f, 1.0f));
	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(-x, +y, +z), XMFLOAT2(0.0f, 1.0f));

	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(-x, -y, -z), XMFLOAT2(0.0f, 0.0f));
	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(+x, -y, -z), XMFLOAT2(1.0f, 0.0f));
	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(+x, -y, +z), XMFLOAT2(1.0f, 1.0f));

	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(-x, -y, -z), XMFLOAT2(0.0f, 0.0f));
	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(+x, -y, +z), XMFLOAT2(1.0f, 1.0f));
	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(-x, -y, +z), XMFLOAT2(0.0f, 1.0f));

	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(-x, +y, +z), XMFLOAT2(0.0f, 0.0f));
	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(-x, +y, -z), XMFLOAT2(1.0f, 0.0f));
	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(-x, -y, -z), XMFLOAT2(1.0f, 1.0f));

	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(-x, +y, +z), XMFLOAT2(0.0f, 0.0f));
	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(-x, -y, -z), XMFLOAT2(1.0f, 1.0f));
	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(-x, -y, +z), XMFLOAT2(0.0f, 1.0f));

	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(+x, +y, -z), XMFLOAT2(0.0f, 0.0f));
	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(+x, +y, +z), XMFLOAT2(1.0f, 0.0f));
	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(+x, -y, +z), XMFLOAT2(1.0f, 1.0f));

	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(+x, +y, -z), XMFLOAT2(0.0f, 0.0f));
	m_vertices[i++] = SkyBoxVertex(position[i] = XMFLOAT3(+x, -y, +z), XMFLOAT2(1.0f, 1.0f));
	m_vertices[i] = SkyBoxVertex(position[i] =   XMFLOAT3(+x, -y, -z), XMFLOAT2(0.0f, 1.0f));

	for (int j = 0; j < 36; ++j) nomals[j] = XMFLOAT3(0.0f, 1.0f, 0.0f);

	int primitives = m_verticesNum / 3;
	UINT index0, index1, index2;
	/*for (int i = 0; i < primitives; i++)
	{
		index0 = i * 3 + 0;
		index1 = i * 3 + 1;
		index2 = i * 3 + 2;
		XMFLOAT3 edge01 = Vector3::Subtract(position[index1], position[index0]);
		XMFLOAT3 edge02 = Vector3::Subtract(position[index2], position[index0]);
		nomals[index0] = nomals[index1] = nomals[index2] = Vector3::CrossProduct(edge01, edge02, true);
	}*/

	for (int j = 0; j < 36; ++j) m_vertices[j].normal = nomals[j];

	m_vertexBuffer = ::CreateBufferResource(device, commandList, m_vertices, stride * m_verticesNum, D3D12_HEAP_TYPE_DEFAULT,
		D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_vertexUploadBuffer);
	//삼각형 메쉬를 리소스(정점 버퍼)로 생성한다.

	m_vertexBufferView.BufferLocation = m_vertexBuffer->GetGPUVirtualAddress();
	m_vertexBufferView.StrideInBytes = stride;
	m_vertexBufferView.SizeInBytes = stride * m_verticesNum;


}

EffectMesh::~EffectMesh()
{
	delete[] m_vertices;
}

EffectMesh::EffectMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
	m_verticesNum = 6;
	int stride = sizeof(EffectVertex);
	int texIdx = 0;

	m_vertices = new EffectVertex[m_verticesNum];
	int i = 0;
	m_vertices[i++] = EffectVertex(XMFLOAT3(0, 0, 0), XMFLOAT2(0.0f, 0.0f), texIdx);
	m_vertices[i++] = EffectVertex(XMFLOAT3(0, 0, 0), XMFLOAT2(1.0f, 0.0f), texIdx);
	m_vertices[i++] = EffectVertex(XMFLOAT3(0, 0, 0), XMFLOAT2(1.0f, 1.0f), texIdx);
	m_vertices[i++] = EffectVertex(XMFLOAT3(0, 0, 0), XMFLOAT2(0.0f, 0.0f), texIdx);
	m_vertices[i++] = EffectVertex(XMFLOAT3(0, 0, 0), XMFLOAT2(1.0f, 1.0f), texIdx);
	m_vertices[i++] = EffectVertex(XMFLOAT3(0, 0, 0), XMFLOAT2(0.0f, 1.0f), texIdx);

	m_vertexBuffer = ::CreateBufferResource(device, commandList, m_vertices, stride * m_verticesNum, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
	m_vertexBufferView.BufferLocation = m_vertexBuffer->GetGPUVirtualAddress();
	m_vertexBufferView.StrideInBytes = stride;
	m_vertexBufferView.SizeInBytes = stride * m_verticesNum;
}

NodeMesh::~NodeMesh()
{
}

NodeMesh::NodeMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList , vector<vector<Node>> nodes , int nodeInterval , TerrainMesh* tmesh)
{
	m_verticesNum = (nodes.size() - 1) * (nodes.size() - 1) * 8;
	int stride = sizeof(DiffuseVertex);
	m_primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_LINELIST;
	m_vertices = new DiffuseVertex[m_verticesNum];
	int count = 0;
	for (int i = 0; i < nodes.size() - 1; ++i) {
		for (int j = 0; j < nodes.size() - 1; ++j) {

			float nx = nodes[i][j].pos.x;
			float nz = nodes[i][j].pos.z;

			float height = 0;
			height = tmesh->GetHeightByCorrectLerp(nx, nz , 256.0f , 256.0f);
			m_vertices[count++] = DiffuseVertex(XMFLOAT3(nx ,height, nz), XMFLOAT4(1.0f,0.0f,0.0f,1.0f));
			height = tmesh->GetHeightByCorrectLerp(nx + nodeInterval, nz , 256.0f, 256.0f);
			m_vertices[count++] = DiffuseVertex(XMFLOAT3(nx + nodeInterval, height, nz), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));


			height = tmesh->GetHeightByCorrectLerp(nx + nodeInterval, nz, 256.0f, 256.0f);
			m_vertices[count++] = DiffuseVertex(XMFLOAT3(nx + nodeInterval, height, nz), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
			height = tmesh->GetHeightByCorrectLerp(nx + nodeInterval, nz + nodeInterval, 256.0f, 256.0f);
			m_vertices[count++] = DiffuseVertex(XMFLOAT3(nx + nodeInterval, height, nz + nodeInterval), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

			height = tmesh->GetHeightByCorrectLerp(nx + nodeInterval, nz + nodeInterval, 256.0f, 256.0f);
			m_vertices[count++] = DiffuseVertex(XMFLOAT3(nx + nodeInterval, height, nz + nodeInterval), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
			height = tmesh->GetHeightByCorrectLerp(nx, nz + nodeInterval, 256.0f, 256.0f);
			m_vertices[count++] = DiffuseVertex(XMFLOAT3(nx, height, nz + nodeInterval), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

			height = tmesh->GetHeightByCorrectLerp(nx, nz + nodeInterval, 256.0f, 256.0f);
			m_vertices[count++] = DiffuseVertex(XMFLOAT3(nx, height, nz + nodeInterval), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
			height = tmesh->GetHeightByCorrectLerp(nx, nz, 256.0f, 256.0f);
			m_vertices[count++] = DiffuseVertex(XMFLOAT3(nx, height, nz), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

		
		}
	}

	m_vertexBuffer = ::CreateBufferResource(device, commandList, m_vertices, stride * m_verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_vertexUploadBuffer);
	m_vertexBufferView.BufferLocation = m_vertexBuffer.Get()->GetGPUVirtualAddress();
	m_vertexBufferView.StrideInBytes = stride;
	m_vertexBufferView.SizeInBytes = stride * m_verticesNum;
}
