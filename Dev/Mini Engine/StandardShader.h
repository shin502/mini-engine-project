#pragma once
#include "Shader.h"

struct MODELINFO {
public:

	int framseNum;
	int skinnedMeshNum = 0;
	int	standardMeshNum = 0;
	int	boundingMeshNum = 0;
	SkinnedMesh** skinnedMeshes = NULL; //[SkinnedMeshes], Skinned Mesh Cache

	MODELINFO() {};
	MODELINFO(int framseNum, int skinnedMeshNum, int standardMeshNum, int boundingMeshNum, SkinnedMesh** skinnedMeshes) {
		this->framseNum = framseNum;
		this->skinnedMeshNum = skinnedMeshNum;
		this->standardMeshNum = skinnedMeshNum;
		this->boundingMeshNum = skinnedMeshNum;
		this->skinnedMeshes = skinnedMeshes;
	}
};

class StandardShader : public Shader
{
protected:
	MODELINFO m_modelInfo;
public:
	StandardShader() {};
	virtual ~StandardShader() {};

	virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();

	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
	virtual D3D12_SHADER_BYTECODE CreatePixelShader();
	virtual void CreatebPipeLineState(ComPtr<ID3D12Device>& device, ComPtr<ID3D12RootSignature>& rootsignature, ComPtr<ID3D12RootSignature>& computeRootSignature);

	virtual int GetShaderNumber();
};