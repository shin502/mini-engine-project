#include "Shader.h"
Shader::Shader()
{
}

Shader::~Shader()
{

}

void Shader::CreateRenderPipeLineState(ComPtr<ID3D12Device>& device, ComPtr<ID3D12RootSignature>& rootsignature)
{
	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
	psoDesc.InputLayout = CreateInputLayout();
	psoDesc.pRootSignature = rootsignature.Get();
	psoDesc.VS = CreateVertexShader();
	psoDesc.PS = CreatePixelShader();
	psoDesc.RasterizerState = CreateRasterizerState();
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
	psoDesc.SampleMask = UINT_MAX;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	psoDesc.NumRenderTargets = 1;
	psoDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	psoDesc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
	psoDesc.SampleDesc.Count = 1;
	ThrowIfFailed(device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_bpipelineState)));
	if (psoDesc.InputLayout.pInputElementDescs) delete[] psoDesc.InputLayout.pInputElementDescs;
	if (m_vsBlob) m_vsBlob->Release();
	if (m_psBlob) m_psBlob->Release();
}

void Shader::CreatebPipeLineState(ComPtr<ID3D12Device>& device, ComPtr<ID3D12RootSignature>& rootsignature)
{
	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
	psoDesc.InputLayout = CreateInputLayout();
	psoDesc.pRootSignature = rootsignature.Get();
	psoDesc.VS = CreateVertexShader();
	psoDesc.PS = CreatePixelShader();
	psoDesc.RasterizerState = CreateRasterizerState();
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
	psoDesc.SampleMask = UINT_MAX;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	psoDesc.NumRenderTargets = 4;
	psoDesc.RTVFormats[0] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	psoDesc.RTVFormats[1] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	psoDesc.RTVFormats[2] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	psoDesc.RTVFormats[3] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	psoDesc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
	psoDesc.SampleDesc.Count = 1;
	ThrowIfFailed(device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_bpipelineState)));

	psoDesc.VS = CreateShadowVertexShader();
	psoDesc.PS = CreateShadowPixelShader();
	
	ThrowIfFailed(device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_ShadowPipeLine)));

	if (psoDesc.InputLayout.pInputElementDescs) delete[] psoDesc.InputLayout.pInputElementDescs;
	if(m_vsBlob) m_vsBlob->Release();
	if(m_psBlob) m_psBlob->Release();
}

void Shader::CreateLinePipeLineState(ComPtr<ID3D12Device>& device, ComPtr<ID3D12RootSignature>& rootsignature)
{
	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
	psoDesc.InputLayout = CreateInputLayout();
	psoDesc.pRootSignature = rootsignature.Get();
	psoDesc.VS = CreateVertexShader();
	psoDesc.PS = CreatePixelShader();
	psoDesc.RasterizerState = CreateRasterizerState();
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
	psoDesc.SampleMask = UINT_MAX;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_LINE;
	psoDesc.NumRenderTargets = 4;
	psoDesc.RTVFormats[0] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	psoDesc.RTVFormats[1] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	psoDesc.RTVFormats[2] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	psoDesc.RTVFormats[3] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	psoDesc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
	psoDesc.SampleDesc.Count = 1;
	ThrowIfFailed(device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_bpipelineState)));

	psoDesc.VS = CreateShadowVertexShader();
	psoDesc.PS = CreateShadowPixelShader();

	ThrowIfFailed(device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_ShadowPipeLine)));

	if (psoDesc.InputLayout.pInputElementDescs) delete[] psoDesc.InputLayout.pInputElementDescs;
	if (m_vsBlob) m_vsBlob->Release();
	if (m_psBlob) m_psBlob->Release();
}

D3D12_INPUT_LAYOUT_DESC Shader::CreateInputLayout()
{
	int num = 2;
	D3D12_INPUT_ELEMENT_DESC*  pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[num];
	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[1] = { "COLOR",   0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }; // 12bytes far 
	D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
	d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
	d3dInputLayoutDesc.NumElements = num;
	return d3dInputLayoutDesc;
}

D3D12_RASTERIZER_DESC Shader::CreateRasterizerState()
{
	CD3DX12_RASTERIZER_DESC rasterizerStateDesc(D3D12_DEFAULT);
	rasterizerStateDesc.CullMode = D3D12_CULL_MODE_BACK; // back face culling
	return rasterizerStateDesc;
}

D3D12_BLEND_DESC Shader::CreateBlendState()
{
	CD3DX12_BLEND_DESC blendDesc(D3D12_DEFAULT);

	return blendDesc;
}

D3D12_DEPTH_STENCIL_DESC Shader::CreateDepthStencilState()
{
	CD3DX12_DEPTH_STENCIL_DESC dsDesc(D3D12_DEFAULT);
	return dsDesc;
}

D3D12_SHADER_BYTECODE Shader::CreateVertexShader()
{
	return(CompileShaderFromFile(L"Shaders.hlsl", "VSBasic", "vs_5_1", &m_vsBlob));
}
D3D12_SHADER_BYTECODE Shader::CreatePixelShader()
{
	return(CompileShaderFromFile(L"Shaders.hlsl", "PSBasic", "ps_5_1", &m_psBlob));
}

D3D12_SHADER_BYTECODE Shader::CreateGeoMetryShader()
{
	return D3D12_SHADER_BYTECODE();
}

D3D12_SHADER_BYTECODE Shader::CreateShadowVertexShader()
{
	return(CompileShaderFromFile(L"Shaders.hlsl", "VSBasic", "vs_5_1", &m_vsBlob));
}

D3D12_SHADER_BYTECODE Shader::CreateShadowPixelShader()
{
	return(CompileShaderFromFile(L"Shaders.hlsl", "PSBasic", "ps_5_1", &m_psBlob));
}

D3D12_SHADER_BYTECODE Shader::CreateShadowGeoMetryShader()
{
	return D3D12_SHADER_BYTECODE();
}

void Shader::PrePareRender(ComPtr<ID3D12GraphicsCommandList>& commandList)
{
	commandList.Get()->SetPipelineState(m_bpipelineState.Get());
}

void Shader::SetShadowPipe(ID3D12GraphicsCommandList* commandList, int idx)
{
	commandList->SetPipelineState(m_ShadowPipeLine.Get());
}

int Shader::GetShaderNumber()
{
	return E_BASICSHADER;
}



vector<objManageMent>& Shader::GetObjVec()
{
	return m_objVec;
}

void Shader::ReleaseUploadBuffer()
{

}
D3D12_SHADER_BYTECODE Shader::CompileShaderFromFile(WCHAR* pszFileName, LPCSTR pszShaderName, LPCSTR pszShaderProfile, ID3DBlob** ppd3dShaderBlob)
{
	UINT nCompileFlags = 0;
#if defined(_DEBUG)
	nCompileFlags = D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
#endif

	ID3DBlob* pd3dErrorBlob = NULL;
	HRESULT hResult = ::D3DCompileFromFile(pszFileName, NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, pszShaderName, pszShaderProfile, nCompileFlags, 0, ppd3dShaderBlob, &pd3dErrorBlob);
	char* pErrorString = NULL;
	if (pd3dErrorBlob) pErrorString = (char*)pd3dErrorBlob->GetBufferPointer();

	D3D12_SHADER_BYTECODE d3dShaderByteCode;
	d3dShaderByteCode.BytecodeLength = (*ppd3dShaderBlob)->GetBufferSize();
	d3dShaderByteCode.pShaderBytecode = (*ppd3dShaderBlob)->GetBufferPointer();

	return(d3dShaderByteCode);
}


//terrain shader 
int TerrainShader::GetShaderNumber()
{
	return E_TERRAINSHADER;
}

void TerrainShader::CreatebPipeLineState(ComPtr<ID3D12Device>& device, ComPtr<ID3D12RootSignature>& rootsignature)
{

	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
	psoDesc.InputLayout = CreateInputLayout();
	psoDesc.pRootSignature = rootsignature.Get();
	psoDesc.VS = CreateVertexShader();
	psoDesc.PS = CreatePixelShader();
	psoDesc.RasterizerState = CreateRasterizerState();
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
	psoDesc.SampleMask = UINT_MAX;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	psoDesc.NumRenderTargets = 4;
	psoDesc.RTVFormats[0] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	psoDesc.RTVFormats[1] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	psoDesc.RTVFormats[2] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	psoDesc.RTVFormats[3] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	psoDesc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
	psoDesc.SampleDesc.Count = 1;
	
	ThrowIfFailed(device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_bpipelineState)));

	psoDesc.VS = CreateShadowVertexShader();
	psoDesc.PS = CreateShadowPixelShader();
	CD3DX12_RASTERIZER_DESC rasterizerStateDesc(D3D12_DEFAULT);
	rasterizerStateDesc.CullMode = D3D12_CULL_MODE_BACK; // back face culling
	rasterizerStateDesc.DepthBias = 10000.0f;
	rasterizerStateDesc.SlopeScaledDepthBias = 200.0f;
	rasterizerStateDesc.DepthBiasClamp = 1.0f;
	
	psoDesc.RasterizerState = rasterizerStateDesc;

	ThrowIfFailed(device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_ShadowPipeLine)));

	if (psoDesc.InputLayout.pInputElementDescs) delete[] psoDesc.InputLayout.pInputElementDescs;
	if (m_vsBlob) m_vsBlob->Release();
	if (m_psBlob) m_psBlob->Release();

}

D3D12_INPUT_LAYOUT_DESC TerrainShader::CreateInputLayout()
{
	int num = 5;
	D3D12_INPUT_ELEMENT_DESC* pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[num];
	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[1] = { "NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }; // 12bytes far 
	pd3dInputElementDescs[2] = { "TEXCOORD",   0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }; //8bytes far
	pd3dInputElementDescs[3] = { "TANGENT",   0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }; // 12bytes far 
	pd3dInputElementDescs[4] = { "BINORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }; // 12bytes far 

	D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
	d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
	d3dInputLayoutDesc.NumElements = num;

	return d3dInputLayoutDesc;
}

D3D12_SHADER_BYTECODE TerrainShader::CreateVertexShader()
{
	return(CompileShaderFromFile(L"Shaders.hlsl", "VSTerrain", "vs_5_1", &m_vsBlob));
}
D3D12_SHADER_BYTECODE TerrainShader::CreatePixelShader()
{
	return(CompileShaderFromFile(L"Shaders.hlsl", "PSTerrain", "ps_5_1", &m_psBlob));
}

D3D12_SHADER_BYTECODE TerrainShader::CreateShadowVertexShader()
{
	return(CompileShaderFromFile(L"Shadow.hlsl", "VS_SHADOW_TERRAIN", "vs_5_1", &m_vsBlob));
}

D3D12_SHADER_BYTECODE TerrainShader::CreateShadowPixelShader()
{
	return(CompileShaderFromFile(L"Shadow.hlsl", "PS_SHADOW", "ps_5_1", &m_psBlob));
}

int UIShader::GetShaderNumber()
{
	return E_UISHADER;
}

D3D12_INPUT_LAYOUT_DESC UIShader::CreateInputLayout()
{
	int num = 3;
	D3D12_INPUT_ELEMENT_DESC* pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[num];
	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[1] = { "TEXCOORD",   0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }; //8bytes far
	pd3dInputElementDescs[2] = { "TEXIDX",   0, DXGI_FORMAT_R32_UINT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }; //4byte

	D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
	d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
	d3dInputLayoutDesc.NumElements = num;
	return d3dInputLayoutDesc;
}

D3D12_SHADER_BYTECODE UIShader::CreateVertexShader()
{
	return(CompileShaderFromFile(L"Shaders.hlsl", "VS_UI", "vs_5_1", &m_vsBlob));
}

D3D12_SHADER_BYTECODE UIShader::CreatePixelShader()
{
	return(CompileShaderFromFile(L"Shaders.hlsl", "PS_UI", "ps_5_1", &m_psBlob));
}

void LightShader::CreateRenderPipeLineState(ComPtr<ID3D12Device>& device, ComPtr<ID3D12RootSignature>& rootsignature)
{
	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
	psoDesc.InputLayout = CreateInputLayout();
	psoDesc.pRootSignature = rootsignature.Get();
	psoDesc.VS = CreateVertexShader();
	psoDesc.PS = CreatePixelShader();
	psoDesc.RasterizerState = CreateRasterizerState();
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.DepthStencilState = CreateDepthStencilState();
	psoDesc.SampleMask = UINT_MAX;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	psoDesc.NumRenderTargets = 1;
	psoDesc.RTVFormats[0] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	psoDesc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
	psoDesc.SampleDesc.Count = 1;
	ThrowIfFailed(device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_bpipelineState)));
	if (psoDesc.InputLayout.pInputElementDescs) delete[] psoDesc.InputLayout.pInputElementDescs;
	if (m_vsBlob) m_vsBlob->Release();
	if (m_psBlob) m_psBlob->Release();
}

D3D12_SHADER_BYTECODE LightShader::CreateVertexShader()
{
	return(CompileShaderFromFile(L"Shaders.hlsl", "VS_DEFFERRED_LIGHT", "vs_5_1", &m_vsBlob));
}

D3D12_SHADER_BYTECODE LightShader::CreatePixelShader()
{
	return(CompileShaderFromFile(L"Shaders.hlsl", "PS_DEFFERRED_LIGHT", "ps_5_1", &m_psBlob));
}

D3D12_DEPTH_STENCIL_DESC LightShader::CreateDepthStencilState()
{
	D3D12_DEPTH_STENCIL_DESC depthStencilDesc;
	::ZeroMemory(&depthStencilDesc, sizeof(D3D12_DEPTH_STENCIL_DESC));
	depthStencilDesc.DepthEnable = TRUE;
	depthStencilDesc.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ZERO;
	depthStencilDesc.DepthFunc = D3D12_COMPARISON_FUNC_LESS_EQUAL;
	depthStencilDesc.StencilEnable = FALSE;
	depthStencilDesc.StencilReadMask = 0x00;
	depthStencilDesc.StencilWriteMask = 0x00;
	depthStencilDesc.FrontFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D12_COMPARISON_FUNC_NEVER;
	depthStencilDesc.BackFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D12_COMPARISON_FUNC_NEVER;

	return depthStencilDesc;
}

RSShader::~RSShader()
{

}

int RSShader::GetShaderNumber()
{
	return E_RSSHADER;
}

void RSShader::CreatebPipeLineState(ComPtr<ID3D12Device>& device, ComPtr<ID3D12RootSignature>& rootsignature)
{
	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
	psoDesc.InputLayout = CreateInputLayout();
	psoDesc.pRootSignature = rootsignature.Get();
	psoDesc.VS = CreateVertexShader();
	psoDesc.GS = CreateGeoMetryShader();
	psoDesc.PS = CreatePixelShader();
	psoDesc.RasterizerState = CreateRasterizerState();
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
	psoDesc.SampleMask = UINT_MAX;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_POINT;
	psoDesc.NumRenderTargets = 4;
	psoDesc.RTVFormats[0] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	psoDesc.RTVFormats[1] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	psoDesc.RTVFormats[2] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	psoDesc.RTVFormats[3] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	psoDesc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
	psoDesc.SampleDesc.Count = 1;

	CD3DX12_RASTERIZER_DESC rasterizerStateDesc(D3D12_DEFAULT);
	rasterizerStateDesc.CullMode = D3D12_CULL_MODE_NONE; // back face culling
	psoDesc.RasterizerState = rasterizerStateDesc;

	D3D12_BLEND_DESC blendDesc = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	for (int i = 0; i < 4; ++i) {
	/*	blendDesc.AlphaToCoverageEnable = TRUE;
		blendDesc.RenderTarget[i].BlendEnable = true;
		blendDesc.RenderTarget[i].SrcBlend = D3D12_BLEND_SRC_ALPHA;
		blendDesc.RenderTarget[i].DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
		blendDesc.RenderTarget[i].SrcBlendAlpha = D3D12_BLEND_ONE;
		blendDesc.RenderTarget[i].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;*/
	}
	psoDesc.BlendState = blendDesc;

	ThrowIfFailed(device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_bpipelineState)));

	psoDesc.VS = CreateShadowVertexShader();
	psoDesc.PS = CreateShadowPixelShader();
	psoDesc.GS = CreateShadowGeoMetryShader();
	ThrowIfFailed(device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_ShadowPipeLine)));

	if (psoDesc.InputLayout.pInputElementDescs) delete[] psoDesc.InputLayout.pInputElementDescs;
	if (m_vsBlob) m_vsBlob->Release();
	if (m_psBlob) m_psBlob->Release();
	if (m_gsBlob) m_gsBlob->Release();

}

D3D12_INPUT_LAYOUT_DESC RSShader::CreateInputLayout()
{
	int num = 8;
	D3D12_INPUT_ELEMENT_DESC* pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[num];
	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[1] = { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[2] = { "TEXCOORD",   0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[3] = { "WORLDMATRIX", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	pd3dInputElementDescs[4] = { "WORLDMATRIX", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	pd3dInputElementDescs[5] = { "WORLDMATRIX", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	pd3dInputElementDescs[6] = { "WORLDMATRIX", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	pd3dInputElementDescs[7] = { "GRASSNUMBER", 0, DXGI_FORMAT_R32_FLOAT, 2, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };


	D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
	d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
	d3dInputLayoutDesc.NumElements = num;
	return d3dInputLayoutDesc;
}

D3D12_SHADER_BYTECODE RSShader::CreateVertexShader()
{
	return(CompileShaderFromFile(L"Shaders.hlsl", "VS_T_REAL_GRASS", "vs_5_1", &m_vsBlob));
}

D3D12_SHADER_BYTECODE RSShader::CreatePixelShader()
{
	return(CompileShaderFromFile(L"Shaders.hlsl", "PS_REAL_GRASS", "ps_5_1", &m_psBlob));
}

D3D12_SHADER_BYTECODE RSShader::CreateGeoMetryShader()
{
	return(CompileShaderFromFile(L"Shaders.hlsl", "GS_REAL_GRASS", "gs_5_1", &m_gsBlob));
}

D3D12_SHADER_BYTECODE RSShader::CreateShadowVertexShader()
{
	return(CompileShaderFromFile(L"Shadow.hlsl", "VS_SHADOW_REAL_GRASS", "vs_5_1", &m_vsBlob));
}

D3D12_SHADER_BYTECODE RSShader::CreateShadowPixelShader()
{
	return(CompileShaderFromFile(L"Shadow.hlsl", "PS_GRASS_SHADOW", "ps_5_1", &m_psBlob));
}

D3D12_SHADER_BYTECODE RSShader::CreateShadowGeoMetryShader()
{
	return(CompileShaderFromFile(L"Shadow.hlsl", "GS_SHADOW_REAL_GRASS", "gs_5_1", &m_gsBlob));
}

void RSShader::CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
	UINT tempObjectByte = 0;
	tempObjectByte = sizeof(VS_GB_INSTANCE_OBJECT);
	m_isResource = ::CreateBufferResource(device, commandList, NULL, sizeof(VS_GB_INSTANCE_OBJECT) * ((UINT)BUFFERSIZE), D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
	//인스턴스 정보를 저장할 정점 버퍼를 업로드 힙 유형으로 생성한다.
	m_isResource->Map(0, NULL, (void**)&m_MappedisObjects);
	//정점 버퍼(업로드 힙)에 대한 포인터를 저장한다.
	m_objectBufferView.BufferLocation = m_isResource->GetGPUVirtualAddress();
	m_objectBufferView.StrideInBytes = tempObjectByte;
	m_objectBufferView.SizeInBytes = tempObjectByte * ((UINT)BUFFERSIZE);
	//정점 버퍼에 대한 뷰를 생성한다.
}

void RSShader::SetPipeLine(ID3D12GraphicsCommandList* commandList, int idx)
{
	commandList->SetPipelineState(m_bpipelineState.Get());
}

void RSShader::SetShadowPipe(ID3D12GraphicsCommandList* commandList, int idx)
{
	commandList->SetPipelineState(m_ShadowPipeLine.Get());
}

VS_GB_INSTANCE_OBJECT* RSShader::GetMappedObject()
{
	return m_MappedisObjects;
}

D3D12_VERTEX_BUFFER_VIEW* RSShader::GetObjectBufferView()
{
	return &m_objectBufferView;
}

void PostShader::CreateRenderPipeLineState(ComPtr<ID3D12Device>& device, ComPtr<ID3D12RootSignature>& rootsignature)
{
	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
	psoDesc.InputLayout = CreateInputLayout();
	psoDesc.pRootSignature = rootsignature.Get();
	psoDesc.VS = CreateVertexShader();
	psoDesc.PS = CreatePixelShader();
	psoDesc.RasterizerState = CreateRasterizerState();
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.DepthStencilState = CreateDepthStencilState();
	psoDesc.SampleMask = UINT_MAX;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	psoDesc.NumRenderTargets = 1;
	psoDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	psoDesc.SampleDesc.Count = 1;
	ThrowIfFailed(device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_bpipelineState)));
	if (psoDesc.InputLayout.pInputElementDescs) delete[] psoDesc.InputLayout.pInputElementDescs;
	if (m_vsBlob) m_vsBlob->Release();
	if (m_psBlob) m_psBlob->Release();
}

D3D12_SHADER_BYTECODE PostShader::CreateVertexShader()
{
	return(CompileShaderFromFile(L"PostShader.hlsl", "VS_POSTEFFECT", "vs_5_1", &m_vsBlob));
}

D3D12_SHADER_BYTECODE PostShader::CreatePixelShader()
{
	return(CompileShaderFromFile(L"PostShader.hlsl", "PS_POSTEFFECT", "ps_5_1", &m_psBlob));
}
