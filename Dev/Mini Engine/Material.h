#pragma once
#include "Shader.h"
#include "StandardInstanceShader.h"
#include "StandardShader.h"
#include "StandardSkinnedAnimationInstanceShader.h"
#include "StandardSkinnedAnimationShader.h"
#include "SkinnedInstancingShader.h"
#include "InstancingShader.h"
#include "ParticleShader.h"
#include "BillBoardShader.h"
#include "SkyBoxShader.h"
#include "WeaponTrailerShader.h"
struct MATERIAL
{
	XMFLOAT4 diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	XMFLOAT4 emissive = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
	XMFLOAT4 specular = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f); //(r,g,b,a=power)
	XMFLOAT4 ambient = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
};

class Material
{
private:
	Shader* m_shader = NULL;

	UINT diffuseTextureIdx = 0;
	UINT normalTextureIdx = 0;
	UINT specularTextureIdx = 0;
	UINT metallicTextureIdx = 0;
	UINT emissionTextureIdx = 0;

public:

	int texturesNum = 0;
	_TCHAR(*textureNames)[128] = NULL;
	Texture** textures = NULL;
	UINT reflection = 0;
	// 객체에 적용될 재질정보
	UINT textureMask = 0x00;
	// 객체에 적용될 텍스쳐정보
	UINT textureIdx = 0; // diffuse, normal, specular, metallic, emission 10자리, 두자리씩 할당. (emission은 최대 42)
	// 객체에 적용될 텍스쳐의 인덱스정보

	XMFLOAT4						albedoColor = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	XMFLOAT4						emissiveColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
	XMFLOAT4						specularColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
	XMFLOAT4						ambientColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);

	float							glossiness = 0.0f;
	float							smoothness = 0.0f;
	float							specularHighlight = 0.0f;
	float							metallic = 0.0f;
	float							glossyReflection = 0.0f;
	



	Material();
	Material(int texturesNum);
	Material(Shader* shader);
	
	~Material();
	void SetShader(Shader* shader);
	Shader* GetShader();

	void LoadTextureFromFile(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, UINT type, UINT rootParameter, _TCHAR* textureName, Texture** textures, FILE* inFile, char* folderName, std::vector<std::pair<int, _TCHAR*>>& textureMapName, int& idx);

	void SetTextureIdx(UINT type, int idx);
	void SetTextureMask(UINT textureMask) { this->textureMask |= textureMask; }
	void ReleaseUploadBuffer();
	void UpdateShaderVariables(ID3D12GraphicsCommandList* commandList);

};

