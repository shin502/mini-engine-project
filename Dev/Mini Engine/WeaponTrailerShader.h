#pragma once
#include "Shader.h"

struct VS_VB_INSTANCE_TRAIL {
	XMFLOAT3 position;
	XMFLOAT2 uv;
	UINT textureIdx;
	XMFLOAT4 color;
	float age;
};

struct WeaponTrailInfo {
	bool isActive;
	XMFLOAT3 position;
	XMFLOAT2 uv;
	UINT textureIdx;
	XMFLOAT4 color;
	float age;
};

class WeaponTrailerShader : public Shader
{
private:

	WeaponTrailInfo** info;
	ComPtr<ID3D12Resource>* buffer;
	VS_VB_INSTANCE_TRAIL** mappedBuffer;
	D3D12_VERTEX_BUFFER_VIEW* bufferView;
	std::vector<int> renderBuffer;
	UINT currentIdx = 0;
public:
	WeaponTrailerShader();
	~WeaponTrailerShader();

	virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();
	virtual D3D12_BLEND_DESC CreateBlendState();
	virtual D3D12_RASTERIZER_DESC CreateRasterizerState();
	virtual D3D12_DEPTH_STENCIL_DESC CreateDepthStencilState();

	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
	virtual D3D12_SHADER_BYTECODE CreatePixelShader();

	virtual void CreateRenderPipeLineState(ComPtr<ID3D12Device>& device, ComPtr<ID3D12RootSignature>& rootsignature);
	virtual void CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, int idx = 0);
	virtual void UpdateShaderVariables(int idx);
	virtual void ReleaseShaderVariables();
	virtual void AnimateObjects(float timeElapsed);
	virtual int GetShaderNumber();

	void AddBuffer(XMFLOAT3 start0, XMFLOAT3 end0, XMFLOAT3 start1, XMFLOAT3 end1, XMFLOAT4 color, float age, UINT textureIdx);
	void BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, void* context);
	void Render(ID3D12GraphicsCommandList* commandList);
};

