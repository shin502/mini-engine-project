#pragma once
#include "Mesh.h"
class StandardMesh : public Mesh
{
public:
	StandardMesh() {};
	StandardMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList) {}
	virtual ~StandardMesh();

protected:
	UINT							vertexSize = 0;

	Vertex* vertex;
	int verticesNum;
	int meshnum;

	XMFLOAT4* colors = NULL;
	XMFLOAT3* normals = NULL;
	XMFLOAT3* tangents = NULL;
	XMFLOAT3* biTangents = NULL;
	XMFLOAT2* textureCoords0 = NULL;
	XMFLOAT2* textureCoords1 = NULL;

	XMFLOAT3* positions;
	ComPtr<ID3D12Resource> positionBuffer = NULL;
	ID3D12Resource* positionUploadBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW positionBufferView;

	ComPtr<ID3D12Resource> textureCoord0Buffer = NULL;
	ID3D12Resource* textureCoord0UploadBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW		textureCoord0BufferView;

	ComPtr<ID3D12Resource> textureCoord1Buffer = NULL;
	ID3D12Resource* textureCoord1UploadBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW		textureCoord1BufferView;

	ComPtr<ID3D12Resource> normalBuffer = NULL;
	ID3D12Resource* normalUploadBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW		normalBufferView;

	ComPtr<ID3D12Resource> tangentBuffer = NULL;
	ID3D12Resource* tangentUploadBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW		tangentBufferView;

	ComPtr<ID3D12Resource> biTangentBuffer = NULL;
	ID3D12Resource* biTangentUploadBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW		biTangentBufferView;

	int	subMeshesNum = 0;
	int* subSetIndicesNum = NULL;
	UINT** subSetIndices = NULL;

	ComPtr<ID3D12Resource>* subSetIndexBuffers = NULL;
	ID3D12Resource** subSetIndexUploadBuffers = NULL;
	D3D12_INDEX_BUFFER_VIEW* subSetIndexBufferViews = NULL;

	char meshName[256] = { 0 };
	UINT type = 0x00;
	UINT slot = 0;
	UINT offset = 0;
	BoundingBox aabbBox;
	XMFLOAT3 boundingBoxAABBCenter = XMFLOAT3(0.0f, 0.0f, 0.0f);
	XMFLOAT3 boundingBoxAABBExtents = XMFLOAT3(0.0f, 0.0f, 0.0f);

	D3D12_PRIMITIVE_TOPOLOGY primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;


public:
	void LoadMeshFromFile(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, FILE* pInFile);
	UINT GetType() { return type; }
	char* GetmeshName() { return meshName; }
	virtual int GetVerticesNum() { return verticesNum; }

	virtual void ReleaseUploadBuffer();
	virtual void Render(ID3D12GraphicsCommandList* commandList, int subSet);
	virtual void Render(ID3D12GraphicsCommandList* commandList, int subsetNum, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingBufferViews);
	virtual void Render(ID3D12GraphicsCommandList* commandList, int subsetNum, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingBufferViews, int weaponNum);

	virtual void ShadowRender(ID3D12GraphicsCommandList* commandList, int subsetNum, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingBufferViews);

};

