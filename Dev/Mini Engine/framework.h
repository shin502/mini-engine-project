#pragma once
#include "ProcessInput.h"
#include "Timer.h"
#include "Player.h"
#include "Scene.h"
#include "Shadow.h"
class FrameWork {
private:
	static const UINT FrameCount = 2;
	float m_clearColor[4] = { 0.0f, 0.0f, 0.0f, 1.0f };

	Scene m_scene;
	ProcessInput m_processInput;
	Camera* m_camera;
	Player* m_player;
	Shadow* m_shadow;
	UINT						m_nMsaa4xQualityLevels = 0;

	HINSTANCE					m_hInstance;
	HWND						m_hWnd;

	UINT m_Width;
	UINT m_Height;
	bool m_useWarpDevice{ false };
	bool m_bMsaa4xEnable{ false };

	//Timer
	Timer m_timer;
	_TCHAR						m_pszFrameRate[50];

	// Pipeline objects.
	CD3DX12_VIEWPORT m_viewport;
	CD3DX12_RECT m_scissorRect;
	ComPtr<IDXGISwapChain3> m_swapChain;
	ComPtr<ID3D12Device> m_device;
	ComPtr<ID3D12Resource> m_renderTargets[FrameCount];
	ID3D12Resource* m_CascadeResources[NUM_CASCADES];
	ID3D12Resource* m_defferredResources[DefferredCount];
	ID3D12Resource* m_PosteffectResources[postRenderCount];
	ComPtr<ID3D12CommandAllocator> m_commandAllocator;
	ComPtr<ID3D12CommandQueue> m_commandQueue;
	ComPtr<ID3D12DescriptorHeap> m_rtvHeap;
	ComPtr<ID3D12DescriptorHeap> m_dsvHeap;
	ComPtr<ID3D12Resource> m_dsvBuffer = nullptr;
	ComPtr<ID3D12Resource> m_shadowBuffer = nullptr;
	ComPtr<ID3D12Resource> m_copyDsvBuffer = nullptr;
	D3D12_CPU_DESCRIPTOR_HANDLE m_dsvhandle;
	D3D12_CPU_DESCRIPTOR_HANDLE m_Shadowhandle;
	D3D12_CPU_DESCRIPTOR_HANDLE m_copyDsvHandle;

	D3D12_CPU_DESCRIPTOR_HANDLE dfdhandle[DefferredCount];
	D3D12_CPU_DESCRIPTOR_HANDLE cascadehandle[NUM_CASCADES];
	D3D12_CPU_DESCRIPTOR_HANDLE posteffectHandle[postRenderCount];

	ComPtr<ID3D12GraphicsCommandList> m_commandList;
	UINT m_rtvDescriptorSize;
	UINT m_dsvDescriptorSize;

	// Synchronization objects.
	UINT m_frameIndex;
	HANDLE m_fenceEvent;
	ComPtr<ID3D12Fence> m_fence;
	UINT64 m_fenceValue;

	D3D12_RESOURCE_DESC m_depthResourceDesc;
	D3D12_HEAP_PROPERTIES m_depthHeapProperties;
	D3D12_CLEAR_VALUE m_depthClearValue;

public:
	FrameWork(UINT width , UINT height);
	~FrameWork();

	bool OnInit(HINSTANCE hInstance, HWND hMainWnd);
	void OnDestroy();

	void CreateSwapChain();
	void CreateDevice();
	void CreateCommandQueueAndList();
	void CreateFence();

	void CreateRtvAndDsvDescriptorHeaps();

	void CreateRenderTargetViews();
	void CreateDepthStencilView();
	void CreateRenderTexture();
	void CreateDepthTexture();


	void ChangeSwapChainState();

	void CraeteCameraAndPlayer();
	void BuildObjects();
	void BuildShadow();
	void ReleaseObjects();
	
	void PrePareDraw();
	void OnDraw();
	void OnCompute();
	
	void PrePareShadow();
	void DrawShadow();

	void PrePareDefferedRender();
	void PreParePostRender();

	void PrePareDefferred();
	void DrawDefferred();
	void AnimateObjects();
	void FrameAdvance();

	void WaitForPreviousFrame();
	void WaitForGPURunning();

	void UpdateState();
	void UpdateDefferredState();
	void UpdateShadowState(int CameraIdx);


	void OnProcessingMouseMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);
	void OnProcessingKeyboardMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);
	LRESULT CALLBACK OnProcessingWindowMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);

	void GetHardwareAdapter(IDXGIFactory1* pFactory, IDXGIAdapter1** ppAdapter, bool requestHighPerformanceAdapter = false);
	void OnResizeBackBuffers();
};