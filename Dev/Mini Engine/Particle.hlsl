#include "Skinned.hlsl"

struct VS_PARTICLE_INPUT
{
    float2 size : SIZE;
    float3 position : POSITION;
    uint texid : TEXID;
    uint copyIdx : COPYIDX;
    uint id : SV_VertexID;
};

struct GS_PARTICLE_INPUT
{
    float3 position : POSITION;
    float2 size : SIZE;
    float age : AGE;
    uint texid : TEXID;
};

struct GS_PARTICLE_OUTPUT
{
    float4 position : SV_Position;
    float3 positionW : Position;
    float2 uv : TEXCOORD;
    uint texid : TEXID;
    float3 normal : NORMAL;
    float age : AGE;
};

GS_PARTICLE_INPUT VSParticle(VS_PARTICLE_INPUT input)
{
    GS_PARTICLE_INPUT output;
    
    output.position = gsbParticleData[input.id + (input.copyIdx * PARTICLESIZE)].position.xyz + input.position;
    output.age = gsbParticleData[input.id + (input.copyIdx * PARTICLESIZE)].age;
    output.size = input.size;
    output.texid = input.texid;
    return output;
}

[maxvertexcount(4)]
void GSParticleDraw(point GS_PARTICLE_INPUT input[1], inout TriangleStream<GS_PARTICLE_OUTPUT> outStream)
{
    if (input[0].age < 0 || input[0].age > 4.0f)
        return;
    
    float3 vUp = float3(0.0f, 1.0f, 0.0f);
    float3 vLook = gvCameraPosition.xyz - input[0].position;
    vLook = normalize(vLook);
    float3 vRight = cross(vUp, vLook);
    float fHalfW = input[0].size * 0.5f;
    float fHalfH = input[0].size * 0.5f;
    float4 pVertices[4];
    float4x4 worldMat = { float4(vRight, 0.0f), float4(vUp, 0.0f), float4(vLook, 0.0f), float4(input[0].position, 1.0f) };
    
    pVertices[0] = float4(input[0].position + fHalfW * vRight - fHalfH * vUp, 1.0f);
    pVertices[1] = float4(input[0].position + fHalfW * vRight + fHalfH * vUp, 1.0f);
    pVertices[2] = float4(input[0].position - fHalfW * vRight - fHalfH * vUp, 1.0f);
    pVertices[3] = float4(input[0].position - fHalfW * vRight + fHalfH * vUp, 1.0f);

    float2 pUVs[4] = { float2(0.0f, 1.0f), float2(0.0f, 0.0f), float2(1.0f, 1.0f), float2(1.0f, 0.0f) };
    GS_PARTICLE_OUTPUT output;
    float3 normalW[4] = { float3(0.0f, -1.0f, 0.0f), float3(0.0f, 1.0f, 0.0f), float3(0.0f, -1.0f, 0.0f), float3(0.0f, 1.0f, 0.0f) };
    
    
    for (int j = 0; j < 4; ++j)
    {
        output.position = mul(mul(pVertices[j], gmtxView), gmtxProjection);
        output.positionW = pVertices[j];
        output.uv = pUVs[j];
        output.texid = input[0].texid;
        output.normal = normalW[j];
        output.age = input[0].age;
        outStream.Append(output);
    }
}
float4 PSParticle(GS_PARTICLE_OUTPUT input) : SV_TARGET
{
    
    float4 normalTex = gtxtParticleTexture[input.texid].Sample(gspWarp, input.uv);
    clip(normalTex.a - 0.1f);
    
    float2 uv = GetPixelUV(input.position);
    
    float4 depthColor = gtxtDeffrredTexture[2].Sample(gspWarp, uv);
    float depth = depthColor.r;
    
    //파티클보다 다른 오브젝트의 뎁스값이 크다면 버린다.
    float pDepth = input.position.z;
    ////depth버퍼의 depth값 
    float tDepth = gtxtDepthTexture.Sample(gspWarp, uv);
    
    float2 tmpUV = GetPixelUV(input.position);
    tmpUV = tmpUV * 2.0f - 1.0f;
    tmpUV.y = -tmpUV.y;
    
    float depthFade = 0.0f;
    //view공간에서의 dpeth값을 구한다.
    float4 depthViewSample = mul(float4(tmpUV, tDepth, 1), gmtxInverseProjection);
    float4 depthViewParticle = mul(float4(tmpUV, pDepth, 1), gmtxInverseProjection);
    float depthDiff = (depthViewSample.z / depthViewSample.w) - (depthViewParticle.z / depthViewParticle.w);
    if (depthDiff < 0)
        discard;
    
    depthFade = saturate(depthDiff / 100.0f);
    float3 rNormal = input.normal;
    rNormal = (rNormal * 0.5f) + 0.5f;
    
    float age = input.age / 4.0f;
    age = saturate(age);
    
    if (input.texid != 3)
    {
        normalTex.a *= (age + 0.3f);
        normalTex.a *= depthFade;
    }
    
    return saturate(normalTex);
}