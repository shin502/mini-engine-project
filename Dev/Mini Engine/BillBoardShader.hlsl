#include "Particle.hlsl"

struct VS_BillBoard_INPUT
{
    float3 position : POSITION;
    float2 size : SIZE;
    uint texid : TEXID;
};

struct GS_BillBoard_INPUT
{
    float3 position : POSITION;
    float2 size : SIZE;
    uint texid : TEXID;
};

struct GS_BillBoard_OUTPUT
{
    float4 position : SV_Position;
    float2 uv : TEXCOORD;
    uint texid : TEXID;
};


GS_BillBoard_INPUT VSBILLBOARD(VS_BillBoard_INPUT input)
{
    GS_BillBoard_INPUT output;
    
    output.position = mul(float4(input.position, 1.0f), gmtxWorld);
    output.size = input.size;
    output.texid = input.texid;
    return output;
}

[maxvertexcount(4)]
void GSBILLBOARD(point GS_BillBoard_INPUT input[1], inout TriangleStream<GS_BillBoard_OUTPUT> outStream)
{
    float3 vUp = float3(0.0f, 1.0f, 0.0f);
    float3 vLook = gvCameraPosition.xyz - input[0].position;
    vLook = normalize(vLook);
    float3 vRight = cross(vUp, vLook);
    float fHalfW = input[0].size * 0.5f;
    float fHalfH = input[0].size * 0.5f;
    float4 pVertices[4];
    float4x4 worldMat = { float4(vRight, 0.0f), float4(vUp, 0.0f), float4(vLook, 0.0f), float4(input[0].position, 1.0f) };
    
    pVertices[0] = float4(input[0].position + fHalfW * vRight - fHalfH * vUp, 1.0f);
    pVertices[1] = float4(input[0].position + fHalfW * vRight + fHalfH * vUp, 1.0f);
    pVertices[2] = float4(input[0].position - fHalfW * vRight - fHalfH * vUp, 1.0f);
    pVertices[3] = float4(input[0].position - fHalfW * vRight + fHalfH * vUp, 1.0f);

    float2 pUVs[4] = { float2(0.0f, 1.0f), float2(0.0f, 0.0f), float2(1.0f, 1.0f), float2(1.0f, 0.0f) };
    GS_BillBoard_OUTPUT output;
    float3 normalW[4] = { float3(0.0f, -1.0f, 0.0f), float3(0.0f, 1.0f, 0.0f), float3(0.0f, -1.0f, 0.0f), float3(0.0f, 1.0f, 0.0f) };
    
    
    for (int j = 0; j < 4; ++j)
    {
        output.position = mul(mul(pVertices[j], gmtxView), gmtxProjection);
        output.uv = pUVs[j];
        output.texid = input[0].texid;
        outStream.Append(output);
    }
}

Defferred_OUTPUT PSBILLBOARD(GS_BillBoard_OUTPUT input) : SV_TARGET
{
    Defferred_OUTPUT output;
    
    float4 normalTex = gtxtSceneTexture[input.texid].Sample(gspWarp, input.uv);
    clip(normalTex.a - 0.8f);
    clip(normalTex.r - 0.3f);
    
    output.DIFFUSE_COLOR = normalTex;
    output.DEPTH_COLOR.r = input.position.z / input.position.w;
    output.DEPTH_COLOR.g = gMaterials[0].m_cAmbient.r;
    output.DEPTH_COLOR.b = gMaterials[0].m_cDiffuse.r;
    output.DEPTH_COLOR.a = input.position.w; 
    output.SPECULAR_COLOR.xyz = gMaterials[0].m_cSpecular.xyz;
    output.SPECULAR_COLOR.a = gMaterials[0].m_cSpecular.a;
    return output;
}