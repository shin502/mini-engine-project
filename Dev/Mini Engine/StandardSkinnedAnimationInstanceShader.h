#pragma once
#include "StandardSkinnedAnimationShader.h"

class StandardSkinnedAnimationInstanceShader : public StandardSkinnedAnimationShader
{
private:
	ComPtr<ID3D12PipelineState> m_pipes[2];


public:
	virtual ~StandardSkinnedAnimationInstanceShader() {}
	virtual void CreatebPipeLineState(ComPtr<ID3D12Device>& device, ComPtr<ID3D12RootSignature>& rootsignature, ComPtr<ID3D12RootSignature>& computeRootSignature);
	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
	virtual D3D12_SHADER_BYTECODE CreateShadowVertexShader();
	virtual int GetShaderNumber();

};
