#define BLOCK_SIZE 256
#define MAX_VERTEX_INFLUENCES 4
#define SKINNED_ANIMATION_BONES 128

cbuffer cbBoneOffset : register(b1)
{
    float4x4 gpmtxBoneOffsets[SKINNED_ANIMATION_BONES];
}

cbuffer cbVertices : register(b2)
{
    uint gnVerticesNum : packoffset(c0);
}

StructuredBuffer<float4x4> gsbBoneTransforms : register(t0);
StructuredBuffer<float3> gsbPosition : register(t1);
StructuredBuffer<float3> gsbNormal : register(t2);
StructuredBuffer<float3> gsbTangent : register(t3);
StructuredBuffer<float3> gsbBiTangent : register(t4);
StructuredBuffer<int4> gsbBoneIndex : register(t5);
StructuredBuffer<float4> gsbBoneWeight : register(t6);

RWStructuredBuffer<float3> grwsbPositionW : register(u0);
RWStructuredBuffer<float3> grwsbNormalW : register(u1);
RWStructuredBuffer<float3> grwsbTangentW : register(u2);
RWStructuredBuffer<float3> grwsbBiTangentW : register(u3);