#include "CSParticle.hlsli"

#define BLOCKSIZE 256
#define PARTICLESIZE 100
#define PARTICLE_TYPE_EXPLOSION 0
#define PARTICLE_TYPE_SCATTER 1

groupshared float3 sharedPos[BLOCKSIZE];

StructuredBuffer<ParticleData> gsbReadParticleData : register(t7);
StructuredBuffer<ParticleData> gsbCopyParticleData : register(t8);
RWStructuredBuffer<ParticleData> gsbWriteParticleData : register(u4);

#define BITONIC_BLOCK_SIZE 512
groupshared double shared_data[BITONIC_BLOCK_SIZE];


[numthreads(BLOCKSIZE, 1, 1)]
void main(uint3 Gid : SV_GroupID, uint3 DTid : SV_DispatchThreadID, uint3 GTid : SV_GroupThreadID, uint GI : SV_GroupIndex)
{

    float3 position = gsbReadParticleData[DTid.x + (bidx * PARTICLESIZE)].position;
    float3 velocity = (gsbReadParticleData[DTid.x + (bidx * PARTICLESIZE)].velocity);
    float age = gsbReadParticleData[DTid.x + (bidx * PARTICLESIZE)].age;
    float3 accel = float3(1.0f, 0.0f, 0.0f);
    position.xyz += velocity.xyz * gfTimeElapsed;
    age -= gfTimeElapsed;
   
    if (DTid.x < PARTICLESIZE)
    {

        gsbWriteParticleData[DTid.x + (bidx * PARTICLESIZE)].age = age;
        if (guParticleType == PARTICLE_TYPE_EXPLOSION)
        {
            gsbWriteParticleData[DTid.x + (bidx * PARTICLESIZE)].position = position;
            gsbWriteParticleData[DTid.x + (bidx * PARTICLESIZE)].velocity = velocity;
        }
        else if (guParticleType == PARTICLE_TYPE_SCATTER)
        {
            if (age < 15.0f) //파티클이 흩날리는 최대 거리 
            {
                gsbWriteParticleData[DTid.x + (bidx * PARTICLESIZE)].position = position;
                gsbWriteParticleData[DTid.x + (bidx * PARTICLESIZE)].velocity = velocity;
            }
            if (age <= 0.0f)
            {
                gsbWriteParticleData[DTid.x + (bidx * PARTICLESIZE)].age = gsbCopyParticleData[DTid.x].age;
                gsbWriteParticleData[DTid.x + (bidx * PARTICLESIZE)].position = gsbCopyParticleData[DTid.x].position;
                gsbWriteParticleData[DTid.x + (bidx * PARTICLESIZE)].velocity = gsbCopyParticleData[DTid.x].velocity;
            }
        }
    }
}