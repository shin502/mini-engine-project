#include "StandardInstanceShader.h"

D3D12_INPUT_LAYOUT_DESC StandardInstanceShader::CreateInputLayout()
{
	UINT inputElementDescsNum = 9;
	D3D12_INPUT_ELEMENT_DESC* inputElementDescs = new D3D12_INPUT_ELEMENT_DESC[inputElementDescsNum];

	inputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[1] = { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 1, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[2] = { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 2, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[3] = { "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 3, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[4] = { "BITANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 4, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };

	inputElementDescs[5] = { "WORLDMATRIX", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 5, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	inputElementDescs[6] = { "WORLDMATRIX", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 5, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	inputElementDescs[7] = { "WORLDMATRIX", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 5, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	inputElementDescs[8] = { "WORLDMATRIX", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 5, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };

	D3D12_INPUT_LAYOUT_DESC inputLayoutDesc;
	inputLayoutDesc.pInputElementDescs = inputElementDescs;
	inputLayoutDesc.NumElements = inputElementDescsNum;

	return(inputLayoutDesc);
}

D3D12_SHADER_BYTECODE StandardInstanceShader::CreateVertexShader()
{
	return(Shader::CompileShaderFromFile(L"Standard.hlsl", "VSStandardInstancing", "vs_5_1", &m_vsBlob));
}

D3D12_SHADER_BYTECODE StandardInstanceShader::CreateShadowVertexShader()
{
	return(Shader::CompileShaderFromFile(L"ShadowCaster.hlsl", "VSCSMStandardInstancing", "vs_5_1", &m_vsBlob));
}

D3D12_SHADER_BYTECODE StandardInstanceShader::CreateShadowPixelShader()
{
	return(Shader::CompileShaderFromFile(L"ShadowCaster.hlsl", "PSCSMStandardInstancing", "ps_5_1", &m_psBlob));
}

void StandardInstanceShader::CreatebPipeLineState(ComPtr<ID3D12Device>& device, ComPtr<ID3D12RootSignature>& rootsignature, ComPtr<ID3D12RootSignature>& computeRootSignature)
{
	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
	psoDesc.InputLayout = CreateInputLayout();
	psoDesc.pRootSignature = rootsignature.Get();
	psoDesc.VS = CreateVertexShader();
	psoDesc.PS = CreatePixelShader();
	psoDesc.RasterizerState = CreateRasterizerState();
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
	psoDesc.SampleMask = UINT_MAX;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	psoDesc.NumRenderTargets = 1;
	psoDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	psoDesc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
	psoDesc.SampleDesc.Count = 1;
	ThrowIfFailed(device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_bpipelineState)));

	if (m_vsBlob) m_vsBlob->Release();
	if (m_psBlob) m_psBlob->Release();

	//shadowPipelineStateNum = 1;
	//shadowPipelineStates = new ID3D12PipelineState * [shadowPipelineStateNum];

	//Shader::CreateShadowShader(device, graphicsRootSignature);

	//if (vertexShaderBlob) vertexShaderBlob->Release();
	//if (pixelShaderBlob) pixelShaderBlob->Release();

	//if (pipelineStateDesc.InputLayout.pInputElementDescs) delete[] pipelineStateDesc.InputLayout.pInputElementDescs;

}

int StandardInstanceShader::GetShaderNumber()
{
	return E_STANDARDINSTANCESHADER;
}
