#pragma once
#include "Camera.h"
#include "GameObject.h"
class Player : public GameObject
{
private:
	Camera* m_camera;
	GameObject* m_model;// Get model 

	float           			m_fPitch;
	float           			m_fYaw;
	float           			m_fRoll;

	XMFLOAT3					m_look;
	XMFLOAT3					m_up;
	XMFLOAT3					m_right;

	int m_statusBody;
	int m_statusLeg;
	BoundingOrientedBox obbBox;
public:
	Player() {}
	Player(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList,  GameObject* model , float pMass);

	Camera* GetCamera() { return m_camera; }
	BoundingOrientedBox GetObbBox() { return obbBox; }

	void SetCamera(Camera* camera) { m_camera = camera; }
	void ChangeModel(int modelNumber);
	void Move(int dir , float speed, float elapsedTime);
	void Rotate(float x, float y, float z);
	void Update(float fTimeElapsed);

	void ChangeCamera(DWORD nNewCameraMode, DWORD nCurrentCamreaMode);
	Camera* ChangeCamera(DWORD nNewCameraMode, float fTimeElapsed);

	void ChangePlayer(GameObject* model);
	void UpdatePlayerWorld();

	XMFLOAT3 GetLookVector();
	XMFLOAT3 GetUpVector();
	XMFLOAT3 GetRightVector();
	XMFLOAT3 GetVel();
	GameObject* GetModel();

	void SetLookVector(XMFLOAT3 lookvector);
	void SetRightVector(XMFLOAT3 lookvector);
	void SetUpVector(XMFLOAT3 lookvector);
	void SetPlayerMass(float mass);
	void SetJumpCool(bool jumpCool);

	int GetStatusBody();
	int GetStatusLeg();
	int GetMass();
	bool GetJumpCool();

	void GetAnimationNum(UCHAR keys[256]);
	void ChangePlayerAnimation(int status1, int status2);
};

