#include "Shaders.hlsli"
#include "Light.hlsli"
#include "Shadow.hlsli"
#include "Particle.hlsli"

struct VS_BASIC_INPUT
{
	float3 position : POSITION;
	float4 color : COLOR;
};

struct VS_BASIC_OUTPUT
{
	float4 position : SV_POSITION;
	float4 color : COLOR;
};

//cube basic
VS_BASIC_OUTPUT VSBasic(VS_BASIC_INPUT input)
{
	VS_BASIC_OUTPUT output;
	
	output.position = mul(mul(mul(float4(input.position, 1.0f), gmtxWorld), gmtxView), gmtxProjection);
	output.color = input.color;

	return output;
}

float4 PSBasic(VS_BASIC_OUTPUT input) : SV_TARGET
{
    float4 cColor = input.color;
	return cColor;
}

//terrain

struct VS_TERRAIN_INPUT
{
    float3 position : POSITION;
    float3 normal : NORMAL;
    float2 uv0 : TEXCOORD0;
    float3 tangent : TANGENT;
    float3 bitangent : BINORMAL;
};

struct VS_TERRAIN_OUTPUT
{
    float4 position : SV_POSITION;
    float4 positionW : POSITION0;
    float3 normal : NORMAL;
    float2 uv0 : TEXCOORD0;
    float3 tangent : TANGENT;
    float3 bitangent : BINORMAL;
    float4 shadowPosition[3] : TEXCOORD1;
    float4 positionV : POSITION1;
};

VS_TERRAIN_OUTPUT VSTerrain(VS_TERRAIN_INPUT input)
{
    VS_TERRAIN_OUTPUT output;

    output.position = mul(mul(mul(float4(input.position, 1.0f), gmtxWorld), gmtxView), gmtxProjection);
    output.positionW = mul(float4(input.position, 1.0f), gmtxWorld);
    output.positionV = mul(mul(float4(input.position, 1.0f), gmtxWorld), gmtxView);
    output.normal = input.normal;
    output.uv0 = input.uv0;
    output.tangent = input.tangent;
    output.bitangent = input.bitangent;
    
    matrix shadowProject[NUM_CASCADES];
    for (int i = 0; i < int(gfCasacdeNum); ++i)
    {
        shadowProject[i] = mul(gmtxLightViewProjection[i], gmtxProjectToTexture); // 라이트에서 본 화면을 텍스처 좌표계로 변경
        output.shadowPosition[i] = mul(output.positionW, shadowProject[i]);
    }
    
    return (output);
}

Defferred_OUTPUT PSTerrain(VS_TERRAIN_OUTPUT input) : SV_TARGET
{
    float4 diffusecColor;
    float4 normalColor;
    
    float4 RedN = float4(0.0f, 0.0f, 0.0f, 0.0f);
    float4 GreenN = float4(0.0f, 0.0f, 0.0f, 0.0f);
    float4 BlueN = float4(0.0f, 0.0f, 0.0f, 0.0f);
    
    
    
    float4 Red = gtxtTerrainDetailTexture[0].Sample(gspAnisotropic, input.uv0);
    float4 Green = gtxtTerrainDetailTexture[1].Sample(gspAnisotropic, input.uv0);
    float4 Blue = gtxtTerrainDetailTexture[2].Sample(gspAnisotropic, input.uv0);
    
    
    
    if (length(gvCameraPosition - input.positionW.xyz) < BUMP_VALUE)
    {
         RedN = gtxtTerrainNormalTexture[0].Sample(gspAnisotropic, input.uv0);
         GreenN = gtxtTerrainNormalTexture[1].Sample(gspAnisotropic, input.uv0);
         BlueN = gtxtTerrainNormalTexture[2].Sample(gspAnisotropic, input.uv0);
   }
    
    
    float blendAmount = 0.0f;
    float slope = 1.0f - input.normal.y;
    if (slope < 0.2f)
    {
        blendAmount = slope / 0.2f;
        diffusecColor = lerp(Red, Green, blendAmount);
        normalColor = lerp(RedN, GreenN, blendAmount);
        
    }
    else if ((slope < 0.7) && (slope >= 0.2f))
    {
        blendAmount = (slope - 0.2f) * (1.0f / (0.7f - 0.2f));
        diffusecColor = lerp(Green, Blue, blendAmount);
        normalColor = lerp(GreenN, BlueN, blendAmount);
    }
    else if (slope >= 0.7)
    {
        diffusecColor = Blue;
        normalColor = BlueN;

    }
    float3 vNormal = input.normal;
    
    if (length(gvCameraPosition - input.positionW.xyz) < BUMP_VALUE)
    {
        //float3 tmpNormal = cross(input.bitangent, input.tangent);
        float3x3 TBN = float3x3(normalize(input.tangent), normalize(input.bitangent), normalize(input.normal));
        vNormal = normalize(normalColor.rgb * 2.0f - 1.0f); //[0, 1] → [-1, 1]
        vNormal = normalize(mul(vNormal, TBN)); //월드 공간으로 변환 
    }
    
    vNormal = (vNormal * 0.5f) + 0.5f;
    //shadow 
    float fShadowFactor = 1.0f;
    fShadowFactor = GetShadowFactor(input.shadowPosition, input.positionW);
    
    Defferred_OUTPUT output;
    output.DIFFUSE_COLOR = (diffusecColor * fShadowFactor);
    output.NORMAL_COLOR = float4(vNormal, 1.0f);
    output.SPECULAR_COLOR.xyz = gMaterials[1].m_cSpecular.xyz;
    output.SPECULAR_COLOR.a = gMaterials[1].m_cSpecular.a;
    output.DEPTH_COLOR.r = input.position.z / input.position.w;
    output.DEPTH_COLOR.g = gMaterials[1].m_cAmbient.r;
    output.DEPTH_COLOR.b = gMaterials[1].m_cDiffuse.r;
    output.DEPTH_COLOR.a = input.position.w;
    return output;
    //float4 cIllumination = Lighting(input.positionW.xyz, vNormal, gMaterials[0]);
    //return saturate(diffusecColor * cIllumination);
}

//ui

struct VS_UI_INPUT
{
    float3 position : POSITION;
    float2 uv0 : TEXCOORD0;
    uint texIdx : TEXIDX;
};

struct VS_UI_OUTPUT
{
    float4 position : SV_POSITION;
    float2 uv0 : TEXCOORD0;
    uint texIdx : TEXIDX;
};

VS_UI_OUTPUT VS_UI(VS_UI_INPUT input)
{
    VS_UI_OUTPUT output;
    output.position = float4(input.position, 1.0f);
    output.uv0 = input.uv0;
    output.texIdx = input.texIdx;
    
    return output;
}

float4 PS_UI(VS_UI_OUTPUT input) : SV_TARGET
{
    uint tidx = input.texIdx;
    float4 cColor = float4(0.0f, 0.0f, 0.0f, 1.0f);
    if (tidx == 0)
    {
        cColor = gtxtDeffrredTexture[0].Sample(gspWarp, input.uv0);
    }
    else if (tidx == 1)
    {
        cColor = gtxtDeffrredTexture[1].Sample(gspWarp, input.uv0);
    }
    else if (tidx == 2)
    {
        cColor = gtxtDeffrredTexture[2].Sample(gspWarp, input.uv0);
    }
    else if (tidx == 3)
    {
        cColor = gtxtDeffrredTexture[3].Sample(gspWarp, input.uv0);
    }
    else if (tidx == 4)
    {
        cColor = gtxtCasCadeTexture[0].Sample(gspWarp, input.uv0);
    }
    else if (tidx == 5)
    {
        cColor = gtxtCasCadeTexture[1].Sample(gspWarp, input.uv0);
    }
    else if (tidx == 6)
    {
        cColor = gtxtCasCadeTexture[2].Sample(gspWarp, input.uv0);
    }
    return cColor;
}

struct VS_LIGHT_OUTPUT
{
    float4 position : SV_POSITION;
    float2 uv0 : TEXCOORD0;
};


VS_LIGHT_OUTPUT VS_DEFFERRED_LIGHT(VS_UI_INPUT input)
{
    VS_LIGHT_OUTPUT output;
    output.position = float4(input.position, 1.0f);
    output.uv0 = input.uv0;
    return output;
}

float4 PS_DEFFERRED_LIGHT(VS_LIGHT_OUTPUT input) : SV_TARGET
{ 
    float4 cColor = float4(0.0f, 0.0f, 0.0f, 1.0f);
    float4 diffuseColor = gtxtDeffrredTexture[0].Sample(gspWarp, input.uv0);
    float4 wNormalColor = gtxtDeffrredTexture[1].Sample(gspWarp, input.uv0);
    float4 depthColor = gtxtDeffrredTexture[2].Sample(gspWarp, input.uv0);
    float4 SpecularColor = gtxtDeffrredTexture[3].Sample(gspWarp, input.uv0);
    
    wNormalColor = (wNormalColor * 2.0f) - 1.0f;
    
    float2 tmpUV = GetPixelUV(input.position);
    tmpUV = tmpUV * 2.0f - 1.0f;
    tmpUV.y = -tmpUV.y;
    float depth = depthColor.r;
    depth = depth * depthColor.a;
    
    float4 projPos = float4(tmpUV.xy, depth, 1.0f);
    float4 viewPos = mul(projPos, gmtxInverseProjection);
    viewPos /= viewPos.w;
    float4 wolrdPos = mul(viewPos, gmtxInverseView);
   
    MATERIAL tmpMaterial;
    tmpMaterial.m_cDiffuse = float4(depthColor.b, depthColor.b, depthColor.b, 1.0f);
    tmpMaterial.m_cAmbient = float4(depthColor.g, depthColor.g, depthColor.g, 1.0f);
    tmpMaterial.m_cSpecular = SpecularColor;
    tmpMaterial.m_cEmissive = float4(0.0f, 0.0f, 0.0f, 1.0f);
    float4 cIllumination = Lighting(wolrdPos.xyz, wNormalColor.xyz, tmpMaterial); // 픽셀과 모든 라이트와의 연산을 해준다.
    
    cIllumination += float4(0.3f, 0.3f, 0.3f, 0.0f);
    
    cColor = saturate(diffuseColor * cIllumination);
    return cColor;
}

struct VS_REAL_GRASS_INPUT
{
    float3 position : POSITION;
    float3 normal : NORMAL;
    float2 uv0 : TEXCOORD0;
    float4x4 transform : WORLDMATRIX;
    float gNumber : GRASSNUMBER;
};

struct VS_REAL_GRASS_OUTPUT
{
    float3 center : POSITION;
    float4x4 transform : WORLDMATRIX;
    float gNumber : GRASSNUMBER;
};

struct GS_REAL_GRASS_OUT
{
    float4 position : SV_POSITION;
    float3 normal : NORMAL;
    float2 uv0 : TEXCOORD0;
};


VS_REAL_GRASS_OUTPUT VS_T_REAL_GRASS(VS_REAL_GRASS_INPUT input)
{
    VS_REAL_GRASS_OUTPUT output;
    output.center = input.position;
    output.transform = input.transform;
    output.gNumber = input.gNumber;
    return output;

}

[maxvertexcount(12)]
void GS_REAL_GRASS(point VS_REAL_GRASS_OUTPUT input[1], inout TriangleStream<GS_REAL_GRASS_OUT> outStream)
{
    float quadSize = 2.0f;
    //quad 0
    float3 c = input[0].center;
    
    [unroll]
    for (int j = 0; j < 3; ++j)
    {
        float3 pVertices[4];
        if (j == 0)
        {
            pVertices[0] = float3(c.x - quadSize, c.y + quadSize, 0.0f); // -w +h
            pVertices[1] = float3(c.x + quadSize, c.y + quadSize, 0.0f); // +w +h
            pVertices[2] = float3(c.x - quadSize, c.y - quadSize, 0.0f); // -w +h
            pVertices[3] = float3(c.x + quadSize, c.y - quadSize, 0.0f); // -w +h
            
        }
        else if (j == 1)
        {
            pVertices[0] = float3(c.x - quadSize, c.y + quadSize, c.z + quadSize); // -w +h +z
            pVertices[1] = float3(c.x + quadSize, c.y + quadSize, c.z - quadSize); // -w +h
            pVertices[2] = float3(c.x - quadSize, c.y - quadSize, c.z + quadSize); // -w +h
            pVertices[3] = float3(c.x + quadSize, c.y - quadSize, c.z - quadSize); // -w +h
        }
        else if (j == 2)
        {
            pVertices[0] = float3(c.x - quadSize, c.y + quadSize, c.z - quadSize); // -w +h +z
            pVertices[1] = float3(c.x + quadSize, c.y + quadSize, c.z + quadSize); // -w +h
            pVertices[2] = float3(c.x - quadSize, c.y - quadSize, c.z - quadSize); // -w +h
            pVertices[3] = float3(c.x + quadSize, c.y - quadSize, c.z + quadSize); // -w +h
        }
       
        float2 pUV[4] = { float2(0.0f, 0.0f), float2(1.0f, 0.0f), float2(0.0f, 1.0f), float2(1.0f, 1.0f) };
        float3 normalW[4] = { float3(0.0f, 1.0f, 0.0f), float3(0.0f, 1.0f, 0.0f), float3(0.0f, -1.0f, 0.0f), float3(0.0f, -1.0f, 0.0f) };
        GS_REAL_GRASS_OUT output;
        for (int i = 0; i < 4; ++i)
        {
            float4 positionW = mul(float4(pVertices[i], 1.0f), input[0].transform);
            if (normalW[i].y > 0.9f)
            {
                positionW.x += cos(gTotalTime + input[0].gNumber);
            }
            output.position = mul(mul(positionW, gmtxView), gmtxProjection);
            output.normal = mul(normalW[i], (float3x3) gmtxWorld);
            output.uv0 = pUV[i];
            outStream.Append(output);
        }
        outStream.RestartStrip();
    }
}


Defferred_OUTPUT PS_REAL_GRASS(GS_REAL_GRASS_OUT input)
{
    Defferred_OUTPUT output;
    
    float3 rNormal = input.normal;
    
    rNormal = (rNormal * 0.5f) + 0.5f;
    
    float4 diffuseColor = gtxtSceneTexture[GRASS_TEXTURE].Sample(gspWarp, input.uv0);
    clip(diffuseColor.a - 0.1);
    output.DIFFUSE_COLOR = diffuseColor;
    output.NORMAL_COLOR = float4(rNormal, 1.0f);
    output.SPECULAR_COLOR.xyz = gMaterials[0].m_cSpecular.xyz;
    output.SPECULAR_COLOR.a = gMaterials[0].m_cSpecular.a;
    output.DEPTH_COLOR.r = input.position.z / input.position.w;
    output.DEPTH_COLOR.g = gMaterials[0].m_cAmbient.r;
    output.DEPTH_COLOR.b = gMaterials[0].m_cDiffuse.r;
    output.DEPTH_COLOR.a = input.position.w;
    return output;
}
