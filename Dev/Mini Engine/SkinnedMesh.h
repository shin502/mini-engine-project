#pragma once
#include "stdafx.h"
#include "Mesh.h"
#include "StandardMesh.h"

class HierarchyObj;


class SkinnedMesh : public StandardMesh
{
public:
	SkinnedMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList) {};
	virtual ~SkinnedMesh();

protected:
	int								bonesPerVertex = 4;

	XMINT4* boneIndices = NULL;
	XMFLOAT4* boneWeights = NULL;

	ComPtr<ID3D12Resource> boneIndexBuffer = NULL;
	ID3D12Resource* boneIndexUploadBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW		boneIndexBufferView;

	ComPtr<ID3D12Resource> boneWeightBuffer = NULL;
	ID3D12Resource* boneWeightUploadBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW		boneWeightBufferView;

public:
	int								skinningBones = 0;

	char(*skinningBoneNames)[128];
	HierarchyObj** skinningBoneFrameCaches = NULL; //[m_nSkinningBones]

	XMFLOAT4X4* xmf4x4BindPoseBoneOffsets = NULL; //Transposed

	ComPtr<ID3D12Resource> bindPoseBoneOffsets = NULL;
	XMFLOAT4X4* mappedBindPoseBoneOffsets = NULL;
	ID3D12Resource* bindPoseBoneOffsetsUploadBuffer = NULL;

	ComPtr<ID3D12Resource> skinningBoneTransforms = NULL;
	XMFLOAT4X4* mappedSkinningBoneTransforms = NULL;

public:
	void PrepareSkinning(HierarchyObj* ModelRootObject);
	void LoadSkinInfoFromFile(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, FILE* pInFile);

	void ComputeSkinning(ID3D12GraphicsCommandList* commandList, UINT instanceNum, ID3D12Resource* buffers[4]);

	virtual void CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	virtual void UpdateShaderVariables(ID3D12GraphicsCommandList* commandList, ID3D12Resource* buffers[4]);
	virtual void ReleaseShaderVariables();

	virtual void ReleaseUploadBuffer();

	virtual void OnPreRender(ID3D12GraphicsCommandList* commandList, UINT instancesNum, ID3D12Resource* buffers[4]);
	virtual void Render(ID3D12GraphicsCommandList* commandList, int subSet, ID3D12Resource* buffers[4]);
	virtual void Render(ID3D12GraphicsCommandList* commandList, int subSet, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingbufferViews, ID3D12Resource* buffers[4]);

	virtual void ShadowRender(ID3D12GraphicsCommandList* commandList, int subSet, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingbufferViews, ID3D12Resource* buffers[4]);





};