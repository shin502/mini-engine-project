#pragma once
#include "Mesh.h"
#include "StandardMesh.h"
#include "SkinnedMesh.h"
#include "Material.h"
#include "Animation.h"
//instancing

#define UNITDAMPING 0.99f
#define UNITSPEED 500.0f //500.f
#define UNITMASS 60.0f
#define UNITGRAVITY 9.8f
#define JUMPPOWER 4.4f
#define FRICTION 2.0f
#define UNITMAXVEL 4.0f //4.0f

#define PLAYERDAMPING 0.99f
#define PLAYERSPEED 10000.0f //500.f
#define PLAYERMASS 60.0f
#define PLAYERGRAVITY 9.8f
#define JUMPPOWER 4.4f
#define FRICTION 2.0f
#define MAXVEL 100.0f //4.0f

class Scene;

struct VS_CB_OBJ_INFO {
	XMFLOAT4X4 world;
};

struct CB_GAMEOBJECT_INFO
{
	XMFLOAT4X4 world;
};

struct VS_VB_INSTANCE_BOUNDING {
	XMFLOAT4X4 transform;
};

struct VS_VB_INSTANCE_PARTICLE {
	UINT textureIdx;
	XMFLOAT2 size;
	XMFLOAT3 position;
	XMFLOAT4 color;
	float alphaDegree;
};

struct VS_VB_INSTANCE_BILLBOARD
{
	XMFLOAT3 position;
	XMFLOAT3 instanceInfo;	// x : width, y : height, z : textureIdx
};


struct TEXTURENAME
{
	std::vector<std::pair<int, _TCHAR*>> diffuse;
	std::vector<std::pair<int, _TCHAR*>> normal;
	std::vector<std::pair<int, _TCHAR*>> specular;
	std::vector<std::pair<int, _TCHAR*>> metallic;

	int GetSize() {
		return (int)diffuse.size() + (int)normal.size() + (int)specular.size() + (int)metallic.size();
	}
};

class GameObject
{
private:
protected:
	XMFLOAT4X4 m_world;
	XMFLOAT4X4 transform;
	XMFLOAT3 m_yrp;

	XMFLOAT3 m_velocity;
	XMFLOAT3 m_gravity;
	XMFLOAT3 m_friction;
	XMFLOAT3 m_acc;
	XMFLOAT3 m_damping;
	float m_mass = 0.0f;
	bool m_jumpCool = false;
	bool m_jumping = false;
	float m_maxVel;

	vector<Mesh*> m_mesh;
	vector<Mesh*> m_shadowMesh;
	vector<Material*> m_material;
	vector<Texture*> textures;
	queue<Node> m_RouteList;
	BoundingBox m_aabbBox;
	int m_state = E_IDLE;

	int m_lightidx = FAILNUMBER;

	int m_ConstantNumber = -1; // -1 -> instancing 
	bool m_isPlayer = false;
	bool m_isSkinned = false; // 
	bool m_isInstanceModel = false;
	bool m_isVisable = true;
	std::chrono::high_resolution_clock::time_point ctime;
	DiffuseCubeMesh* m_boundingMesh = NULL;
	vector<Mesh*> m_temporaryMesh;
	int m_objNum = 0;
	float m_grassNumber = 0.0f;
	int m_instanceNum = 0;
	int m_weaponNum = 1;

public:
	GameObject();
	virtual ~GameObject();
	//void Release();
	XMFLOAT3 GetTransFormScale();

	XMFLOAT3 GetPosition();
	XMFLOAT3 GetLook();
	XMFLOAT3 GetUp();
	XMFLOAT3 GetRight();
	XMFLOAT4X4 GetWorld();
	vector<Material*> GetMaterial();
	int GetConstantNumber();
	bool GetisPlayer();
	vector<Texture*>& GetTextures();
	XMFLOAT3 GetTransFormPos();
	XMFLOAT3 GetTransFormLook();
	XMFLOAT4X4 GetTransForm();

	bool GetIsVisable();

	void SetScale(XMFLOAT3 scale);
	void SetPosition(XMFLOAT3 pos);
	void SetPosition(float x, float y, float z);
	void SetHeight(float height);
	void SetTransformHeight(float height);
	void SetTransFormPos(float x, float y, float z);
	void SetTransFormPos(XMFLOAT3 pos);
	void SetTransFormLook(XMFLOAT3 look);
	void SetTransFormUp(XMFLOAT3 up);
	void SetTransFormRight(XMFLOAT3 right);


	void SetTransFormScale(float x, float y, float z);
	void SetTransForm(XMFLOAT4X4 transform);
	void SetIsVisable(bool isVisable);
	void SetInstanceNum(int instanceNum);

	void SetLook(XMFLOAT3 look);
	void SetRight(XMFLOAT3 right);
	void SetUp(XMFLOAT3 up);
	void SetWorld(XMFLOAT4X4 world);
	void SetConstantNumber(int n);
	void SetisPlayer(bool p);
	void SetBoundingMesh(DiffuseCubeMesh* mesh);
	void SetTemporaryMesh(Mesh* mesh);
	void SetisSkinned(bool isSkinned);
	void SetisInstanceModel(bool isModel);
	void SetLightIdx(int lightidx);
	void SetState(int state);
	void SetRouteList(vector<int>& routeList , vector<vector<Node>>& astarNodes);

	void CreateAABBBox(XMFLOAT4X4 transform);
	void UpdateTransForm(int idx, VS_VB_INSTANCE_OBJECT* MappedObjects);
	//grass
	void UpdateGrassTransForm(int idx, VS_GB_INSTANCE_OBJECT* MappedObjects);

	void MovingObjects(vector<vector<Node>>& astarNodes, float elapsedTime);

	virtual void Animate() {}
	virtual void Animate(float timeElapsed, XMFLOAT4X4* parent) {}

	virtual void SetMesh(Mesh* mesh);
	virtual void SetMesh(int idx, Mesh* mesh);
	virtual void SetShadowMesh(Mesh* mesh);
	void SetGrassNumber(float GrassNumber);


	virtual void SetMaterial(int idx, Material* material);
	virtual void InsertMaterial(Material* material);
	
	virtual void SetWeaponNum(int weaponNum);
	virtual int GetWeaponNum();

	virtual Mesh* GetMesh(int idx);
	virtual void SetObjNum(int num);
	virtual int GetObjNum();
	virtual int GetObjKinds();
	virtual BoundingBox& GetBounding();
	int GetInstanceNum();
	int GetLightIdx();
	int GetState();
	float GetGrassNumber();
	void CheckMaxVel(XMFLOAT3& velocity);
	void ApplyFriction(XMFLOAT3& velocity, float elapsedTime);

	virtual void BasicRenderIdx(ComPtr<ID3D12GraphicsCommandList>& commandList, int midx) {};

	virtual void BasicRender(ComPtr<ID3D12GraphicsCommandList>& commandList); // �⺻ ������ 
	virtual void ReleaseUploadBuffer() {}
	virtual void InstancingAnimateRender(ID3D12GraphicsCommandList* commandList , UINT instanceNum, D3D12_VERTEX_BUFFER_VIEW* instancingStandardBufferView, D3D12_VERTEX_BUFFER_VIEW instancingObjIdxBufferView, ComPtr<ID3D12Resource>* buffers[4], Shader* tmpshader , bool isShadow) {}
	virtual void InstancingRender(ID3D12GraphicsCommandList* commandList, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW* instancingGameObjectBufferView , bool isShadow) {}
	virtual void InstancingRender(ID3D12GraphicsCommandList* commandList, UINT instanceNum, D3D12_VERTEX_BUFFER_VIEW instancingBufferView);
	virtual void OnPrePareInstancingRender(ID3D12GraphicsCommandList* commandList, Scene* scene) {} // set Texture etc
	bool GetisInstanceModel() {return m_isInstanceModel;}
	TEXTURENAME		textureName;

	virtual void UpdateTransform();
};

class CubeObj : public GameObject{
private:

public:
	virtual void ReleaseUploadBuffer();
};

class TerrainObj : public CubeObj {
private:
	
public:

};

class SunMoonObj : public GameObject {
private:

public:
	virtual void BasicRenderIdx(ComPtr<ID3D12GraphicsCommandList>& commandList, int midx);
};
