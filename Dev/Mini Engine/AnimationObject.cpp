#include "AnimationObject.h"
#include "Scene.h"

AnimationObj::AnimationObj(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, LoadedModelInfo* model, int animationTracks, int job) : HierarchyObj()
{
	LoadedModelInfo* tmpModel = model;
	m_model = model;
	SetChild(tmpModel->modelRootObject);
	skinnedAnimationController = new AnimationController(device, commandList, animationTracks, tmpModel);

	if (job == PlayerJobRifle)
	{
		skinnedAnimationController->animationSets->animationSets[MainCharacterReload]->animationType = AnimationTypeOnce;
	}

	skinnedAnimationController->playerJob = job;
}

void AnimationObj::SetInstancingTexture(Scene* scene,ID3D12Device* device, ID3D12GraphicsCommandList* commandList,LoadedModelInfo* model)
{
	auto tmpmd = model->modelRootObject;
		if (tmpmd->textureName.diffuse.size() > 0)
		{
			Texture* diffuseTexture = new Texture((int)tmpmd->textureName.diffuse.size(), ResourceTexture2D, 0);
			for (int i = 0; i < tmpmd->textureName.diffuse.size(); ++i)
			{
				diffuseTexture->LoadTextureFromFile(device, commandList, tmpmd->textureName.diffuse[i].second, tmpmd->textureName.diffuse[i].first);
			}
			this->diffuseIdx = scene->CreateShaderResourceViews(device, commandList, diffuseTexture->m_textures, GraphicsRootDiffuseTextures, diffuseTexture->m_textureNum , ResourceTexture2D);
			textures.emplace_back(diffuseTexture);
		}
		if (tmpmd->textureName.normal.size() > 0)
		{
			Texture* normalTexture = new Texture((int)tmpmd->textureName.normal.size(), ResourceTexture2D, 0);
			for (int i = 0; i < tmpmd->textureName.normal.size(); ++i)
			{
				normalTexture->LoadTextureFromFile(device, commandList, tmpmd->textureName.normal[i].second, tmpmd->textureName.normal[i].first);
			}
			this->normalIdx = scene->CreateShaderResourceViews(device, commandList, normalTexture->m_textures, GraphicsRootNormalTextures , normalTexture->m_textureNum , ResourceTexture2D);
			textures.emplace_back(normalTexture);
		}
		if (tmpmd->textureName.specular.size() > 0)
		{
			Texture* specularTexture = new Texture((int)tmpmd->textureName.specular.size(), ResourceTexture2D, 0);
			for (int i = 0; i < tmpmd->textureName.specular.size(); ++i)
			{
				specularTexture->LoadTextureFromFile(device, commandList, tmpmd->textureName.specular[i].second, tmpmd->textureName.specular[i].first);
			}
			this->specularIdx = scene->CreateShaderResourceViews(device, commandList, specularTexture->m_textures, GraphicsRootSpecularTextures,
				specularTexture->m_textureNum, ResourceTexture2D);
			textures.emplace_back(specularTexture);
		}
		if (tmpmd->textureName.metallic.size() > 0)
		{
			Texture* metallicTexture = new Texture((int)tmpmd->textureName.metallic.size(), ResourceTexture2D, 0);
			for (int i = 0; i < tmpmd->textureName.metallic.size(); ++i)
			{
				metallicTexture->LoadTextureFromFile(device, commandList, tmpmd->textureName.metallic[i].second, tmpmd->textureName.metallic[i].first);
			}
			this->metallicIdx = scene->CreateShaderResourceViews(device, commandList, metallicTexture->m_textures, GraphicsRootMetallicTextures,
				metallicTexture->m_textureNum, ResourceTexture2D);
			textures.emplace_back(metallicTexture);
		}

}

int AnimationObj::GetObjKinds()
{
	return E_ANIMATIONOBJ;
}




AnimationObj::~AnimationObj()
{
	HierarchyObj::~HierarchyObj();
}

