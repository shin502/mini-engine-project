#pragma once
#include "StandardInstanceShader.h"
class StandardSkinnedAnimationShader : public StandardShader
{
public:
	StandardSkinnedAnimationShader() {};
	virtual ~StandardSkinnedAnimationShader() {};

	virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();
	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
	virtual D3D12_SHADER_BYTECODE CreateShadowVertexShader();
	virtual D3D12_SHADER_BYTECODE CreateComputeShader();
	virtual int GetShaderNumber();
};

