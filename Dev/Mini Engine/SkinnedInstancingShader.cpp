#include "SkinnedInstancingShader.h"
#include "AnimationObject.h"
SkinnedInstancingShader::~SkinnedInstancingShader()
{
	delete[] m_PositionW;
	delete[] m_NormalW;
	delete[] m_TangentW;
	delete[] m_BiTangentW;

	delete[] m_MappedSkinned;
	delete[] m_MappedStandard;
	delete[] m_StandardBufferView;
	delete[] m_StandardResource;
	delete[] m_SkinnedResource;

}
void SkinnedInstancingShader::CreatebPipeLineState(ComPtr<ID3D12Device>& device, ComPtr<ID3D12RootSignature>& rootsignature, ComPtr<ID3D12RootSignature>& computeRootSignature)
{
	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
	psoDesc.InputLayout = CreateInputLayout();
	psoDesc.pRootSignature = rootsignature.Get();
	psoDesc.VS = CreateVertexShader();
	psoDesc.PS = CreatePixelShader();
	psoDesc.RasterizerState = CreateRasterizerState();
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
	psoDesc.SampleMask = UINT_MAX;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	psoDesc.NumRenderTargets = 4;
	psoDesc.RTVFormats[0] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	psoDesc.RTVFormats[1] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	psoDesc.RTVFormats[2] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	psoDesc.RTVFormats[3] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	psoDesc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
	psoDesc.SampleDesc.Count = 1;
	ThrowIfFailed(device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_pipes[0])));
	if (psoDesc.InputLayout.pInputElementDescs) delete[] psoDesc.InputLayout.pInputElementDescs;
	
	UINT inputElementDescsNum = 9;
	D3D12_INPUT_ELEMENT_DESC* inputElementDescs = new D3D12_INPUT_ELEMENT_DESC[inputElementDescsNum];

	inputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[1] = { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 1, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[2] = { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 2, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[3] = { "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 3, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[4] = { "BITANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 4, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };

	inputElementDescs[5] = { "WORLDMATRIX", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 5, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	inputElementDescs[6] = { "WORLDMATRIX", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 5, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	inputElementDescs[7] = { "WORLDMATRIX", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 5, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	inputElementDescs[8] = { "WORLDMATRIX", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 5, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };

	D3D12_INPUT_LAYOUT_DESC inputLayoutDesc;
	inputLayoutDesc.pInputElementDescs = inputElementDescs;
	inputLayoutDesc.NumElements = inputElementDescsNum;

	psoDesc.InputLayout = inputLayoutDesc;
	psoDesc.VS = Shader::CompileShaderFromFile(L"Skinned.hlsl", "VSStandardInstancing", "vs_5_1", &m_vsBlob);
	ThrowIfFailed(device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_pipes[1])));
	if (psoDesc.InputLayout.pInputElementDescs) delete[] psoDesc.InputLayout.pInputElementDescs;


	//UINT inputElementDescsNum = 5;
	//D3D12_INPUT_ELEMENT_DESC* BoundinginputElementDescs = new D3D12_INPUT_ELEMENT_DESC[inputElementDescsNum];
	//BoundinginputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	//BoundinginputElementDescs[1] = { "WORLDMATRIX", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	//BoundinginputElementDescs[2] = { "WORLDMATRIX", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	//BoundinginputElementDescs[3] = { "WORLDMATRIX", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	//BoundinginputElementDescs[4] = { "WORLDMATRIX", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };

	//psoDesc.InputLayout.NumElements = inputElementDescsNum;
	//psoDesc.InputLayout.pInputElementDescs = BoundinginputElementDescs;
	//psoDesc.RasterizerState.FillMode = D3D12_FILL_MODE_WIREFRAME;
	//psoDesc.VS = Shader::CompileShaderFromFile(L"Shaders.hlsl", "VSBasic", "vs_5_1", &m_vsBlob);
	//psoDesc.PS = Shader::CompileShaderFromFile(L"Shaders.hlsl", "PSBasic", "ps_5_1", &m_psBlob);
	//ThrowIfFailed(device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_pipes[2])));

	//if (psoDesc.InputLayout.pInputElementDescs) delete[] psoDesc.InputLayout.pInputElementDescs;
	

	//skinnedMesh shadow pipe
	inputElementDescsNum = 2;
	inputElementDescs = new D3D12_INPUT_ELEMENT_DESC[inputElementDescsNum];

	inputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[1] = { "OBJIDX", 0, DXGI_FORMAT_R32_UINT, 1, 0, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	inputLayoutDesc.pInputElementDescs = inputElementDescs;
	inputLayoutDesc.NumElements = inputElementDescsNum;

	psoDesc.InputLayout = inputLayoutDesc;
	psoDesc.VS = Shader::CompileShaderFromFile(L"Shadow.hlsl", "VS_SHADOW_SKINNED_INSTANCING", "vs_5_1", &m_vsBlob);
	psoDesc.PS = Shader::CompileShaderFromFile(L"Shadow.hlsl", "PS_SHADOW", "ps_5_1", &m_psBlob);

	ThrowIfFailed(device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_ShadowPipes[0])));
	if (psoDesc.InputLayout.pInputElementDescs) delete[] psoDesc.InputLayout.pInputElementDescs;


	//standardMesh pipe 
	inputElementDescsNum = 5;
	inputElementDescs = new D3D12_INPUT_ELEMENT_DESC[inputElementDescsNum];

	inputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[1] = { "WORLDMATRIX", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	inputElementDescs[2] = { "WORLDMATRIX", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	inputElementDescs[3] = { "WORLDMATRIX", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	inputElementDescs[4] = { "WORLDMATRIX", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };


	inputLayoutDesc.pInputElementDescs = inputElementDescs;
	inputLayoutDesc.NumElements = inputElementDescsNum;

	psoDesc.InputLayout = inputLayoutDesc;
	psoDesc.VS = Shader::CompileShaderFromFile(L"Shadow.hlsl", "VS_SHADOW_INSTANCING", "vs_5_1", &m_vsBlob);
	psoDesc.PS = Shader::CompileShaderFromFile(L"Shadow.hlsl", "PS_SHADOW", "ps_5_1", &m_psBlob);


	ThrowIfFailed(device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_ShadowPipes[1])));
	if (psoDesc.InputLayout.pInputElementDescs) delete[] psoDesc.InputLayout.pInputElementDescs;
	if (m_vsBlob) m_vsBlob->Release();
	if (m_psBlob) m_psBlob->Release();

	D3D12_COMPUTE_PIPELINE_STATE_DESC computePipelineStateDesc;
	//컴퓨트 스키닝
	::ZeroMemory(&computePipelineStateDesc, sizeof(D3D12_COMPUTE_PIPELINE_STATE_DESC));
	computePipelineStateDesc.pRootSignature = computeRootSignature.Get();
	computePipelineStateDesc.CS = CreateComputeShader();

	ThrowIfFailed(device->CreateComputePipelineState(&computePipelineStateDesc, IID_PPV_ARGS(&m_ComputePipes)));
	if (m_csBlob) m_csBlob->Release();
}

void SkinnedInstancingShader::CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
	m_skinnedStride = sizeof(XMFLOAT4X4) * SKINNED_ANIMATION_BONES;
	m_SkinnedResource = new ComPtr<ID3D12Resource>[m_modelInfo.skinnedMeshNum];
	m_MappedSkinned = new XMFLOAT4X4 * [m_modelInfo.skinnedMeshNum];
	
	m_standardStride = sizeof(VS_VB_INSTANCE_OBJECT);
	m_StandardResource = new ComPtr<ID3D12Resource>[m_modelInfo.standardMeshNum];
	m_MappedStandard = new VS_VB_INSTANCE_OBJECT *[m_modelInfo.standardMeshNum];
	m_StandardBufferView = new D3D12_VERTEX_BUFFER_VIEW[m_modelInfo.standardMeshNum];

	objIdxStride = sizeof(UINT);



	for (int i = 0; i < m_modelInfo.skinnedMeshNum; ++i)
	{
		m_SkinnedResource[i] = ::CreateBufferResource(device, commandList, NULL, m_skinnedStride * SKINNED_BUFFERSIZE, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_GENERIC_READ, NULL);
		m_SkinnedResource[i]->Map(0, NULL, (void**)&m_MappedSkinned[i]);
	}
	//skinned Bone TransFrom Buffer

	m_objIdx = ::CreateBufferResource(device, commandList, NULL, objIdxStride * (UINT)SKINNED_BUFFERSIZE, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
	m_objIdx->Map(0, NULL, (void**)&m_MappedObjIdx);
	m_ObjIdxBufferView.BufferLocation = m_objIdx->GetGPUVirtualAddress();
	m_ObjIdxBufferView.StrideInBytes = objIdxStride;
	m_ObjIdxBufferView.SizeInBytes = objIdxStride * (UINT)SKINNED_BUFFERSIZE;
	//object VertexBuffer 

	for (int i = 0; i < m_modelInfo.standardMeshNum; ++i)
	{
		m_StandardResource[i] = ::CreateBufferResource(device, commandList, NULL, m_standardStride * (UINT)SKINNED_BUFFERSIZE, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
		m_StandardResource[i]->Map(0, NULL, (void**)&m_MappedStandard[i]);

		m_StandardBufferView[i].BufferLocation = m_StandardResource[i]->GetGPUVirtualAddress();
		m_StandardBufferView[i].StrideInBytes = m_standardStride;
		m_StandardBufferView[i].SizeInBytes = m_standardStride * ((UINT)SKINNED_BUFFERSIZE);
	}

	m_PositionW = new ComPtr<ID3D12Resource>[m_modelInfo.skinnedMeshNum];
	m_NormalW = new ComPtr<ID3D12Resource>[m_modelInfo.skinnedMeshNum];
	m_TangentW = new ComPtr<ID3D12Resource>[m_modelInfo.skinnedMeshNum];
	m_BiTangentW = new ComPtr<ID3D12Resource>[m_modelInfo.skinnedMeshNum];

	for (int i = 0; i < m_modelInfo.skinnedMeshNum; ++i) {
		UINT bytesNum = sizeof(XMFLOAT3) * m_modelInfo.skinnedMeshes[i]->GetVerticesNum() * SKINNED_BUFFERSIZE;
		m_PositionW[i] = ::CreateBufferResource(device, commandList, NULL, bytesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, NULL, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);
		m_NormalW[i] = ::CreateBufferResource(device, commandList, NULL, bytesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, NULL, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);
		m_TangentW[i] = ::CreateBufferResource(device, commandList, NULL, bytesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, NULL, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);
		m_BiTangentW[i] = ::CreateBufferResource(device, commandList, NULL, bytesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, NULL, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);

		::ResourceBarrier(commandList, m_PositionW[i].Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
		::ResourceBarrier(commandList, m_NormalW[i].Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
		::ResourceBarrier(commandList, m_TangentW[i].Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
		::ResourceBarrier(commandList, m_BiTangentW[i].Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
	}
}

void SkinnedInstancingShader::UpdateShaderVariable(int objIdx) // process dummy
{
	for (int i = 0; i < m_modelInfo.skinnedMeshNum; ++i)
	{
		for (int j = 0; j < m_modelInfo.skinnedMeshes[i]->skinningBones; ++j)
		{
			XMStoreFloat4x4(&m_MappedSkinned[i][(objIdx * SKINNED_ANIMATION_BONES) + j], // 스킨 메쉬 bones들에 월드 변환 행렬을 넣어준다.
				XMMatrixTranspose(XMLoadFloat4x4(&m_modelInfo.skinnedMeshes[i]->skinningBoneFrameCaches[j]->GetWorld())));
		}
	}
}

void SkinnedInstancingShader::UpdateShaderVariables(int objNum)
{
	for (int j = 0; j < objNum; ++j)
	{
		m_MappedObjIdx[j] = j;
	}
}

D3D12_SHADER_BYTECODE SkinnedInstancingShader::CreateComputeShader()
{
	return(Shader::CompileShaderFromFile(L"CSSkinning.hlsl", "main", "cs_5_1", &m_csBlob));
}

int SkinnedInstancingShader::GetShaderNumber()
{
	return E_SKINNEDINSTANCINGSHADER;
}

void SkinnedInstancingShader::SetPipeLine(ID3D12GraphicsCommandList* commandList, int idx)
{
	commandList->SetPipelineState(m_pipes[idx].Get());
}

void SkinnedInstancingShader::SetShadowPipe(ID3D12GraphicsCommandList* commandList, int idx)
{
	commandList->SetPipelineState(m_ShadowPipes[idx].Get());
}

void SkinnedInstancingShader::SetComputePipelineState(ID3D12GraphicsCommandList* commandList)
{
	commandList->SetPipelineState(m_ComputePipes.Get());
}
