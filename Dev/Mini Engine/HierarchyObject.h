#pragma once
#include"GameObject.h"

struct WeaponPoint {
	XMFLOAT4X4 start;
	XMFLOAT4X4 end;

	void Clear()
	{
		start._11 = 0.0f; start._12 = 0.0f; start._13 = 0.0f; start._14 = 1.0f;
		start._21 = 0.0f; start._22 = 0.0f; start._23 = 0.0f; start._24 = 1.0f;
		start._31 = 0.0f; start._32 = 0.0f; start._33 = 0.0f; start._34 = 1.0f;
		start._41 = 0.0f; start._42 = 0.0f; start._43 = 0.0f; start._44 = 1.0f;

		end._11 = 0.0f; end._12 = 0.0f; end._13 = 0.0f; end._14 = 1.0f;
		end._21 = 0.0f; end._22 = 0.0f; end._23 = 0.0f; end._24 = 1.0f;
		end._31 = 0.0f; end._32 = 0.0f; end._33 = 0.0f; end._34 = 1.0f;
		end._41 = 0.0f; end._42 = 0.0f; end._43 = 0.0f; end._44 = 1.0f;
	}
};

class HierarchyObj : public CubeObj
{
private:
protected:
	HierarchyObj* parent = NULL;
	HierarchyObj* child = NULL;
	HierarchyObj* sibling = NULL;
	HierarchyObj* toParent;
	LoadedModelInfo* m_model;
	VS_VB_INSTANCE_OBJECT** m_MappedObject = NULL;
	bool isCollisionSkinnedModel = false;

	char frameName[128] = { '\0' };
	UINT shaderType = 0;
	int framesNum = 0;
	int maximumMaterialsNum = 0;

	int diffuseIdx = -1;
	int normalIdx = -1;
	int specularIdx = -1;
	int metallicIdx = -1;

public:
	AnimationController* skinnedAnimationController = NULL;
	int animationStatus = MainCharacterIdle;
	int upAnimationStatus = MainCharacterIdle;
	int loAnimationStatus = MainCharacterIdle;

	HierarchyObj();
	HierarchyObj(int meshesNum);
	virtual ~HierarchyObj();

	HierarchyObj* GetParent() { return(parent); }
	int  GetParentFrame(char name[]);
	void UpdateTransform(XMFLOAT4X4* parent);
	void UpdateTransform(CB_GAMEOBJECT_INFO** cbMappedGameObjects, UINT cbGameObjectBytes, int idx, XMFLOAT4X4* parent = NULL);
	void UpdateTransform(int idx, XMFLOAT4X4* parent);
	void UpdateTransformOnlyStandardMesh(CB_GAMEOBJECT_INFO** cbMappedGameObjects, UINT cbGameObjectBytes, int idx, int framesNum, XMFLOAT4X4* parent);
	WeaponPoint UpdateTransformOnlyStandardMesh(VS_VB_INSTANCE_OBJECT** vbMappedGameObjects, int idx, int framesNum, int weaponNum, XMFLOAT4X4* parent, XMFLOAT4X4* toArrow);

	void UpdateBoundingTransForm(VS_VB_INSTANCE_BOUNDING* vbMappedBoudnigs, int idx, XMFLOAT4X4 pWorld, int bidx);
	void UpdateSkinnedBoundingTransform(VS_VB_INSTANCE_BOUNDING** vbMappedBoundings, std::vector<BoundingOrientedBox>& obbs, std::vector<HierarchyObj*>* objVec, int idx, int framesNum, XMFLOAT4X4* parent);
	virtual void UpdateTransform(VS_VB_INSTANCE_BILLBOARD* vbMappedBillboardObject, int idx) {}
	virtual void UpdateTransform(VS_VB_INSTANCE_PARTICLE* vbMappedBillboardObject, int idx) {}
	virtual void UpdateTransform(VS_VB_INSTANCE_OBJECT** worldCashe, int idx, XMFLOAT4X4* parent);

	virtual void GetHierarchyMaterial(std::vector<MATERIAL>& materialReflection, int& idx);
	virtual void Animate(float timeElapsed, XMFLOAT4X4* parent);
	virtual void InstancingAnimateRender(ID3D12GraphicsCommandList* commandList, UINT instanceNum, D3D12_VERTEX_BUFFER_VIEW* instancingStandardBufferView, D3D12_VERTEX_BUFFER_VIEW instancingObjIdxBufferView, ComPtr<ID3D12Resource>* buffers[4], Shader* tmpshader, bool isShadow);
	virtual void InstancingRender(ID3D12GraphicsCommandList* commandList, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW* instancingGameObjectBufferView , bool isShadow);
	virtual void OnPrePareInstancingRender(ID3D12GraphicsCommandList* commandList, Scene* scene);

	int GetFrameNum() { return framesNum; }
	virtual int GetObjKinds();

	void SetTransform(XMFLOAT4X4 transform);
	void SetChild(HierarchyObj* child);
	void SetModel(LoadedModelInfo* model);
	void SetInstancingTexture(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, Scene* scene);

	void Rotate(float pitch, float yaw, float roll);
	void ReleaseObj();
	
	HierarchyObj* FindFrame(char* pstrFrameName);
	int CompareName(char name[]) { return strcmp(frameName, name); };
	_TCHAR* FindReplicatedTexture(_TCHAR* textureName);
	UINT GetMeshType(int idx = 0) { return((m_mesh[idx]) ? m_mesh[idx]->GetType() : 0x00); }
	void FindAndSetSkinnedMesh(SkinnedMesh** SkinnedMeshes, int* SkinnedMesh);
	void ReleaseUploadBuffer();

	void LoadMaterialsFromFile(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, GameObject* parent, FILE* inFile, char* folderName, TEXTURENAME& textureMapNames, int textureIdx[5]);
	static LoadedModelInfo* LoadGeometryAndAnimationFromFile(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, char* fileName, char* folderName);
	static HierarchyObj* LoadFrameHierarchyFromFile(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, GameObject* parent, FILE* inFile, char* folderName, TEXTURENAME& textureMapNames, int textureIdx[5], int& frameCounter, int& materialCounter, int* skinnedMeshesNum, int* standardMeshCounter, int* boundingMeshNum);
	static void LoadAnimationFromFile(FILE* inFile, LoadedModelInfo* loadedModel);
	static HierarchyObj* LoadGeometryFromFile(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, char* fileName, char* folderName);
};

