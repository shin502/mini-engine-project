#include "StandardSkinnedAnimationInstanceShader.h"

void StandardSkinnedAnimationInstanceShader::CreatebPipeLineState(ComPtr<ID3D12Device>& device, ComPtr<ID3D12RootSignature>& rootsignature, ComPtr<ID3D12RootSignature>& computeRootSignature)
{	
	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
	psoDesc.InputLayout = CreateInputLayout();
	psoDesc.pRootSignature = rootsignature.Get();
	psoDesc.VS = CreateVertexShader();
	psoDesc.PS = CreatePixelShader();
	psoDesc.RasterizerState = CreateRasterizerState();
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
	psoDesc.SampleMask = UINT_MAX;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	psoDesc.NumRenderTargets = 1;
	psoDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	psoDesc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
	psoDesc.SampleDesc.Count = 1;
	ThrowIfFailed(device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_pipes[0])));
	Shader::CreatebPipeLineState(device , rootsignature);
	psoDesc.VS = Shader::CompileShaderFromFile(L"Standard.hlsl", "VSSkinnedAnimationStandard", "vs_5_1", &m_vsBlob);
	HRESULT hResult = device->CreateGraphicsPipelineState(&psoDesc, __uuidof(ID3D12PipelineState), (void**)&m_pipes[1]);

	if (psoDesc.InputLayout.pInputElementDescs) delete[] psoDesc.InputLayout.pInputElementDescs;
	if (m_vsBlob) m_vsBlob->Release();
	if (m_psBlob) m_psBlob->Release();
}

D3D12_SHADER_BYTECODE StandardSkinnedAnimationInstanceShader::CreateVertexShader()
{
	return(Shader::CompileShaderFromFile(L"Skinned.hlsl", "VSSkinnedAnimationInstancing", "vs_5_1", &m_vsBlob));
}

D3D12_SHADER_BYTECODE StandardSkinnedAnimationInstanceShader::CreateShadowVertexShader()
{
	return D3D12_SHADER_BYTECODE();
}

int StandardSkinnedAnimationInstanceShader::GetShaderNumber()
{
	return E_STANDARDSKINNEDANIMATIONINSTANCESHADER;
}
