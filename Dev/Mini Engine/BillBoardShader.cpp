#include "BillBoardShader.h"

void BillBoardShader::CreatebPipeLineState(ComPtr<ID3D12Device>& device, ComPtr<ID3D12RootSignature>& rootsignature)
{
	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
	psoDesc.InputLayout = CreateInputLayout();
	psoDesc.pRootSignature = rootsignature.Get();
	psoDesc.VS = CreateVertexShader();
	psoDesc.GS = CreateGeoMetryShader();
	psoDesc.PS = CreatePixelShader();
	psoDesc.RasterizerState = CreateRasterizerState();
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
	psoDesc.SampleMask = UINT_MAX;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_POINT;
	psoDesc.NumRenderTargets = 4;
	psoDesc.RTVFormats[0] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	psoDesc.RTVFormats[1] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	psoDesc.RTVFormats[2] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	psoDesc.RTVFormats[3] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	psoDesc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
	psoDesc.SampleDesc.Count = 1;


	ThrowIfFailed(device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_bpipelineState)));
	if(psoDesc.InputLayout.pInputElementDescs) delete[] psoDesc.InputLayout.pInputElementDescs;
}

D3D12_INPUT_LAYOUT_DESC BillBoardShader::CreateInputLayout()
{
	UINT inputElementDescsNum = 3;
	D3D12_INPUT_ELEMENT_DESC* inputElementDescs = new D3D12_INPUT_ELEMENT_DESC[inputElementDescsNum];

	inputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[1] = { "SIZE", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[2] = { "TEXID", 0, DXGI_FORMAT_R32_UINT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	D3D12_INPUT_LAYOUT_DESC inputLayout;
	inputLayout.NumElements = inputElementDescsNum;
	inputLayout.pInputElementDescs = inputElementDescs;
	return inputLayout;
}

D3D12_SHADER_BYTECODE BillBoardShader::CreateVertexShader()
{
	return(CompileShaderFromFile(L"BillBoardShader.hlsl", "VSBILLBOARD", "vs_5_1", &m_vsBlob));
}

D3D12_SHADER_BYTECODE BillBoardShader::CreatePixelShader()
{
	return(CompileShaderFromFile(L"BillBoardShader.hlsl", "PSBILLBOARD", "ps_5_1", &m_psBlob));
}

D3D12_SHADER_BYTECODE BillBoardShader::CreateGeoMetryShader()
{
	return(CompileShaderFromFile(L"BillBoardShader.hlsl", "GSBILLBOARD", "gs_5_1", &m_gsBlob));
}

int BillBoardShader::GetShaderNumber()
{
	return E_BILLBOARDSHADER;
}
