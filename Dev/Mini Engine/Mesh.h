#pragma once
#include "Vertex.h"

struct Node {
	XMFLOAT3 pos;

	float F;
	float G;
	float H;
	Node* ParentNode;
	int number;

	Node() {
		pos = XMFLOAT3();

		number = 0;
	}

	Node(float x, float y ,float z, int number) {
		pos.x = x;
		pos.y = y;
		pos.z = z;
		this->number = number;
	}

};

class Mesh
{
private:
	Vertex* m_vertices = NULL;
	
	
protected:

	int m_verticesNum = 0;

	UINT* m_indices = NULL;
	int m_indicesNum = 0;

	XMFLOAT3 m_aabbBoxCenter;
	XMFLOAT3 m_aabbBoxExtents;

	D3D12_PRIMITIVE_TOPOLOGY		m_primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	UINT							m_Slot = 0;
	UINT							m_Offset = 0;

	ComPtr<ID3D12Resource>			m_vertexBuffer;
	ID3D12Resource*					m_vertexUploadBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW		m_vertexBufferView;

	ComPtr<ID3D12Resource>			m_indexBuffer = nullptr;
	ID3D12Resource*					m_indexUploadBuffer = NULL;
	D3D12_INDEX_BUFFER_VIEW			m_indexBufferView;



public:
	Mesh();
	virtual ~Mesh();
	virtual void Render(ID3D12GraphicsCommandList* commandList, int subsetNum, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingBufferViews, ID3D12Resource* buffers[4]) {};
	virtual void Render(ComPtr<ID3D12GraphicsCommandList>& commandList);
	virtual void Render(ID3D12GraphicsCommandList* commandList, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingBufferViews);
	virtual void Render(ID3D12GraphicsCommandList* commandList, int subsetNum, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingBufferViews)
	{
		Render(commandList, instancesNum, instancingBufferViews);
	};

	virtual void Render(ID3D12GraphicsCommandList* commandList, int subsetNum, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingBufferViews, int weaponNum)
	{
		Render(commandList, instancesNum, instancingBufferViews);
	};


	virtual void ShadowRender(ID3D12GraphicsCommandList* commandList, int subsetNum, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingBufferViews) {}
	virtual void ShadowRender(ID3D12GraphicsCommandList* commandList, int subSet, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingbufferViews, ID3D12Resource* buffers[4]) {}



	virtual void ReleaseUploadBuffer();
	virtual UINT GetType() { return UINT(1); }
};

class DiffuseCubeMesh : public Mesh {
private:
	DiffuseVertex* m_vertices = NULL;


public:
	virtual ~DiffuseCubeMesh();
	DiffuseCubeMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, float width, float height, float depth);

};

class TerrainMesh : public Mesh {
private:
	TexturedVertex* m_vertices = NULL;
	int m_width;
	int m_length;
	XMFLOAT3 m_scale;
	vector<BYTE> m_heightMapPixel;
public:
	virtual ~TerrainMesh();
	TerrainMesh(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList, int width, int length, XMFLOAT3 scale);
	void LoadHeightMapFile();
	XMFLOAT3 CalNormal(int index);
	XMFLOAT3 CalTangent(int index0, int index1, int index2, XMFLOAT3& tangent, XMFLOAT3& biTangents, XMFLOAT3& normals);
	float GetPixelHeight(int x, int z);
	float GetHeightByPos(float px,  float pz , int startx , int startz , int width , int length);
	bool GetTerrainRayPos(XMFLOAT3 rayOrgin, XMFLOAT3 dir, int startx, int startz, int width, int length , XMFLOAT3& fPos);
	float GetHeightBylerp(float px, float pz);
	float GetHeightByCorrectLerp(float px, float pz, float worldx, float worldz);

};

class UIRectMesh : public  Mesh {
	//projection 변환까지 하면 x,y는 -1에서 1사이 이다..
	TexVertex* m_vertices = NULL;
public:
	virtual ~UIRectMesh();
	UIRectMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList , int textureNum);
	UIRectMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	
};

class RealGrassMesh : public Mesh {
	UVNORMALVertex* m_vertices = NULL;
public:
	virtual ~RealGrassMesh();
	RealGrassMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, float w, float h , float z);
	RealGrassMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
};

class BillBoardMesh : public Mesh {
	BillBoardVertex* m_vertices = NULL;
public:
	~BillBoardMesh();

	BillBoardMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, int tidx);
};

class SkyBoxMesh : public Mesh {
	SkyBoxVertex* m_vertices = NULL;
public:
	~SkyBoxMesh();
	SkyBoxMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, float fWidth, float fHeight, float fdepth);
};

class EffectMesh : public Mesh {
	EffectVertex* m_vertices = NULL;
public:
	~EffectMesh();
	EffectMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
};

class NodeMesh : public Mesh {
	DiffuseVertex* m_vertices = NULL;
public:
	~NodeMesh();
	NodeMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList , vector<vector<Node>> nodes, int nodeInterval, TerrainMesh* tmesh);
};