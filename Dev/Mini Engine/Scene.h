#pragma once
#include "AnimationObject.h"
#include "HierarchyObject.h"
#include "GameObject.h"
#include "Animation.h"
#include "QuadTree.h"
constexpr int TerrainDetailTextureNum = 3;
constexpr int TerrainNormalTextureNum = 3;
constexpr int SceneTextureNum = 3; //0: grassTexture, 1 : suntexture , 2 : moontexture
constexpr int MaterialTextureNum = 10;
constexpr int UITextureNum = 4 + NUM_CASCADES;
constexpr int DefferredCount = 4;
constexpr int ShadowCount = 1;
constexpr int SceneTextureCount = 10;
constexpr int ParticleTextureCount = 10;
constexpr int SkyTextureCount = 1;
constexpr int DepthTexture = 1;
constexpr int PostEffectTexture = 10;


//character count
constexpr int SwordCharacterNum = 10;

//terrain world
constexpr float TerrainWorld = 256.0f;



static const int postRenderCount = 1; // sun shaft

class Scene;
struct LIGHT
{
	XMFLOAT4				ambient;
	XMFLOAT4				diffuse;
	XMFLOAT4				specular;
	XMFLOAT3				position;
	float 					falloff;
	XMFLOAT3				direction;
	float 					theta; //cos(m_fTheta)
	XMFLOAT3				attenuation;
	float					phi; //cos(m_fPhi)
	bool					enable;
	int						type;
	float					range;
	float					padding;
};

struct LIGHTS
{
	LIGHT lights[MAX_LIGHTS];
	XMFLOAT4 globalAmbient;
};


struct MATERIALS
{
	MATERIAL reflections[MAX_MATERIALS];
};

struct ConstantInform {
	ComPtr<ID3D12Resource> m_cbGameObjects = NULL;
	UINT8* m_cbMappedGameObjects = NULL;
};

struct SRVROOTARGUMENTINFO
{
	UINT							rootParameterIndex;
	D3D12_GPU_DESCRIPTOR_HANDLE		srvGpuDescriptorHandle;

	SRVROOTARGUMENTINFO() { rootParameterIndex = 0; }
	SRVROOTARGUMENTINFO(UINT idx, D3D12_GPU_DESCRIPTOR_HANDLE handle)
	{
		rootParameterIndex = idx;
		srvGpuDescriptorHandle = handle;
	}
};
class Scene
{
private:
	static int rootArgumentCurrentIdx;

	ComPtr<ID3D12RootSignature> m_rootSignature;
	ComPtr<ID3D12RootSignature> m_ComputeRootsignature;
	ComPtr<ID3D12DescriptorHeap> m_DescriptorHeap;
	vector<vector<GameObject*>> m_GameObjects;
	vector<vector<GameObject*>> m_UIObjects;
	vector<vector<GameObject*>> m_LightObjects;

	vector<ConstantInform> m_GameObjCsi;
	vector<SRVROOTARGUMENTINFO>	m_rootArgumentInfos;
	vector<Texture*> m_textures;
	vector<int> m_RootArguIdx; // scene에서 읽어온 텍스처의 GPU HANDLE을 저장하는 m_rootArgumentInfos의 idx가 담겨져 있다.
	QuadTree* m_QuadRoot = NULL;
	vector<LoadedModelInfo*> m_Modelvec; //skinning Model
	vector<HierarchyObj*> m_Objects; // hier Model
	vector<vector<Node>> m_AstarNodes;
	MATERIALS* m_materials = NULL;
	int	m_materialNum = 0;
	float m_lightDegree = 0;

	bool shadowRender = true;
	LIGHTS m_lights;
	XMFLOAT3 m_rtDir;
	XMFLOAT3 m_sunPos;
	WeaponPoint pWeaponPoint;
	int curLightidx = 0;
	vector<int> lightStart;


	ComPtr<ID3D12Resource> m_cbLights = NULL;
	LIGHTS* m_cbMappedLights = NULL;

	std::vector<MATERIAL> m_materialReflection;
	ComPtr<ID3D12Resource> m_cbMaterials = NULL;
	ID3D12Resource* m_materialUploadBuffer = NULL;
	UINT m_gCbvSrvDescIncrementSize;

	D3D12_CPU_DESCRIPTOR_HANDLE			cbvCPUDescStartHandle;
	D3D12_GPU_DESCRIPTOR_HANDLE			cbvGPUDescStartHandle;

	D3D12_CPU_DESCRIPTOR_HANDLE			srvCPUDescStartHandle;
	D3D12_GPU_DESCRIPTOR_HANDLE			srvGPUDescStartHandle;

	D3D12_CPU_DESCRIPTOR_HANDLE			uavCPUDescStartHandle;
	D3D12_GPU_DESCRIPTOR_HANDLE			uavGPUDescStartHandle;

	D3D12_CPU_DESCRIPTOR_HANDLE			cbvCPUDescNextHandle;
	D3D12_GPU_DESCRIPTOR_HANDLE			cbvGPUDescNextHandle;

	D3D12_CPU_DESCRIPTOR_HANDLE			srvCPUDescNextHandle;
	D3D12_GPU_DESCRIPTOR_HANDLE			srvGPUDescNextHandle;

	D3D12_CPU_DESCRIPTOR_HANDLE			uavCPUDescNextHandle;
	D3D12_GPU_DESCRIPTOR_HANDLE			uavGPUDescNextHandle;

	ID3D12Resource* m_shadowBuffer[ShadowCount];
	ID3D12Resource* m_CasCadeBuffer[NUM_CASCADES];
	ID3D12Resource* m_depthBuffer[DepthTexture];
	ID3D12Resource* m_posteffectBuffer[postRenderCount];

public:
	ID3D12Resource* m_defferedBuffer[DefferredCount];
	~Scene();
	D3D12_ROOT_PARAMETER CreateRootDesCriptor(D3D12_ROOT_PARAMETER_TYPE pt, UINT sgn, UINT rs, D3D12_SHADER_VISIBILITY sv);
	D3D12_ROOT_PARAMETER CreateRootTable(D3D12_ROOT_PARAMETER_TYPE pt, UINT ndr, D3D12_DESCRIPTOR_RANGE* dr, D3D12_SHADER_VISIBILITY sv);
	D3D12_ROOT_PARAMETER CreateRoot32Constants(D3D12_ROOT_PARAMETER_TYPE pt, UINT bv, UINT sg, UINT rs, D3D12_SHADER_VISIBILITY sv);
	D3D12_DESCRIPTOR_RANGE CreateDesCriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE rt, UINT ndr, UINT bsr, UINT rs, UINT oid);
	D3D12_STATIC_SAMPLER_DESC CraeteSamplerDesc(D3D12_FILTER fillter, D3D12_TEXTURE_ADDRESS_MODE u, D3D12_TEXTURE_ADDRESS_MODE v, D3D12_TEXTURE_ADDRESS_MODE w, FLOAT Mip, UINT mat, D3D12_COMPARISON_FUNC cpf, D3D12_STATIC_BORDER_COLOR bc, FLOAT minLOD, FLOAT maxLOD, UINT sr, UINT rs, D3D12_SHADER_VISIBILITY svb);

	void UpdateObjectPosition();
	void LoadandCreateTextureResource(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	void LoadModelResource(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	void LoadObjectResource(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	void CreateCbvSrvUavDescriptorHeap(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, int nConstantBufferViews, int nShaderResourceViews, int nUnorderedAccessViews);
	void BuildTextures(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList);
	void InitCbInform(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	void InitCbInform(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList, int idx);
	void UpdateCbInform(ComPtr<ID3D12GraphicsCommandList>& commandList, int idx, int startNumber , int objNum);
	void UpdateInstanceObjInform(float timeElapsed, XMFLOAT3 cameraPos, XMFLOAT3 playerPos);
	void UpdateDerationalLight(float timeElpased);
	void UpdateFrustumCulling(BoundingFrustum& frustum);

	void CreateComputeRootSignature(ComPtr<ID3D12Device>& device);
	void CreateGrapicsRootSignature(ComPtr<ID3D12Device>& device);
	void BuildMaterialAndLight();
	void BuildTerrainObjcet(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList);
	void BuildCubeObjects(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList);
	void BuildInstancingSkinnedObjects(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList, int& idx);
	void BuildInstancingObjects(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList, int& idx);
	void BuildEffectObjects(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList);
	void BuildUIObjects(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList);
	void BuildLightObjects(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList);
	void BuildRealGrassObjects(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList);
	void BuildParticleObjects(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList);
	void BuildBillBoardObjects(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList);
	void BuildSkyBoxObjects(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList);
	void BuildPostObjects(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList);
	void BuildAstarNode(ComPtr<ID3D12Device>& device , ComPtr<ID3D12GraphicsCommandList>& commandList);



	void CreateQuadTree(QuadTree** parent, int number, float startX, float startZ, float width, float length);
	void ProcessPickingQuadTree(XMFLOAT3 rayOrigin, XMFLOAT3 rayDir, vector<QuadTree*>& nodes, QuadTree* node);
	QuadTree* ProcessOBBQuadTree(BoundingOrientedBox obbBox);
	QuadTree* InsertQuadObject(BoundingBox aabbBox);
	void ProcessCheckVisable(BoundingFrustum& frustum, QuadTree* node);
	void DelQuadTree(QuadTree* parent);
	vector<int> RunAstaralgorithm(GameObject* Obj, Node* fNode, float mapWorld);

	void PrePareCompute(ID3D12GraphicsCommandList* commandList);
	void PrePareRender(ComPtr<ID3D12GraphicsCommandList>& commandList);
	void OnRender(ComPtr<ID3D12GraphicsCommandList>& commandList);
	void OnShadwRender(ComPtr<ID3D12GraphicsCommandList>& commandList);
	void OnEffectRender(ComPtr<ID3D12GraphicsCommandList>& commandList);
	void OnParticleRender(ComPtr<ID3D12GraphicsCommandList>& commandList, XMFLOAT3 cPos);
	void OnNodeRender(ComPtr<ID3D12GraphicsCommandList>& commandList);
	void OnUIRender(ComPtr<ID3D12GraphicsCommandList>& commandList);
	void OnLightRender(ComPtr<ID3D12GraphicsCommandList>& commandList);
	void OnPostRender(ComPtr<ID3D12GraphicsCommandList>& commandList);
	void ReleaseUploadBuffer();
	void SetCbvSrvDescIncrementSize(UINT incrementSize);
	void SetShadowBuffer(ID3D12Resource* shadowResource);
	void SetCasCadeBuffer(ID3D12Resource** CascadeResource);
	void SetPostEffectBuffer(ID3D12Resource** PosteffectResource);
	void SetDepthBuffer(ID3D12Resource* depthResource);
	void SetParticlePosition(ID3D12GraphicsCommandList* commandList);

	ComPtr<ID3D12RootSignature> GetGraphicsRS() { return m_rootSignature; }
	ComPtr<ID3D12RootSignature> GetComputeRS() { return m_ComputeRootsignature; }
	vector<vector<GameObject*>>& GetGameObjects();
	vector<LoadedModelInfo*>& GetModelVec();
	vector<SRVROOTARGUMENTINFO>& GetRootArguMentInof() { return m_rootArgumentInfos; }
	QuadTree* GetQuad();
	LIGHTS* GetLight();
	XMFLOAT3 GetSunPos();
	WeaponPoint GetpWeaponPoint();

	int CreateShaderResourceViews(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12Resource** textures, UINT rootParaStartIdx, int textureNum, UINT textureType , D3D12_SHADER_RESOURCE_VIEW_DESC* desc = NULL);
};

