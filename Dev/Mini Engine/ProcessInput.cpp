#include "ProcessInput.h"

void ProcessInput::processInputMessage(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList , HWND& hWnd , Player* player)
{
	DWORD dir = 0;
	float mDeltaX = 0.0f;
	float mDeltaY = 0.0f;
	dir = GetKeyBoradDir();
	GetMouseDeltaPos(mDeltaX, mDeltaY, hWnd);
	MovingPlayer(dir, mDeltaX, mDeltaY, player , commandList);
}

ProcessInput::ProcessInput()
{
}

void ProcessInput::SetOldCusor(POINT oldCusor)
{
	m_oldCusor = oldCusor;
}

void ProcessInput::Init(Timer* timer , Scene* scene , HWND hWnd)
{
	m_timer = timer;
	m_scene = scene;
	IsPicking = false;
	m_hWnd = hWnd;
}

POINT ProcessInput::GetOldCusor()
{
	return m_oldCusor;
}

DWORD ProcessInput::GetKeyBoradDir()
{
	UCHAR pKeysBuffer[256];
	DWORD dwDirection = 0;

	characterNum = FAILNUMBER;

	if (::GetKeyboardState(pKeysBuffer))
	{
		if (pKeysBuffer[87] & 0xF0) dwDirection |= DIR_FORWARD;
		if (pKeysBuffer[83] & 0xF0) dwDirection |= DIR_BACKWARD;
		if (pKeysBuffer[65] & 0xF0) dwDirection |= DIR_LEFT;
		if (pKeysBuffer[68] & 0xF0) dwDirection |= DIR_RIGHT;
		if (pKeysBuffer[0x20] & 0xF0 ) dwDirection |= DIR_JUMP;
		//if (pKeysBuffer[VK_PRIOR] & 0xF0) dwDirection |= DIR_UP;
		//if (pKeysBuffer[VK_NEXT] & 0xF0) dwDirection |= DIR_DOWN;
		if (pKeysBuffer['0'] & 0xF0) characterNum = E_CUBEOBJ;
		if (pKeysBuffer['1'] & 0xF0) characterNum = E_SOLDIEROBJ;

	}
	return dwDirection;
}

void ProcessInput::GetMouseDeltaPos(float& mDeltax, float& mDeltay , HWND& hWnd)
{
	if (GetCapture() == hWnd)
	{
		::SetCursor(NULL);
		
		::GetCursorPos(&ptCursorPos);
		mDeltax = (float)(ptCursorPos.x - m_oldCusor.x) / 3.0f;
		mDeltay = (float)(ptCursorPos.y - m_oldCusor.y) / 3.0f;
		::SetCursorPos(m_oldCusor.x, m_oldCusor.y); // 
	}
}

void ProcessInput::MovingPlayer(DWORD dir, float mDeltaX, float mDeltaY , Player *player ,ComPtr<ID3D12GraphicsCommandList>& commandList)
{
	UCHAR pKeysBuffer[256];
	::GetKeyboardState(pKeysBuffer);
	if (mDeltaX != 0.0f || mDeltaY != 0.0f) {
		if (mDeltaX || mDeltaY) {
			if (pKeysBuffer[VK_RBUTTON] & 0xF0)
				player->Rotate(mDeltaY, 0.0f, -mDeltaX); // rotate y 
			else
				player->Rotate(mDeltaY, mDeltaX, 0.0f); // rotate z 
		}
		
	}
	player->Move(dir, PLAYERSPEED, m_timer->GetTimeElapsed());
	GameObject* gameobj = player->GetModel();
	if (gameobj->GetObjKinds() == E_HIERARCHYOBJ || gameobj->GetObjKinds() == E_ANIMATIONOBJ) {
		player->GetAnimationNum(pKeysBuffer);
	}


	if (ATTACKCOOLTIME <= coolTime && (player->GetStatusBody() == MainCharacterWalkingFire || player->GetStatusLeg() ==  MainCharacterWalkingFire || player->GetStatusBody() == MainCharacterFire) && player->GetCamera()->GetMode() == THIRD_PERSON_CAMERA)
	{
		coolTime = 0.0f;
		PlayerAttack(player, commandList);
	}
	else {
		coolTime += m_timer->GetTimeElapsed();
	}
	//processQuadTree
	auto pObb = player->GetObbBox();
	QuadTree* node =  m_scene->ProcessOBBQuadTree(pObb);
	TerrainMesh* tmesh = (TerrainMesh*)m_scene->GetGameObjects()[E_TERRAINOBJ][0]->GetMesh(0);
	if (node != NULL && player->GetCamera()->GetMode() == THIRD_PERSON_CAMERA) { // outside of map
		//TerrainMesh* tmesh = (TerrainMesh*)m_scene->GetGameObjects()[E_TERRAINOBJ][0]->GetMesh(0);
		//XMFLOAT3 tTrans = m_scene->GetGameObjects()[E_TERRAINOBJ][1]->GetPosition();
		//int startx = node->startidxX;
		//int startz = node->startidxZ;
		//startx = (startx - int(tTrans.x)) / 2;
		//startz = (startz - int(tTrans.z)) / 2;
		//XMFLOAT3 pPos = Vector3::TransformCoord(pObb.Center, Matrix4x4::Inverse(m_scene->GetGameObjects()[E_TERRAINOBJ][1]->GetWorld()));

		////GetPlayerTerrainHeight
		//float height = tmesh->GetHeightByPos(pPos.x, pPos.z, startx, startz, node->width, node->length) + 2.0f;
		//player->SetPosition(player->GetPosition().x, height, player->GetPosition().z);

		XMFLOAT3 playerPos = player->GetPosition();
		XMFLOAT3 pPos = Vector3::TransformCoord(playerPos, Matrix4x4::Inverse(m_scene->GetGameObjects()[E_TERRAINOBJ][1]->GetWorld()));
		//real terrain is 1
		float height = tmesh->GetHeightBylerp(pPos.x, pPos.z);
		if (playerPos.y <= height) {
			player->SetPosition(player->GetPosition().x, height, player->GetPosition().z);
			player->SetJumpCool(false);
		}
		else {
			player->SetJumpCool(true); //플레이어가 공중에 떠있음 
		}

	}
	Camera* camera = player->GetCamera();
	if (camera->GetMode() == THIRD_PERSON_CAMERA) {
		XMFLOAT3 cPos = camera->GetPosition();
		cPos = Vector3::TransformCoord(cPos, Matrix4x4::Inverse(m_scene->GetGameObjects()[E_TERRAINOBJ][1]->GetWorld()));
		float cheight = tmesh->GetHeightBylerp(cPos.x, cPos.z);
		camera->SetTerrainHeight(cheight);
	}
	player->Update(m_timer->GetTimeElapsed());
	
}

void ProcessInput::SwitchingCharacter(Player* player)
{
		
}

void ProcessInput::PlayerAttack(Player* player, ComPtr<ID3D12GraphicsCommandList>& commandList)
{
	XMFLOAT3 pickingPoint{ 0.0f, 0.0f, 0.0f };
	float nearDistance = FLT_MAX;
	int bIdx = -1;
	Camera* pcamera = player->GetCamera();
	POINT cursor;
	::GetCursorPos(&cursor);
	auto tempCursorPos = cursor;
	ScreenToClient(m_hWnd, &tempCursorPos);

	float pointX = ((2.0f * (float)tempCursorPos.x) / (float)FRAME_BUFFER_WIDTH) - 1.0f;
	float pointY = ((2.0f * (float)tempCursorPos.y) / (float)FRAME_BUFFER_HEIGHT) - 1.0f;

	XMFLOAT4X4 projectionMartix = pcamera->GetProj();
	 
	pointX = pointX / projectionMartix._11;
	pointY = -pointY / projectionMartix._22;

	XMFLOAT3 rayOrigin = XMFLOAT3(0.0f, 0.0f, 0.0f);
	XMFLOAT3 rayDirection = XMFLOAT3(pointX, pointY, 1.0f);
	XMFLOAT4X4 inverseViewMatrix = Matrix4x4::Inverse(pcamera->GetView());
	rayOrigin = Vector3::TransformCoord(rayOrigin, inverseViewMatrix);
	rayDirection = Vector3::TransformCoord(rayDirection, inverseViewMatrix);
	rayDirection = Vector3::Normalize(Vector3::Subtract(rayDirection, rayOrigin));
	
	XMFLOAT3 cPos = pcamera->GetPosition();

	QuadTree* m_quadRoot = m_scene->GetQuad();
	m_scene->ProcessPickingQuadTree(rayOrigin, rayDirection , m_qtVector, m_quadRoot);
	sort(m_qtVector.begin(), m_qtVector.end(), [&cPos](QuadTree* a, QuadTree* b) {
		return Vector3::Length(Vector3::Subtract(a->aabbBox.Center, cPos)) < Vector3::Length(Vector3::Subtract(b->aabbBox.Center, cPos));
	}); //sort camera distance


	//바운딩 박스 안에 있는 지형이나 사람하고 이제 피킹을 다시 한다. -> 일단 지형부터 구현하자.
	TerrainMesh* tmesh = (TerrainMesh*)m_scene->GetGameObjects()[E_TERRAINOBJ][0]->GetMesh(0);
	XMFLOAT3 tTrans = m_scene->GetGameObjects()[E_TERRAINOBJ][1]->GetPosition();

	rayOrigin = Vector3::TransformCoord(rayOrigin, Matrix4x4::Inverse(m_scene->GetGameObjects()[E_TERRAINOBJ][1]->GetWorld()));

	XMFLOAT3 fPos = XMFLOAT3(0.0f, 0.0f, 0.0f);
	int qSize = m_qtVector.size();
	if (qSize > 1500) {
		qSize = 1500;
	}

	for (int i = 0; i < qSize; ++i) {
		int startx = m_qtVector[i]->startidxX;
		int startz = m_qtVector[i]->startidxZ;
		startx = (startx - int(tTrans.x)) / 2;
		startz = (startz - int(tTrans.z)) / 2;
		//GetPlayerTerrainHeight
		if (tmesh->GetTerrainRayPos(rayOrigin, rayDirection, startx, startz, m_qtVector[i]->width, m_qtVector[i]->length, fPos)) {
			break;
		}
	}
	fPos = Vector3::TransformCoord(fPos, m_scene->GetGameObjects()[E_TERRAINOBJ][1]->GetWorld());
	fPos.y;
	ParticleShader* pshader = (ParticleShader*)m_scene->GetGameObjects()[E_PARTICLEOBJ][0]->GetMaterial()[0]->GetShader();
	ParticleBlobModify blob;
	blob.ageMax = 0.8f;
	blob.ageMin = 0.5f;

	blob.velocityMax = XMFLOAT3(0.1f, 4.0f, 0.3f);
	blob.velocityMin = XMFLOAT3(0.1f, 2.8f, 0.1f);

	blob.positionMax = XMFLOAT3(0.1f, 4.0f, 0.1f);
	blob.positionMin = XMFLOAT3(-0.1f,0.0f,-0.1f);

	blob.texid = E_DUSTPARTICLE;

	float pBundleAge = 0.4f;

	pshader->CreateDynamicParticle(commandList.Get(), pshader->GetParticleNum(), PARTICLE_TYPE_SCATTER, E_DUSTPARTICLE, 1.0f ,1.0f , pBundleAge, fPos, blob);
	WeaponTrailerShader* wtShader = (WeaponTrailerShader*)m_scene->GetGameObjects()[E_EFFECTOBJ][0]->GetMaterial()[0]->GetShader();
	// Laser
	float age = 0.1f;
	XMFLOAT4 rColor = XMFLOAT4{ 0.75, 0.75f, 0.75f, 0.7f };
	WeaponPoint pwPoint = m_scene->GetpWeaponPoint();
	XMFLOAT3 start0 = Vector3::Add(XMFLOAT3(pwPoint.end._41, pwPoint.end._42, pwPoint.end._43), XMFLOAT3(-0.005f, -0.005f, -0.005f));
	XMFLOAT3 end0 = Vector3::Add(XMFLOAT3(pwPoint.end._41, pwPoint.end._42, pwPoint.end._43), XMFLOAT3(0.005f, 0.005f, 0.005f));

	XMFLOAT3 start1 = Vector3::Add(fPos, XMFLOAT3(-0.005f, -0.005f, -0.005f));
	XMFLOAT3 end1 = Vector3::Add(fPos, XMFLOAT3(0.005f, 0.005f, 0.005f));
	wtShader->AddBuffer(start0, end0, start1, end1, rColor, age, E_GUNFIREPARTICLE1);
}
