#pragma once
#include "stdafx.h"

class Vertex
{
private:

protected:
	XMFLOAT3 pos;
public:
	Vertex()
	{
		pos = XMFLOAT3(0.0f, 0.0f, 0.0f);
	}
	Vertex(XMFLOAT3 pos) : pos(pos) {}
};

class DiffuseVertex : public Vertex {
private:

protected:
	XMFLOAT4 color;
public:
	DiffuseVertex() {}
	~DiffuseVertex() {}

	DiffuseVertex(XMFLOAT3 pos, XMFLOAT4 color) : color(color)
	{
		this->pos = pos;
	}
};

class UVVertex : public Vertex {
	XMFLOAT2 texUV;
public:
	UVVertex() {}
	~UVVertex() {}
	UVVertex(XMFLOAT3 pos, XMFLOAT2 texUV) {
		this->pos = pos;
		this->texUV = texUV;
	}
};

class UVNORMALVertex : public Vertex {
	XMFLOAT3 normal;
	XMFLOAT2 texUV;
public:
	UVNORMALVertex() {}
	~UVNORMALVertex() {}
	UVNORMALVertex(XMFLOAT3 pos, XMFLOAT3 normal ,XMFLOAT2 texUV) {
		this->pos = pos;
		this->normal = normal;
		this->texUV = texUV;
	}
};


class TexVertex : public Vertex {
private:
	XMFLOAT2 texUV;
	int texIdx;
public:
	TexVertex() { }
	~TexVertex() {}

	TexVertex(XMFLOAT3 pos, XMFLOAT2 texUV, int texIdx) {
		this->pos = pos;
		this->texUV = texUV;
		this->texIdx = texIdx;
	}
};


class TexturedVertex : public Vertex {
private:
	XMFLOAT3 normal;
	XMFLOAT2 texUV;
	XMFLOAT3 tangent;
	XMFLOAT3 bitangent;
public:
	TexturedVertex() {}
	~TexturedVertex() {}

	TexturedVertex(XMFLOAT3 pos, XMFLOAT3 normal, XMFLOAT2 texUV) {
		this->pos = pos;
		this->normal = normal;
		this->texUV = texUV;
	}
	TexturedVertex(XMFLOAT3 pos, XMFLOAT2 texUV) {
		this->pos = pos;
		this->texUV = texUV;
	}

	void SetNormal(XMFLOAT3 normal);
	void SetTangent(XMFLOAT3 tangent);
	void SetBiTangent(XMFLOAT3 bitangent);
	XMFLOAT3 GetPos();
	XMFLOAT2 GetUV();

};

class BillBoardVertex : public Vertex {
public:
	XMFLOAT2 size;
	UINT textureIdx;
	BillBoardVertex() {}
	BillBoardVertex(XMFLOAT3 pos, XMFLOAT2 size, UINT textureIdx) {
		this->pos = pos;
		this->size = size;
		this->textureIdx = textureIdx;
	}

};

class ParticleVertex
{
public:
	XMFLOAT2 size;
	XMFLOAT3 position;
	UINT texid;
	UINT copyIdx;
	ParticleVertex()
	{
		size = XMFLOAT2(5.0f, 5.0f);
	}
	ParticleVertex(XMFLOAT3 position, XMFLOAT2 size, UINT textureIdx) {
		this->position = position;
		this->size = size;
		this->texid = textureIdx;
	}
	~ParticleVertex() {}

};

class SkyBoxVertex : public Vertex {
public:
	XMFLOAT2 texUV;
	XMFLOAT3 normal;
	SkyBoxVertex() {}
	SkyBoxVertex(XMFLOAT3 pos, XMFLOAT2 texUV, XMFLOAT3 normal) {
		this->pos = pos;
		this->texUV = texUV;
		this->normal = normal;
	}
	SkyBoxVertex(XMFLOAT3 pos, XMFLOAT2 texUV) {
		this->pos = pos;
		this->texUV = texUV;
	}


};

class EffectVertex : public Vertex {
public:
	XMFLOAT2 texUV;
	UINT texid;

	EffectVertex();

	EffectVertex(XMFLOAT3 pos, XMFLOAT2 texUV, UINT texid) {
		this->pos = pos;
		this->texUV = texUV;
		this->texid = texid;
	}
};