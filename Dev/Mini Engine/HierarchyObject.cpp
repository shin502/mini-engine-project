#include "HierarchyObject.h"
#include "Scene.h"


HierarchyObj::HierarchyObj()
{
	m_world = Matrix4x4::Identity();
	transform = Matrix4x4::Identity();
	ctime = std::chrono::high_resolution_clock::now();
}

HierarchyObj::HierarchyObj(int meshesNum)
{
	XMStoreFloat4x4(&m_world, XMMatrixIdentity());
	transform = Matrix4x4::Identity();

	m_mesh.resize(meshesNum);
	for (int i = 0; i < meshesNum; ++i)
	{
		m_mesh[i] = NULL;
	}
	//maxVelocityXZ = 6.0f;
	//friction = 20.0f;
	ctime = std::chrono::high_resolution_clock::now();
}

HierarchyObj::~HierarchyObj()
{
	GameObject::~GameObject();
	if (skinnedAnimationController) {
		delete skinnedAnimationController;
		skinnedAnimationController = NULL;
	}

}

int HierarchyObj::GetParentFrame(char name[])
{
	HierarchyObj* p = GetParent();
	if (p == NULL) return 0;
	else if (strcmp("swat:Hips", name) == 0 || strcmp("Hips", name) == 0) return 0;
	else if (strcmp(frameName, name) == 0)	return 1;
	else if (strcmp(p->frameName, name) == 0) return 1;
	else p->GetParentFrame(name);

	//error
	return -1;
}



void HierarchyObj::UpdateTransform(XMFLOAT4X4* parent)
{
	m_world = (parent) ? Matrix4x4::Multiply(transform, *parent) : transform;

	if (sibling) sibling->UpdateTransform(parent);
	if (child) child->UpdateTransform(&m_world);
}

void HierarchyObj::UpdateTransform(CB_GAMEOBJECT_INFO** cbMappedGameObjects, UINT cbGameObjectBytes, int idx, XMFLOAT4X4* parent)
{
	static int counter = 0;

	m_world = (parent) ? Matrix4x4::Multiply(transform, *parent) : transform;

	CB_GAMEOBJECT_INFO* mappedCbGameObject = (CB_GAMEOBJECT_INFO*)((UINT8*)cbMappedGameObjects[counter] + (cbGameObjectBytes * idx));
	XMStoreFloat4x4(&mappedCbGameObject->world, XMMatrixTranspose(XMLoadFloat4x4(&m_world)));

	counter++;
	if (sibling) sibling->UpdateTransform(cbMappedGameObjects, cbGameObjectBytes, idx, parent);
	if (child) child->UpdateTransform(cbMappedGameObjects, cbGameObjectBytes, idx, &m_world);

	if (parent == NULL)
		counter = 0;
}

void HierarchyObj::UpdateTransform(int idx, XMFLOAT4X4* parent)
{
	static int counter = 0;

	m_world = (parent) ? Matrix4x4::Multiply(transform, *parent) : transform;
	m_world = (isCollisionSkinnedModel) ? Matrix4x4::Multiply(transform, toParent->m_world) : m_world;

	XMStoreFloat4x4(&m_MappedObject[counter][idx].transform, XMMatrixTranspose(XMLoadFloat4x4(&m_world)));

	counter++;
	if (sibling) sibling->UpdateTransform(idx, parent);
	if (child) child->UpdateTransform(idx, &m_world);

	if (parent == NULL)
		counter = 0;
}

void HierarchyObj::UpdateTransformOnlyStandardMesh(CB_GAMEOBJECT_INFO** cbMappedGameObjects, UINT cbGameObjectBytes, int idx, int framesNum, XMFLOAT4X4* parent)
{
	static int counter = 0;

	if (shaderType == ShaderTypeStandard) {

		m_world = (parent) ? Matrix4x4::Multiply(transform, *parent) : transform;

		CB_GAMEOBJECT_INFO* mappedCbGameObject = (CB_GAMEOBJECT_INFO*)((UINT8*)cbMappedGameObjects[counter] + (cbGameObjectBytes * idx));
		XMStoreFloat4x4(&mappedCbGameObject->world, XMMatrixTranspose(XMLoadFloat4x4(&m_world)));

		counter++;
	}

	if (sibling) sibling->UpdateTransformOnlyStandardMesh(cbMappedGameObjects, cbGameObjectBytes, idx, framesNum, parent);
	if (child) child->UpdateTransformOnlyStandardMesh(cbMappedGameObjects, cbGameObjectBytes, idx, framesNum, &m_world);

	if (counter == framesNum)
		counter = 0;
}

WeaponPoint HierarchyObj::UpdateTransformOnlyStandardMesh(VS_VB_INSTANCE_OBJECT** vbMappedGameObjects, int idx, int framesNum, int weaponNum, XMFLOAT4X4* parent, XMFLOAT4X4* toArrow)
{
	static int counter = 0;
	static WeaponPoint result{};

	if (shaderType == ShaderTypeStandard) { // set up load .bin file

		m_world = (parent) ? Matrix4x4::Multiply(transform, *parent) : transform;

		if (toArrow != NULL && counter == 0)
			*toArrow = m_world;
		XMStoreFloat4x4(&vbMappedGameObjects[counter][idx].transform, XMMatrixTranspose(XMLoadFloat4x4(&m_world)));

		if (weaponNum == counter)
		{
			if (this->child)
			{
				result.end = this->child->m_world;
				if (this->child->sibling)
					if (this->child->sibling->sibling)
						result.start = this->child->sibling->sibling->m_world;
			}
		}

		counter++;
	}

	if (sibling) sibling->UpdateTransformOnlyStandardMesh(vbMappedGameObjects, idx, framesNum, weaponNum, parent, toArrow);
	if (child) child->UpdateTransformOnlyStandardMesh(vbMappedGameObjects, idx, framesNum, weaponNum, &m_world, toArrow);


	if (counter == framesNum)
	{
		counter = 0;
	}

	return result;
}

void HierarchyObj::UpdateBoundingTransForm(VS_VB_INSTANCE_BOUNDING* vbMappedBoudnigs, int idx, XMFLOAT4X4 pWorld, int bidx)
{
	auto fworld = Matrix4x4::Multiply(pWorld, m_world);
	XMStoreFloat4x4(&vbMappedBoudnigs[idx].transform, XMMatrixTranspose(XMLoadFloat4x4(&fworld)));
}

void HierarchyObj::UpdateSkinnedBoundingTransform(VS_VB_INSTANCE_BOUNDING** vbMappedBoundings, std::vector<BoundingOrientedBox>& obbs, std::vector<HierarchyObj*>* objVec, int idx, int framesNum, XMFLOAT4X4* parent)
{
	static int counter = 0;

	if (shaderType == ShaderTypeBounding) {

		m_world = (parent) ? Matrix4x4::Multiply(transform, *parent) : transform;

		XMStoreFloat4x4(&vbMappedBoundings[counter][idx].transform, XMMatrixTranspose(XMLoadFloat4x4(&m_world)));
		if (_isnan(m_world._11) == false) {
			BoundingOrientedBox tempObb{ XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.5f, 0.5f, 0.5f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
			tempObb.Transform(obbs[counter], XMLoadFloat4x4(&m_world));
			if (objVec)
				(*objVec)[counter]->m_world = m_world;
		}
		counter++;
	}

	if (sibling) sibling->UpdateSkinnedBoundingTransform(vbMappedBoundings, obbs, objVec, idx, framesNum, parent);
	if (child) child->UpdateSkinnedBoundingTransform(vbMappedBoundings, obbs, objVec, idx, framesNum, &m_world);

	if (counter == framesNum)
		counter = 0;
}

void HierarchyObj::UpdateTransform(VS_VB_INSTANCE_OBJECT** mappedObject, int idx, XMFLOAT4X4* parent)
{
	static int counter = 0;

	m_world = (parent) ? Matrix4x4::Multiply(transform, *parent) : transform;
	m_world = (isCollisionSkinnedModel) ? Matrix4x4::Multiply(transform, toParent->m_world) : m_world;

	XMStoreFloat4x4(&mappedObject[counter][idx].transform, XMMatrixTranspose(XMLoadFloat4x4(&m_world)));

	counter++;
	if (sibling) sibling->UpdateTransform(mappedObject, idx, parent);
	if (child) child->UpdateTransform(mappedObject, idx, &m_world);

	if (parent == NULL)
		counter = 0;
}

void HierarchyObj::GetHierarchyMaterial(std::vector<MATERIAL>& materialReflection, int& idx)
{
	if (m_material.size() > 0)
	{
		for (int i = 0; i < m_material.size(); ++i)
		{
			MATERIAL temp;
			temp.diffuse = m_material[i]->albedoColor;
			temp.ambient = m_material[i]->ambientColor;
			temp.emissive = m_material[i]->emissiveColor;
			temp.specular = m_material[i]->specularColor;

			materialReflection.emplace_back(temp);
			m_material[i]->reflection = BASIC_REFLECTION + idx++; // 객체에 사용될 재질정보 번호를 기록 
		}
	}
	if (sibling) sibling->GetHierarchyMaterial(materialReflection, idx);
	if (child) child->GetHierarchyMaterial(materialReflection, idx);
}

void HierarchyObj::Animate(float timeElapsed, XMFLOAT4X4* parent)
{
	if (skinnedAnimationController)
	{
		if (!skinnedAnimationController->middleTransform)
			skinnedAnimationController->AdvanceTime(timeElapsed, this);
		else
			skinnedAnimationController->MiddleAnimation(timeElapsed, this);
	}
	if (sibling) sibling->Animate(timeElapsed, parent);
	if (child) child->Animate(timeElapsed, &m_world);
}

void HierarchyObj::InstancingAnimateRender(ID3D12GraphicsCommandList* commandList, UINT instanceNum, D3D12_VERTEX_BUFFER_VIEW* instancingStandardBufferView, D3D12_VERTEX_BUFFER_VIEW instancingObjIdxBufferView, ComPtr<ID3D12Resource>* buffers[4] , Shader* tmpshader, bool isShadow)
{
	static int counter = 0;
	static int skinnnedMeshCount = 0;

	for (int i = 0; i < m_material.size(); ++i)
	{
		if (m_material[i]) {
			if (m_material[i]->GetShader()) {
				if (isShadow == false) {
					if (shaderType == ShaderTypeSkinned)
						tmpshader->SetPipeLine(commandList, 0);
					else
						tmpshader->SetPipeLine(commandList, 1);
				}
				else {
					if (shaderType == ShaderTypeSkinned)
						tmpshader->SetShadowPipe(commandList, 0);
					else
						tmpshader->SetShadowPipe(commandList, 1);
				}
			}
			m_material[i]->UpdateShaderVariables(commandList); //send to material number shader  
		}

		if (m_mesh.size() > 0)
		{
			for (int j = 0; j < m_mesh.size(); j++)
			{
				if (m_mesh[j])
				{
					if (isShadow == false) {
						if (shaderType != ShaderTypeSkinned)
						{
							if(m_weaponNum == counter)
							m_mesh[j]->Render(commandList, i, instanceNum, instancingStandardBufferView[counter]);
						}
						else
						{
							ID3D12Resource* buffer[4] = { buffers[0][skinnnedMeshCount].Get(), buffers[1][skinnnedMeshCount].Get(), buffers[2][skinnnedMeshCount].Get(), buffers[3][skinnnedMeshCount].Get() };
							m_mesh[j]->Render(commandList, i, instanceNum, instancingObjIdxBufferView, buffer);
						}
					}
					else {
						if (shaderType != ShaderTypeSkinned)
						{
							if (m_weaponNum == counter)
							m_mesh[j]->ShadowRender(commandList, i, instanceNum, instancingStandardBufferView[counter]);
						}
						else
						{
							ID3D12Resource* buffer[4] = { buffers[0][skinnnedMeshCount].Get(), buffers[1][skinnnedMeshCount].Get(), buffers[2][skinnnedMeshCount].Get(), buffers[3][skinnnedMeshCount].Get() };
							m_mesh[j]->ShadowRender(commandList, i, instanceNum, instancingObjIdxBufferView, buffer);
						}
					}
				}
			}
		}
	}

	if (shaderType == ShaderTypeSkinned)
		skinnnedMeshCount++;

	if (shaderType == ShaderTypeStandard)
		counter++;

	if (sibling) sibling->InstancingAnimateRender(commandList, instanceNum, instancingStandardBufferView, instancingObjIdxBufferView, buffers, tmpshader, isShadow);
	if (child) child->InstancingAnimateRender(commandList, instanceNum, instancingStandardBufferView, instancingObjIdxBufferView, buffers, tmpshader, isShadow);

	if (parent == NULL)
	{
		counter = 0;
		skinnnedMeshCount = 0;
	}
}

void HierarchyObj::InstancingRender(ID3D12GraphicsCommandList* commandList, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW* instancingGameObjectBufferView, bool isShadow)
{
	static int counter = 0;

	if (m_material.size() > 0)
	{
		for (int j = 0; j < m_material.size(); ++j)
		{
			if (m_material[j])
			{
				m_material[j]->UpdateShaderVariables(commandList);
				// GameObject Material
			}

			if (m_mesh.size() > 0)
			{
				for (int i = 0; i < m_mesh.size(); i++)
				{
					if (isShadow == true) {
						if (m_mesh[i])
							m_mesh[i]->ShadowRender(commandList, j, instancesNum, instancingGameObjectBufferView[counter]);
					}
					else {
						if (m_mesh[i])
							m_mesh[i]->Render(commandList, j, instancesNum, instancingGameObjectBufferView[counter]);
					}
				}
			}
		}
	}
	counter++;

	if (sibling) sibling->InstancingRender(commandList, instancesNum, instancingGameObjectBufferView , isShadow);
	if (child) child->InstancingRender(commandList, instancesNum, instancingGameObjectBufferView , isShadow);

	if (parent == NULL)
	{
		counter = 0;
	}

}

void HierarchyObj::OnPrePareInstancingRender(ID3D12GraphicsCommandList* commandList, Scene* scene)
{
	if (diffuseIdx != FAILNUMBER)
		commandList->SetGraphicsRootDescriptorTable(scene->GetRootArguMentInof()[diffuseIdx].rootParameterIndex, scene->GetRootArguMentInof()[diffuseIdx].srvGpuDescriptorHandle);
	if (normalIdx != FAILNUMBER)
		commandList->SetGraphicsRootDescriptorTable(scene->GetRootArguMentInof()[normalIdx].rootParameterIndex, scene->GetRootArguMentInof()[normalIdx].srvGpuDescriptorHandle);
	if (specularIdx != FAILNUMBER)
		commandList->SetGraphicsRootDescriptorTable(scene->GetRootArguMentInof()[specularIdx].rootParameterIndex, scene->GetRootArguMentInof()[specularIdx].srvGpuDescriptorHandle);
	if (metallicIdx != FAILNUMBER)
		commandList->SetGraphicsRootDescriptorTable(scene->GetRootArguMentInof()[metallicIdx].rootParameterIndex, scene->GetRootArguMentInof()[metallicIdx].srvGpuDescriptorHandle);
}


void HierarchyObj::SetTransform(XMFLOAT4X4 transform)
{
	this->transform = transform;
}

void HierarchyObj::SetChild(HierarchyObj* child)
{
	//자식이 있다면 자식의 형제로 설정해주고 자식 포인터가 가르키는 것을 바꿔준다.
	if (this->child)
	{
		if (child) child->sibling = this->child->sibling;
		this->child->sibling = child;
	}
	else
	{
		this->child = child;
	}
	//부모로 설정해줌 
	if (child)
	{
		child->parent = this;
	}
}

void HierarchyObj::SetModel(LoadedModelInfo* model)
{
	m_model = model;
}

void HierarchyObj::SetInstancingTexture(ID3D12Device* device, ID3D12GraphicsCommandList* commandList , Scene* scene)
{
	HierarchyObj* rootobj = this->child;
	textureName = rootobj->textureName;

	bool isEqual = false;
	int equalidx = -1;

	if (rootobj->textureName.diffuse.size() > 0)
	{
		Texture* diffuseTexture = NULL;
		diffuseTexture = new Texture((int)textureName.diffuse.size(), ResourceTexture2D, 0);
		for (int i = 0; i < textureName.diffuse.size(); ++i)
		{
			diffuseTexture->LoadTextureFromFile(device, commandList, textureName.diffuse[i].second, textureName.diffuse[i].first);
		}

		diffuseIdx = scene->CreateShaderResourceViews(device, commandList, diffuseTexture->m_textures, GraphicsRootDiffuseTextures, diffuseTexture->m_textureNum, ResourceTexture2D);
		textures.emplace_back(diffuseTexture);
	}

	if (rootobj->textureName.normal.size() > 0)
	{
		Texture* normalTexture = NULL;
		normalTexture = new Texture((int)textureName.normal.size(), ResourceTexture2D, 0);
		for (int i = 0; i < textureName.normal.size(); ++i)
		{
			normalTexture->LoadTextureFromFile(device, commandList, textureName.normal[i].second, textureName.normal[i].first);
		}

		normalIdx = scene->CreateShaderResourceViews(device, commandList, normalTexture->m_textures, GraphicsRootNormalTextures, normalTexture->m_textureNum, ResourceTexture2D);
		textures.emplace_back(normalTexture);
	}

	if (rootobj->textureName.specular.size() > 0)
	{
		Texture* specularTexture = NULL;
		specularTexture = new Texture((int)textureName.specular.size(), ResourceTexture2D, 0);
		for (int i = 0; i < textureName.specular.size(); ++i)
		{
			specularTexture->LoadTextureFromFile(device, commandList, textureName.specular[i].second, textureName.specular[i].first);
		}

		specularIdx = scene->CreateShaderResourceViews(device, commandList, specularTexture->m_textures, GraphicsRootSpecularTextures, specularTexture->m_textureNum, ResourceTexture2D);
		textures.emplace_back(specularTexture);
	}

	if (rootobj->textureName.metallic.size() > 0)
	{
		Texture* metallicTexture = NULL;
		metallicTexture = new Texture((int)textureName.metallic.size(), ResourceTexture2D, 0);
		for (int i = 0; i < textureName.metallic.size(); ++i)
		{
			metallicTexture->LoadTextureFromFile(device, commandList, textureName.metallic[i].second, textureName.metallic[i].first);
		}

		metallicIdx = scene->CreateShaderResourceViews(device, commandList, metallicTexture->m_textures, GraphicsRootMetallicTextures, metallicTexture->m_textureNum, ResourceTexture2D);
		textures.emplace_back(metallicTexture);
	}
}




void HierarchyObj::Rotate(float pitch, float yaw, float roll)
{
	XMMATRIX rotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(pitch), XMConvertToRadians(yaw), XMConvertToRadians(roll));
	transform = Matrix4x4::Multiply(rotate, transform);

	UpdateTransform(NULL);
}

void HierarchyObj::ReleaseObj()
{
	if (sibling) sibling->ReleaseObj();
	if (child) child->ReleaseObj();

	delete this;

}

int HierarchyObj::GetObjKinds()
{
	return E_HIERARCHYOBJ;
}

HierarchyObj* HierarchyObj::FindFrame(char* frameName)
{
	HierarchyObj* frameObject = NULL;
	if (!strncmp(this->frameName, frameName, strlen(frameName))) return(this);

	if (sibling) if (frameObject = sibling->FindFrame(frameName)) return(frameObject);
	if (child) if (frameObject = child->FindFrame(frameName)) return(frameObject);

	return(NULL);
}

_TCHAR* HierarchyObj::FindReplicatedTexture(_TCHAR* textureName)
{
	for (int i = 0; i < m_material.size(); i++)
	{
		if (m_material[i])
		{
			for (int j = 0; j < m_material[i]->texturesNum; j++)
			{
				if (m_material[i]->textures[j])
				{
					if (!_tcsncmp(m_material[i]->textureNames[j], textureName, _tcslen(textureName))) return(m_material[i]->textureNames[j]);
				}
			}
		}
	}
	_TCHAR* name = NULL;
	if (sibling) if (name = sibling->FindReplicatedTexture(textureName)) return(name);
	if (child) if (name = child->FindReplicatedTexture(textureName)) return(name);

	return(NULL);
}

void HierarchyObj::FindAndSetSkinnedMesh(SkinnedMesh** SkinnedMeshes, int* SkinnedMeshnum)
{
	if (m_mesh[0] && (m_mesh[0]->GetType() & VERTEXT_BONE_INDEX_WEIGHT)) SkinnedMeshes[(*SkinnedMeshnum)++] = (SkinnedMesh*)(m_mesh[0]);

	if (sibling) sibling->FindAndSetSkinnedMesh(SkinnedMeshes, SkinnedMeshnum);
	if (child) child->FindAndSetSkinnedMesh(SkinnedMeshes, SkinnedMeshnum);
}

void HierarchyObj::ReleaseUploadBuffer()
{
	if (m_temporaryMesh.size() > 0) {
		for (int i = 0; i < m_temporaryMesh.size(); i++)
		{
			if (m_temporaryMesh[i])
				m_temporaryMesh[i]->ReleaseUploadBuffer();
		}
	}

	if (m_mesh.size() > 0)
	{
		for (int i = 0; i < m_mesh.size(); i++)
		{
			if (m_mesh[i])
				m_mesh[i]->ReleaseUploadBuffer();
		}
	}
	if (m_material.size() > 0)
	{
		for (int i = 0; i < m_material.size(); ++i)
		{
			if (m_material[i])
				m_material[i]->ReleaseUploadBuffer();
		}
	}
	if (sibling) sibling->ReleaseUploadBuffer();
	if (child) child->ReleaseUploadBuffer();

}

void HierarchyObj::LoadMaterialsFromFile(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, GameObject* parent, FILE* inFile, char* folderName, TEXTURENAME& textureMapNames, int textureIdx[5])
{
	char pstrToken[64] = { '\0' };

	int nMaterial = 0;
	BYTE nStrLength = 0;

	int materialsNum;

	UINT nReads = (UINT)::fread(&materialsNum, sizeof(int), 1, inFile);


	m_material.resize(materialsNum);
	for (int i = 0; i < materialsNum; i++)
		m_material[i] = NULL;

	Material* pMaterial = NULL;

	for (; ; )
	{
		nReads = (UINT)::fread(&nStrLength, sizeof(BYTE), 1, inFile);
		nReads = (UINT)::fread(pstrToken, sizeof(char), nStrLength, inFile);
		pstrToken[nStrLength] = '\0';

		if (!strcmp(pstrToken, "<Material>:"))
		{
			nReads = (UINT)::fread(&nMaterial, sizeof(int), 1, inFile);

			pMaterial = new Material(7); //0:Albedo, 1:Specular, 2:Metallic, 3:Normal, 4:Emission, 5:DetailAlbedo, 6:DetailNormal

			UINT nMeshType = GetMeshType();
			if (nMeshType & (VertexPosition | VertexNormal | VertexTangent | VertexTexCoord0))
			{
				if (nMeshType & VERTEXT_BONE_INDEX_WEIGHT)
				{
					pMaterial->SetShader(new StandardSkinnedAnimationInstanceShader); //skinned shader set 
				}
				else
				{
					pMaterial->SetShader(new StandardInstanceShader); // stanrdShader set 
				}
			}

			SetMaterial(nMaterial, pMaterial);
		}
		else if (!strcmp(pstrToken, "<AlbedoColor>:"))
		{
			nReads = (UINT)::fread(&(pMaterial->albedoColor), sizeof(float), 4, inFile);
		}
		else if (!strcmp(pstrToken, "<EmissiveColor>:"))
		{
			nReads = (UINT)::fread(&(pMaterial->emissiveColor), sizeof(float), 4, inFile);
		}
		else if (!strcmp(pstrToken, "<SpecularColor>:"))
		{
			nReads = (UINT)::fread(&(pMaterial->specularColor), sizeof(float), 4, inFile);
		}
		else if (!strcmp(pstrToken, "<Glossiness>:"))
		{
			nReads = (UINT)::fread(&(pMaterial->glossiness), sizeof(float), 1, inFile);
		}
		else if (!strcmp(pstrToken, "<Smoothness>:"))
		{
			nReads = (UINT)::fread(&(pMaterial->smoothness), sizeof(float), 1, inFile);
		}
		else if (!strcmp(pstrToken, "<Metallic>:"))
		{
			nReads = (UINT)::fread(&(pMaterial->metallic), sizeof(float), 1, inFile);
		}
		else if (!strcmp(pstrToken, "<SpecularHighlight>:"))
		{
			nReads = (UINT)::fread(&(pMaterial->specularHighlight), sizeof(float), 1, inFile);
		}
		else if (!strcmp(pstrToken, "<GlossyReflection>:"))
		{
			nReads = (UINT)::fread(&(pMaterial->glossyReflection), sizeof(float), 1, inFile);
		}
		else if (!strcmp(pstrToken, "<AlbedoMap>:"))
		{
			pMaterial->LoadTextureFromFile(device, commandList, MATERIAL_DIFFUSE_MAP, 6, pMaterial->textureNames[0], &(pMaterial->textures[0]), inFile, folderName, textureMapNames.diffuse, textureIdx[0]);
		}
		else if (!strcmp(pstrToken, "<NormalMap>:"))
		{
			m_material[nMaterial]->LoadTextureFromFile(device, commandList, MATERIAL_NORMAL_MAP, 7, pMaterial->textureNames[1], &(pMaterial->textures[1]), inFile, folderName, textureMapNames.normal, textureIdx[1]);
		}
		else if (!strcmp(pstrToken, "<SpecularMap>:"))
		{
			m_material[nMaterial]->LoadTextureFromFile(device, commandList, MATERIAL_SPECULAR_MAP, 8, pMaterial->textureNames[2], &(pMaterial->textures[2]), inFile, folderName, textureMapNames.specular, textureIdx[2]);
		}
		else if (!strcmp(pstrToken, "<MetallicMap>:"))
		{
			m_material[nMaterial]->LoadTextureFromFile(device, commandList, MATERIAL_METALLIC_MAP, 9, pMaterial->textureNames[3], &(pMaterial->textures[3]), inFile, folderName, textureMapNames.metallic, textureIdx[3]);
		}
		else if (!strcmp(pstrToken, "<EmissionMap>:"))
		{
			char pstrTextureName[64] = { '\0' };

			BYTE nStrLength = 64;
			UINT nReads = (UINT)::fread(&nStrLength, sizeof(BYTE), 1, inFile);
			nReads = (UINT)::fread(pstrTextureName, sizeof(char), nStrLength, inFile);
		}
		else if (!strcmp(pstrToken, "<DetailAlbedoMap>:"))
		{
			char pstrTextureName[64] = { '\0' };

			BYTE nStrLength = 64;
			UINT nReads = (UINT)::fread(&nStrLength, sizeof(BYTE), 1, inFile);
			nReads = (UINT)::fread(pstrTextureName, sizeof(char), nStrLength, inFile);
		}
		else if (!strcmp(pstrToken, "<DetailNormalMap>:"))
		{
			char pstrTextureName[64] = { '\0' };

			BYTE nStrLength = 64;
			UINT nReads = (UINT)::fread(&nStrLength, sizeof(BYTE), 1, inFile);
			nReads = (UINT)::fread(pstrTextureName, sizeof(char), nStrLength, inFile);
		}
		else if (!strcmp(pstrToken, "</Materials>"))
		{
			break;
		}
	}
}

LoadedModelInfo* HierarchyObj::LoadGeometryAndAnimationFromFile(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, char* fileName, char* folderName)
{
	TEXTURENAME textureMapName;
	int textureMapIdx[5]{};

	char token[64] = { '\0' };
	LoadedModelInfo* modelInfo = new LoadedModelInfo();

	FILE* inFile = NULL;
	::fopen_s(&inFile, fileName, "rb");
	if (inFile == NULL)
	{
		std::cout << "빈파일!" << std::endl;
		return modelInfo;
	}
	::rewind(inFile);
	int frameCounter = 0, materialCounter = 0;

	for (; ; )
	{
		if (::ReadStringFromFile(inFile, token))
		{
			if (!strcmp(token, "<Hierarchy>:"))
			{
				modelInfo->modelRootObject = HierarchyObj::LoadFrameHierarchyFromFile(device, commandList, NULL, inFile, folderName, textureMapName, textureMapIdx, frameCounter, materialCounter, &modelInfo->skinnedMeshNum, &modelInfo->standardMeshNum, &modelInfo->boundingMeshNum);
				::ReadStringFromFile(inFile, token); //"</Hierarchy>"
				modelInfo->modelRootObject->textureName = textureMapName;
			}
			else if (!strcmp(token, "<Animation>:"))
			{
				HierarchyObj::LoadAnimationFromFile(inFile, modelInfo);
				modelInfo->PrepareSkinning();
			}
			else if (!strcmp(token, "</Animation>:"))
			{
				break;
			}
		}
		else
		{
			break;
		}
	}
	::fclose(inFile);

#ifdef _WITH_DEBUG_FRAME_HIERARCHY
	TCHAR pstrDebug[256] = { 0 };
	_stprintf_s(pstrDebug, 256, _T("Frame Hierarchy\n"));
	OutputDebugString(pstrDebug);

	CGameObject::PrintFrameInfo(pGameObject, NULL);
#endif

	return(modelInfo);
}

HierarchyObj* HierarchyObj::LoadFrameHierarchyFromFile(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, GameObject* parent, FILE* inFile, char* folderName, TEXTURENAME& textureMapNames, int textureIdx[5], int& frameCounter, int& materialCounter, int* skinnedMeshsNum, int* standardMeshNum, int* boundingMeshNum)
{
	char token[64] = { '\0' };
	
	BYTE strLength = 0;
	UINT readsNum = 0;

	int frameNum = 0, texturesNum = 0;
	static int maximum = 0;
	HierarchyObj* gameObject = NULL;

	for (; ; )
	{
		::ReadStringFromFile(inFile, token);

		if (!strcmp(token, "<Frame>:"))
		{
			gameObject = new HierarchyObj(1);

			readsNum = (UINT)::fread(&frameNum, sizeof(int), 1, inFile);
			readsNum = (UINT)::fread(&texturesNum, sizeof(int), 1, inFile);

			readsNum = (UINT)::fread(&strLength, sizeof(BYTE), 1, inFile);
			readsNum = (UINT)::fread(gameObject->frameName, sizeof(char), strLength, inFile);
			gameObject->frameName[strLength] = '\0';
			frameCounter++;
		}
		else if (!strcmp(token, "<Transform>:"))
		{
			XMFLOAT3 xmf3Position, xmf3Rotation, xmf3Scale;
			XMFLOAT4 xmf4Rotation;
			readsNum = (UINT)::fread(&xmf3Position, sizeof(float), 3, inFile);
			readsNum = (UINT)::fread(&xmf3Rotation, sizeof(float), 3, inFile); //Euler Angle
			readsNum = (UINT)::fread(&xmf3Scale, sizeof(float), 3, inFile);
			readsNum = (UINT)::fread(&xmf4Rotation, sizeof(float), 4, inFile); //Quaternion
		}
		else if (!strcmp(token, "<TransformMatrix>:"))
		{
			readsNum = (UINT)::fread(&gameObject->transform, sizeof(float), 16, inFile);
		}
		else if (!strcmp(token, "<Mesh>:"))
		{
			StandardMesh* mesh = new StandardMesh(device, commandList);
			//boundMesh
			DiffuseCubeMesh* bMesh = NULL;
			mesh->LoadMeshFromFile(device, commandList, inFile);

			if (strncmp(mesh->GetmeshName(), "Cube", 256) == 0)
			{
				//bMesh = new DiffuseCubeMesh(device, commandList, 0.5f, 0.5f, 0.5f);
				/*if (boundingMeshNum) {
					bMesh->meshnum = *boundingMeshNum;
				}*/
				gameObject->SetBoundingMesh(bMesh);
				gameObject->shaderType = ShaderTypeBounding;
				gameObject->SetTemporaryMesh(mesh); // delete
				//if (boundingMeshNum) (*boundingMeshNum)++;
			}
			else
			{
				/*if (standardMeshNum) {
					mesh->meshnum = *standardMeshNum;
				}*/
				gameObject->SetMesh(0, mesh); // weapon
				gameObject->shaderType = ShaderTypeStandard;
				if (standardMeshNum) (*standardMeshNum)++;
			}
		}
		else if (!strcmp(token, "<SkinningInfo>:"))
		{
			if (skinnedMeshsNum) (*skinnedMeshsNum)++;

			SkinnedMesh* pSkinnedMesh = new SkinnedMesh(device, commandList);
			pSkinnedMesh->LoadSkinInfoFromFile(device, commandList, inFile);
			pSkinnedMesh->CreateShaderVariables(device, commandList);

			::ReadStringFromFile(inFile, token); //<Mesh>:
			if (!strcmp(token, "<Mesh>:")) pSkinnedMesh->LoadMeshFromFile(device, commandList, inFile);

			gameObject->SetMesh(0, pSkinnedMesh);
			gameObject->shaderType = ShaderTypeSkinned;
		}
		else if (!strcmp(token, "<Materials>:"))
		{
			gameObject->LoadMaterialsFromFile(device, commandList, parent, inFile, folderName, textureMapNames, textureIdx);
			materialCounter = max(materialCounter, gameObject->m_material.size());
		}
		else if (!strcmp(token, "<Children>:"))
		{
			int childsNum = 0;
			readsNum = (UINT)::fread(&childsNum, sizeof(int), 1, inFile);
			if (childsNum > 0)
			{
				for (int i = 0; i < childsNum; i++)
				{
					HierarchyObj* child = HierarchyObj::LoadFrameHierarchyFromFile(device, commandList, gameObject, inFile, folderName, textureMapNames, textureIdx, frameCounter, materialCounter, skinnedMeshsNum, standardMeshNum, boundingMeshNum);
					if (child) gameObject->SetChild(child);
#ifdef _WITH_DEBUG_FRAME_HIERARCHY
					TCHAR pstrDebug[256] = { 0 };
					_stprintf_s(pstrDebug, 256, _T("(Frame: %p) (Parent: %p)\n"), pChild, gameObject);
					OutputDebugString(pstrDebug);
#endif
				}
			}
		}
		else if (!strcmp(token, "</Frame>"))
		{
			gameObject->framesNum = frameCounter;
			gameObject->maximumMaterialsNum = materialCounter;
			break;
		}
	}
	return(gameObject);
}

void HierarchyObj::LoadAnimationFromFile(FILE* pInFile, LoadedModelInfo* pLoadedModel)
{

	char pstrToken[64] = { '\0' };
	UINT nReads = 0;

	int nAnimationSets = 0;

	for (; ; )
	{
		::ReadStringFromFile(pInFile, pstrToken);
		if (!strcmp(pstrToken, "<AnimationSets>:"))
		{
			nAnimationSets = ::ReadIntegerFromFile(pInFile);
			pLoadedModel->animationSets = new AnimationSets(nAnimationSets);
		}
		else if (!strcmp(pstrToken, "<FrameNames>:"))
		{
			pLoadedModel->animationSets->animatedBoneFrames = ::ReadIntegerFromFile(pInFile);
			pLoadedModel->animationSets->animatedBoneFrameCaches = new HierarchyObj * [pLoadedModel->animationSets->animatedBoneFrames];

			for (int j = 0; j < pLoadedModel->animationSets->animatedBoneFrames; j++)
			{
				::ReadStringFromFile(pInFile, pstrToken);
				pLoadedModel->animationSets->animatedBoneFrameCaches[j] = pLoadedModel->modelRootObject->FindFrame(pstrToken);

#ifdef _WITH_DEBUG_SKINNING_BONE
				TCHAR pstrDebug[256] = { 0 };
				TCHAR pwstrAnimationBoneName[64] = { 0 };
				TCHAR pwstrBoneCacheName[64] = { 0 };
				size_t nConverted = 0;
				mbstowcs_s(&nConverted, pwstrAnimationBoneName, 64, pstrToken, _TRUNCATE);
				mbstowcs_s(&nConverted, pwstrBoneCacheName, 64, pLoadedModel->m_ppAnimatedBoneFrameCaches[j]->m_pstrFrameName, _TRUNCATE);
				_stprintf_s(pstrDebug, 256, _T("AnimationBoneFrame:: Cache(%s) AnimationBone(%s)\n"), pwstrBoneCacheName, pwstrAnimationBoneName);
				OutputDebugString(pstrDebug);
#endif
			}
		}
		else if (!strcmp(pstrToken, "<AnimationSet>:"))
		{
			int nAnimationSet = ::ReadIntegerFromFile(pInFile);

			::ReadStringFromFile(pInFile, pstrToken); //Animation Set Name

			float fLength = ::ReadFloatFromFile(pInFile);
			int nFramesPerSecond = ::ReadIntegerFromFile(pInFile);
			int nKeyFrames = ::ReadIntegerFromFile(pInFile);

			pLoadedModel->animationSets->animationSets[nAnimationSet] = new AnimationSet(fLength, nFramesPerSecond, nKeyFrames, pLoadedModel->animationSets->animatedBoneFrames, pstrToken);

			for (int i = 0; i < nKeyFrames; i++)
			{
				::ReadStringFromFile(pInFile, pstrToken);
				if (!strcmp(pstrToken, "<Transforms>:"))
				{
					int nKey = ::ReadIntegerFromFile(pInFile); //i
					float fKeyTime = ::ReadFloatFromFile(pInFile);

					AnimationSet* animationSet = pLoadedModel->animationSets->animationSets[nAnimationSet];
					animationSet->keyFrameTimes[i] = fKeyTime;
					nReads = (UINT)::fread(animationSet->keyFrameTransforms[i], sizeof(XMFLOAT4X4), pLoadedModel->animationSets->animatedBoneFrames, pInFile);
				}
			}
		}
		else if (!strcmp(pstrToken, "</AnimationSets>"))
		{
			break;
		}
	}
}

HierarchyObj* HierarchyObj::LoadGeometryFromFile(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, char* fileName, char* folderName)
{
	TEXTURENAME textureMapName;
	int textureMapIdx[5]{};
	FILE* inFile = NULL;
	::fopen_s(&inFile, fileName, "rb");
	::rewind(inFile);
	int frameCounter = 0, materialCounter = 0;
	HierarchyObj* gameObject = HierarchyObj::LoadFrameHierarchyFromFile(device, commandList, NULL, inFile, folderName, textureMapName, textureMapIdx, frameCounter, materialCounter, NULL, NULL, NULL);
	gameObject->textureName = textureMapName;

	//::fclose(inFile);

#ifdef _WITH_DEBUG_FRAME_HIERARCHY
	TCHAR pstrDebug[256] = { 0 };
	_stprintf_s(pstrDebug, 256, _T("Frame Hierarchy\n"));
	OutputDebugString(pstrDebug);

	CGameObject::PrintFrameInfo(pGameObject, NULL);
#endif

	return(gameObject);
}
