#include "InstancingShader.h"
InstancingShader::~InstancingShader()
{
	delete[] m_MappedObject;
	delete[] m_ObjectinstancBufferView;
	delete[] m_ObjectResource;
}
// 0 : Default 1 : Blending 2: Billboard(GS)
void InstancingShader::CreatebPipeLineState(ComPtr<ID3D12Device>& device, ComPtr<ID3D12RootSignature>& rootsignature, ComPtr<ID3D12RootSignature>& computeRootSignature)
{
	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
	psoDesc.InputLayout = CreateInputLayout();
	psoDesc.pRootSignature = rootsignature.Get();
	psoDesc.VS = CreateVertexShader();
	psoDesc.PS = CreatePixelShader();
	psoDesc.RasterizerState = CreateRasterizerState();
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
	psoDesc.SampleMask = UINT_MAX;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	psoDesc.NumRenderTargets = 4;
	psoDesc.RTVFormats[0] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	psoDesc.RTVFormats[1] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	psoDesc.RTVFormats[2] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	psoDesc.RTVFormats[3] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	psoDesc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
	psoDesc.SampleDesc.Count = 1;
	ThrowIfFailed(device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_pipes[0])));

	psoDesc.RasterizerState.CullMode = D3D12_CULL_MODE_NONE;
	ThrowIfFailed(device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_pipes[1])));

	//release inputlayout
	if (psoDesc.InputLayout.pInputElementDescs) delete[] psoDesc.InputLayout.pInputElementDescs;
	UINT inputElementDescsNum = 5;
	D3D12_INPUT_ELEMENT_DESC* inputElementDescs = new D3D12_INPUT_ELEMENT_DESC[inputElementDescsNum];
	inputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[1] = { "WORLDMATRIX", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	inputElementDescs[2] = { "WORLDMATRIX", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	inputElementDescs[3] = { "WORLDMATRIX", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	inputElementDescs[4] = { "WORLDMATRIX", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };


	psoDesc.InputLayout.NumElements = inputElementDescsNum;
	psoDesc.InputLayout.pInputElementDescs = inputElementDescs; 
	psoDesc.VS = Shader::CompileShaderFromFile(L"Shadow.hlsl", "VS_SHADOW_INSTANCING", "vs_5_1", &m_vsBlob);
	psoDesc.PS = Shader::CompileShaderFromFile(L"Shadow.hlsl", "PS_SHADOW", "ps_5_1", &m_psBlob);

	ThrowIfFailed(device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_ShadowPipeLine)));

	if (psoDesc.InputLayout.pInputElementDescs) delete[] psoDesc.InputLayout.pInputElementDescs; 

	if (m_vsBlob) m_vsBlob->Release();
	if (m_psBlob) m_psBlob->Release();

}

void InstancingShader::CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
	UINT tempObjectByte = 0;
	tempObjectByte = sizeof(VS_VB_INSTANCE_OBJECT);

	m_ObjectResource = new ComPtr<ID3D12Resource>[m_modelInfo.framseNum];
	m_MappedObject = new VS_VB_INSTANCE_OBJECT * [m_modelInfo.framseNum];
	m_ObjectinstancBufferView = new D3D12_VERTEX_BUFFER_VIEW[m_modelInfo.framseNum];

	for (int i = 0; i < m_modelInfo.framseNum; ++i) { // frameNum * objNum => instancing 
		m_ObjectResource[i] = ::CreateBufferResource(device, commandList, NULL, sizeof(VS_VB_INSTANCE_OBJECT) * ((UINT)BUFFERSIZE), D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
		//create buffer upload heap
		m_ObjectResource[i]->Map(0, NULL, (void**)&m_MappedObject[i]);
		//store vertexbuffer pointer
		m_ObjectinstancBufferView[i].BufferLocation = m_ObjectResource[i]->GetGPUVirtualAddress();
		m_ObjectinstancBufferView[i].StrideInBytes = tempObjectByte;
		m_ObjectinstancBufferView[i].SizeInBytes = tempObjectByte * ((UINT)BUFFERSIZE);
		//create vertexBufferView
	}
}

void InstancingShader::UpdateShaderVariables(ID3D12GraphicsCommandList* commandList)
{

}

void InstancingShader::ReleaseShaderVariables()
{
}

void InstancingShader::SetPipeLine(ID3D12GraphicsCommandList* commandList, int idx)
{
	commandList->SetPipelineState(m_pipes[idx].Get());
}

int InstancingShader::GetShaderNumber()
{
	return E_INSTANCESHADER;
}

void InstancingShader::SetModelInfo(MODELINFO modelinfo)
{
	m_modelInfo = modelinfo;
}
