#pragma once
#include "StandardInstanceShader.h"
#define INSTANCINGPIPECOUNT 4

class InstancingShader : public StandardInstanceShader
{
private:
	ComPtr<ID3D12PipelineState> m_pipes[INSTANCINGPIPECOUNT];
protected:

	ComPtr<ID3D12Resource>* m_ObjectResource = NULL;
	VS_VB_INSTANCE_OBJECT** m_MappedObject = NULL;

	UINT m_objectNum = 0;
	std::vector<Texture*> textures;
	bool showBounding = false;


public:

	InstancingShader() {}
	virtual ~InstancingShader();

	virtual void CreatebPipeLineState(ComPtr<ID3D12Device>& device, ComPtr<ID3D12RootSignature>& rootsignature, ComPtr<ID3D12RootSignature>& computeRootSignature);
	virtual void CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	virtual void UpdateShaderVariables(ID3D12GraphicsCommandList* commandList);
	virtual void ReleaseShaderVariables();
	virtual void SetPipeLine(ID3D12GraphicsCommandList* commandList, int idx);

	virtual int GetShaderNumber();
	VS_VB_INSTANCE_OBJECT** GetMappedObject() { return m_MappedObject; }

	void SetModelInfo(MODELINFO modelinfo);

	D3D12_VERTEX_BUFFER_VIEW* m_ObjectinstancBufferView = NULL;


};

