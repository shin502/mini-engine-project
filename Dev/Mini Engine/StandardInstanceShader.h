#pragma once
#include"StandardShader.h"
class StandardInstanceShader : public StandardShader
{
public:
	StandardInstanceShader() {};
	virtual ~StandardInstanceShader() {};

	virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();
	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
	virtual D3D12_SHADER_BYTECODE CreateShadowVertexShader();
	virtual D3D12_SHADER_BYTECODE CreateShadowPixelShader();
	virtual void CreatebPipeLineState(ComPtr<ID3D12Device>& device, ComPtr<ID3D12RootSignature>& rootsignature, ComPtr<ID3D12RootSignature>& computeRootSignature);

	virtual int GetShaderNumber();



};