#include "StandardShader.h"

D3D12_INPUT_LAYOUT_DESC StandardShader::CreateInputLayout()
{
	UINT inputElementDescsNum = 5;
	D3D12_INPUT_ELEMENT_DESC* inputElementDescs = new D3D12_INPUT_ELEMENT_DESC[inputElementDescsNum];

	inputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[1] = { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 1, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[2] = { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 2, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[3] = { "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 3, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[4] = { "BITANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 4, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };

	D3D12_INPUT_LAYOUT_DESC inputLayoutDesc;
	inputLayoutDesc.pInputElementDescs = inputElementDescs;
	inputLayoutDesc.NumElements = inputElementDescsNum;

	return(inputLayoutDesc);
}

D3D12_SHADER_BYTECODE StandardShader::CreateVertexShader()
{
	return(Shader::CompileShaderFromFile(L"Standard.hlsl", "VSStandard", "vs_5_1", &m_vsBlob));
}

D3D12_SHADER_BYTECODE StandardShader::CreatePixelShader()
{
	return(Shader::CompileShaderFromFile(L"Standard.hlsl", "PSStandard", "ps_5_1", &m_psBlob));
}

void StandardShader::CreatebPipeLineState(ComPtr<ID3D12Device>& device, ComPtr<ID3D12RootSignature>& rootsignature, ComPtr<ID3D12RootSignature>& computeRootSignature)
{
	
	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
	psoDesc.InputLayout = CreateInputLayout();
	psoDesc.pRootSignature = rootsignature.Get();
	psoDesc.VS = CreateVertexShader();
	psoDesc.PS = CreatePixelShader();
	psoDesc.RasterizerState = CreateRasterizerState();
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
	psoDesc.SampleMask = UINT_MAX;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	psoDesc.NumRenderTargets = 1;
	psoDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	psoDesc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
	psoDesc.SampleDesc.Count = 1;
	ThrowIfFailed(device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_bpipelineState)));

	if (m_vsBlob) m_vsBlob->Release();
	if (m_psBlob) m_psBlob->Release();
}

int StandardShader::GetShaderNumber()
{
	return E_STANDARDSHADER;
}
