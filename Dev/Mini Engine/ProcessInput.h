#pragma once
#include "Player.h"
#include "Scene.h"
#include "Timer.h"
#define ATTACKCOOLTIME 0.2f
class ProcessInput
{
private:
	POINT m_oldCusor;
	POINT ptCursorPos;
	Timer* m_timer;
	Scene* m_scene;
	vector<QuadTree*> m_qtVector;
	HWND m_hWnd;
	int characterNum;
	float coolTime = 0.0f;
	bool IsPicking;
public:
	ProcessInput(Timer* timer) {}
	ProcessInput();
	void processInputMessage(ComPtr<ID3D12Device>& device , ComPtr<ID3D12GraphicsCommandList>& commandList , HWND& hWnd , Player *player);
	void SetOldCusor(POINT oldCusor);
	void Init(Timer* timer , Scene* scene, HWND hWnd);

	POINT GetOldCusor();
	DWORD GetKeyBoradDir();
	void GetMouseDeltaPos(float& mDeltax , float& mDeltay, HWND& hWnd);
	void MovingPlayer(DWORD dir, float mDeltaX, float mDeltaY , Player *player , ComPtr<ID3D12GraphicsCommandList>& commandList);
	void SwitchingCharacter(Player* player);
	void PlayerAttack(Player* player, ComPtr<ID3D12GraphicsCommandList>& commandList);
};

