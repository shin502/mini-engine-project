#pragma once
#include "stdafx.h"

#define ASPECT_RATIO (float(FRAME_BUFFER_WIDTH) / float(FRAME_BUFFER_HEIGHT))
#define FIRST_PERSON_CAMERA			0x01
#define SPACESHIP_CAMERA			0x02
#define THIRD_PERSON_CAMERA			0x03

class Player;

struct VS_CB_CAMERA_INFO {
	XMFLOAT4X4 view;
	XMFLOAT4X4 projection;
	XMFLOAT4X4 Iview;
	XMFLOAT4X4 Iprojection;
	XMFLOAT3   posW;
};

class Camera
{
protected:
	Player* m_player;
	
	XMFLOAT3 m_pos;
	//카메라의 월드 위치 
	float m_pitch;
	float m_roll;
	float m_yaw;
	//카메라가 x-축, z-축, y-축으로 얼마만큼 회전했는 가를 나타내는 각도이다.

	DWORD m_mode;
	//카메라의 종류(1인칭 카메라, 스페이스-쉽 카메라, 3인칭 카메라)를 나타낸다.

	XMFLOAT3 m_lookAtWorld;
	//플레이어가 바라볼 위치 벡터이다. 주로 3인칭 카메라에서 사용된다.

	XMFLOAT3 m_right;
	XMFLOAT3 m_up;
	XMFLOAT3 m_look;
	//카메라의 로컬 x-축(Right), y-축(Up), z-축(Look)을 나타내는 벡터이다.


	XMFLOAT4X4 m_view;
	XMFLOAT4X4 m_projection;

	D3D12_VIEWPORT m_viewport;
	D3D12_VIEWPORT m_Defferredviewport[4]; // diffuse,normal,depth,spec
	D3D12_RECT m_scissorRect;
	D3D12_RECT m_DefferredscissorRect[4];
	ComPtr<ID3D12Resource> m_cbCamera;
	VS_CB_CAMERA_INFO* m_cbMappedCamera;

	float m_nearPlaneDistance;
	float m_farPlaneDistance;
	float m_aspectRatio;
	float m_fovAngle;

	XMFLOAT3 m_orthoCenter;
	float m_orthoRadius;

	BoundingFrustum m_frustum;
	BoundingOrientedBox m_orthoFrustum;

	XMFLOAT3 m_offset;
	float m_timelag;

	XMFLOAT3 orthoCenter;
	float orthoRadius;

	float terrainHeight = 0.0f;
public:
	Camera();
	~Camera();
	Camera(Camera* pCamera);


	virtual void CreateCbBuffer(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList);
	//카메라의 정보를 셰이더 프로그램에게 전달하기 위한 상수 버퍼를 생성하고 갱신한다.
	virtual void UpateCbBuffer(ComPtr<ID3D12GraphicsCommandList>& commandList);
	//카메라의 위치정보 업데이트 
	virtual void CreateViewMatrix();
	virtual void CreateViewMatrix(XMFLOAT3 pos , XMFLOAT3 look , XMFLOAT3 up);
	virtual void CreateFrustum();
	virtual BoundingFrustum GetFrustum();

	virtual void ReCreateViewMatrix();
	//카메라가 여러번 회전시 누적된 실수 연산의 부정확성 떄문에 서로 직교하지 않을 수 있으므로 다시 직교하게 만들어줌 
	virtual void GenerateOrthograhpicsOffCenterMatrix(float l, float r, float b, float t, float zn, float zf);
	virtual void CreateProjectionMatrix(float nearPlaneDistance, float farPlaneDistance, float aspectRatio, float fovAngle);
	virtual void Rotate(float pitch, float yaw, float roll) {}
	virtual void Update(XMFLOAT3& xmf3LookAt, float fTimeElapsed) {}

	void SetViewport(int topLeftX, int topLeftY, int width, int height, float minZ = 0.0f, float maxZ = 1.0f);
	void SetDefferredViewPort(int topLeftX, int topLeftY, int width, int height, float minZ = 0.0f, float maxZ = 1.0f);
	void SetScissorRect(LONG leftX, LONG topY, LONG rightX, LONG bottomY);
	void SetDefferredScissorRect(LONG leftX, LONG topY, LONG rightX, LONG bottomY);
	void SetOrthoInfo(XMFLOAT3 center, float radius);

	virtual void SetViewportsAndScissorRects(ComPtr<ID3D12GraphicsCommandList>& commandList);
	virtual void SetDefferedViewPortsAndScissorRects(ComPtr<ID3D12GraphicsCommandList>& commandList);

	void SetViewMatrix(XMFLOAT4X4 view) { this->m_view = view; }
	void SetLookAt(const XMFLOAT3& lookAt);
	void SetPos(const XMFLOAT3& pos);
	void SetPlayer(Player* player);
	void SetTimeLag(float timelag);
	void SetOffset(XMFLOAT3 offset);
	void SetTerrainHeight(float theight);

	virtual void Move(const XMFLOAT3& xmf3Shift);

	XMFLOAT3 GetPosition();
	XMFLOAT3 GetLocalLook();
	XMFLOAT3 GetLocalUp();
	XMFLOAT3 GetLocalRight();
	XMFLOAT3 GetOffset();
	XMFLOAT4X4 GetView();
	XMFLOAT4X4 GetProj();
	DWORD GetMode();
	float& GetAspectRatio() { return m_aspectRatio; }
	float& GetFOV() { return m_fovAngle; }
	D3D12_VIEWPORT& GetViewPort();
	D3D12_RECT& GetSissorRect();

	void UpdateLightCBbuffer(Camera* camera, ComPtr<ID3D12GraphicsCommandList>& commandList);
	void SetLightViewportsAndScissorRects(Camera* camera, ComPtr<ID3D12GraphicsCommandList>& commandList);


};

class SpaceShipCamera : public Camera
{
public:
	SpaceShipCamera(Camera* Camera);
	virtual ~SpaceShipCamera() {}

	virtual void Rotate(float pitch = 0.0f, float yaw = 0.0f, float roll = 0.0f);
};

class FirstPersonCamera : public Camera
{
public:
	FirstPersonCamera(Camera* camera);
	virtual ~FirstPersonCamera();

	virtual void Rotate(float pitch = 0.0f, float yaw = 0.0f, float roll = 0.0f);
};


class ThirdPersonCamera : public Camera
{
public:
	ThirdPersonCamera(Camera* pCamera);
	virtual ~ThirdPersonCamera() { }

	virtual void Update(XMFLOAT3& xmf3LookAt, float fTimeElapsed);
	virtual void SetLookAt(XMFLOAT3& vLookAt);
};

