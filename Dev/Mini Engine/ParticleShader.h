#pragma once
#include "Shader.h"

struct VS_VB_PARTICLE {
	XMFLOAT2 size;
	XMFLOAT3 position;
	UINT texid;
	UINT copyIdx;
};

struct ParticleInfoInCPU {
	XMFLOAT3 position;
	float age;
	int type;
	XMFLOAT2 size;
	UINT texid;
	int copyIdx;

	ParticleInfoInCPU() {};
	ParticleInfoInCPU(float x, float y, float z, float age, int type , XMFLOAT2 size, UINT texid, int copyIdx) {
		this->position = XMFLOAT3(x, y, z);
		this->age = age;
		this->type = type;
		this->size = size;
		this->texid = texid;
		this->copyIdx = copyIdx;
	}
};

struct ParticleBlobModify {

	XMFLOAT3 positionMin;
	XMFLOAT3 positionMax;
	XMFLOAT3 velocityMin;
	XMFLOAT3 velocityMax;
	float ageMin;
	float ageMax;

	ParticleBlobModify() {};
	ParticleBlobModify(XMFLOAT3 positionMin, XMFLOAT3 positionMax, XMFLOAT3 velocityMin, XMFLOAT3 velocityMax, float ageMin, float ageMax) {
		this->positionMin = positionMin;
		this->positionMax = positionMax;
		this->velocityMin = velocityMin;
		this->velocityMax = velocityMax;
		this->ageMin = ageMin;
		this->ageMax = ageMax;
	};
	int texid;

};

struct VS_SB_PARTICLE
{
	XMFLOAT3 position;
	XMFLOAT3 velocity;
	float age = 0;
};


class ParticleShader : public Shader
{
	ComPtr<ID3D12PipelineState> m_ComputePipeline;
	ComPtr<ID3D12Resource> vertexBuffer = NULL;
	ID3D12Resource* uploadVertexBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW vertexBufferView;

	ComPtr<ID3D12Resource> copyBuffer[BUFFERSIZE];
	VS_SB_PARTICLE* mappedCopyBuffer[BUFFERSIZE];
	ComPtr<ID3D12Resource> copyBufferCS[BUFFERSIZE];

	ComPtr<ID3D12Resource> particleBuffer = NULL;

	UINT particlesNum = 0;
	int currentParticleIdx = 0;

public:
	vector<ParticleInfoInCPU> particles;
	VS_VB_PARTICLE* mappedBuffer = NULL;

	virtual ~ParticleShader();

	virtual void CreatebPipeLineState(ComPtr<ID3D12Device>& device, ComPtr<ID3D12RootSignature>& rootsignature, ID3D12RootSignature* computeRootSignature);
	virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();
	virtual void AnimateAndUpdate(float timeElapsed, XMFLOAT3 cameraPos);
	virtual D3D12_DEPTH_STENCIL_DESC CreateDepthStencilState();
	virtual void CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	virtual void UpdateShaderVariables(int idx);
	virtual void Compute(ID3D12GraphicsCommandList* commandList, int idx , int instanceNum);
	virtual void CreateParticle(ID3D12GraphicsCommandList* commandList, int idx, int type, XMFLOAT2 size, UINT texid);
	virtual void CreateDynamicParticle(ID3D12GraphicsCommandList* commandList, int idx, int type, int texid, float sizeX, float sizeY , float age , XMFLOAT3 pos , ParticleBlobModify blob);
	virtual void ModifyParticle(ID3D12GraphicsCommandList* commandList, int idx, int copyIdx, ParticleBlobModify blob);
	virtual void Render(ID3D12GraphicsCommandList* commandList, XMFLOAT3 cPos);
	virtual void SortParticle(XMFLOAT3 cPos);
	virtual void ShadowRender(ID3D12GraphicsCommandList* commandList);

	virtual void BuildShaderParticle(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	void PrePareCompute(ID3D12GraphicsCommandList* commandList);
	void ReleaseUploadBuffer();


	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
	virtual D3D12_SHADER_BYTECODE CreatePixelShader();
	virtual D3D12_SHADER_BYTECODE CreateGeoMetryShader();
	virtual D3D12_SHADER_BYTECODE CreateShadowVertexShader();
	virtual D3D12_SHADER_BYTECODE CreateShadowPixelShader();
	virtual D3D12_SHADER_BYTECODE CreateShadowGeoMetryShader();
	virtual D3D12_SHADER_BYTECODE CreateComputeShader();
	virtual D3D12_RASTERIZER_DESC CreateRasterizerState();
	virtual int GetShaderNumber();
	int GetParticleNum() { return particlesNum; }
	vector<ParticleInfoInCPU>& GetParticleCPU() { return particles; }

};

