#include "Standard.hlsl"
struct VS_SKINNED_STANDARD_INPUT
{
    float3 position : POSITION;
    float2 uv : TEXCOORD;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 bitangent : BITANGENT;
    int4 indices : BONEINDEX;
    float4 weights : BONEWEIGHT;

    uint vertexID : SV_VertexID;
};

VS_STANDARD_OUTPUT VSSkinnedAnimationStandard(VS_SKINNED_STANDARD_INPUT input)
{
    VS_STANDARD_OUTPUT output;

    output.positionW = gsbSkinnedPosition[input.vertexID];
    output.normalW = gsbSkinnedNormal[input.vertexID];
    output.tangentW = gsbSkinnedTangent[input.vertexID];
    output.bitangentW = gsbSkinnedBiTangent[input.vertexID];
    
    float4 cameraPos = mul(float4(output.positionW, 1.0f), gmtxView);
    output.position = mul(cameraPos, gmtxProjection);
    
    output.uv = input.uv;
    
    //matrix shadowProject[MAX_CASCADE_SIZE];
    
    //for (int i = 0; i < int(gfCasacdeNum); ++i)
    //{
    //    shadowProject[i] = mul(gmtxLightViewProjection[i], gmtxProjectToTexture);
    //    output.shadowPosition[i] = mul(float4(output.positionW, 1.0f), shadowProject[i]);
    //}
    //output.fogFactor = GetFogFactor(cameraPos.z);

    return (output);
}

/////////////////////////////////////////////////////////////////////////////

struct VS_SKINNED_INSTANCING_INPUT
{
    float3 position : POSITION;
    float2 uv : TEXCOORD;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 bitangent : BITANGENT;
    int4 indices : BONEINDEX;
    float4 weights : BONEWEIGHT;
    uint objIdx : OBJIDX;
    
    uint vertexID : SV_VertexID;
};

VS_STANDARD_OUTPUT VSSkinnedAnimationInstancing(VS_SKINNED_INSTANCING_INPUT input)
{
    VS_STANDARD_OUTPUT output;

    output.positionW = gsbSkinnedPosition[input.objIdx * gnVerticesNum + input.vertexID];
    output.normalW = gsbSkinnedNormal[input.objIdx * gnVerticesNum + input.vertexID];
    output.tangentW = gsbSkinnedTangent[input.objIdx * gnVerticesNum + input.vertexID];
    output.bitangentW = gsbSkinnedBiTangent[input.objIdx * gnVerticesNum + input.vertexID];
    
    float4 cameraPos = mul(float4(output.positionW, 1.0f), gmtxView);
    output.position = mul(cameraPos, gmtxProjection);
    
    output.uv = input.uv;
    
    matrix shadowProject[NUM_CASCADES];
    
    for (int i = 0; i < int(gfCasacdeNum); ++i)
    {
        shadowProject[i] = mul(gmtxLightViewProjection[i], gmtxProjectToTexture);
        output.shadowPosition[i] = mul(float4(output.positionW, 1.0f), shadowProject[i]);
    }
    //output.fogFactor = GetFogFactor(cameraPos.z);
    
    return (output);
}