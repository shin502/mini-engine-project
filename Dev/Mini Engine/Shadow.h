#pragma once
#include "Scene.h"
#include "Camera.h"
struct VS_CB_SHADOW_INFO
{
	XMFLOAT4X4 viewProjection[NUM_CASCADES];
	XMFLOAT4 cascadeLength[NUM_CASCADES];
	float shadowTexSize;
	float cascadeNum;
};

class Shadow
{
	Camera* m_lightCamera; // shadow map camera 
	Camera* m_mainCamera;
	Scene* m_scene;
	float* m_cascadePos = NULL;
	UINT m_cascadeNum = 0;
	float m_cascadeBias = 0;

	ComPtr<ID3D12Resource> m_lightBuffer;	
	VS_CB_SHADOW_INFO* m_lightMappedCamera;


public:
	~Shadow();
	Shadow(Camera* m_camera , Scene* scene);
	void PrePareShadowMap();
	void CreateLightCamera(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList> commandList);
	void InitCbInform(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	void UpdateCbState(ID3D12GraphicsCommandList* commandList);
	void SetMainCamera(Camera* mainCamera);
	Camera* GetLightCamera(int cIdx);


};

