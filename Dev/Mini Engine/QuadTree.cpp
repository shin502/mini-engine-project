#include "QuadTree.h"

QuadTree::QuadTree()
{
	for (int i = 0; i < 4; ++i) {
		Node[i] = NULL;
	}
}

QuadTree::QuadTree(float startX, float startZ, float width, float length)
{

	this->startidxX = (int)startX;
	this->startidxZ = (int)startZ;
	this->width = (int)width;
	this->length = (int)length;

	for (int i = 0; i < 4; ++i) { Node[i] = NULL;}
	//aabbBox.Center.x = startX + (width / 2);
	//aabbBox.Center.z = startZ + (length / 2);
	//aabbBox.Center.y = 0;
	aabbBox.Center = XMFLOAT3(startX + (width / 2), 0, startZ + (length / 2));
	aabbBox.Extents = XMFLOAT3(width / 2, BASIC_QUAD_HEIGHT, length / 2);
}
