#include "SkinnedMesh.h"
#include "HierarchyObject.h"

SkinnedMesh::~SkinnedMesh()
{
	if (boneIndices) delete[] boneIndices;
	if (boneWeights) delete[] boneWeights;

	if (skinningBoneFrameCaches) delete[] skinningBoneFrameCaches;
	if (skinningBoneNames) delete[] skinningBoneNames;

	if (xmf4x4BindPoseBoneOffsets) delete[] xmf4x4BindPoseBoneOffsets;
	if (bindPoseBoneOffsets) bindPoseBoneOffsets->Release();

	ReleaseShaderVariables();
}

void SkinnedMesh::CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
}

void SkinnedMesh::UpdateShaderVariables(ID3D12GraphicsCommandList* commandList, ID3D12Resource* buffers[4])
{
	D3D12_GPU_VIRTUAL_ADDRESS positionWorldGpuVirtualAddress = buffers[0]->GetGPUVirtualAddress();
	commandList->SetGraphicsRootShaderResourceView(GraphicsRootSkinnedPositionWorld, positionWorldGpuVirtualAddress);

	D3D12_GPU_VIRTUAL_ADDRESS normalWorldGpuVirtualAddress = buffers[1]->GetGPUVirtualAddress();
	commandList->SetGraphicsRootShaderResourceView(GraphicsRootSkinnedNormalWorld, normalWorldGpuVirtualAddress);

	D3D12_GPU_VIRTUAL_ADDRESS tangentWorldGpuVirtualAddress = buffers[2]->GetGPUVirtualAddress();
	commandList->SetGraphicsRootShaderResourceView(GraphicsRootSkinnedTangentWorld, tangentWorldGpuVirtualAddress);

	D3D12_GPU_VIRTUAL_ADDRESS biTangentWorldGpuVirtualAddress = buffers[3]->GetGPUVirtualAddress();
	commandList->SetGraphicsRootShaderResourceView(GraphicsRootSkinnedBiTangentWorld, biTangentWorldGpuVirtualAddress);

	commandList->SetGraphicsRoot32BitConstant(GraphicsRootSkinnedVerticesNum, verticesNum, 0);
}

void SkinnedMesh::ReleaseShaderVariables()
{
}

void SkinnedMesh::ReleaseUploadBuffer()
{
	StandardMesh::ReleaseUploadBuffer();

	if (boneIndexUploadBuffer) boneIndexUploadBuffer->Release();
	boneIndexUploadBuffer = NULL;

	if (boneWeightUploadBuffer) boneWeightUploadBuffer->Release();
	boneWeightUploadBuffer = NULL;

	if (bindPoseBoneOffsetsUploadBuffer) bindPoseBoneOffsetsUploadBuffer->Release();
	bindPoseBoneOffsetsUploadBuffer = NULL;
}

void SkinnedMesh::PrepareSkinning(HierarchyObj* pModelRootObject)
{
	for (int j = 0; j < skinningBones; j++)
	{
		skinningBoneFrameCaches[j] = pModelRootObject->FindFrame(skinningBoneNames[j]);
	}
}


void SkinnedMesh::LoadSkinInfoFromFile(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, FILE* pInFile)
{
	char pstrToken[64] = { '\0' };
	UINT nReads = 0;

	::ReadStringFromFile(pInFile, meshName);

	for (; ; )
	{
		::ReadStringFromFile(pInFile, pstrToken);
		if (!strcmp(pstrToken, "<BonesPerVertex>:"))
		{
			bonesPerVertex = ::ReadIntegerFromFile(pInFile);
		}
		else if (!strcmp(pstrToken, "<Bounds>:"))
		{
			nReads = (UINT)::fread(&boundingBoxAABBCenter, sizeof(XMFLOAT3), 1, pInFile);
			nReads = (UINT)::fread(&boundingBoxAABBExtents, sizeof(XMFLOAT3), 1, pInFile);
		}
		else if (!strcmp(pstrToken, "<BoneNames>:"))
		{
			skinningBones = ::ReadIntegerFromFile(pInFile);
			if (skinningBones > 0)
			{
				skinningBoneNames = new char[skinningBones][128];
				skinningBoneFrameCaches = new HierarchyObj * [skinningBones];
				for (int i = 0; i < skinningBones; i++)
				{
					::ReadStringFromFile(pInFile, skinningBoneNames[i]);
					skinningBoneFrameCaches[i] = NULL;
				}
			}
		}
		else if (!strcmp(pstrToken, "<BoneOffsets>:"))
		{
			skinningBones = ::ReadIntegerFromFile(pInFile);
			if (skinningBones > 0)
			{
				xmf4x4BindPoseBoneOffsets = new XMFLOAT4X4[skinningBones];
				nReads = (UINT)::fread(xmf4x4BindPoseBoneOffsets, sizeof(XMFLOAT4X4), skinningBones, pInFile);

				XMFLOAT4X4 boneOffset[SKINNED_ANIMATION_BONES];
				for (int i = 0; i < skinningBones; i++)
				{
					XMStoreFloat4x4(&boneOffset[i], XMMatrixTranspose(XMLoadFloat4x4(&xmf4x4BindPoseBoneOffsets[i])));
				}

				UINT ncbElementBytes = (((sizeof(XMFLOAT4X4) * SKINNED_ANIMATION_BONES) + 255) & ~255); //256�� ���
				bindPoseBoneOffsets = ::CreateBufferResource(device, commandList, boneOffset, ncbElementBytes, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &bindPoseBoneOffsetsUploadBuffer);

			}
		}
		else if (!strcmp(pstrToken, "<BoneIndices>:"))
		{
			type |= VERTEXT_BONE_INDEX_WEIGHT;

			verticesNum = ::ReadIntegerFromFile(pInFile);
			if (verticesNum > 0)
			{
				boneIndices = new XMINT4[verticesNum];

				nReads = (UINT)::fread(boneIndices, sizeof(XMINT4), verticesNum, pInFile);
				boneIndexBuffer = ::CreateBufferResource(device, commandList, boneIndices, sizeof(XMINT4) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &boneIndexUploadBuffer);

				boneIndexBufferView.BufferLocation = boneIndexBuffer->GetGPUVirtualAddress();
				boneIndexBufferView.StrideInBytes = sizeof(XMINT4);
				boneIndexBufferView.SizeInBytes = sizeof(XMINT4) * verticesNum;
			}
		}
		else if (!strcmp(pstrToken, "<BoneWeights>:"))
		{
			type |= VERTEXT_BONE_INDEX_WEIGHT;

			verticesNum = ::ReadIntegerFromFile(pInFile);
			if (verticesNum > 0)
			{
				boneWeights = new XMFLOAT4[verticesNum];

				nReads = (UINT)::fread(boneWeights, sizeof(XMFLOAT4), verticesNum, pInFile);
				boneWeightBuffer = ::CreateBufferResource(device, commandList, boneWeights, sizeof(XMFLOAT4) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &boneWeightUploadBuffer);

				boneWeightBufferView.BufferLocation = boneWeightBuffer->GetGPUVirtualAddress();
				boneWeightBufferView.StrideInBytes = sizeof(XMFLOAT4);
				boneWeightBufferView.SizeInBytes = sizeof(XMFLOAT4) * verticesNum;
			}
		}
		else if (!strcmp(pstrToken, "</SkinningInfo>"))
		{
			break;
		}
	}
}

void SkinnedMesh::ComputeSkinning(ID3D12GraphicsCommandList* commandList, UINT instancesNum, ID3D12Resource* buffers[4])
{
	// CBV, SRV
	if (bindPoseBoneOffsets)
	{
		D3D12_GPU_VIRTUAL_ADDRESS boneOffsetsGpuVirtualAddress = bindPoseBoneOffsets->GetGPUVirtualAddress();
		commandList->SetComputeRootConstantBufferView(ComputeRootBoneOffset, boneOffsetsGpuVirtualAddress); //Skinned Bone Offsets
	}
	if (skinningBoneTransforms)
	{
		D3D12_GPU_VIRTUAL_ADDRESS boneTransformsGpuVirtualAddress = skinningBoneTransforms.Get()->GetGPUVirtualAddress();
		commandList->SetComputeRootShaderResourceView(ComputeRootBoneTransform, boneTransformsGpuVirtualAddress); //Skinned Bone Transforms
	}
	::ResourceBarrier(commandList, positionBuffer.Get(), D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
	::ResourceBarrier(commandList, normalBuffer.Get(), 
		D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
	::ResourceBarrier(commandList, tangentBuffer.Get(), 
		D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
	::ResourceBarrier(commandList, biTangentBuffer.Get(), 
		D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
	::ResourceBarrier(commandList, boneIndexBuffer.Get(), 
		D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
	::ResourceBarrier(commandList, boneWeightBuffer.Get(), 
		D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);

	D3D12_GPU_VIRTUAL_ADDRESS positionGpuVirtualAddress = positionBuffer->GetGPUVirtualAddress();
	commandList->SetComputeRootShaderResourceView(ComputeRootSkinnedPosition, positionGpuVirtualAddress);

	D3D12_GPU_VIRTUAL_ADDRESS normalGpuVirtualAddress = normalBuffer->GetGPUVirtualAddress();
	commandList->SetComputeRootShaderResourceView(ComputeRootSkinnedNormal, normalGpuVirtualAddress);

	D3D12_GPU_VIRTUAL_ADDRESS tangentGpuVirtualAddress = tangentBuffer->GetGPUVirtualAddress();
	commandList->SetComputeRootShaderResourceView(ComputeRootSkinnedTangent, tangentGpuVirtualAddress);

	D3D12_GPU_VIRTUAL_ADDRESS biTangentGpuVirtualAddress = biTangentBuffer->GetGPUVirtualAddress();
	commandList->SetComputeRootShaderResourceView(ComputeRootSkinnedBiTangent, biTangentGpuVirtualAddress);

	D3D12_GPU_VIRTUAL_ADDRESS boneIndexGpuVirtualAddress = boneIndexBuffer->GetGPUVirtualAddress();
	commandList->SetComputeRootShaderResourceView(ComputeRootSkinnedBoneIndex, boneIndexGpuVirtualAddress);

	D3D12_GPU_VIRTUAL_ADDRESS boneWeightGpuVirtualAddress = boneWeightBuffer->GetGPUVirtualAddress();
	commandList->SetComputeRootShaderResourceView(ComputeRootSkinnedBoneWeight, boneWeightGpuVirtualAddress);

	::ResourceBarrier(commandList, buffers[0], D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_UNORDERED_ACCESS);
	::ResourceBarrier(commandList, buffers[1], D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_UNORDERED_ACCESS);	
	::ResourceBarrier(commandList, buffers[2], D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_UNORDERED_ACCESS);		
	::ResourceBarrier(commandList, buffers[3], D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_UNORDERED_ACCESS);

	// UAV
	D3D12_GPU_VIRTUAL_ADDRESS positionWorldGpuVirtualAddress = buffers[0]->GetGPUVirtualAddress();
	commandList->SetComputeRootUnorderedAccessView(ComputeRootSkinnedPositionWorld, positionWorldGpuVirtualAddress);

	D3D12_GPU_VIRTUAL_ADDRESS normalWorldGpuVirtualAddress = buffers[1]->GetGPUVirtualAddress();
	commandList->SetComputeRootUnorderedAccessView(ComputeRootSkinnedNormalWorld, normalWorldGpuVirtualAddress);

	D3D12_GPU_VIRTUAL_ADDRESS tangentWorldGpuVirtualAddress = buffers[2]->GetGPUVirtualAddress();
	commandList->SetComputeRootUnorderedAccessView(ComputeRootSkinnedTangentWorld, tangentWorldGpuVirtualAddress);

	D3D12_GPU_VIRTUAL_ADDRESS biTangentWorldGpuVirtualAddress = buffers[3]->GetGPUVirtualAddress();
	commandList->SetComputeRootUnorderedAccessView(ComputeRootSkinnedBiTangentWorld, biTangentWorldGpuVirtualAddress);

	commandList->SetComputeRoot32BitConstant(ComputeRootSkinnedVerticesNum, verticesNum, 0);

	commandList->Dispatch(static_cast<int>(ceil(verticesNum / BLOCKSIZE)), instancesNum, 1);

	::ResourceBarrier(commandList, buffers[0], D3D12_RESOURCE_STATE_UNORDERED_ACCESS, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
	::ResourceBarrier(commandList, buffers[1], D3D12_RESOURCE_STATE_UNORDERED_ACCESS, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
	::ResourceBarrier(commandList, buffers[2], D3D12_RESOURCE_STATE_UNORDERED_ACCESS, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
	::ResourceBarrier(commandList, buffers[3], D3D12_RESOURCE_STATE_UNORDERED_ACCESS, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);


	::ResourceBarrier(commandList, positionBuffer.Get(), D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);
	::ResourceBarrier(commandList, normalBuffer.Get(), D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, 
		D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);
	::ResourceBarrier(commandList, tangentBuffer.Get(), D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE,
		D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);
	::ResourceBarrier(commandList, biTangentBuffer.Get(), D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);
	::ResourceBarrier(commandList, boneIndexBuffer.Get(), D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);
	::ResourceBarrier(commandList, boneWeightBuffer.Get(), D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);
}

void SkinnedMesh::OnPreRender(ID3D12GraphicsCommandList* commandList, UINT instancesNum, ID3D12Resource* buffers[4])
{
	// ComputeShader Skinning
	ComputeSkinning(commandList, instancesNum, buffers);

}

void SkinnedMesh::Render(ID3D12GraphicsCommandList* commandList, int subSet, ID3D12Resource* buffers[4])
{
	// Set SRV, CBV
	UpdateShaderVariables(commandList, buffers);

	D3D12_VERTEX_BUFFER_VIEW pVertexBufferViews[7] = { positionBufferView, textureCoord0BufferView, normalBufferView, tangentBufferView, biTangentBufferView, boneIndexBufferView, boneWeightBufferView };
	commandList->IASetVertexBuffers(slot, 7, pVertexBufferViews);

	commandList->IASetPrimitiveTopology(primitiveTopology);

	if ((subMeshesNum > 0) && (subSet < subMeshesNum))
	{
		commandList->IASetIndexBuffer(&(subSetIndexBufferViews[subSet]));
		commandList->DrawIndexedInstanced(subSetIndicesNum[subSet], 1, 0, 0, 0);
	}
	else
	{
		commandList->DrawInstanced(verticesNum, 1, offset, 0);
	}
}

void SkinnedMesh::Render(ID3D12GraphicsCommandList* commandList, int subSet, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingBufferViews, ID3D12Resource* buffers[4])
{
	// Set SRV, CBV
	UpdateShaderVariables(commandList, buffers);

	D3D12_VERTEX_BUFFER_VIEW* vertexBufferViews;
	vertexBufferViews = new D3D12_VERTEX_BUFFER_VIEW[8];
	vertexBufferViews[0] = positionBufferView;
	vertexBufferViews[1] = textureCoord0BufferView;
	vertexBufferViews[2] = normalBufferView;
	vertexBufferViews[3] = tangentBufferView;
	vertexBufferViews[4] = biTangentBufferView;
	vertexBufferViews[5] = boneIndexBufferView;
	vertexBufferViews[6] = boneWeightBufferView;
	vertexBufferViews[7] = instancingBufferViews;

	commandList->IASetVertexBuffers(slot, 8, vertexBufferViews);
	commandList->IASetPrimitiveTopology(primitiveTopology);

	if ((subMeshesNum > 0) && (subSet < subMeshesNum))
	{
		commandList->IASetIndexBuffer(&(subSetIndexBufferViews[subSet]));
		commandList->DrawIndexedInstanced(subSetIndicesNum[subSet], instancesNum, 0, 0, 0);
	}
	else
	{
		commandList->DrawInstanced(verticesNum, instancesNum, offset, 0);
	}
	delete[] vertexBufferViews;
}

void SkinnedMesh::ShadowRender(ID3D12GraphicsCommandList* commandList, int subSet, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingbufferViews, ID3D12Resource* buffers[4])
{
	// Set SRV, CBV
	UpdateShaderVariables(commandList, buffers);

	D3D12_VERTEX_BUFFER_VIEW* vertexBufferViews;
	vertexBufferViews = new D3D12_VERTEX_BUFFER_VIEW[2];
	vertexBufferViews[0] = positionBufferView;
	vertexBufferViews[1] = instancingbufferViews;

	commandList->IASetVertexBuffers(slot, 2, vertexBufferViews);
	commandList->IASetPrimitiveTopology(primitiveTopology);

	if ((subMeshesNum > 0) && (subSet < subMeshesNum))
	{
		commandList->IASetIndexBuffer(&(subSetIndexBufferViews[subSet]));
		commandList->DrawIndexedInstanced(subSetIndicesNum[subSet], instancesNum, 0, 0, 0);
	}
	else
	{
		commandList->DrawInstanced(verticesNum, instancesNum, offset, 0);
	}
	delete[] vertexBufferViews;
}
