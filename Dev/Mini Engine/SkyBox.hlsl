#include "BillBoardShader.hlsl"

struct VS_SKYBOX_INPUT
{
    float3 position : POSITION;
    float2 uv0 : TEXCOORD;
    float3 normal : NORMAL;
};

struct VS_SKYBOX_OUTPUT
{
    float4 position : SV_POSITION;
    float3 positionL : POSITION;
    float2 uv0 : TEXCOORD;
    float3 normalW : NORMAL;
};

VS_SKYBOX_OUTPUT VS_SKYBOX(VS_SKYBOX_INPUT input)
{
    VS_SKYBOX_OUTPUT output;
    output.position = mul(mul(mul(float4(input.position, 1.0f), gmtxWorld), gmtxView), gmtxProjection);
    output.positionL = input.position;
    output.uv0 = input.uv0;
    output.normalW = mul(input.normal, (float3x3)gmtxWorld);
    return output;
}
Defferred_OUTPUT PS_SKYBOX(VS_SKYBOX_OUTPUT input)
{
    Defferred_OUTPUT output;
    float4 diffuseColor = gtxtSkyBoxTexture.Sample(gspClamp, input.positionL);
    float3 normalW = input.normalW;
    normalW = (normalW * 0.5f) + 0.5f;
    float posW = input.position.w;
    float posZ = 0.99999f;
    posW = 0.000001f;
    output.DIFFUSE_COLOR = diffuseColor;
    output.NORMAL_COLOR = float4(normalW, 1.0f);
    output.SPECULAR_COLOR.xyz = gMaterials[0].m_cSpecular.xyz;
    output.SPECULAR_COLOR.a = gMaterials[0].m_cSpecular.a;
    output.DEPTH_COLOR.r = posZ / posW;
    output.DEPTH_COLOR.g = gMaterials[0].m_cAmbient.r;
    output.DEPTH_COLOR.b = gMaterials[0].m_cDiffuse.r;
    output.DEPTH_COLOR.a = posW;
    return output;

}
