#include "SkyBox.hlsl"



#define NUM_STEPS 64
static const float NUM_DELTA = 1.0 / 64.0f;
//float4 lightShaftValue
//Density 범위
//Decay 0,1.0f 사이
//Weight 50
//Exposure 100

//cbuffer RayTraceConstants : register(b0)
//{
//    float2 gSunPos;
//    float gInitDecay;
//    float gDistDecay;
//    float3 gRayColor;
//    float gMaxDeltaLen;
//}


float4 lightShaft(float2 texCoord)
{
    
    float gMaxDeltaLen = 500.0f; //
    float gDistDecay = 2.0f;
    float gInitDecay = 0.05f;
   
    //lightShaft
    float2 dirToSun = (sunPos.xy - texCoord);
    float lengthToSun = length(dirToSun);
    dirToSun = normalize(dirToSun);
    
    float deltaLen = min(gMaxDeltaLen, lengthToSun * NUM_DELTA);
    float2 rayDelta = dirToSun * deltaLen;
    
    // Each step decay
    float stepDecay = gDistDecay * deltaLen;
    
    float2 rayOffset = float2(0.0, 0.0);
    float decay = gInitDecay;
    float rayIntensity = 0.0f;
    
    for (int i = 0; i < NUM_STEPS; i++)
    {
        // Sample at the current location
        float2 sampPos = texCoord + rayOffset;
        float occulusinColor = gtxtDepthTexture.SampleLevel(gspClamp, sampPos, 0.0f);
        if (occulusinColor < 0.999f) // 객체들 
        {
            occulusinColor = 0.0f;
        }
        else //배경 
        {
            occulusinColor = 1.0f;
        }
        float fCurIntensity = occulusinColor;

        // Sum the intensity taking decay into account
        rayIntensity += fCurIntensity * decay;

        // Advance to the next position
        rayOffset += rayDelta;

        // Update the decay
        decay = saturate(decay - stepDecay);
    }
    
    if (sunCamZ < 0.0f)
        rayIntensity = 0.0f;
    
    // The resultant intensity of the pixel.
    return float4(rayIntensity, rayIntensity, rayIntensity, 1.0);
}




struct VS_POST_OUTPUT
{
    float4 position : SV_POSITION;
    float2 uv0 : TEXCOORD0;
};

VS_POST_OUTPUT VS_POSTEFFECT(VS_UI_INPUT input)
{
    VS_POST_OUTPUT output;
    output.position = float4(input.position, 1.0f);
    output.uv0 = input.uv0;
    return output;
}

float4 PS_POSTEFFECT(VS_POST_OUTPUT input) : SV_TARGET
{
    float4 depthColor = float4(0.0f, 0.0f, 0.0f, 1.0f);
    float2 tmpUV = GetPixelUV(input.position);
    tmpUV = tmpUV * 2.0f - 1.0f;
    tmpUV.y = -tmpUV.y;
    depthColor = gtxtDepthTexture.Sample(gspClamp, input.uv0).r;
    float4 pColor = gtxtPostEffectTexture[DFFEREDTEXUTER].Sample(gspWarp, input.uv0);
    float3 RayColor = float3(1.0f, 1.0f, 1.0f);
    float cColor = lightShaft(input.uv0);
    float4 finalColor = float4(RayColor * cColor, 1.0f);
    return saturate(pColor.xyzw + finalColor.xyzw);

}