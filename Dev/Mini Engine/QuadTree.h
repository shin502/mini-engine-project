#pragma once
#include "stdafx.h"

constexpr float BASIC_QUAD_HEIGHT = 1000.0f;
class GameObject;

enum EQUADTREE {
	E_BL,
	E_BR,
	E_UL,
	E_UR,
	E_ROOT
};

struct QuadTree {
	QuadTree* Node[4];
	BoundingBox aabbBox;
	vector<GameObject*> gVec;

	int startidxX;
	int startidxZ;
	int width;
	int length;
	QuadTree();
	QuadTree(float startX, float startZ, float width, float length);
	

};
