#pragma once
#include "FrameWork.h"
FrameWork::FrameWork(UINT width, UINT height)
{
    m_Width = width;
    m_Height = height;
    m_viewport = CD3DX12_VIEWPORT(0.0f, 0.0f, static_cast<float>(width), static_cast<float>(height));
    m_scissorRect = CD3DX12_RECT(0, 0, static_cast<LONG>(width), static_cast<LONG>(height));
    m_rtvDescriptorSize = 0;

    _tcscpy_s(m_pszFrameRate, _T("LabProject ("));

    for (int i = 0; i < 2; ++i) {
        m_renderTargets[i] = nullptr;
    }
}

FrameWork::~FrameWork()
{
    if (m_camera) {
        delete m_camera;
        m_camera = NULL;
    }

    if (m_player)
    {
        delete m_player;
        m_player = NULL;
    }

    for (int i = 0; i < DefferredCount; ++i) {
        m_defferredResources[i]->Release();
    }

    for (int i = 0; i < NUM_CASCADES; ++i) {
        m_CascadeResources[i]->Release();
    }

    for (int i = 0; i < postRenderCount; ++i) {
        m_PosteffectResources[i]->Release();
    }

    delete m_shadow;
}

bool FrameWork::OnInit(HINSTANCE hInstance, HWND hMainWnd)
{
    m_hInstance = hInstance;
    m_hWnd = hMainWnd;

    CreateDevice();
    CreateFence();
    CreateCommandQueueAndList();
    CreateRtvAndDsvDescriptorHeaps();
    CreateSwapChain();
    CreateRenderTargetViews();
    CreateDepthStencilView();
    CreateRenderTexture();
    CreateDepthTexture();

    m_scene.SetCbvSrvDescIncrementSize(m_device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV));

    CoInitialize(NULL);

    ThrowIfFailed(m_commandAllocator->Reset());
    ThrowIfFailed(m_commandList->Reset(m_commandAllocator.Get(), NULL));

    m_processInput.Init(&m_timer, &m_scene, m_hWnd);
    BuildObjects();
    CraeteCameraAndPlayer();
    BuildShadow();

    ThrowIfFailed(m_commandList->Close());
    ID3D12CommandList* ppCommandLists[] = { m_commandList.Get() };
    m_commandQueue->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);
    WaitForGPURunning();
    //releaseUploadBuffer;
    m_scene.ReleaseUploadBuffer();
    return true;
}

void FrameWork::OnDestroy()
{

}

void FrameWork::CreateSwapChain()
{

    UINT dxgiFactoryFlags = 0;

#if defined(_DEBUG)
    // Enable the debug layer (requires the Graphics Tools "optional feature").
    // NOTE: Enabling the debug layer after device creation will invalidate the active device.
    {
        ComPtr<ID3D12Debug> debugController;
        if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&debugController))))
        {
            debugController->EnableDebugLayer();

            // Enable additional debug layers.
            dxgiFactoryFlags |= DXGI_CREATE_FACTORY_DEBUG;
        }
    }
#endif

    ComPtr<IDXGIFactory4> factory;
    ThrowIfFailed(CreateDXGIFactory2(dxgiFactoryFlags, IID_PPV_ARGS(&factory)));

    // Describe and create the swap chain.
    DXGI_SWAP_CHAIN_DESC1 swapChainDesc = {};
    swapChainDesc.BufferCount = FrameCount;
    swapChainDesc.Width = m_Width;
    swapChainDesc.Height = m_Height;
    swapChainDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
    swapChainDesc.SampleDesc.Count = 1;

    ComPtr<IDXGISwapChain1> swapChain;
    ThrowIfFailed(factory->CreateSwapChainForHwnd(
        m_commandQueue.Get(),        // Swap chain needs the queue so that it can force a flush on it.
        m_hWnd,
        &swapChainDesc,
        nullptr,
        nullptr,
        &swapChain
    ));


    // This sample does not support fullscreen transitions.
    ThrowIfFailed(factory->MakeWindowAssociation(m_hWnd, DXGI_MWA_NO_ALT_ENTER));

    ThrowIfFailed(swapChain.As(&m_swapChain));
    m_frameIndex = m_swapChain->GetCurrentBackBufferIndex();
}

void FrameWork::CreateDevice()
{
    UINT dxgiFactoryFlags = 0;

#if defined(_DEBUG)
    // Enable the debug layer (requires the Graphics Tools "optional feature").
    // NOTE: Enabling the debug layer after device creation will invalidate the active device.
    {
        ComPtr<ID3D12Debug> debugController;
        if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&debugController))))
        {
            debugController->EnableDebugLayer();

            // Enable additional debug layers.
            dxgiFactoryFlags |= DXGI_CREATE_FACTORY_DEBUG;
        }
    }
#endif
    ComPtr<IDXGIFactory4> factory;
    ::ThrowIfFailed(CreateDXGIFactory2(dxgiFactoryFlags, IID_PPV_ARGS(&factory)));

    //DXGI 팩토리를 생성한다.
    HRESULT hResult;
    IDXGIAdapter1* adapter = NULL;
    for (UINT i = 0; DXGI_ERROR_NOT_FOUND != factory->EnumAdapters1(i, &adapter); i++)
    {
        DXGI_ADAPTER_DESC1 adapterDesc;
        adapter->GetDesc1(&adapterDesc);
        if (adapterDesc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE) continue;
        if (SUCCEEDED(hResult = D3D12CreateDevice(adapter, D3D_FEATURE_LEVEL_12_0, IID_PPV_ARGS(&m_device)))) break;
    }
    _com_error err(hResult);
    err.ErrorMessage();

    //모든 하드웨어 어댑터 대하여 특성 레벨 12.0을 지원하는 하드웨어 디바이스를 생성한다.

    if (!adapter)
    {
        factory->EnumWarpAdapter(IID_PPV_ARGS(&m_device));
        D3D12CreateDevice(adapter, D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&m_device));
    }
    //특성 레벨 12.0을 지원하는 하드웨어 디바이스를 생성할 수 없으면 WARP 디바이스를 생성한다.

    if (adapter) adapter->Release();
}
void FrameWork::CreateCommandQueueAndList()
{
    // Describe and create the command queue.
    D3D12_COMMAND_QUEUE_DESC queueDesc = {};
    queueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
    queueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;

    ThrowIfFailed(m_device->CreateCommandQueue(&queueDesc, IID_PPV_ARGS(&m_commandQueue)));

    ThrowIfFailed(m_device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&m_commandAllocator)));

    ThrowIfFailed(m_device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, m_commandAllocator.Get(), NULL, IID_PPV_ARGS(&m_commandList)));

    m_commandList.Get()->Close();

}

void FrameWork::CreateFence()
{
    ThrowIfFailed(m_device->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&m_fence)));
    m_fenceValue = 1;

    // Create an event handle to use for frame synchronization.
    m_fenceEvent = CreateEvent(nullptr, FALSE, FALSE, nullptr);
    if (m_fenceEvent == nullptr)
    {
        ThrowIfFailed(HRESULT_FROM_WIN32(GetLastError()));
    }
}

void FrameWork::CreateRtvAndDsvDescriptorHeaps()
{
    // Create descriptor heaps.
    {
        // Describe and create a render target view (RTV) descriptor heap.
        D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc;
        ::ZeroMemory(&rtvHeapDesc, sizeof(D3D12_DESCRIPTOR_HEAP_DESC));
        rtvHeapDesc.NumDescriptors = (FrameCount + DefferredCount + NUM_CASCADES + postRenderCount); // depth,normal,diffuse,specaulr + 2
        rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
        rtvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
        rtvHeapDesc.NodeMask = 0;
        ThrowIfFailed(m_device->CreateDescriptorHeap(&rtvHeapDesc, IID_PPV_ARGS(&m_rtvHeap)));


        m_rtvDescriptorSize = m_device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);

        //Create DSV descriptor heap
        rtvHeapDesc.NumDescriptors = 3; // + shadowDsv + copybuffer
        rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
        ThrowIfFailed(m_device->CreateDescriptorHeap(&rtvHeapDesc, IID_PPV_ARGS(&m_dsvHeap)));
        m_dsvDescriptorSize = m_device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_DSV);
    }
}

void FrameWork::CreateRenderTargetViews()
{
    CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle(m_rtvHeap->GetCPUDescriptorHandleForHeapStart());
    // Create a RTV for each frame.
    for (UINT n = 0; n < FrameCount; n++)
    {
        ThrowIfFailed(m_swapChain->GetBuffer(n, IID_PPV_ARGS(&m_renderTargets[n])));
        m_device->CreateRenderTargetView(m_renderTargets[n].Get(), NULL, rtvHandle);
        rtvHandle.Offset(1, m_rtvDescriptorSize);
    }
}

void FrameWork::CreateDepthStencilView()
{

    m_depthResourceDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
    m_depthResourceDesc.Alignment = 0;
    m_depthResourceDesc.Width = FRAME_BUFFER_WIDTH;
    m_depthResourceDesc.Height = FRAME_BUFFER_HEIGHT;
    m_depthResourceDesc.DepthOrArraySize = 1;
    m_depthResourceDesc.MipLevels = 1;
    m_depthResourceDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    m_depthResourceDesc.SampleDesc.Count = (m_bMsaa4xEnable) ? 4 : 1;
    m_depthResourceDesc.SampleDesc.Quality = (m_bMsaa4xEnable) ? (m_nMsaa4xQualityLevels - 1) : 0;
    m_depthResourceDesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
    m_depthResourceDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;

    ::ZeroMemory(&m_depthHeapProperties, sizeof(D3D12_HEAP_PROPERTIES));
    m_depthHeapProperties.Type = D3D12_HEAP_TYPE_DEFAULT;
    m_depthHeapProperties.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
    m_depthHeapProperties.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
    m_depthHeapProperties.CreationNodeMask = 1;
    m_depthHeapProperties.VisibleNodeMask = 1;

    m_depthClearValue.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    m_depthClearValue.DepthStencil.Depth = 1.0f;
    m_depthClearValue.DepthStencil.Stencil = 0;

    m_device->CreateCommittedResource(&m_depthHeapProperties, D3D12_HEAP_FLAG_NONE, &m_depthResourceDesc, D3D12_RESOURCE_STATE_DEPTH_WRITE, &m_depthClearValue, __uuidof(ID3D12Resource), (void**)&m_dsvBuffer);

    D3D12_DEPTH_STENCIL_VIEW_DESC d3dDepthStencilViewDesc;
    ::ZeroMemory(&d3dDepthStencilViewDesc, sizeof(D3D12_DEPTH_STENCIL_VIEW_DESC));
    d3dDepthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    d3dDepthStencilViewDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
    d3dDepthStencilViewDesc.Flags = D3D12_DSV_FLAG_NONE;

    m_dsvhandle = m_dsvHeap->GetCPUDescriptorHandleForHeapStart();
    m_device->CreateDepthStencilView(m_dsvBuffer.Get(), &d3dDepthStencilViewDesc, m_dsvhandle);

}

void FrameWork::CreateRenderTexture()
{
    D3D12_CLEAR_VALUE clearValue;
    FLOAT clear[4]{ 1.0f , 1.0f , 1.0f , 1.0f };
    clearValue.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
    clearValue.Color[0] = m_clearColor[0]; clearValue.Color[1] = m_clearColor[1]; clearValue.Color[2] = m_clearColor[2]; clearValue.Color[3] = 1.0f;


    for (int i = 0; i < DefferredCount; ++i) {
        D3D12_CPU_DESCRIPTOR_HANDLE tmpHandle = m_rtvHeap->GetCPUDescriptorHandleForHeapStart();
        tmpHandle.ptr += (FrameCount + i) * m_rtvDescriptorSize;
        m_defferredResources[i] = CreateTexture2DResource(m_device.Get(), m_commandList.Get(), FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT, 1, 1, DXGI_FORMAT_R32G32B32A32_FLOAT, D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET, D3D12_RESOURCE_STATE_RENDER_TARGET, &clearValue);
        m_device->CreateRenderTargetView(m_defferredResources[i], NULL, tmpHandle);
        dfdhandle[i] = tmpHandle;
    }
    for (int i = 0; i < DefferredCount; ++i) {
        m_scene.m_defferedBuffer[i] = m_defferredResources[i];
    }

    for (int i = 0; i < NUM_CASCADES; ++i) {
        D3D12_CPU_DESCRIPTOR_HANDLE tmpHandle = m_rtvHeap->GetCPUDescriptorHandleForHeapStart();
        tmpHandle.ptr += ((FrameCount + DefferredCount) + i) * m_rtvDescriptorSize;
        m_CascadeResources[i] = CreateTexture2DResource(m_device.Get(), m_commandList.Get(), SHADOW_BUFFER_SIZE * (i + 1), SHADOW_BUFFER_SIZE, 1, 1, DXGI_FORMAT_R32G32B32A32_FLOAT, D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET, D3D12_RESOURCE_STATE_RENDER_TARGET, &clearValue);
        m_device->CreateRenderTargetView(m_CascadeResources[i], NULL, tmpHandle);
        cascadehandle[i] = tmpHandle;
    }

    for (int i = 0; i < postRenderCount; ++i) {
        D3D12_CPU_DESCRIPTOR_HANDLE tmpHandle = m_rtvHeap->GetCPUDescriptorHandleForHeapStart();
        tmpHandle.ptr += ((FrameCount + DefferredCount + NUM_CASCADES) + i) * m_rtvDescriptorSize;
        m_PosteffectResources[i] = CreateTexture2DResource(m_device.Get(), m_commandList.Get(), FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT, 1, 1, DXGI_FORMAT_R32G32B32A32_FLOAT, D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET, D3D12_RESOURCE_STATE_RENDER_TARGET, &clearValue);
        m_device->CreateRenderTargetView(m_PosteffectResources[i], NULL, tmpHandle);
        posteffectHandle[i] = tmpHandle;
    }
    m_scene.SetCasCadeBuffer(m_CascadeResources);
    m_scene.SetPostEffectBuffer(m_PosteffectResources);
}

void FrameWork::CreateDepthTexture()
{
    m_depthResourceDesc.Width = SHADOW_BUFFER_SIZE * NUM_CASCADES;
    m_depthResourceDesc.Height = SHADOW_BUFFER_SIZE;
    m_depthResourceDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    m_depthResourceDesc.DepthOrArraySize = 1;
    m_device->CreateCommittedResource(&m_depthHeapProperties, D3D12_HEAP_FLAG_NONE, &m_depthResourceDesc, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, &m_depthClearValue, __uuidof(ID3D12Resource), (void**)&m_shadowBuffer);

    m_depthResourceDesc.Width = FRAME_BUFFER_WIDTH;
    m_depthResourceDesc.Height = FRAME_BUFFER_HEIGHT;
    m_device->CreateCommittedResource(&m_depthHeapProperties, D3D12_HEAP_FLAG_NONE, &m_depthResourceDesc, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, &m_depthClearValue, __uuidof(ID3D12Resource), (void**)&m_copyDsvBuffer);

    D3D12_DEPTH_STENCIL_VIEW_DESC d3dDepthStencilViewDesc;
    ::ZeroMemory(&d3dDepthStencilViewDesc, sizeof(D3D12_DEPTH_STENCIL_VIEW_DESC));
    d3dDepthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    d3dDepthStencilViewDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
    d3dDepthStencilViewDesc.Flags = D3D12_DSV_FLAG_NONE;
    m_Shadowhandle = m_dsvHeap->GetCPUDescriptorHandleForHeapStart();
    m_Shadowhandle.ptr += m_dsvDescriptorSize;
    m_device->CreateDepthStencilView(m_shadowBuffer.Get(), &d3dDepthStencilViewDesc, m_Shadowhandle);

    m_copyDsvHandle.ptr = m_Shadowhandle.ptr + m_dsvDescriptorSize;
    m_device->CreateDepthStencilView(m_copyDsvBuffer.Get(), &d3dDepthStencilViewDesc, m_copyDsvHandle);

    m_scene.SetShadowBuffer(m_shadowBuffer.Get());
    m_scene.SetDepthBuffer(m_copyDsvBuffer.Get());
  
}

void FrameWork::ChangeSwapChainState()
{

}

void FrameWork::CraeteCameraAndPlayer()
{
    // m_camera = new Camera();
    // m_camera->CreateViewMatrix();
    // m_camera->CreateCbBuffer(m_device, m_commandList);
    // m_camera->SetPos(XMFLOAT3(0.0f, 0.0f, 50.0f));
    // m_camera->CreateViewMatrix(XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.0f , 0.0f ,1.0f) , XMFLOAT3(0.0f , 1.0f, 0.0f) );

    //set player model
    vector<vector<GameObject*>> objs = m_scene.GetGameObjects();
    m_player = new Player(m_device, m_commandList, objs[E_SOLDIEROBJ][0], PLAYERMASS);// set dummy to model
    m_camera = m_player->GetCamera();
    m_shadow = new Shadow(m_camera, &m_scene);
}

void FrameWork::BuildObjects()
{
    m_scene.CreateGrapicsRootSignature(m_device);
    m_scene.CreateComputeRootSignature(m_device);

    m_scene.BuildTextures(m_device, m_commandList);

    m_scene.LoadObjectResource(m_device.Get(), m_commandList.Get());
    m_scene.LoadModelResource(m_device.Get(), m_commandList.Get());

    m_scene.BuildSkyBoxObjects(m_device, m_commandList);
    m_scene.BuildTerrainObjcet(m_device, m_commandList); //CBV
    m_scene.BuildParticleObjects(m_device, m_commandList);
    m_scene.BuildEffectObjects(m_device, m_commandList); //
    m_scene.BuildCubeObjects(m_device, m_commandList); //CBV
    int mSize = m_scene.GetModelVec().size();

    //스킨모델이랑 계층모델이랑 분리해서 한다 
    for (int i = 0; i < mSize; ++i) {
        m_scene.BuildInstancingSkinnedObjects(m_device, m_commandList, i);
    }
    int Osize = 0;

    m_scene.BuildInstancingObjects(m_device, m_commandList, Osize);
    //blendGrassObjects
    m_scene.BuildRealGrassObjects(m_device, m_commandList); 
    m_scene.BuildBillBoardObjects(m_device, m_commandList);
    m_scene.BuildUIObjects(m_device, m_commandList);
    m_scene.BuildLightObjects(m_device, m_commandList);
    m_scene.BuildPostObjects(m_device, m_commandList);
    m_scene.BuildAstarNode(m_device, m_commandList);

    //setParticlePos
    m_scene.UpdateObjectPosition();
    m_scene.SetParticlePosition(m_commandList.Get());
}

void FrameWork::BuildShadow()
{
    m_shadow->CreateLightCamera(m_device, m_commandList);
    m_shadow->InitCbInform(m_device.Get(), m_commandList.Get());
}

void FrameWork::ReleaseObjects()
{
}

void FrameWork::AnimateObjects()
{
    //upadate player state
    m_player->UpdatePlayerWorld();
    m_scene.GetLight()->lights[2].position = m_player->GetPosition();
    m_scene.GetLight()->lights[2].position.y;
    m_scene.GetLight()->lights[2].direction = m_player->GetLook();

    //frustum culling
    m_scene.UpdateFrustumCulling(m_camera->GetFrustum());

    //update object state
    m_scene.UpdateInstanceObjInform(m_timer.GetTimeElapsed() , m_camera->GetPosition() , m_player->GetPosition()); // 인스턴스 객체들의 위치를 update해준다.
    m_scene.UpdateObjectPosition(); // 모든 오브젝트에 적용되는 위치를 업데이트해준다.
    m_scene.UpdateDerationalLight(m_timer.GetTimeElapsed());
    m_shadow->PrePareShadowMap();
   
}

void FrameWork::FrameAdvance()
{
    m_timer.Tick(0.0f);

    PrePareDraw(); //set resource, Reset CommandList

    //process
    m_processInput.processInputMessage(m_device, m_commandList, m_hWnd, m_player);
    //animate
    AnimateObjects();
    OnCompute(); //compute Skinning Animation and Particles 
    PrePareShadow(); // OMsetRenderTarget and clear DepthBuffer
    DrawShadow(); // Rendering Shadow
    ::ResourceBarrier(m_commandList.Get(), m_shadowBuffer.Get(), D3D12_RESOURCE_STATE_DEPTH_WRITE, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);

    UpdateDefferredState();  //update Camera ,Player cbBuffer and set Compute
    PrePareDefferred(); // clear RenderTarget, DepthBuffer 
    OnDraw();  //defferred Draw call -> Draw diffuse , normal ,specualr, Depth
    //render to texture
    PrePareDefferedRender(); // clear RenderTarget and copyResource 
    //final draw 
    m_scene.OnLightRender(m_commandList); // caluate light and Rendering 

    ////draw posteffect
    PreParePostRender(); // Rendering posEffect
    m_scene.OnPostRender(m_commandList);
    //forward Rendering -> Blending objects -> particles and effects
    m_commandList->ClearDepthStencilView(m_dsvhandle, D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, 1.0f, 0, 0, NULL);
    CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle(m_rtvHeap->GetCPUDescriptorHandleForHeapStart(), m_frameIndex, m_rtvDescriptorSize);
    m_commandList->OMSetRenderTargets(1, &rtvHandle, TRUE, &m_dsvhandle);

    m_scene.OnUIRender(m_commandList);
    m_scene.OnEffectRender(m_commandList);
    m_scene.OnParticleRender(m_commandList, m_camera->GetPosition());
    //rendering over
    m_commandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(m_renderTargets[m_frameIndex].Get(), D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT));
    ThrowIfFailed(m_commandList->Close());
    ID3D12CommandList* ppCommandLists[] = { m_commandList.Get() };
    m_commandQueue->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);

    // Present the frame.
    ThrowIfFailed(m_swapChain->Present(0, 0));

    WaitForPreviousFrame();

    m_timer.GetFrameRate(m_pszFrameRate + 12, 37);

    XMFLOAT3 xmf3Position = m_player->GetPosition();
    size_t nLength = _tcslen(m_pszFrameRate);
    _stprintf_s(m_pszFrameRate + nLength, 70 - nLength, _T("(%f, %f, %f)"), xmf3Position.x, xmf3Position.y, xmf3Position.z);

    ::SetWindowText(m_hWnd, m_pszFrameRate);

}

void FrameWork::PrePareDraw()
{
    ThrowIfFailed(m_commandAllocator->Reset());
    ThrowIfFailed(m_commandList->Reset(m_commandAllocator.Get(), NULL));

    m_commandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(m_renderTargets[m_frameIndex].Get(), D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET));

    m_commandList->SetGraphicsRootSignature(m_scene.GetGraphicsRS().Get());
    m_commandList->SetComputeRootSignature(m_scene.GetComputeRS().Get());

    //set compute
    float elapsed = m_timer.GetTimeElapsed();
    float total = m_timer.GetTotalTime();
    XMFLOAT3 cPos = m_camera->GetPosition();
    XMFLOAT3 sPos = m_scene.GetSunPos();
    XMFLOAT3 camPos = Vector3::TransformCoord(sPos, m_camera->GetView());
    XMFLOAT3 pixelSpos = Vector3::TransformCoord(Vector3::TransformCoord(sPos, m_camera->GetView()) , m_camera->GetProj());//원근나눗셈까지 해준것
    XMFLOAT3 tempPos = Vector3::TransformCoord(pixelSpos, Matrix4x4::Inverse(m_camera->GetProj()));

    XMFLOAT4X4 gmtxProjectToTexture = { 0.5f, 0.0f, 0.0f, 0.0f, 0.0f, -0.5f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.5f, 0.5f, 0.0f, 1.0f };
    XMFLOAT3 rpos = Vector3::TransformCoord(pixelSpos, gmtxProjectToTexture); // 태양을 바라보고 있따면 0~1
    m_camera->CreateFrustum();

    m_commandList->SetComputeRoot32BitConstants(ComputeRootTimeInfo, 1, &total, 0);
    m_commandList->SetComputeRoot32BitConstants(ComputeRootTimeInfo, 1, &elapsed, 1);

    m_commandList->SetComputeRoot32BitConstants(ComputeRootParticleCameraPos, 3, &pixelSpos, 0);

    //set grapics root constants
    m_commandList->SetGraphicsRoot32BitConstants(GraphicsRootTimer, 1, &total, 0);
    m_commandList->SetGraphicsRoot32BitConstants(GraphicsRootTimer, 1, &elapsed, 1);

    m_commandList->SetGraphicsRoot32BitConstants(GraphicsSunPosData, 3, &rpos, 0);
    m_commandList->SetGraphicsRoot32BitConstants(GraphicsSunPosData, 1, &camPos.z, 3);
    m_scene.PrePareRender(m_commandList);
}

void FrameWork::OnDraw()
{
    m_scene.OnRender(m_commandList);
}

void FrameWork::OnCompute()
{

    m_scene.PrePareCompute(m_commandList.Get());
}

void FrameWork::PrePareShadow()
{
    for (int i = 0; i < NUM_CASCADES; ++i)
        m_commandList->ClearRenderTargetView(cascadehandle[i], m_clearColor, 0, nullptr);

    ::ResourceBarrier(m_commandList.Get(), m_shadowBuffer.Get(), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_DEPTH_WRITE);
    m_commandList->ClearDepthStencilView(m_Shadowhandle, D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, 1.0f, 0, 0, NULL);
}

void FrameWork::DrawShadow()
{
    m_shadow->UpdateCbState(m_commandList.Get());
    for (int i = 0; i < NUM_CASCADES; ++i) { //rendering shadow
        UpdateShadowState(i);
        m_scene.OnShadwRender(m_commandList);
    }
}

void FrameWork::PrePareDefferedRender()
{
    ::ResourceBarrier(m_commandList.Get(), m_dsvBuffer.Get(), D3D12_RESOURCE_STATE_DEPTH_WRITE, D3D12_RESOURCE_STATE_COPY_SOURCE);
    ::ResourceBarrier(m_commandList.Get(), m_copyDsvBuffer.Get(), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_COPY_DEST);
    m_commandList.Get()->CopyResource(m_copyDsvBuffer.Get(), m_dsvBuffer.Get());
    ::ResourceBarrier(m_commandList.Get(), m_dsvBuffer.Get(), D3D12_RESOURCE_STATE_COPY_SOURCE, D3D12_RESOURCE_STATE_DEPTH_WRITE);
    ::ResourceBarrier(m_commandList.Get(), m_copyDsvBuffer.Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);


    m_commandList->OMSetRenderTargets(1, &posteffectHandle[0], TRUE, &m_dsvhandle);
    m_commandList->ClearRenderTargetView(posteffectHandle[0], m_clearColor, 0, nullptr);
    m_commandList->ClearDepthStencilView(m_dsvhandle, D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, 1.0f, 0, 0, NULL);
}

void FrameWork::PreParePostRender()
{
   CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle(m_rtvHeap->GetCPUDescriptorHandleForHeapStart(), m_frameIndex, m_rtvDescriptorSize);
   m_commandList->OMSetRenderTargets(1, &rtvHandle, TRUE, NULL);
   m_commandList->ClearRenderTargetView(rtvHandle, m_clearColor, 0, nullptr);
}



void FrameWork::PrePareDefferred()
{
    for (int i = 0; i < DefferredCount; ++i) {
        m_commandList->ClearRenderTargetView(dfdhandle[i], m_clearColor, 0, nullptr);
    }
    m_commandList->ClearDepthStencilView(m_dsvhandle, D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, 1.0f, 0, 0, NULL);
    m_commandList->OMSetRenderTargets(DefferredCount, dfdhandle, FALSE, &m_dsvhandle); //diffuse , normal , depth , spec
}

void FrameWork::DrawDefferred()
{
}


void FrameWork::WaitForPreviousFrame()
{
    const UINT64 fence = m_fenceValue;
    ThrowIfFailed(m_commandQueue->Signal(m_fence.Get(), fence));
    m_fenceValue++;

    // Wait until the previous frame is finished.
    if (m_fence->GetCompletedValue() < fence)
    {
        ThrowIfFailed(m_fence->SetEventOnCompletion(fence, m_fenceEvent));
        WaitForSingleObject(m_fenceEvent, INFINITE);
    }

    m_frameIndex = m_swapChain->GetCurrentBackBufferIndex();
}

void FrameWork::WaitForGPURunning()
{
    const UINT64 fence = m_fenceValue;
    ThrowIfFailed(m_commandQueue->Signal(m_fence.Get(), fence));
    m_fenceValue++;

    // Wait until the previous frame is finished.
    if (m_fence->GetCompletedValue() < fence)
    {
        ThrowIfFailed(m_fence->SetEventOnCompletion(fence, m_fenceEvent));
        WaitForSingleObject(m_fenceEvent, INFINITE);
    }
}

void FrameWork::UpdateState()
{
    m_camera->UpateCbBuffer(m_commandList);
    m_camera->SetViewportsAndScissorRects(m_commandList);
}

void FrameWork::UpdateDefferredState()
{
    m_camera->UpateCbBuffer(m_commandList);
    m_camera->SetViewportsAndScissorRects(m_commandList);
}

void FrameWork::UpdateShadowState(int CameraIdx)
{
    m_commandList->OMSetRenderTargets(1, &cascadehandle[CameraIdx], FALSE, &m_Shadowhandle);
    m_commandList->SetGraphicsRoot32BitConstants(GraphicsRootCameraNumber, 1, &CameraIdx, 0);
    m_camera->SetLightViewportsAndScissorRects(m_shadow->GetLightCamera(CameraIdx), m_commandList);
}


void FrameWork::OnProcessingMouseMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam)
{
    switch (nMessageID)
    {
    case WM_LBUTTONDOWN:
    case WM_RBUTTONDOWN:
        ::SetCapture(hWnd);
        POINT oldCusor;
        ::GetCursorPos(&oldCusor);
        m_processInput.SetOldCusor(oldCusor);
        break;
    case WM_LBUTTONUP:
    case WM_RBUTTONUP:
        ::ReleaseCapture();
        break;
    case WM_MOUSEMOVE:
        break;
    default:
        break;
    }

}

void FrameWork::OnProcessingKeyboardMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam)
{
    switch (nMessageID)
    {
    case WM_KEYUP:
        switch (wParam)
        {
        case VK_ESCAPE:
            ::PostQuitMessage(0);
            break;
        case VK_RETURN:
            break;
        case VK_F1:
        case VK_F2:
        case VK_F3:
            m_camera = m_player->ChangeCamera((DWORD)(wParam - VK_F1 + 1), m_timer.GetTimeElapsed());
            m_shadow->SetMainCamera(m_camera);
            break;
        case VK_F9:
        {
            /* BOOL bFullScreenState = FALSE;
             m_pdxgiSwapChain->GetFullscreenState(&bFullScreenState, NULL);
             m_pdxgiSwapChain->SetFullscreenState(!bFullScreenState, NULL);

             DXGI_MODE_DESC dxgiTargetParameters;
             dxgiTargetParameters.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
             dxgiTargetParameters.Width = m_nWndClientWidth;
             dxgiTargetParameters.Height = m_nWndClientHeight;
             dxgiTargetParameters.RefreshRate.Numerator = 60;
             dxgiTargetParameters.RefreshRate.Denominator = 1;
             dxgiTargetParameters.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
             dxgiTargetParameters.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
             m_pdxgiSwapChain->ResizeTarget(&dxgiTargetParameters);

             OnResizeBackBuffers();*/

            break;
        }
        case VK_F10:
            break;
        default:
            break;
        }
        break;
    default:
        break;
    }

}

LRESULT FrameWork::OnProcessingWindowMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam)
{
    switch (nMessageID)
    {
    case WM_ACTIVATE:
    {
        if (LOWORD(wParam) == WA_INACTIVE)
            m_timer.Stop();
        else
            m_timer.Start();
        break;
    }
    case WM_SIZE:
    {
        m_Width = LOWORD(lParam);
        m_Height = HIWORD(lParam);

       // OnResizeBackBuffers();
        break;
    }
    case WM_LBUTTONDOWN:
    case WM_RBUTTONDOWN:
    case WM_LBUTTONUP:
    case WM_RBUTTONUP:
    case WM_MOUSEMOVE:
        OnProcessingMouseMessage(hWnd, nMessageID, wParam, lParam);
        break;
    case WM_KEYDOWN:
    case WM_KEYUP:
        OnProcessingKeyboardMessage(hWnd, nMessageID, wParam, lParam);
        break;
    }
    return(0);
}

void FrameWork::GetHardwareAdapter(IDXGIFactory1* pFactory, IDXGIAdapter1** ppAdapter, bool requestHighPerformanceAdapter)
{
    *ppAdapter = nullptr;

    ComPtr<IDXGIAdapter1> adapter;

    ComPtr<IDXGIFactory6> factory6;
    if (SUCCEEDED(pFactory->QueryInterface(IID_PPV_ARGS(&factory6))))
    {
        for (
            UINT adapterIndex = 0;
            DXGI_ERROR_NOT_FOUND != factory6->EnumAdapterByGpuPreference(
                adapterIndex,
                requestHighPerformanceAdapter == true ? DXGI_GPU_PREFERENCE_HIGH_PERFORMANCE : DXGI_GPU_PREFERENCE_UNSPECIFIED,
                IID_PPV_ARGS(&adapter));
            ++adapterIndex)
        {
            DXGI_ADAPTER_DESC1 desc;
            adapter->GetDesc1(&desc);

            if (desc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE)
            {
                // Don't select the Basic Render Driver adapter.
                // If you want a software adapter, pass in "/warp" on the command line.
                continue;
            }

            // Check to see whether the adapter supports Direct3D 12, but don't create the
            // actual device yet.
            if (SUCCEEDED(D3D12CreateDevice(adapter.Get(), D3D_FEATURE_LEVEL_11_0, _uuidof(ID3D12Device), nullptr)))
            {
                break;
            }
        }
    }
    else
    {
        for (UINT adapterIndex = 0; DXGI_ERROR_NOT_FOUND != pFactory->EnumAdapters1(adapterIndex, &adapter); ++adapterIndex)
        {
            DXGI_ADAPTER_DESC1 desc;
            adapter->GetDesc1(&desc);

            if (desc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE)
            {
                // Don't select the Basic Render Driver adapter.
                // If you want a software adapter, pass in "/warp" on the command line.
                continue;
            }

            // Check to see whether the adapter supports Direct3D 12, but don't create the
            // actual device yet.
            if (SUCCEEDED(D3D12CreateDevice(adapter.Get(), D3D_FEATURE_LEVEL_11_0, _uuidof(ID3D12Device), nullptr)))
            {
                break;
            }
        }
    }

    *ppAdapter = adapter.Detach();

}

void FrameWork::OnResizeBackBuffers()
{
    WaitForGPURunning();
    m_commandList->Reset(m_commandAllocator.Get(), NULL);

    for (int i = 0; i < FrameCount; i++) m_renderTargets[i].Reset();
    if (m_dsvBuffer) m_dsvBuffer.Reset();

#ifdef _WITH_ONLY_RESIZE_BACKBUFFERS
    DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
    m_pdxgiSwapChain->GetDesc(&dxgiSwapChainDesc);
    m_pdxgiSwapChain->ResizeBuffers(0, 0, 0, DXGI_FORMAT_UNKNOWN, 0);
#else
    //m_pdxgiSwapChain->ResizeBuffers(0, 0, 0, DXGI_FORMAT_UNKNOWN, DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH);
    DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
    m_swapChain.Get()->GetDesc(&dxgiSwapChainDesc);
    m_swapChain.Get()->ResizeBuffers(FrameCount, m_Width, m_Height, dxgiSwapChainDesc.BufferDesc.Format, dxgiSwapChainDesc.Flags);
#endif
    m_frameIndex = 0;

    CreateRenderTargetViews();
    CreateDepthStencilView();

    m_commandList.Get()->Close();

    ID3D12CommandList* ppd3dCommandLists[] = { m_commandList.Get() };
    m_commandQueue->ExecuteCommandLists(1, ppd3dCommandLists);
    WaitForGPURunning();

}

