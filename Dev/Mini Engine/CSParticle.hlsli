cbuffer cbTimeInfo : register(b0)
{
    float gfTotalTime;
    float gfTimeElapsed;
}

cbuffer cbParticleType : register(b3)
{
    int guParticleType;
    int bidx; //buffer idx 
}

cbuffer cbParticleType : register(b4)
{
    float3 gCameraPos;
}

struct ParticleData
{
    float3 position;
    float3 velocity;
    float age;
};
