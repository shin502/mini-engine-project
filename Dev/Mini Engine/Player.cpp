#include "Player.h"
#include "HierarchyObject.h"
#include "AnimationObject.h"

Player::Player(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList, GameObject* model , float pMass)
{
	m_model = model;
	m_model->SetisPlayer(true);
	
	SetPosition(XMFLOAT3(0.0f, 100.0f, 100.0f));
	m_camera = ChangeCamera(SPACESHIP_CAMERA, 0.0f);
	m_camera->CreateCbBuffer(device, commandList);

	m_fPitch = 0.0f;
	m_fRoll = 0.0f;
	m_fYaw = 0.0f;

	m_look = GetLook();
	m_up = GetUp();
	m_right = GetRight();

	m_mass = pMass;
	m_damping = XMFLOAT3(PLAYERDAMPING, PLAYERDAMPING, PLAYERDAMPING);
	m_gravity = XMFLOAT3(0.0f, -PLAYERGRAVITY, 0.0f);
	m_friction = XMFLOAT3(FRICTION, FRICTION, FRICTION);
	m_maxVel = MAXVEL;

	obbBox.Center = XMFLOAT3(0.0f, 0.0f, 0.0f);
	obbBox.Extents = XMFLOAT3(0.01f, 0.01f, 0.01f);
	obbBox.Orientation = XMFLOAT4(0.0f, 0.0f, 0.0f , 1.0f);
	obbBox.Transform(obbBox, XMLoadFloat4x4(&this->GetWorld()));
}
void Player::ChangeModel(int modelNumber)
{
}
void Player::Move(int dir , float f , float elapsedTime)
{
	XMFLOAT3 shift = XMFLOAT3(0.0f, 0.0f, 0.0f);
	if (m_jumpCool == true) {
		shift = XMFLOAT3(0.0f, -PLAYERGRAVITY * 50.0f, 0.0f);
	}
	else {
		m_velocity.y = 0.0;
	}
	//calculateVelocity;
	
	if (dir & DIR_FORWARD) 
		shift = Vector3::Add(shift, GetLook(), f);
	if(dir & DIR_RIGHT)	
		shift = Vector3::Add(shift, GetRight(), f);
	if(dir & DIR_BACKWARD) 
		shift = Vector3::Add(shift, GetLook(), -f);
	if(dir & DIR_LEFT)
		shift = Vector3::Add(shift, GetRight(), -f);
	if ((dir & DIR_JUMP) && m_jumpCool == false) 
	{
		m_velocity.y += JUMPPOWER;
		m_jumping = true;
	}
	//shift�� ���̴�.
	m_acc = Vector3::ScalarProduct(shift, 1 / m_mass, false);
	m_velocity = Vector3::Add(Vector3::ScalarProduct(m_velocity , PLAYERDAMPING, false), Vector3::ScalarProduct(m_acc, elapsedTime, false));
	CheckMaxVel(m_velocity);
	ApplyFriction(m_velocity, elapsedTime);
	
	// s = s0 + vt + 1/2at*t
	XMFLOAT3 evel = Vector3::ScalarProduct(m_velocity, elapsedTime, false);
	XMFLOAT3 eacc = Vector3::ScalarProduct(m_acc, elapsedTime * elapsedTime * 0.5, false);
	XMFLOAT3 fmove = Vector3::Subtract(evel, eacc);



	SetPosition(Vector3::Add(GetPosition(), fmove));
	//obbBox update
	obbBox.Center = XMFLOAT3(0.0f, 0.0f, 0.0f);
	obbBox.Orientation = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
	obbBox.Transform(obbBox, XMLoadFloat4x4(&this->GetWorld()));

	m_camera->Move(fmove);
}

void Player::Rotate(float x, float y, float z)
{
	int currentCameraMode = m_camera->GetMode();
	if (currentCameraMode == FIRST_PERSON_CAMERA || currentCameraMode == THIRD_PERSON_CAMERA) {
		if (x != 0.0f)
		{
			if (m_fPitch + x < 20.0f && m_fPitch + x > -60.0f) {
				m_fPitch += x;
			}
		}
		if (y != 0.0f)
		{
			m_fYaw += y;
			if (m_fYaw > 360.0f) m_fYaw -= 360.0f;
			if (m_fYaw < 0.0f) m_fYaw += 360.0f;
		}
		if (z != 0.0f)
		{
			m_fRoll += z;
			if (m_fRoll > +20.0f) { z -= (m_fRoll - 20.0f); m_fRoll = +20.0f; }
			if (m_fRoll < -20.0f) { z -= (m_fRoll + 20.0f); m_fRoll = -20.0f; }
		}
		m_camera->Rotate(x, y, z);
		if (x != 0.0f)
		{
			XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&GetRight()), XMConvertToRadians(x));
		}
		if (y != 0.0f)
		{
			XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&GetUp()), XMConvertToRadians(y));
			SetLook(Vector3::TransformNormal(GetLook(), xmmtxRotate));
			SetRight(Vector3::TransformNormal(GetRight(), xmmtxRotate));

			
		}
	}
	else if (currentCameraMode == SPACESHIP_CAMERA)
	{
		m_camera->Rotate(x, y, z);
		if (x != 0.0f)
		{
			XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&GetRight()), XMConvertToRadians(x));
			SetLook(Vector3::TransformNormal(GetLook(), xmmtxRotate));
			SetUp(Vector3::TransformNormal(GetUp(), xmmtxRotate));
		}
		if (y != 0.0f)
		{
			XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&GetUp()), XMConvertToRadians(y));
			SetLook(Vector3::TransformNormal(GetLook(), xmmtxRotate));
			SetRight(Vector3::TransformNormal(GetRight(), xmmtxRotate));
		}
		if (z != 0.0f)
		{
			XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&GetLook()), XMConvertToRadians(z));
			SetUp(Vector3::TransformNormal(GetUp(), xmmtxRotate));
			SetRight(Vector3::TransformNormal(GetRight(), xmmtxRotate));
		}
	}

	SetLook(Vector3::Normalize(GetLook()));
	SetRight(Vector3::CrossProduct(GetUp(), GetLook(), true));
	SetUp(Vector3::CrossProduct(GetLook(), GetRight(), true));

	SetLookVector(GetLook());
	SetRightVector(GetUp());
	SetUpVector(GetUp());

	XMMATRIX xmmtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&GetRight()), XMConvertToRadians(m_fPitch));
	SetLookVector(Vector3::TransformNormal(GetLookVector(), xmmtxRotate));
	SetUpVector(Vector3::TransformNormal(GetUpVector(), xmmtxRotate));

	SetLookVector(Vector3::Normalize(GetLookVector()));
	SetRightVector(Vector3::CrossProduct(GetUpVector(), GetLookVector(), true));
	SetUpVector(Vector3::CrossProduct(GetLookVector(), GetRightVector(), true));

}

void Player::Update(float fTimeElapsed)
{
	DWORD nCurrentCameraMode = m_camera->GetMode();
	if (nCurrentCameraMode == THIRD_PERSON_CAMERA) m_camera->Update(GetPosition(), fTimeElapsed);
	if (nCurrentCameraMode == THIRD_PERSON_CAMERA) m_camera->SetLookAt(GetPosition());
	
	m_camera->ReCreateViewMatrix();

	//obbBox update
	obbBox.Center = XMFLOAT3(0.0f, 0.0f, 0.0f);
	obbBox.Orientation = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
	obbBox.Transform(obbBox, XMLoadFloat4x4(&this->GetWorld()));
}

void Player::ChangeCamera(DWORD nNewCameraMode, DWORD nCurrentCamreaMode)
{
	Camera* newCamera = NULL;
	switch (nNewCameraMode) {
	case FIRST_PERSON_CAMERA:
		newCamera = new FirstPersonCamera(m_camera);
		break;
	case THIRD_PERSON_CAMERA:
		 newCamera = new ThirdPersonCamera(m_camera);
		 break;
	case SPACESHIP_CAMERA:
		newCamera = new SpaceShipCamera(m_camera);
		break;
	}
	if (nCurrentCamreaMode == SPACESHIP_CAMERA)
	{
		XMFLOAT3 pRight = GetRight();
		XMFLOAT3 pUp = GetUp();
		XMFLOAT3 pLook = GetLook();


		SetRight(Vector3::Normalize(XMFLOAT3(pRight.x, 0.0f, pRight.z)));
		SetUp(Vector3::Normalize(XMFLOAT3(0.0f, 1.0f, 0.0f)));
		SetLook(Vector3::Normalize(XMFLOAT3(pLook.x, 0.0f, pLook.z)));

		m_fPitch = 0.0f;
		m_fRoll = 0.0f;
		m_fYaw = Vector3::Angle(XMFLOAT3(0.0f, 0.0f, 1.0f), GetLook());
		if (GetLook().x < 0.0f) m_fYaw = -m_fYaw;
	}
	else if ((nNewCameraMode == SPACESHIP_CAMERA) && m_camera)
	{
		SetRight(m_camera->GetLocalRight());
		SetUp(m_camera->GetLocalUp());
		SetLook(m_camera->GetLocalLook());
	}

	if (newCamera)
	{
		//		pNewCamera->SetMode(nNewCameraMode);
		newCamera->SetPlayer(this);
	}

	if (m_camera) delete m_camera;

	m_camera = newCamera;
}

Camera* Player::ChangeCamera(DWORD nNewCameraMode, float fTimeElapsed)
{
	DWORD nCurrentCameraMode = (m_camera) ? m_camera->GetMode() : 0x00;
	if (nCurrentCameraMode == nNewCameraMode) return(m_camera);
	switch (nNewCameraMode)
	{
	case FIRST_PERSON_CAMERA:
		ChangeCamera(FIRST_PERSON_CAMERA, nCurrentCameraMode);
		m_camera->SetTimeLag(0.0f);
		m_camera->SetOffset(XMFLOAT3(0.0f, -8.8f, 0.0f));
		m_camera->CreateProjectionMatrix(1.01f, 5000.0f, ASPECT_RATIO, 60.0f);
		m_camera->SetViewport(0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT, 0.0f, 1.0f);
		m_camera->SetDefferredViewPort(0, 0, 40.0f, 40.0f, 0.0f, 1.0f);
		m_camera->SetScissorRect(0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT);
		m_camera->SetDefferredScissorRect(0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT);
		break;
	case SPACESHIP_CAMERA:
		ChangeCamera(SPACESHIP_CAMERA, nCurrentCameraMode);
		m_camera->SetTimeLag(0.0f);
		m_camera->SetOffset(XMFLOAT3(0.0f, 0.0f, 0.0f));
		m_camera->CreateProjectionMatrix(1.01f, 5000.0f, ASPECT_RATIO, 60.0f);
		m_camera->SetViewport(0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT, 0.0f, 1.0f);
		m_camera->SetDefferredViewPort(0, 0, 40.0f, 40.0f, 0.0f, 1.0f);
		m_camera->SetScissorRect(0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT);
		m_camera->SetDefferredScissorRect(0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT);
		break;
	case THIRD_PERSON_CAMERA:
		ChangeCamera(THIRD_PERSON_CAMERA, nCurrentCameraMode);
		m_camera->SetTimeLag(0.25f);
		m_camera->SetOffset(XMFLOAT3(0.0f, 5.0f, -2.5f));
		m_camera->CreateProjectionMatrix(1.01f, 5000.0f, ASPECT_RATIO, 60.0f);
		m_camera->SetDefferredViewPort(0, 0, 40.0f, 40.0f, 0.0f, 1.0f);
		m_camera->SetViewport(0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT, 0.0f, 1.0f);
		m_camera->SetScissorRect(0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT);
		m_camera->SetDefferredScissorRect(0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT);
		break;
	default:
		break;
	}

	m_camera->SetPos(Vector3::Add(GetPosition(), m_camera->GetOffset()));
	Update(fTimeElapsed);

	return(m_camera);
}

void Player::ChangePlayer(GameObject* model)
{
	m_model->SetisPlayer(false);
	m_model = model;
	m_model->SetisPlayer(true);
}

void Player::UpdatePlayerWorld()
{
	m_model->SetTransForm(m_world);
}

XMFLOAT3 Player::GetLookVector()
{
	return m_look;
}

XMFLOAT3 Player::GetUpVector()
{
	return m_up;
}

XMFLOAT3 Player::GetRightVector()
{
	return m_right;
}

XMFLOAT3 Player::GetVel()
{
	return m_velocity;
}

GameObject* Player::GetModel()
{
	return m_model;
}

void Player::SetLookVector(XMFLOAT3 lookvector)
{
	m_look = lookvector;
}

void Player::SetRightVector(XMFLOAT3 rightvector)
{
	m_right = rightvector;
}

void Player::SetUpVector(XMFLOAT3 upvector)
{
	m_up = upvector;
}

void Player::SetPlayerMass(float mass)
{
	m_mass = mass;
}

void Player::SetJumpCool(bool jumpCool)
{
	m_jumpCool = jumpCool;
}

int Player::GetStatusBody()
{
	return m_statusBody;
}

int Player::GetStatusLeg()
{
	return m_statusLeg;
}

int Player::GetMass()
{
	return m_mass;
}

bool Player::GetJumpCool()
{
	return m_jumpCool;
}

void Player::GetAnimationNum(UCHAR keys[256])
{
	int statusBody = 0, statusLeg = 0;

	if (keys[87] & 0xF0) {
		statusLeg = MainCharacterWalk;
		// ��	
	}
	if (keys[83] & 0xF0) {
		if (statusLeg == MainCharacterWalk)
			statusLeg = MainCharacterIdle;
		else
			statusLeg = MainCharacterWalkBack;
		// ��
	}
	if (keys[65] & 0xF0) {
		if (statusLeg != MainCharacterWalk || statusLeg != MainCharacterWalkBack)
			statusLeg = MainCharacterWalkLeft;
		// ��
	}
	if (keys[68] & 0xF0) {
		if (statusLeg != MainCharacterWalk || statusLeg != MainCharacterWalkBack)
			statusLeg = MainCharacterWalkRight;
		else if (statusLeg == MainCharacterWalkRight)
			statusLeg = MainCharacterIdle;
		// ��
	}
	statusBody = statusLeg;

	if (keys[VK_RBUTTON] & 0xF0) {
		statusBody = MainCharacterAim;
	}

	if (keys[VK_LBUTTON] & 0xF0) {
		if (statusBody == MainCharacterWalk || statusBody == MainCharacterWalkBack) {
			statusBody = MainCharacterWalkingFire;
			statusLeg = MainCharacterWalkingFire;
		}
		else
			statusBody = MainCharacterFire;
	}

	//R button
	if (keys[82] & 0xF0) {
		if (statusLeg != MainCharacterIdle  || statusBody != MainCharacterIdle) {
			statusBody = MainCharacterWalkingReload;
			statusLeg = MainCharacterWalkingReload;
		}
		else
		{
			statusLeg = MainCharacterReload;
			statusBody = MainCharacterReload;
		}
	}

	//Shift
	if (keys[160] & 0xF0) {
		if (statusBody == statusLeg) {
			if (statusLeg == MainCharacterWalk)
				statusLeg = MainCharacterRun;
			else if (statusLeg == MainCharacterWalkBack)
				statusLeg = MainCharacterRunBack;
			else if (statusLeg == MainCharacterWalkLeft)
				statusLeg = MainCharacterRunLeft;
			else if (statusLeg == MainCharacterWalkRight)
				statusLeg = MainCharacterRunRight;

			statusBody = statusLeg;
		}
	}
	m_statusBody = statusBody;
	m_statusLeg = statusLeg;

	ChangePlayerAnimation(statusBody, statusLeg);


}

void Player::ChangePlayerAnimation(int status1, int status2)
{
	HierarchyObj* tmpModel = (HierarchyObj*)m_model;
	AnimationController* skinnedAnimationController = tmpModel->skinnedAnimationController;
	
	if (skinnedAnimationController->animationSets->animationSets[skinnedAnimationController->animationTracks[0].upanimationSet]->lockanimation) return;

	if (skinnedAnimationController->animationSets->animationSets[skinnedAnimationController->animationTracks[0].upanimationSet]->lockupanimation == false)
	{
		if (tmpModel->upAnimationStatus != status1 && tmpModel->loAnimationStatus != status2)
		{
			skinnedAnimationController->SetTrackAnimationSet(0, status1, status2);
			tmpModel->upAnimationStatus = status1;
			tmpModel->loAnimationStatus = status2;
		}
		else if (tmpModel->upAnimationStatus != status1)
		{
			skinnedAnimationController->SetTrackAnimationSet(0, status1, tmpModel->loAnimationStatus);
			tmpModel->upAnimationStatus = status1;
		}
		else if (tmpModel->loAnimationStatus != status2)
		{
			skinnedAnimationController->SetTrackAnimationSet(0, tmpModel->upAnimationStatus, status2);
			tmpModel->loAnimationStatus = status2;
		}
	}
	else
	{
		if (tmpModel->loAnimationStatus != status2)
		{
			skinnedAnimationController->SetTrackAnimationSet(0, tmpModel->upAnimationStatus, status2);
			tmpModel->loAnimationStatus = status2;
		}
	}

	if (skinnedAnimationController->animationSets->animationSets[skinnedAnimationController->animationTracks[0].upanimationSet]->lockupanimation == false)
	{
		if (status1 == MainCharacterReload || status1 == MainCharacterWalkingReload)
		{
			skinnedAnimationController->animationSets->animationSets[skinnedAnimationController->animationTracks[0].upanimationSet]->lockupanimation = true;
			skinnedAnimationController->animationTracks[0].position = 0.0f;
		}
	}
}
