#include "GameObject.h"

GameObject::GameObject()
{
   m_world = Matrix4x4::Identity();
   transform = Matrix4x4::Identity();

   m_mass = UNITMASS;
   m_damping = XMFLOAT3(UNITDAMPING, UNITDAMPING, UNITDAMPING);
   m_gravity = XMFLOAT3(0.0f, -UNITGRAVITY, 0.0f);
   m_friction = XMFLOAT3(FRICTION, FRICTION, FRICTION);
   m_maxVel = UNITMAXVEL;
}

GameObject::~GameObject()
{   
    for (int i = 0; i < m_temporaryMesh.size(); ++i) {
        if (m_temporaryMesh[i]) {
            delete m_temporaryMesh[i];
            m_temporaryMesh[i] = NULL;
        }
    }

    for (int i = 0; i < m_mesh.size(); ++i) {
        if (m_mesh[i]) {
            delete m_mesh[i];
            m_mesh[i] = NULL;
        }
    }
    m_mesh.clear();

    if (m_material.size() > 0) {
        for (int i = 0; i < m_material.size(); ++i) {
            if (m_material[i]) {
                delete m_material[i];
                m_material[i] = NULL;
            }
        }
    }

    for (int j = 0; j < textures.size(); ++j) {
        for (int k = 0; k < textures[j]->m_textureNum; ++k) {
            textures[j]->m_textures[k]->Release();
        }
        delete[] textures[j]->m_textures;
        textures[j]->m_textures = NULL;
        delete textures[j];
        textures[j] = NULL;
    }

}

XMFLOAT3 GameObject::GetTransFormScale()
{
    return XMFLOAT3(transform._11, transform._22, transform._33);
}

XMFLOAT3 GameObject::GetPosition()
{
    
    return XMFLOAT3(m_world._41 , m_world._42 , m_world._43);
}

XMFLOAT3 GameObject::GetLook()
{
    return XMFLOAT3(m_world._31, m_world._32, m_world._33);
}

XMFLOAT3 GameObject::GetUp()
{
    return XMFLOAT3(m_world._21, m_world._22, m_world._23);
}

XMFLOAT3 GameObject::GetRight()
{
    return XMFLOAT3(m_world._11, m_world._12, m_world._13);
}

XMFLOAT4X4 GameObject::GetWorld()
{
    return m_world;
}

vector<Material*> GameObject::GetMaterial()
{
    return m_material;
}

int GameObject::GetConstantNumber()
{
    return m_ConstantNumber;
}

bool GameObject::GetisPlayer()
{
    return m_isPlayer;
}

vector<Texture*>& GameObject::GetTextures()
{
    return textures;
}

XMFLOAT3 GameObject::GetTransFormPos()
{
    return XMFLOAT3(transform._41, transform._42, transform._43);
}

XMFLOAT3 GameObject::GetTransFormLook()
{
    return XMFLOAT3(transform._31, transform._32, transform._33);
}

XMFLOAT4X4 GameObject::GetTransForm()
{
    return transform;
}

bool GameObject::GetIsVisable()
{
    return m_isVisable;
}

void GameObject::SetPosition(XMFLOAT3 pos)
{
    m_world._41 = pos.x;
    m_world._42 = pos.y;
    m_world._43 = pos.z;

}
void GameObject::SetScale(XMFLOAT3 scale)
{
    m_world._11 = scale.x;
    m_world._22 = scale.y;
    m_world._33 = scale.z;
}
void GameObject::SetPosition(float x, float y, float z)
{
    m_world._41 = x;
    m_world._42 = y;
    m_world._43 = z;
}
void GameObject::SetHeight(float height)
{
    m_world._42 = height;
}
void GameObject::SetTransformHeight(float height)
{
    transform._42 = height;
}
void GameObject::SetTransFormPos(float x, float y, float z)
{
    transform._41 = x;
    transform._42 = y;
    transform._43 = z;
}
void GameObject::SetTransFormPos(XMFLOAT3 pos)
{
    transform._41 = pos.x;
    transform._42 = pos.y;
    transform._43 = pos.z;
}
void GameObject::SetTransFormLook(XMFLOAT3 look)
{
    transform._31 = look.x;
    transform._32 = look.y;
    transform._33 = look.z;
}
void GameObject::SetTransFormUp(XMFLOAT3 up)
{
    transform._21 = up.x;
    transform._22 = up.y;
    transform._23 = up.z;
}
void GameObject::SetTransFormRight(XMFLOAT3 right)
{
    transform._11 = right.x;
    transform._12 = right.y;
    transform._13 = right.z;
}
void GameObject::SetTransFormScale(float x, float y, float z)
{
    transform._11 = x;
    transform._22 = y;
    transform._33 = z;
}
void GameObject::SetTransForm(XMFLOAT4X4 transform)
{
    this->transform = transform;
}
void GameObject::SetIsVisable(bool isVisable)
{
    this->m_isVisable = isVisable;
}
void GameObject::SetInstanceNum(int instanceNum)
{
    m_instanceNum = instanceNum;
}
void GameObject::SetLook(XMFLOAT3 look)
{
    m_world._31 = look.x;
    m_world._32 = look.y;
    m_world._33 = look.z;
}
void GameObject::SetRight(XMFLOAT3 right)
{
    m_world._11 = right.x;
    m_world._12 = right.y;
    m_world._13 = right.z;
}
void GameObject::SetUp(XMFLOAT3 up)
{
    m_world._21 = up.x;
    m_world._22 = up.y;
    m_world._23 = up.z;
}
void GameObject::SetWorld(XMFLOAT4X4 world)
{
    m_world = world;
}
void GameObject::SetConstantNumber(int n)
{
    m_ConstantNumber = n;
}
void GameObject::SetisPlayer(bool p)
{
    m_isPlayer = p;
}
void GameObject::SetBoundingMesh(DiffuseCubeMesh* mesh)
{
    m_boundingMesh = mesh;
}
void GameObject::SetTemporaryMesh(Mesh* mesh)
{
    m_temporaryMesh.emplace_back(mesh);
}
void GameObject::SetisSkinned(bool isSkinned)
{
    m_isSkinned = isSkinned;
}
void GameObject::SetisInstanceModel(bool isModel)
{
    m_isInstanceModel = isModel;
}
void GameObject::SetLightIdx(int lightidx)
{
    m_lightidx = lightidx;
}
void GameObject::SetState(int state)
{
    m_state = state;
}
void GameObject::SetRouteList(vector<int>& routeList , vector<vector<Node>>& astarNodes)
{
    for (int i = routeList.size() - 1; i >= 0; --i) {
        int idx = routeList[i];
        int cz = (idx / (AstarLineNode + 1));
        int cx = idx % (AstarLineNode + 1);
        Node tmpNode = astarNodes[cz][cx];
        m_RouteList.push(tmpNode);
    }
}
void GameObject::CreateAABBBox(XMFLOAT4X4 transform)
{
    m_aabbBox.Center = XMFLOAT3(0.0f, 0.0f, 0.0f);
    m_aabbBox.Extents = XMFLOAT3(2.0f, 2.0f, 2.0f);
    m_aabbBox.Transform(m_aabbBox, XMLoadFloat4x4(&transform));
}
void GameObject::UpdateTransForm(int idx, VS_VB_INSTANCE_OBJECT* cbMappedGameObjects)
{
    m_world = transform;
    XMStoreFloat4x4(&cbMappedGameObjects[idx].transform, XMMatrixTranspose(XMLoadFloat4x4(&m_world)));
}
void GameObject::UpdateGrassTransForm(int idx, VS_GB_INSTANCE_OBJECT* MappedObjects)
{
    m_world = transform;
    XMStoreFloat4x4(&MappedObjects[idx].transform, XMMatrixTranspose(XMLoadFloat4x4(&m_world)));
    MappedObjects[idx].moveGrass = m_grassNumber;

}
void GameObject::MovingObjects(vector<vector<Node>>& astarNodes , float elapsedTime)
{
    if (m_RouteList.size() == 0)
        return;

    Node* node = &m_RouteList.front();

    XMFLOAT3 nodePos = node->pos;
    XMFLOAT3 objPos = GetTransFormPos();

    if (Vector3::Length(Vector3::Subtract(objPos , nodePos)) < 0.5f) {
        m_RouteList.pop();
        return;
    }
    else {
        SetTransFormLook(Vector3::Normalize(Vector3::Subtract(nodePos, objPos)));//look
        SetTransFormRight(Vector3::CrossProduct(GetUp(),GetLook()));
    }
    XMFLOAT3 shift = XMFLOAT3(0.0f, -UNITGRAVITY * 50.0f, 0.0f);
    //�̵�
    shift = Vector3::Add(shift, GetLook(), UNITSPEED);
    m_acc = Vector3::ScalarProduct(shift, 1 / m_mass, false);
    m_velocity = Vector3::Add(Vector3::ScalarProduct(m_velocity, UNITDAMPING, false), Vector3::ScalarProduct(m_acc, elapsedTime, false));
    CheckMaxVel(m_velocity);
    ApplyFriction(m_velocity, elapsedTime);

    // s = s0 + vt + 1/2at*t
    XMFLOAT3 evel = Vector3::ScalarProduct(m_velocity, elapsedTime, false);
    XMFLOAT3 eacc = Vector3::ScalarProduct(m_acc, elapsedTime * elapsedTime * 0.5, false);
    XMFLOAT3 fmove = Vector3::Subtract(evel, eacc);
    SetTransFormPos(Vector3::Add(GetTransFormPos(), fmove));

}
void GameObject::SetMesh(Mesh* mesh)
{
    m_mesh.emplace_back(mesh);
}

void GameObject::SetMesh(int idx, Mesh* mesh)
{
    if (m_mesh[idx]) {
        delete m_mesh[idx];
        m_mesh[idx] = NULL;
    }
    m_mesh[idx] = mesh;
}

void GameObject::SetShadowMesh(Mesh* mesh)
{
  

}

void GameObject::SetGrassNumber(float GrassNumber)
{
    m_grassNumber = GrassNumber;
}

void GameObject::SetMaterial(int idx, Material* material)
{
   if (this->m_material[idx])
        delete m_material[idx];
   m_material[idx] = material;
}

void GameObject::InsertMaterial(Material* material)
{
    m_material.emplace_back(material);
}

void GameObject::SetWeaponNum(int weaponNum)
{
    m_weaponNum = weaponNum;
}

int GameObject::GetWeaponNum()
{
    return m_weaponNum;
}

Mesh* GameObject::GetMesh(int idx)
{
    return m_mesh[idx];
}

void GameObject::SetObjNum(int num)
{
    m_objNum = num;
}

int GameObject::GetObjNum()
{
    return m_objNum;
}

int GameObject::GetObjKinds()
{
    return E_BASICGAMEOBJ;
}

BoundingBox& GameObject::GetBounding()
{
    return m_aabbBox;
}

int GameObject::GetInstanceNum()
{
    return m_instanceNum;
}

int GameObject::GetLightIdx()
{
    return m_lightidx;
}

int GameObject::GetState()
{
    return m_state;
}

float GameObject::GetGrassNumber()
{
    return m_grassNumber;
}

void GameObject::CheckMaxVel(XMFLOAT3& vel)
{
    if (abs(vel.x) > m_maxVel) {
        if (0 > vel.x)
            vel.x = -m_maxVel;
        else
            vel.x = m_maxVel;
    }

    if (abs(vel.z) > m_maxVel) {
        if (0 > vel.z)
            vel.z = -m_maxVel;
        else
            vel.z = m_maxVel;
    }
}

void GameObject::ApplyFriction(XMFLOAT3& velocity, float elapsedTime)
{
    float tmpx = m_friction.x * elapsedTime;
    float tmpz = m_friction.z * elapsedTime;

    if (abs(velocity.x) > 0) {
        if (velocity.x > 0) {
            velocity.x -= tmpx;
            if (velocity.x < 0)
                velocity.x = 0;
        }
        else {
            velocity.x += tmpx;
            if (velocity.x > 0)
                velocity.x = 0;
        }

    }

    if (abs(velocity.z) > 0) {
        if (velocity.z > 0) {
            velocity.z -= tmpz;
            if (velocity.z < 0)
                velocity.z = 0;
        }
        else {
            velocity.z += tmpz;
            if (velocity.z > 0)
                velocity.z = 0;
        }
    }


}


void GameObject::BasicRender(ComPtr<ID3D12GraphicsCommandList>& commandList)
{
    if (!m_mesh.empty()) {for (auto& m : m_mesh) m->Render(commandList);}
}
void GameObject::InstancingRender(ID3D12GraphicsCommandList* commandList, UINT instanceNum , D3D12_VERTEX_BUFFER_VIEW instancingBufferView)
{
    if (!m_mesh.empty()) { for (auto& m : m_mesh) m->Render(commandList, instanceNum, instancingBufferView); }
}
void GameObject::UpdateTransform()
{
    m_world = transform;
}
void CubeObj::ReleaseUploadBuffer()
{
    for (int i = 0; i < m_mesh.size(); ++i) {
        m_mesh[i]->ReleaseUploadBuffer();
    }
}

void SunMoonObj::BasicRenderIdx(ComPtr<ID3D12GraphicsCommandList>& commandList, int midx)
{
    if (!m_mesh.empty()) m_mesh[midx]->Render(commandList);
}
