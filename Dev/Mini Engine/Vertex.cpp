#pragma once
#include "Vertex.h"

void TexturedVertex::SetNormal(XMFLOAT3 normal)
{
	this->normal = normal;
}

void TexturedVertex::SetTangent(XMFLOAT3 tangent)
{
	this->tangent = tangent;
}

void TexturedVertex::SetBiTangent(XMFLOAT3 bitangent)
{
	this->bitangent = bitangent;
}

XMFLOAT3 TexturedVertex::GetPos()
{
	return pos;
}

XMFLOAT2 TexturedVertex::GetUV()
{
	return texUV;
}

EffectVertex::EffectVertex()
{
}
