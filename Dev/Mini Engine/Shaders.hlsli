// Texture resource
#define MATERIAL_DIFFUSE_MAP		0x01
#define MATERIAL_NORMAL_MAP			0x02
#define MATERIAL_SPECULAR_MAP		0x04
#define MATERIAL_METALLIC_MAP		0x08
#define MATERIAL_EMISSION_MAP		0x10
#define MAX_MATERIALS				700
#define BUMP_VALUE                  300.0f

// Screen Size
#define FRAME_BUFFER_WIDTH		1280
#define FRAME_BUFFER_HEIGHT		720

//SceneTexture Define
#define GRASS_TEXTURE 0
#define SUN_TEXUTER 1
#define MOON_TEXTURE 2

//PosteffectTexture Define
#define DFFEREDTEXUTER 0

struct Defferred_OUTPUT
{
    float4 DIFFUSE_COLOR : SV_Target0;
    float4 NORMAL_COLOR : SV_Target1;
    float4 DEPTH_COLOR : SV_Target2;
    float4 SPECULAR_COLOR : SV_Target3;
};


//constantBuffer
cbuffer cbCameraInfo : register(b0)
{
	matrix gmtxView : packoffset(c0);
	matrix gmtxProjection : packoffset(c4);
    matrix gmtxInverseView : packoffset(c8);
    matrix gmtxInverseProjection : packoffset(c12);
	float3 gvCameraPosition : packoffset(c16);
};
cbuffer cbGameObjectInfo : register(b1)
{
	matrix gmtxWorld : packoffset(c0);
};

//Inverse Screen Transform
float2 GetPixelUV(float4 position)
{
    float2 uv = float2(position.x / FRAME_BUFFER_WIDTH, position.y / FRAME_BUFFER_HEIGHT);
    return uv;
}



//texture
Texture2D gtxtTerrainDetailTexture[3] : register(t0); //t0 ~ t2
Texture2D gtxtTerrainNormalTexture[3] : register(t67); //t67 ~ 69

Texture2D gtxtDiffuseTexture[10] : register(t7);
Texture2D gtxtNormalTexture[10] : register(t17);
Texture2D gtxtSpecularTexture[10] : register(t27);
Texture2D gtxtMetallicTexture[10] : register(t37);
Texture2D gtxtBillboardTexture[10] : register(t47);
Texture2D gtxtParticleTexture[10] : register(t57);

//defferred
Texture2D gtxtDeffrredTexture[4] : register(t70);

//sceneTexture
Texture2D gtxtSceneTexture[10] : register(t78);

//skyMaptexture
TextureCube gtxtSkyBoxTexture : register(t89);

//depthtexture
Texture2D gtxtDepthTexture : register(t90);

//posteffectTexture
Texture2D gtxtPostEffectTexture[10] : register(t91);

//sampler
SamplerState gspAnisotropic : register(s0);
SamplerState gspWarp : register(s1);
SamplerComparisonState gspCmpShadowClamp : register(s2);
SamplerState gspClamp : register(s3);

//skinned
cbuffer cbGameObjectInfo : register(b2)
{
    uint gnVerticesNum : packoffset(c0);
};

cbuffer cbMaterialInfo : register(b3)
{
    uint gnMaterial : packoffset(c0.x);
    uint gnTextureMask : packoffset(c0.y);
    uint gnTextureIdx : packoffset(c0.z);
};

//timer
cbuffer cbTimer : register(b8)
{
    float gTotalTime;
    float gFrameTime;
}

cbuffer cbSun : register(b9)
{
    float3 sunPos;
    float sunCamZ;
}


StructuredBuffer<float3> gsbSkinnedPosition : register(t3);
StructuredBuffer<float3> gsbSkinnedNormal : register(t4);
StructuredBuffer<float3> gsbSkinnedTangent : register(t5);
StructuredBuffer<float3> gsbSkinnedBiTangent : register(t6);
