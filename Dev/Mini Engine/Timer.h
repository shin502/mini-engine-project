#pragma once
#include"stdafx.h"

class Timer {
private:
	bool hardwareHasPerformanceCounter;
	bool stopped;

	double timeScale;
	float timeElapsed;

	__int64 basePerformanceCounter;
	__int64	pausedPerformanceCounter;
	__int64	stopPerformanceCounter;
	__int64	currentPerformanceCounter;
	__int64	lastPerformanceCounter;

	__int64 performanceFrequencyPerSec;

	float frameTime[MAX_SAMPLE_COUNT];
	ULONG sampleCount;

	unsigned long currentFrameRate;
	unsigned long framesPerSecond;
	float fpsTimeElapsed;
public:
	Timer();
	virtual ~Timer();

	void Tick(float fLockFPS = 0.0f);
	void Start();
	void Stop();
	void Reset();

	unsigned long GetFrameRate(LPTSTR lpszString = NULL, int nCharacters = 0);
	float GetTimeElapsed();
	float GetTotalTime();

};