#include"Shaders.hlsl"

struct VS_STANDARD_INPUT
{
    float3 position : POSITION;
    float2 uv : TEXCOORD;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 bitangent : BITANGENT;
};

struct VS_STANDARD_OUTPUT
{
    float4 position : SV_POSITION;
    float3 positionW : POSITION;
    float2 uv : TEXCOORD;
    float3 normalW : NORMAL;
    float3 tangentW : TANGENT;
    float3 bitangentW : BITANGENT;
    
    float4 shadowPosition[3] : TEXCOORD1;
    
    float fogFactor : FACTOR;
};

VS_STANDARD_OUTPUT VSStandard(VS_STANDARD_INPUT input)
{
    VS_STANDARD_OUTPUT output;
    output.positionW = (float3) mul(float4(input.position, 1.0f), gmtxWorld);
    float4 cameraPos = mul(float4(output.positionW, 1.0f), gmtxView);
    output.position = mul(cameraPos, gmtxProjection);
    output.normalW = (float3) mul(float4(input.normal, 1.0f), gmtxWorld);
    output.tangentW = (float3) mul(float4(input.tangent, 1.0f), gmtxWorld);
    output.bitangentW = (float3) mul(float4(input.bitangent, 1.0f), gmtxWorld);
    output.uv = input.uv;
    
    //matrix shadowProject[MAX_CASCADE_SIZE];
    
    //for (int i = 0; i < int(gfCasacdeNum); ++i)
    //{
    //    shadowProject[i] = mul(gmtxLightViewProjection[i], gmtxProjectToTexture);
    //    output.shadowPosition[i] = mul(float4(output.positionW, 1.0f), shadowProject[i]);
    //}
    for (int i = 0; i < 3; ++i)
    {
        output.shadowPosition[i] = 1.0f;
    }
    output.fogFactor = 1.0f;
    
    return output;
}

struct VS_INSTANCING_INPUT
{
    float3 position : POSITION;
    float2 uv : TEXCOORD;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 bitangent : BITANGENT;
    
    float4x4 transform : WORLDMATRIX;
};

VS_STANDARD_OUTPUT VSStandardInstancing(VS_INSTANCING_INPUT input)
{
    VS_STANDARD_OUTPUT output;
    output.positionW = mul(float4(input.position, 1.0f), input.transform).xyz;
    float4 cameraPos = mul(float4(output.positionW, 1.0f), gmtxView);
    output.position = mul(cameraPos, gmtxProjection);
    output.normalW = mul(input.normal, (float3x3) input.transform);
    output.tangentW = mul(input.tangent, (float3x3) input.transform);
    output.bitangentW = mul(input.bitangent, (float3x3) input.transform);
    output.uv = input.uv;
    matrix shadowProject[NUM_CASCADES];
    
    for (int i = 0; i < int(gfCasacdeNum); ++i)
    {
        shadowProject[i] = mul(gmtxLightViewProjection[i], gmtxProjectToTexture);
        output.shadowPosition[i] = mul(float4(output.positionW, 1.0f), shadowProject[i]);
    }
    //output.fogFactor = GetFogFactor(cameraPos.z);
    
    return (output);
}

Defferred_OUTPUT PSStandard(VS_STANDARD_OUTPUT input) : SV_TARGET
{
    
    uint textureIdx = gnTextureIdx;
    uint uTextureIdx[5];
    
    for (int i = 0; i < 5; ++i)
    {
        uTextureIdx[i] = textureIdx % 100;
        textureIdx = textureIdx / 100;
    }
    
    float4 cDiffuseColor = float4(0.0f, 0.0f, 0.0f, 0.0f);
    if (gnTextureMask & MATERIAL_DIFFUSE_MAP)
        cDiffuseColor = gtxtDiffuseTexture[uTextureIdx[0]].Sample(gspWarp, input.uv);
    float4 cNormalColor = float4(0.0f, 0.0f, 0.0f, 0.0f);
    if (gnTextureMask & MATERIAL_NORMAL_MAP)
        cNormalColor = gtxtNormalTexture[uTextureIdx[1]].Sample(gspWarp, input.uv);
    float4 cSpecularColor = float4(0.0f, 0.0f, 0.0f, 0.0f);
    if (gnTextureMask & MATERIAL_SPECULAR_MAP)
        cSpecularColor = gtxtSpecularTexture[uTextureIdx[2]].Sample(gspWarp, input.uv);
    float4 cMetallicColor = float4(0.0f, 0.0f, 0.0f, 0.0f);
    if (gnTextureMask & MATERIAL_METALLIC_MAP)
        cMetallicColor = gtxtMetallicTexture[uTextureIdx[3]].Sample(gspWarp, input.uv);

    float3 normalW;
    float4 cColor = cDiffuseColor;
    
    if (gnTextureMask & MATERIAL_NORMAL_MAP)
    {
        float3x3 TBN = float3x3(normalize(input.tangentW), normalize(input.bitangentW), normalize(input.normalW));
        float3 vNormal = normalize(cNormalColor.rgb * 2.0f - 1.0f); //[0, 1] �� [-1, 1]
        normalW = normalize(mul(vNormal, TBN));
    }
    else
    {
        //return BlendFog(float4(cColor.xyz * fShadowFactor, cColor.w), input.fogFactor);
        //normalW = normalize(input.normalW);
    }
    
    
    //shadow
    float4 positionV = mul(float4(input.positionW, 1.0f), gmtxView);
    float fShadowFactor = 1.0f, fBias = 0.001f, fsDepth = 1.0f, fShadowOffset = 0;
    float idx = 0.0f;
    for (int j = 0; j < NUM_CASCADES; ++j)
    {
        if (positionV.z > gfCascadeLength[j].x)
        {
            idx = float(j);
        }
    }
    if ((saturate(input.shadowPosition[idx].x) == input.shadowPosition[idx].x) &&
        (saturate(input.shadowPosition[idx].y) == input.shadowPosition[idx].y) &&
        (saturate(input.shadowPosition[idx].z) == input.shadowPosition[idx].z))
    {
        //fShadowOffset = idx / float(NUM_CASCADES);
       
        //float depth = input.shadowPosition[idx].z;
        //float2 shadowUV = input.shadowPosition[idx].xy;
        //shadowUV.x /= gfCasacdeNum;
        //shadowUV.x += fShadowOffset;
        //fShadowFactor = gtxtShadowTexture.SampleCmpLevelZero(gspCmpShadowClamp, shadowUV, depth);
    }
    

    normalW = (normalW * 0.5f) + 0.5f;
    
    Defferred_OUTPUT output;
    output.DIFFUSE_COLOR = cColor * fShadowFactor;
    output.NORMAL_COLOR = float4(normalW, 1.0f);
    output.SPECULAR_COLOR.xyz = (cSpecularColor.xyz + cMetallicColor.xyz);
    output.SPECULAR_COLOR.a = gMaterials[gnMaterial].m_cSpecular.a;
    output.DEPTH_COLOR.r = input.position.z / input.position.w;
    output.DEPTH_COLOR.g = gMaterials[gnMaterial].m_cAmbient.r;
    output.DEPTH_COLOR.b = gMaterials[gnMaterial].m_cDiffuse.r;
    output.DEPTH_COLOR.a = input.position.w;
    return output;
    
    //uint materialNum = gnMaterial;
    //MATERIAL tmpMaterial = gMaterials[materialNum];
    //tmpMaterial.m_cSpecular += cSpecularColor + cMetallicColor;
    
    //float4 cIllumination = Lighting(input.positionW, normalW, tmpMaterial);
    //cIllumination += float4(0.5f, 0.5f, 0.5f, 1.0f);
    
    
    //float4 finalColor = saturate(cColor * cIllumination);
    //return finalColor;
   // return BlendFog(float4(finalColor.xyz * fShadowFactor, finalColor.w), input.fogFactor);
}


