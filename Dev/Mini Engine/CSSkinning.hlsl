#include "CSSkinned.hlsli"
//dispatch함수는 쓰레드 그룹의 개수를 정해준다
//numthreads(X,Y,Z) -> 하나의 쓰레드 "그룹" 에서 실행될 "쓰레드의 개수"를 정의 
//SV_GroupThreadID -> 쓰레드 그룹에서 계산 셰이더가 실행되고 있는 "쓰레드의 인덱스" 
//SV_GroupID -> 계산 쉐이더가 실행하고 있는 "쓰레드 그룹"의 인덱스 
//SV_DispatchThreadID -> 계산 쉐이더가 실행하고 있는 쓰레드와 쓰레드 그룹이 결합된 인덱스 
//(DisPatch()로 호출로 생성된 모든 쓰레드를 구별가능)


//distpatch함수가 쓰레드 그룹의 개수를 정해준다 X,Y,Z에는 쓰레드 그룹이 몇개인지
[numthreads(BLOCK_SIZE, 1, 1)] //-> x그룹에서 256개가 실행된다. 나머지는 1개씩 
void main(uint3 Gid : SV_GroupID, uint3 DTid : SV_DispatchThreadID)
{
    if (DTid.x < gnVerticesNum)
    {
        float4x4 mtxVertexToBoneWorld = (float4x4) 0.0f;
        for (int i = 0; i < MAX_VERTEX_INFLUENCES; ++i)
        {
            mtxVertexToBoneWorld += gsbBoneWeight[DTid.x][i] * mul(gpmtxBoneOffsets[gsbBoneIndex[DTid.x][i]], gsbBoneTransforms[(Gid.y * SKINNED_ANIMATION_BONES) + gsbBoneIndex[DTid.x][i]]);
        }
        float3 positionW = mul(float4(gsbPosition[DTid.x], 1.0f), mtxVertexToBoneWorld).xyz;
        float3 normalW = mul(gsbNormal[DTid.x], (float3x3) mtxVertexToBoneWorld).xyz;
        float3 tangentW = mul(gsbTangent[DTid.x], (float3x3) mtxVertexToBoneWorld).xyz;
        float3 bitangentW = mul(gsbBiTangent[DTid.x], (float3x3) mtxVertexToBoneWorld).xyz;
        
        //Dtid.x가 쓰레드 그룹에서의 쓰레드가 실행하고 있는 정점의 인덱스를 나타냄 즉 몇번째 정점인 지 알수있게함
        grwsbPositionW[Gid.y * gnVerticesNum + DTid.x] = positionW;//gid.y는 dispatch 함수로 넘겨준 instanceNum이다
        //[(객체의 인덱스 * 객체의 총 버텍스 개수) + 몇번째 버텍스 인텍스] = 정점의 인덱스 이다.
        grwsbNormalW[Gid.y * gnVerticesNum + DTid.x] = normalW;
        grwsbTangentW[Gid.y * gnVerticesNum + DTid.x] = tangentW;
        grwsbBiTangentW[Gid.y * gnVerticesNum + DTid.x] = bitangentW;
    }
    return;
}