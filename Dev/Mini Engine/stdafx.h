// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이
// 들어 있는 포함 파일입니다.
//

#pragma once

#define WIN32_LEAN_AND_MEAN             // 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
// Windows 헤더 파일:
#include <windows.h>

// C의 런타임 헤더 파일입니다.
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <math.h>
#include <comdef.h>
#include <string>
#include <wrl.h>
#include <shellapi.h>

#include <iostream>
#include <random>
#include <fstream>
#include <vector>
#include <chrono>
using namespace std;

#include <d3d12.h>
#include <dxgi1_4.h>
#include <D3Dcompiler.h>
#include <DirectXMath.h>
#include <dxgi1_6.h>
#include "d3dx12.h"
#include <string>
#include <map>
#include <unordered_map>
#include <wrl.h>
#include <shellapi.h>
#include <timeapi.h>
#include <queue>

#include <DirectXPackedVector.h>
#include <DirectXColors.h>
#include <DirectXCollision.h>

#include <Mmsystem.h>

#ifdef _DEBUG
#include <dxgidebug.h>
#endif

using namespace DirectX;
using namespace DirectX::PackedVector;

using Microsoft::WRL::ComPtr;

extern HINSTANCE						ghAppInstance;
const ULONG MAX_SAMPLE_COUNT = 50;
//#define _WITH_SWAPCHAIN_FULLSCREEN_STATE

#define FRAME_BUFFER_WIDTH		1280
#define FRAME_BUFFER_HEIGHT		720
#define SHADOW_BUFFER_SIZE		2048
#define FAILNUMBER				-1
//#define _WITH_CB_GAMEOBJECT_32BIT_CONSTANTS
//#define _WITH_CB_GAMEOBJECT_ROOT_DESCRIPTOR
#define BLOCKSIZE 256.0f
// ComputeShader가 동작할 때 BLOCKSIZE만큼 동작한다. ex) [numthreads(BLOCKSIZE, 1, 1)] , Dispatch(static_cast<int>(ceil(PARTICLESIZE / BLOCKSIZE)), 1, 1);
#define SUNDISTANCE 2000.0f
#define MAX_LIGHTS					200
#define MAX_MATERIALS				700
#define DIR_FORWARD 0x01
#define DIR_RIGHT 0x02
#define DIR_BACKWARD 0x04
#define DIR_LEFT 0x08
#define DIR_JUMP 0x10
#define _WITH_CB_WORLD_MATRIX_DESCRIPTOR_TABLE

#define VERTEXT_BONE_INDEX_WEIGHT		0x1000
#define SKINNED_ANIMATION_BONES		128
// 스키닝 애니메이션 본의 갯수
#define ANIMATION_CALLBACK_EPSILON	0.015f

#define MATERIAL_DIFFUSE_MAP		0x01
#define MATERIAL_NORMAL_MAP			0x02
#define MATERIAL_SPECULAR_MAP		0x04
#define MATERIAL_METALLIC_MAP		0x08
#define MATERIAL_EMISSION_MAP		0x10\

#define BASIC_REFLECTION 1
#define DUMMY_WAITING 15553321.0f
#define SKINNED_BUFFERSIZE	60
#define BUFFERSIZE	1000
#define PARTICLESIZE 100
#define PARTICLE_TYPE_EXPLOSION		0
#define PARTICLE_TYPE_SCATTER		1
#define EFFECTCOUNTS				6
// Particle 한덩어리당 PARTICLESIZE만큼의 입자를 갖는다.

#define POINT_LIGHT					1
#define SPOT_LIGHT					2
#define DIRECTIONAL_LIGHT			3

#define NUM_CASCADES 3

#pragma comment(lib, "d3dcompiler.lib")
#pragma comment(lib, "d3d12.lib")
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "winmm.lib")

//AstarNode
constexpr int AstarInterval = 16;
constexpr int AstarTotalNode = 1024;
constexpr int AstarLineNode = 512 / AstarInterval;

enum TextureResource
{
	ResourceTexture2D = 0,
	ResourceTexture2D_Array,	// texture들의 배열
	ResourceTexture2DArray,		// textureArray
	ResourceTextureCube,
	ResourceBuffer
};

enum eRootParameters {
	GraphicsRootCamera,
	GraphicsRootNoInstancingObj,
	GraphicsRootTerrainDetail,
	GraphicsRootSkinnedPositionWorld,			// SRV
	GraphicsRootSkinnedNormalWorld,				// SRV
	GraphicsRootSkinnedTangentWorld,			// SRV
	GraphicsRootSkinnedBiTangentWorld,			// SRV
	GraphicsRootSkinnedVerticesNum,
	GraphicsRootMaterialInfo,
	GraphicsRootDiffuseTextures,				// DESCRIPTORTABLE
	GraphicsRootNormalTextures,					// DESCRIPTORTABLE
	GraphicsRootSpecularTextures,				// DESCRIPTORTABLE
	GraphicsRootMetallicTextures,				// DESCRIPTORTABLE
	GraphicsRootBillboardTextures,				// DESCRIPTORTABLE
	GraphicsRootParticleTextures,				// DESCRIPTORTABLE
	GraphicsRootMaterials,						// CBV
	GraphicsRootLights,							// CBV
	GraphicsRootTerrainNormal,					// TABLE
	GraphicsRootDeferred,						// TABLE
	GraphicsRootShadow,							// TABLE
	GraphicsRootLightCamera,					// CBV
	GraphicsRootCasCade,						// TABLE
	GraphicsRootCameraNumber,					// RootConstant
	GraphicsRootSceneTexture,					// TABLE //-> billboard texture	
	GraphicsRootTimer,							// RootConstant
	GraphicsParticleData,						// SRV
	GraphicsSkyMapTexture,						// TABLE
	GraphicsDepthTexture,						// TABLE
	GraphicsSunPosData,							// RootConstant
	GraphicsPostEffectTexture,					// TABLE
	GraphicsRootParametersNum
};

enum eAnimationType
{
	AnimationTypeOnce = 0,
	AnimationTypeLoop,
	AnimationTypePingpong
};

enum eAnimationChange // 총 모델 애니메이션 번호
{
	MainCharacterAim = 0,
	MainCharacterIdle = 1,
	MainCharacterIdle2,
	MainCharacterIdle3,
	MainCharacterIdle4,

	MainCharacterAimUp = 5,
	MainCharacterAimDown = 6,

	MainCharacterWalk = 7,
	MainCharacterWalkBack = 8,
	MainCharacterWalkLeft = 9,
	MainCharacterWalkRight = 10,

	MainCharacterRun = 11,
	MainCharacterRunBack = 12,
	MainCharacterRunLeft = 13,
	MainCharacterRunRight = 14,

	MainCharacterFire = 17,
	MainCharacterWalkingFire = 18,
	MainCharacterReload = 19,
	MainCharacterWalkingReload = 20,
	MainCharacterHit = 21,
	MainCharacterDeath = 22
};

enum SwordModelAnimationChange
{
	SwordCharacterIdle = 0,
	SwordCharacterIdle2,

	SwordCharacterWalk,
	SwordCharacterWalkBack,
	SwordCharacterWalkLeft,
	SwordCharacterWalkRight,

	SwordCharacterRun,
	SwordCharacterRunBack,
	SwordCharacterRunLeft,
	SwordCharacterRunRight,

	SwordCharacterAttack,
	SwordCharacterAttack2,
	SwordCharacterAttack3,

	SwordCharacterHit,
	SwordCharacterDeath
};


enum eShaders {
	E_BASICSHADER,
	E_TERRAINSHADER,
	E_UISHADER,
	E_RSSHADER,
	E_STANDARDSHADER,
	E_STANDARDINSTANCESHADER,
	E_STANDARDSKINNEDANIMATIONSHADER,
	E_STANDARDSKINNEDANIMATIONINSTANCESHADER,
	E_INSTANCESHADER,
	E_SKINNEDINSTANCINGSHADER,
	E_PARTICLESHADER,
	E_WEAPONTRAILSHADER,
	E_BILLBOARDSHADER,
	E_SKYBOXSHADER
};

enum eConstantNumber {
	E_SKYBOXCONSTANTNUMBER,
	E_TERRAINCONSTANTNUMBER,
	E_CUBECONSTANTNUMBER,
	E_BILLCONSTANTNUMBER,
	E_NODECONSTANTNUMBER
};

enum eGOBJ {
	E_SKYBOXOBJ,
	E_TERRAINOBJ,
	E_PARTICLEOBJ,
	E_EFFECTOBJ,
	E_CUBEOBJ,
	E_SOLDIEROBJ,
	E_SWORDOBJ,
	E_REDCAROBJ,
	E_LAMPOBJ,
	E_GRASSOBJ,
	E_BILLBOARDOBJ,
	E_NODEOBJ
};

enum eOBJKINDS {
	E_BASICGAMEOBJ,
	E_HIERARCHYOBJ,
	E_ANIMATIONOBJ
};

enum eVertexStructure
{
	VertexPosition = 0x01,
	VertexColor = 0x02,
	VertexNormal = 0x04,
	VertexTangent = 0x08,
	VertexTexCoord0 = 0x10,
	VertexTexCoord1 = 0x20
};

enum eShaderType
{
	ShaderTypeStandard = 1,
	ShaderTypeInstance,
	ShaderTypeSkinned,
	ShaderTypeBounding
};

enum ePlayerJob
{
	PlayerJobRifle = 0,
	PlayerJobSword,
	PlayerJobBow
};

enum eModelKINDS
{
	RIFLEMODEL,
	SWORDMODEL,
	MODELCOUNT
};

enum eObjectKINDS {
	REDCARMODEL,
	LAMPMODEL,
	OBJECTMODELCOUNT
};

enum eRenderTarget {

	RENDERTARGETDEPTH = 2,
	RENDERTARGETDIFFUSE,
	RENDERTARGETNORMAL,
	RENDERTARGETSPECCULAR,
	RENDERTARGETNUM	
};

enum ComputeRootParameters
{
	ComputeRootTimeInfo = 0,					// ROOTCONSTANT
	ComputeRootBoneOffset,						// CBV
	ComputeRootBoneTransform,					// SRV
	ComputeRootSkinnedPosition,					// SRV
	ComputeRootSkinnedNormal,					// SRV
	ComputeRootSkinnedTangent,					// SRV
	ComputeRootSkinnedBiTangent,				// SRV
	ComputeRootSkinnedBoneIndex,				// SRV
	ComputeRootSkinnedBoneWeight,				// SRV
	ComputeRootSkinnedPositionWorld,			// UAV
	ComputeRootSkinnedNormalWorld,				// UAV
	ComputeRootSkinnedTangentWorld,				// UAV
	ComputeRootSkinnedBiTangentWorld,			// UAV
	ComputeRootSkinnedVerticesNum,				// ROOTCONSTANT
	ComputeRootParticleType,					// ROOTCONSTANT
	ComputeRootParticleInfoSRV,					// SRV
	ComputeRootParticleCopySRV,					// SRV
	ComputeRootParticleInfoUAV,					// UAV
	ComputeRootParticleCameraPos,				// ROOTCONSTANT
	ComputeRootParametersNum					// PAMETERSNUM
};

enum ePARTICLENUMBER {
	E_SMOKE1PARTICLE,
	E_BLOOD2PARTICLE,
	E_DUSTPARTICLE,
	E_FIRE2PARTICLE,
	E_GUNFIREPARTICLE1,
	E_LAVAPARTICLE1,
	E_LIGHTPARTICLE1,
	E_BLOOD0PARTICLE1,
	E_SMOKE0PARTICLE1,
	E_LIGHTPARTICLE,
};

enum eLIGHTBOJNUMBER {
	E_LIGHTBOJ,
	E_POSTOBJ,
	E_LIGHTOBJNUM
};

enum eLIGHTPOSIDX {
	E_LIGHTRIFLEIDX,
	E_LIGHTLAMPIDX,
	E_LIGHTIDXCOUNT
};

enum eOBJECTSTATE {
	E_MOVING,
	E_IDLE,
	E_ATTACK
};

inline std::string HrToString(HRESULT hr)
{
	char s_str[64] = {};
	sprintf_s(s_str, "HRESULT of 0x%08X", static_cast<UINT>(hr));
	return std::string(s_str);
}

class HrException : public std::runtime_error
{
public:
	HrException(HRESULT hr) : std::runtime_error(HrToString(hr)), m_hr(hr) {}
	HRESULT Error() const { return m_hr; }
private:
	const HRESULT m_hr;
};

#define SAFE_RELEASE(p) if (p) (p)->Release()

inline void ThrowIfFailed(HRESULT hr)
{
	if (FAILED(hr))
	{
		throw HrException(hr);
	}
}
#define EPSILION				1.0e-10f

inline bool IsZero(float value) { return ((fabsf(value) < EPSILION)); }
inline bool IsZero(float fValue, float fEpsilon) { return((fabsf(fValue) < fEpsilon)); }
inline bool IsEqual(float A, float B) { return (::IsZero(A - B)); }
inline bool IsEqual(float fA, float fB, float fEpsilon) { return(::IsZero(fA - fB, fEpsilon)); }
inline float InverseSqrt(float value) { return 1.0f / sqrtf(value); }
inline void Swap(float* S, float* T) { float temp = *S; *S = *T; *T = temp; }
inline float RandomPercent();
#define RANDOM_COLOR XMFLOAT4(rand() / float(RAND_MAX), rand() / float(RAND_MAX), rand() / float(RAND_MAX), 1.0f)



extern ComPtr<ID3D12Resource> CreateBufferResource(ID3D12Device* device, ID3D12GraphicsCommandList
	* commandList, void* pData, UINT nBytes, D3D12_HEAP_TYPE d3dHeapType,
	D3D12_RESOURCE_STATES d3dResourceStates, ID3D12Resource** ppd3dUploadBuffer, D3D12_RESOURCE_FLAGS flags = D3D12_RESOURCE_FLAG_NONE);

extern ID3D12Resource* NoComPtrCreateBufferResource(ID3D12Device* device, ID3D12GraphicsCommandList
	* commandList, void* pData, UINT nBytes, D3D12_HEAP_TYPE d3dHeapType,
	D3D12_RESOURCE_STATES d3dResourceStates, ID3D12Resource** ppd3dUploadBuffer, D3D12_RESOURCE_FLAGS flags = D3D12_RESOURCE_FLAG_NONE);

extern ID3D12Resource* CreateTextureResourceFromFile(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, wchar_t* pszFileName, ID3D12Resource** ppd3dUploadBuffer, D3D12_RESOURCE_STATES d3dResourceStates);

extern D3D12_SHADER_RESOURCE_VIEW_DESC GetShaderResourceViewDesc(D3D12_RESOURCE_DESC d3dResourceDesc, UINT nTextureType);

extern ID3D12Resource* CreateTexture2DResource(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, UINT nWidth, UINT nHeight, UINT nElements, UINT nMipLevels, DXGI_FORMAT dxgiFormat, D3D12_RESOURCE_FLAGS d3dResourceFlags, D3D12_RESOURCE_STATES d3dResourceStates, D3D12_CLEAR_VALUE* pd3dClearValue);


//3차원 벡터의 연산
namespace Vector3
{
	// XMVECTOR의 값을 XMFLOAT3로 변환시켜주는 함수
	inline XMFLOAT3 XMVectorToFloat3(const XMVECTOR& vector)
	{
		XMFLOAT3 result;
		XMStoreFloat3(&result, vector);
		return(result);
	}

	// XMFLOAT3의 스칼라 곱 연산 (정규화된 벡터로 연산 수행)
	inline XMFLOAT3 ScalarProduct(const XMFLOAT3& vector, float scalar, bool normalize = true)
	{
		XMFLOAT3 result;
		if (normalize)
			XMStoreFloat3(&result, XMVector3Normalize(XMLoadFloat3(&vector)) * scalar);
		else
			XMStoreFloat3(&result, XMLoadFloat3(&vector) * scalar);
		return(result);
	}

	// XMFLOAT3 두개의 덧셈 연산
	inline XMFLOAT3 Add(const XMFLOAT3& vector1, const XMFLOAT3& vector2)
	{
		XMFLOAT3 result;
		XMStoreFloat3(&result, XMLoadFloat3(&vector1) + XMLoadFloat3(&vector2));
		return(result);
	}

	// XMFLOAT3 두개의 덧셈 연산( 두번째 값은 스칼라 곱 후 더함 )
	inline XMFLOAT3 Add(const XMFLOAT3& vector1, const XMFLOAT3& vector2, float scalar)
	{
		XMFLOAT3 result;
		XMStoreFloat3(&result, XMLoadFloat3(&vector1) + (XMLoadFloat3(&vector2) * scalar));
		return(result);
	}

	// XMFLOAT3 두개의 뺄셈 연산
	inline XMFLOAT3 Subtract(const XMFLOAT3& vector1, const XMFLOAT3& vector2)
	{
		XMFLOAT3 result;
		XMStoreFloat3(&result, XMLoadFloat3(&vector1) - XMLoadFloat3(&vector2));
		return(result);
	}

	// XMFLOAT3 두개의 내적 연산
	inline float DotProduct(const XMFLOAT3& vector1, const XMFLOAT3& vector2)
	{
		XMFLOAT3 result;
		XMStoreFloat3(&result, XMVector3Dot(XMLoadFloat3(&vector1), XMLoadFloat3(&vector2)));
		return(result.x);
	}

	// XMFLOAT3 두개의 외적 연산 (정규화된 벡터로 연산 수행)
	inline XMFLOAT3 CrossProduct(const XMFLOAT3& vector1, const XMFLOAT3& vector2, bool normalize = true)
	{
		XMFLOAT3 result;
		if (normalize)
			XMStoreFloat3(&result, XMVector3Normalize(XMVector3Cross(XMLoadFloat3(&vector1), XMLoadFloat3(&vector2))));
		else
			XMStoreFloat3(&result, XMVector3Cross(XMLoadFloat3(&vector1), XMLoadFloat3(&vector2)));
		return(result);
	}

	// XMFLOAT3 의 정규화 연산
	inline XMFLOAT3 Normalize(const XMFLOAT3& vector)
	{
		XMFLOAT3 normal;
		XMStoreFloat3(&normal, XMVector3Normalize(XMLoadFloat3(&vector)));
		return(normal);
	}

	// XMFLOAT3 의 크기를 구하는 함수
	inline float Length(const XMFLOAT3& vector)
	{
		XMFLOAT3 result;
		XMStoreFloat3(&result, XMVector3Length(XMLoadFloat3(&vector)));
		return(result.x);
	}

	// XMFLOAT3 두개 사이의 각도를 구하는 함수 (도 값으로 리턴함)
	inline float Angle(const XMVECTOR& vector1, const XMVECTOR& vector2)
	{
		XMVECTOR angle = XMVector3AngleBetweenNormals(vector1, vector2);
		return(XMConvertToDegrees(acosf(XMVectorGetX(angle))));
		// 라디안 값을 도 값으로 바꾸어 반환
	}

	// XMFLOAT3 두개 사이의 각도를 구하는 함수 (도 값으로 리턴함)
	inline float Angle(const XMFLOAT3& vector1, const XMFLOAT3& vector2)
	{
		return(Angle(XMLoadFloat3(&vector1), XMLoadFloat3(&vector2)));
	}

	// XMFLOAT3 와 행렬 연산 (입력 벡터는 노멀 벡터)
	// 회전 및 스케일링을 위해 입력 행 0, 1, 2를 사용하여 변환을 수행하고 행 3을 무시합니다.
	inline XMFLOAT3 TransformNormal(const XMFLOAT3& vector, const XMMATRIX& transform)
	{
		XMFLOAT3 result;
		XMStoreFloat3(&result, XMVector3TransformNormal(XMLoadFloat3(&vector), transform));
		return(result);
	}

	// XMFLOAT3 와 행렬 연산 
	// 입력 벡터의 w 구성 요소를 무시하고 대신 1.0 값을 사용합니다. 반환되는 벡터의 w 구성 요소는 항상 1.0입니다.
	inline XMFLOAT3 TransformCoord(const XMFLOAT3& vector, const XMMATRIX& transform)
	{
		XMFLOAT3 result;
		XMStoreFloat3(&result, XMVector3TransformCoord(XMLoadFloat3(&vector), transform));
		return(result);
	}

	// XMFLOAT3 와 행렬 연산 
	inline XMFLOAT3 TransformCoord(const XMFLOAT3& vector, const XMFLOAT4X4& matrix)
	{
		return(TransformCoord(vector, XMLoadFloat4x4(&matrix)));
	}

	// 3-차원 벡터가 영벡터인 가를 반환하는 함수이다
	inline bool IsZero(XMFLOAT3& vector)
	{
		if (::IsZero(vector.x) && ::IsZero(vector.y) && ::IsZero(vector.z))
			return (true);
		return false;
	}

	inline XMFLOAT3 Floor(const XMFLOAT3 vector)
	{
		XMFLOAT3 result;
		XMStoreFloat3(&result, XMVectorFloor(XMLoadFloat3(&vector)));
		return(result);
	}

	inline XMFLOAT3 Round(const XMFLOAT3& vector)
	{
		XMFLOAT3 result;
		XMStoreFloat3(&result, XMVectorRound(XMLoadFloat3(&vector)));
		return(result);
	}

	inline XMFLOAT3 Multply(const XMFLOAT3& vector1, const XMFLOAT3& vector2) {
		XMFLOAT3 tmp{0.0f , 0.0f ,0.0f};
		tmp.x =  vector1.x *  vector2.x;
		tmp.y =  vector1.y *  vector2.y;
		tmp.z =  vector1.z *  vector2.z;

		return tmp;
	}

}
//4차원 벡터의 연산
namespace Vector4
{
	// XMFLAOT4 두개의 덧셈 연산
	inline XMFLOAT4 Add(const XMFLOAT4& vector1, const XMFLOAT4& vector2)
	{
		XMFLOAT4 result;
		XMStoreFloat4(&result, XMLoadFloat4(&vector1) + XMLoadFloat4(&vector2));
		return(result);
	}

	// 4-차원 벡터와 스칼라(실수)의 곱을 반환하는 함수이다.
	inline XMFLOAT4 Multiply(float scalar, XMFLOAT4& vector)
	{
		XMFLOAT4 result;
		XMStoreFloat4(&result, scalar * XMLoadFloat4(&vector));
		return(result);
	}

	inline XMFLOAT4 TransformCoord(const XMFLOAT4& vector, const XMFLOAT4X4& matrix)
	{
		return TransformCoord(vector, matrix);
	}
}
//행렬의 연산
namespace Matrix4x4
{
	// 단위행렬 생성
	inline XMFLOAT4X4 Identity()
	{
		XMFLOAT4X4 result;
		XMStoreFloat4x4(&result, XMMatrixIdentity());
		return(result);
	}

	// 행렬 두개의 곱을 연산
	inline XMFLOAT4X4 Multiply(const XMFLOAT4X4& matrix1, const XMFLOAT4X4& matrix2)
	{
		XMFLOAT4X4 result;
		XMStoreFloat4x4(&result, XMLoadFloat4x4(&matrix1) * XMLoadFloat4x4(&matrix2));
		return(result);
	}

	// 행렬 두개의 곱을 연산
	inline XMFLOAT4X4 Multiply(const XMFLOAT4X4& matrix1, const XMMATRIX& matrix2)
	{
		XMFLOAT4X4 result;
		XMStoreFloat4x4(&result, XMLoadFloat4x4(&matrix1) * matrix2);
		return(result);
	}

	// 행렬 두개의 곱을 연산
	inline XMFLOAT4X4 Multiply(const XMMATRIX& matrix1, const XMFLOAT4X4& matrix2)
	{
		XMFLOAT4X4 result;
		XMStoreFloat4x4(&result, matrix1 * XMLoadFloat4x4(&matrix2));
		return(result);
	}

	// 행렬의 역행렬을 구하는 함수
	inline XMFLOAT4X4 Inverse(const XMFLOAT4X4& matrix)
	{
		XMFLOAT4X4 result;
		XMStoreFloat4x4(&result, XMMatrixInverse(NULL, XMLoadFloat4x4(&matrix)));
		return(result);
	}

	// 0.0f 의 값으로 채워진 4x4 행렬
	inline XMFLOAT4X4 Zero()
	{
		XMFLOAT4X4 xmf4x4Result;
		XMStoreFloat4x4(&xmf4x4Result, XMMatrixSet(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f));
		return(xmf4x4Result);
	}

	// 두 행렬을 보간 계산하는 함수
	inline XMFLOAT4X4 Interpolate(XMFLOAT4X4& xmf4x4Matrix1, XMFLOAT4X4& xmf4x4Matrix2, float t)
	{
		XMFLOAT4X4 xmf4x4Result;
		XMVECTOR S0, R0, T0, S1, R1, T1;
		XMMatrixDecompose(&S0, &R0, &T0, XMLoadFloat4x4(&xmf4x4Matrix1));
		XMMatrixDecompose(&S1, &R1, &T1, XMLoadFloat4x4(&xmf4x4Matrix2));
		XMVECTOR S = XMVectorLerp(S0, S1, t);
		XMVECTOR T = XMVectorLerp(T0, T1, t);
		XMVECTOR R = XMQuaternionSlerp(R0, R1, t);
		XMStoreFloat4x4(&xmf4x4Result, XMMatrixAffineTransformation(S, XMVectorZero(), R, T));
		return(xmf4x4Result);
	}

	// 행렬의 스케일을 늘리는 함수
	inline XMFLOAT4X4 Scale(XMFLOAT4X4& xmf4x4Matrix, float fScale)
	{
		XMFLOAT4X4 xmf4x4Result;
		XMStoreFloat4x4(&xmf4x4Result, XMLoadFloat4x4(&xmf4x4Matrix) * fScale);
		/*
				XMVECTOR S, R, T;
				XMMatrixDecompose(&S, &R, &T, XMLoadFloat4x4(&xmf4x4Matrix));
				S = XMVectorScale(S, fScale);
				T = XMVectorScale(T, fScale);
				R = XMVectorScale(R, fScale);
				//R = XMQuaternionMultiply(R, XMVectorSet(0, 0, 0, fScale));
				XMStoreFloat4x4(&xmf4x4Result, XMMatrixAffineTransformation(S, XMVectorZero(), R, T));
		*/
		return(xmf4x4Result);
	}

	// 두 행렬을 더하는 함수
	inline XMFLOAT4X4 Add(XMFLOAT4X4& xmmtx4x4Matrix1, XMFLOAT4X4& xmmtx4x4Matrix2)
	{
		XMFLOAT4X4 xmf4x4Result;
		XMStoreFloat4x4(&xmf4x4Result, XMLoadFloat4x4(&xmmtx4x4Matrix1) + XMLoadFloat4x4(&xmmtx4x4Matrix2));
		return(xmf4x4Result);
	}

	// 행렬의 전치행렬을 구하는 함수
	inline XMFLOAT4X4 Transpose(const XMFLOAT4X4& xmmtx4x4Matrix)
	{
		XMFLOAT4X4 xmmtx4x4Result;
		XMStoreFloat4x4(&xmmtx4x4Result, XMMatrixTranspose(XMLoadFloat4x4(&xmmtx4x4Matrix)));
		return(xmmtx4x4Result);
	}

	// 시야 기반 왼손좌표계 투영변환행렬을 만든다. 
	// FovAngleY:하향시야각(라디안), AspectRatio:뷰 공간의 가로,세로 비율X:Y, NearZ:근평면까지의 거리(0이상), FarZ:원평면까지의 거리(0이상)
	inline XMFLOAT4X4 PerspectiveFovLH(float fovAngleY, float aspectRatio, float nearZ, float farZ)
	{
		XMFLOAT4X4 result;
		XMStoreFloat4x4(&result, XMMatrixPerspectiveFovLH(fovAngleY, aspectRatio, nearZ, farZ));
		return(result);
	}

	inline XMFLOAT4X4 OrthographicLH(float w, float h, float zn, float zf)
	{
		XMFLOAT4X4 result;
		XMStoreFloat4x4(&result, XMMatrixOrthographicLH(w, h, zn, zf));
		return(result);
	}

	inline XMFLOAT4X4 OrthographicOffCenterLH(float l, float r, float b, float t, float zn, float zf)
	{
		XMFLOAT4X4 result;
		XMStoreFloat4x4(&result, XMMatrixOrthographicOffCenterLH(l, r, b, t, zn, zf));
		return(result);
	}

	// 카메라의 위치, Look벡터, Up벡터를 사용하여 왼손좌표계의 카메라변환 행렬을 만든다.
	inline XMFLOAT4X4 LookAtLH(const XMFLOAT3& eyePosition, const XMFLOAT3& lookAtPosition, const XMFLOAT3& upDirection)
	{
		XMFLOAT4X4 result;
		XMStoreFloat4x4(&result, XMMatrixLookAtLH(XMLoadFloat3(&eyePosition), XMLoadFloat3(&lookAtPosition), XMLoadFloat3(&upDirection)));
		return(result);
	}
}

int ReadIntegerFromFile(FILE* pInFile);

float ReadFloatFromFile(FILE* pInFile);

BYTE ReadStringFromFile(FILE* pInFile, char* pstrToken);

void ResourceBarrier(ID3D12GraphicsCommandList* commandList, ID3D12Resource* resource, D3D12_RESOURCE_STATES before, D3D12_RESOURCE_STATES after);

