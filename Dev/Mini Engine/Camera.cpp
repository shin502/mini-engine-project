#include "Camera.h"
#include "Player.h"
Camera::Camera()
{
	m_view = Matrix4x4::Identity();
	m_projection = Matrix4x4::Identity();
	m_viewport = { 0, 0, FRAME_BUFFER_WIDTH , FRAME_BUFFER_HEIGHT, 0.0f, 1.0f };
	m_scissorRect = { 0, 0, FRAME_BUFFER_WIDTH , FRAME_BUFFER_HEIGHT };
	m_offset = XMFLOAT3(0.0f, .0f, 0.0f);
	m_timelag = 0.0f;
	m_pos = XMFLOAT3(0.0f, 0.0f, 0.0f);
	m_right = XMFLOAT3(1.0f, 0.0f, 0.0f);
	m_look = XMFLOAT3(0.0f, 0.0f, 1.0f);
	m_up = XMFLOAT3(0.0f, 1.0f, 0.0f);
	m_pitch = 0.0f;
	m_roll = 0.0f;
	m_yaw = 0.0f;
	m_mode = 0x00;
	CreateProjectionMatrix(1.01f, 5000.0f, ASPECT_RATIO, 60.0f);
	m_player = NULL;

}

Camera::~Camera()
{
}

Camera::Camera(Camera* pCamera)
{
	if (pCamera) {
		*this = *pCamera;
	}
	else {
		m_view = Matrix4x4::Identity();
		m_projection = Matrix4x4::Identity();
		m_viewport = { 0, 0, FRAME_BUFFER_WIDTH , FRAME_BUFFER_HEIGHT, 0.0f, 1.0f };
		m_scissorRect = { 0, 0, FRAME_BUFFER_WIDTH , FRAME_BUFFER_HEIGHT };
		m_offset = XMFLOAT3(0.0f, .0f, 0.0f);
		m_timelag = 0.0f;
		m_pos = XMFLOAT3(0.0f, 0.0f, 0.0f);
		m_right = XMFLOAT3(1.0f, 0.0f, 0.0f);
		m_look = XMFLOAT3(0.0f, 0.0f, 1.0f);
		m_up = XMFLOAT3(0.0f, 1.0f, 0.0f);
		m_pitch = 0.0f;
		m_roll = 0.0f;
		m_yaw = 0.0f;
		CreateProjectionMatrix(1.01f, 5000.0f, ASPECT_RATIO, 60.0f);
		m_player = NULL;
	}

}

void Camera::CreateCbBuffer(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList)
{
	UINT cbElementBytes = ((sizeof(VS_CB_CAMERA_INFO) + 255) & ~255); //256의 배수
	m_cbCamera = ::CreateBufferResource(device.Get(), commandList.Get(), NULL, cbElementBytes, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
	m_cbCamera.Get()->Map(0, NULL, (void**)&m_cbMappedCamera);
}

void Camera::UpateCbBuffer(ComPtr<ID3D12GraphicsCommandList>& commandList)
{
	XMFLOAT4X4 view;
	XMStoreFloat4x4(&view, XMMatrixTranspose(XMLoadFloat4x4(&this->m_view)));
	::memcpy(&m_cbMappedCamera->view, &view, sizeof(XMFLOAT4X4));

	XMFLOAT4X4 projection;
	XMStoreFloat4x4(&projection, XMMatrixTranspose(XMLoadFloat4x4(&this->m_projection)));
	::memcpy(&m_cbMappedCamera->projection, &projection, sizeof(XMFLOAT4X4));

	XMStoreFloat4x4(&view, XMMatrixTranspose(XMLoadFloat4x4(&Matrix4x4::Inverse(this->m_view))));
	::memcpy(&m_cbMappedCamera->Iview, &view, sizeof(XMFLOAT4X4));

	XMStoreFloat4x4(&projection, XMMatrixTranspose(XMLoadFloat4x4(&Matrix4x4::Inverse(this->m_projection))));
	::memcpy(&m_cbMappedCamera->Iprojection, &projection, sizeof(XMFLOAT4X4));

	::memcpy(&m_cbMappedCamera->posW, &m_pos, sizeof(XMFLOAT3));

	D3D12_GPU_VIRTUAL_ADDRESS d3dGpuVirtualAddress = m_cbCamera->GetGPUVirtualAddress();
	commandList.Get()->SetGraphicsRootConstantBufferView(GraphicsRootCamera, d3dGpuVirtualAddress);

}

void Camera::CreateViewMatrix()
{
	m_view = Matrix4x4::LookAtLH(m_pos, m_look, m_up);
	m_look.x = m_view._13; m_look.y = m_view._23; m_look.z = m_view._33;
	m_up.x = m_view._12; m_up.y = m_view._22; m_up.z = m_view._32;
	m_right.x = m_view._11; m_right.y = m_view._21; m_right.z = m_view._31;
}

void Camera::CreateViewMatrix(XMFLOAT3 pos, XMFLOAT3 look, XMFLOAT3 up)
{
	m_pos = pos;
	m_look = look;
	m_up = up;
	CreateViewMatrix();
}

void Camera::CreateFrustum()
{
	XMFLOAT4X4 frustumProj = Matrix4x4::PerspectiveFovLH(XMConvertToRadians(m_fovAngle + 10), m_aspectRatio, m_nearPlaneDistance, m_farPlaneDistance);
	m_frustum.CreateFromMatrix(m_frustum, XMLoadFloat4x4(&frustumProj)); //Create Bounding Frustum
	XMFLOAT4X4 frustumView = m_view;
	frustumView._43 += 10;
	XMMATRIX viewInv = XMMatrixInverse(NULL, XMLoadFloat4x4(&frustumView));
	m_frustum.Transform(m_frustum, viewInv); //transform world
}

BoundingFrustum Camera::GetFrustum()
{
	return m_frustum;
}

void Camera::ReCreateViewMatrix()
{
	//카메라의 z-축을 기준으로 카메라의 좌표축들이 직교하도록 카메라 변환 행렬을 갱신한다.
	//카메라의 z-축 벡터를 정규화한다.
	//m_look = Vector3::Normalize(m_look);
	////카메라의 z-축과 y-축에 수직인 벡터를 x-축으로 설정한다.
	//m_right = Vector3::CrossProduct(m_up, m_look, true);
	////카메라의 z-축과 x-축에 수직인 벡터를 y-축으로 설정한다.
	//m_up = Vector3::CrossProduct(m_look, m_right, true);
	m_view._11 = m_right.x; m_view._12 = m_up.x; m_view._13 = m_look.x;
	m_view._21 = m_right.y; m_view._22 = m_up.y; m_view._23 = m_look.y;
	m_view._31 = m_right.z; m_view._32 = m_up.z; m_view._33 = m_look.z;
	m_view._41 = -Vector3::DotProduct(m_pos, m_right);
	m_view._42 = -Vector3::DotProduct(m_pos, m_up);
	m_view._43 = -Vector3::DotProduct(m_pos, m_look);
}
void Camera::GenerateOrthograhpicsOffCenterMatrix(float l, float r, float b, float t, float zn, float zf)
{
	m_projection = Matrix4x4::OrthographicOffCenterLH(l, r, b, t, zn, zf);
}
void Camera::CreateProjectionMatrix(float nearPlaneDistance, float farPlaneDistance, float aspectRatio, float fovAngle)
{
	m_nearPlaneDistance = nearPlaneDistance;
	m_farPlaneDistance = farPlaneDistance;
	m_aspectRatio = aspectRatio;
	m_fovAngle = fovAngle;
	m_projection = Matrix4x4::PerspectiveFovLH(XMConvertToRadians(fovAngle), aspectRatio, nearPlaneDistance, farPlaneDistance);
}

void Camera::SetViewport(int topLeftX, int topLeftY, int width, int height, float minZ, float maxZ)
{
	m_viewport.TopLeftX = float(topLeftX);
	m_viewport.TopLeftY = float(topLeftY);
	m_viewport.Width = float(width);
	m_viewport.Height = float(height);
	m_viewport.MinDepth = minZ;
	m_viewport.MaxDepth = maxZ;
}

void Camera::SetDefferredViewPort(int topLeftX, int topLeftY, int width, int height, float minZ, float maxZ)
{
	m_Defferredviewport[0].TopLeftX = float(topLeftX);
	m_Defferredviewport[0].TopLeftY = float(topLeftY);
	m_Defferredviewport[0].Width = float(width);
	m_Defferredviewport[0].Height = float(height);
	m_Defferredviewport[0].MinDepth = minZ;
	m_Defferredviewport[0].MaxDepth = maxZ;

	m_Defferredviewport[1].TopLeftX = float(topLeftX) + float(width);
	m_Defferredviewport[1].TopLeftY = float(topLeftY);
	m_Defferredviewport[1].Width = float(width);
	m_Defferredviewport[1].Height = float(height);
	m_Defferredviewport[1].MinDepth = minZ;
	m_Defferredviewport[1].MaxDepth = maxZ;

	m_Defferredviewport[2].TopLeftX = float(topLeftX);
	m_Defferredviewport[2].TopLeftY = float(topLeftY) + float(height);
	m_Defferredviewport[2].Width = float(width);
	m_Defferredviewport[2].Height = float(height);
	m_Defferredviewport[2].MinDepth = minZ;
	m_Defferredviewport[2].MaxDepth = maxZ;

	m_Defferredviewport[3].TopLeftX = float(topLeftX) + float(width);
	m_Defferredviewport[3].TopLeftY = float(topLeftY) + float(height);
	m_Defferredviewport[3].Width = float(width);
	m_Defferredviewport[3].Height = float(height);
	m_Defferredviewport[3].MinDepth = minZ;
	m_Defferredviewport[3].MaxDepth = maxZ;
}

void Camera::SetScissorRect(LONG leftX, LONG topY, LONG rightX, LONG bottomY)
{
	m_scissorRect.left = leftX;
	m_scissorRect.top = topY;
	m_scissorRect.right = rightX;
	m_scissorRect.bottom = bottomY;
}

void Camera::SetDefferredScissorRect(LONG leftX, LONG topY, LONG rightX, LONG bottomY)
{
	for (int i = 0; i < 4; ++i) {
		m_DefferredscissorRect[i].left = leftX;
		m_DefferredscissorRect[i].top = topY;
		m_DefferredscissorRect[i].right = rightX;
		m_DefferredscissorRect[i].bottom = bottomY;
	}
}

void Camera::SetOrthoInfo(XMFLOAT3 center, float radius)
{
	this->orthoCenter = center;
	this->orthoRadius = radius;
}


void Camera::SetViewportsAndScissorRects(ComPtr<ID3D12GraphicsCommandList>& commandList)
{
	commandList.Get()->RSSetViewports(1, &m_viewport);
	commandList.Get()->RSSetScissorRects(1, &m_scissorRect);
}

void Camera::SetDefferedViewPortsAndScissorRects(ComPtr<ID3D12GraphicsCommandList>& commandList)
{
	commandList.Get()->RSSetViewports(4, &m_Defferredviewport[0]);
	commandList.Get()->RSSetScissorRects(4, &m_DefferredscissorRect[0]);
}

void Camera::SetLookAt(const XMFLOAT3& lookAt)
{
	//현재 카메라의 위치에서 플레이어를 바라보기 위한 카메라 변환 행렬을 생성한다.
	XMFLOAT4X4 tempLookAt = Matrix4x4::LookAtLH(m_pos, lookAt, XMFLOAT3(0.0f, 1.0f, 0.0f));
	//카메라 변환 행렬에서 카메라의 x-축, y-축, z-축을 구한다.
	m_right = XMFLOAT3(tempLookAt._11, tempLookAt._21, tempLookAt._31);
	m_up = XMFLOAT3(tempLookAt._12, tempLookAt._22, tempLookAt._32);
	m_look = XMFLOAT3(tempLookAt._13, tempLookAt._23, tempLookAt._33);
	m_lookAtWorld = lookAt;

	m_view = tempLookAt;
}

void Camera::SetPos(const XMFLOAT3& pos)
{
	m_pos = pos;
}

void Camera::SetPlayer(Player* player)
{
	m_player = player;
}

void Camera::SetTimeLag(float timelag)
{
	m_timelag = timelag;
}

void Camera::SetOffset(XMFLOAT3 offset)
{
	m_offset = offset;
}

void Camera::SetTerrainHeight(float theight)
{
	terrainHeight = theight;
}

void Camera::Move(const XMFLOAT3& xmf3Shift)
{
	m_pos.x += xmf3Shift.x;
	m_pos.y += xmf3Shift.y;
	m_pos.z += xmf3Shift.z;
}

XMFLOAT3 Camera::GetPosition()
{
	return m_pos;
}

XMFLOAT3 Camera::GetLocalLook()
{
	return m_look;
}

XMFLOAT3 Camera::GetLocalUp()
{
	return m_up;
}

XMFLOAT3 Camera::GetLocalRight()
{
	return m_right;
}

XMFLOAT4X4 Camera::GetView()
{
	return m_view;
}

XMFLOAT4X4 Camera::GetProj()
{
	return m_projection;
}

DWORD Camera::GetMode()
{
	return m_mode;
}

D3D12_VIEWPORT& Camera::GetViewPort()
{
	return m_viewport;
}

D3D12_RECT& Camera::GetSissorRect()
{
	return m_scissorRect;
}

void Camera::UpdateLightCBbuffer(Camera* camera, ComPtr<ID3D12GraphicsCommandList>& commandList)
{
	XMFLOAT4X4 view;
	XMStoreFloat4x4(&view, XMMatrixTranspose(XMLoadFloat4x4(&camera->GetView())));
	::memcpy(&m_cbMappedCamera->view, &view, sizeof(XMFLOAT4X4));

	XMFLOAT4X4 projection;
	XMStoreFloat4x4(&projection, XMMatrixTranspose(XMLoadFloat4x4(&camera->GetProj())));
	::memcpy(&m_cbMappedCamera->projection, &projection, sizeof(XMFLOAT4X4));

	XMStoreFloat4x4(&view, XMMatrixTranspose(XMLoadFloat4x4(&Matrix4x4::Inverse(camera->GetView()))));
	::memcpy(&m_cbMappedCamera->Iview, &view, sizeof(XMFLOAT4X4));

	XMStoreFloat4x4(&projection, XMMatrixTranspose(XMLoadFloat4x4(&Matrix4x4::Inverse(camera->GetProj()))));
	::memcpy(&m_cbMappedCamera->Iprojection, &projection, sizeof(XMFLOAT4X4));

	::memcpy(&m_cbMappedCamera->posW, &camera->GetPosition(), sizeof(XMFLOAT3));

	D3D12_GPU_VIRTUAL_ADDRESS d3dGpuVirtualAddress = m_cbCamera.Get()->GetGPUVirtualAddress();
	commandList.Get()->SetGraphicsRootConstantBufferView(GraphicsRootCamera, d3dGpuVirtualAddress);

}

void Camera::SetLightViewportsAndScissorRects(Camera* camera, ComPtr<ID3D12GraphicsCommandList>& commandList)
{
	commandList.Get()->RSSetViewports(1, &camera->GetViewPort());
	commandList.Get()->RSSetScissorRects(1, &camera->GetSissorRect());
}

XMFLOAT3 Camera::GetOffset()
{
	return m_offset;
}

SpaceShipCamera::SpaceShipCamera(Camera* Camera) : Camera(Camera)
{
	m_mode = SPACESHIP_CAMERA;
}

void SpaceShipCamera::Rotate(float pitch, float yaw, float roll)
{
	if (m_player && (pitch != 0.0f)) {
		XMFLOAT3 pRight = m_player->GetRight();
		
		XMVECTOR q = XMQuaternionRotationNormal(XMLoadFloat3(&pRight), XMConvertToRadians(pitch));
		XMMATRIX mtxRotate = XMMatrixRotationQuaternion(q);
		m_right = Vector3::TransformNormal(m_right, mtxRotate);
		m_up = Vector3::TransformNormal(m_up, mtxRotate);
		m_look = Vector3::TransformNormal(m_look, mtxRotate);
		m_pos = Vector3::Subtract(m_pos, m_player->GetPosition());//플레이어랑 카메라랑 위치를 같게한다.
		m_pos = Vector3::TransformCoord(m_pos, mtxRotate); //로테이트
		m_pos = Vector3::Add(m_pos, m_player->GetPosition()); // 
	}
	if (m_player && (yaw != 0.0f)) {
		XMFLOAT3 pUp = m_player->GetUp();
		XMMATRIX mtxRotate = XMMatrixRotationQuaternion(XMQuaternionRotationNormal(XMLoadFloat3(&pUp), XMConvertToRadians(yaw)));
		m_right = Vector3::TransformNormal(m_right, mtxRotate);
		m_up = Vector3::TransformNormal(m_up, mtxRotate);
		m_look = Vector3::TransformNormal(m_look, mtxRotate);
		m_pos = Vector3::Subtract(m_pos, m_player->GetPosition());//플레이어랑 카메라랑 위치를 같게한다.
		m_pos = Vector3::TransformCoord(m_pos, mtxRotate); //로테이트
		m_pos = Vector3::Add(m_pos, m_player->GetPosition()); // 
	}
	if (m_player && (roll != 0.0f)) {
		XMFLOAT3 pLook = m_player->GetLook();
		XMMATRIX mtxRotate = XMMatrixRotationQuaternion(XMQuaternionRotationNormal(XMLoadFloat3(&pLook), XMConvertToRadians(roll)));
		m_right = Vector3::TransformNormal(m_right, mtxRotate);
		m_up = Vector3::TransformNormal(m_up, mtxRotate);
		m_look = Vector3::TransformNormal(m_look, mtxRotate);
		m_pos = Vector3::Subtract(m_pos, m_player->GetPosition());//플레이어랑 카메라랑 위치를 같게한다.
		m_pos = Vector3::TransformCoord(m_pos, mtxRotate); //로테이트
		m_pos = Vector3::Add(m_pos, m_player->GetPosition()); // 다시 플레이어 앞으로 간다.
	}


}

FirstPersonCamera::FirstPersonCamera(Camera* camera) : Camera(camera)
{
	m_mode = FIRST_PERSON_CAMERA;
	if (camera) {
		if (camera->GetMode() == SPACESHIP_CAMERA) {
			m_up = XMFLOAT3(0.0f, 1.0f, 0.0f);
			m_right.y = 0.0f;
			m_look.y = 0.0f;
			m_right = Vector3::Normalize(m_right);
			m_look = Vector3::Normalize(m_look);
		}
	}

}

FirstPersonCamera::~FirstPersonCamera()
{


}

void FirstPersonCamera::Rotate(float pitch, float yaw, float roll)
{
	if (pitch != 0.0f)
	{
		XMMATRIX xmmtxRotate = XMMatrixRotationQuaternion(XMQuaternionRotationNormal(XMLoadFloat3(&m_right), XMConvertToRadians(pitch)));
		m_look = Vector3::TransformNormal(m_look, xmmtxRotate);
		m_up = Vector3::TransformNormal(m_up, xmmtxRotate);
		m_right = Vector3::TransformNormal(m_right, xmmtxRotate);
	}
	if (m_player && (yaw != 0.0f))
	{
		XMFLOAT3 xmf3Up = m_player->GetUp();
		XMMATRIX xmmtxRotate = XMMatrixRotationQuaternion(XMQuaternionRotationNormal(XMLoadFloat3(&xmf3Up), XMConvertToRadians(yaw)));
		m_look = Vector3::TransformNormal(m_look, xmmtxRotate);
		m_up = Vector3::TransformNormal(m_up, xmmtxRotate);
		m_right = Vector3::TransformNormal(m_right, xmmtxRotate);
	}
	if (m_player && (roll != 0.0f))
	{
		XMFLOAT3 xmf3Look = m_player->GetLook();
		XMMATRIX xmmtxRotate = XMMatrixRotationQuaternion(XMQuaternionRotationNormal(XMLoadFloat3(&xmf3Look), XMConvertToRadians(roll)));
		m_pos = Vector3::Subtract(m_pos, m_player->GetPosition());
		m_pos = Vector3::TransformCoord(m_pos, xmmtxRotate);
		m_pos = Vector3::Add(m_pos, m_player->GetPosition());
		m_look = Vector3::TransformNormal(m_look, xmmtxRotate);
		m_up = Vector3::TransformNormal(m_up, xmmtxRotate);
		m_right = Vector3::TransformNormal(m_right, xmmtxRotate);
	}
}

ThirdPersonCamera::ThirdPersonCamera(Camera* pCamera) : Camera(pCamera)
{
	m_mode = THIRD_PERSON_CAMERA;
	if (pCamera)
	{

		m_up = XMFLOAT3(0.0f, 1.0f, 0.0f);
		m_right.y = 0.0f;
		m_look.y = 0.0f;
		m_right = Vector3::Normalize(m_right);
		m_look = Vector3::Normalize(m_look);

		m_player->SetLook(m_look);
		m_player->SetRight(m_right);
		m_player->SetUp((m_up));

		m_player->SetLookVector(m_player->GetLook());
		m_player->SetRightVector(m_player->GetRight());
		m_player->SetUpVector(m_player->GetUp());


		if (pCamera->GetMode() == SPACESHIP_CAMERA)
		{
			
		}
	}

}

void ThirdPersonCamera::Update(XMFLOAT3& xmf3LookAt, float fTimeElapsed)
{
	if (m_player) { // player exist
		XMFLOAT4X4 rt = Matrix4x4::Identity();
		XMFLOAT3 rightv = m_player->GetRightVector();
		XMFLOAT3 upv = m_player->GetUpVector();
		XMFLOAT3 lookv = m_player->GetLookVector();
		rt._11 = rightv.x;  rt._12 = rightv.y; rt._13 = rightv.z;
		rt._21 = upv.x;		rt._22 = upv.y;	   rt._23 = upv.z;
		rt._31 = lookv.x;	rt._32 = lookv.y;  rt._33 = lookv.z;

		XMFLOAT3 offset = Vector3::TransformCoord(m_offset, rt); // 오프셋에 로테이트 변환을 해준다
		XMFLOAT3 pos = Vector3::Add(m_player->GetPosition(), offset); // 플레이어 위치에다가 변환된 오프셋을 더해준다.
		//rotate->transform
		XMFLOAT3 dir = Vector3::Subtract(pos, m_pos);
		float fLength = Vector3::Length(dir);
		dir = Vector3::Normalize(dir);
		float timeLagScale = (m_timelag) ? fTimeElapsed * (1.0f / m_timelag) : 1.0f;
		float fDistance = fLength * timeLagScale;
		if (fDistance > fLength) fDistance = fLength;
		if (fLength < 0.01f) fDistance = fLength;
		if (fDistance > 0) {
			m_pos = Vector3::Add(m_pos, dir, fDistance);
			if (m_pos.y < terrainHeight + 2.0f) {
				m_pos.y = terrainHeight + 2.0f;
			}
			
			SetLookAt(xmf3LookAt);
		}

	}

}

void ThirdPersonCamera::SetLookAt(XMFLOAT3& vLookAt)
{
	XMFLOAT4X4 mtxLookAt = Matrix4x4::LookAtLH(m_pos, m_look, m_player->GetUp());
	m_right = XMFLOAT3(mtxLookAt._11, mtxLookAt._21, mtxLookAt._31);
	m_up = XMFLOAT3(mtxLookAt._12, mtxLookAt._22, mtxLookAt._32);
	m_look = XMFLOAT3(mtxLookAt._13, mtxLookAt._23, mtxLookAt._33);
	//view 변환행렬 
}
