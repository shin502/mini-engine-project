#include "Shadow.h"

Shadow::~Shadow()
{
	delete[] m_cascadePos;
	delete[] m_lightCamera;
}

Shadow::Shadow(Camera* m_camera, Scene* scene)
{
	m_mainCamera = m_camera;
	m_lightCamera = new Camera[NUM_CASCADES];
	m_cascadeNum = NUM_CASCADES;
	m_scene = scene;
	m_cascadeBias = 1.001f;
}

void Shadow::PrePareShadowMap()
{
	for (int i = 0; i < m_cascadeNum; ++i)
	{
		m_lightCamera[i].CreateViewMatrix(XMFLOAT3(0.0f, 0.0f, 0.0f), Vector3::Normalize(m_scene->GetLight()->lights[0].direction), XMFLOAT3(0.0f, 1.0f, 0.0f));
	}


	XMFLOAT4X4 camView = m_mainCamera->GetView();
	XMFLOAT4X4 camViewInv = ::Matrix4x4::Inverse(camView);

	float ar = 1.0f / m_mainCamera->GetAspectRatio();
	float fov = m_mainCamera->GetFOV() + 30;
	float tanThetaX = tanf(::XMConvertToRadians(fov / 2.0f));
	float tanThetaY = tanf(::XMConvertToRadians(fov * ar / 2.0f));

	for (int i = 0; i < m_cascadeNum; ++i)
	{
		XMFLOAT4X4 lightView = m_lightCamera[i].GetView();

		float xn = m_cascadePos[i] * tanThetaX;
		float xf = m_cascadePos[i + 1] * tanThetaX;
		float yn = m_cascadePos[i] * tanThetaY;
		float yf = m_cascadePos[i + 1] * tanThetaY;

		XMFLOAT3 frustumCornersV[8] = {
			// near plane
			XMFLOAT3(xn, yn, m_cascadePos[i]),
			XMFLOAT3(-xn, yn, m_cascadePos[i]),
			XMFLOAT3(xn, -yn, m_cascadePos[i]),
			XMFLOAT3(-xn, -yn, m_cascadePos[i]),

			// far plane
			XMFLOAT3(xf, yf, m_cascadePos[i + 1]),
			XMFLOAT3(-xf, yf, m_cascadePos[i + 1]),
			XMFLOAT3(xf, -yf, m_cascadePos[i + 1]),
			XMFLOAT3(-xf, -yf, m_cascadePos[i + 1])
		};

		XMFLOAT3 fustumCornersL[8];
		float minX = FLT_MAX;
		float maxX = -FLT_MAX;
		float minY = FLT_MAX;
		float maxY = -FLT_MAX;
		float minZ = FLT_MAX;
		float maxZ = -FLT_MAX;
		for (int j = 0; j < 8; ++j)
		{
			// CamView -> World
			XMFLOAT3 frustumCornersW = Vector3::TransformCoord(frustumCornersV[j], camViewInv);
			// World -> LightView
			fustumCornersL[j] = Vector3::TransformCoord(frustumCornersW, lightView);
			//lightView공간에서 퓨러스텀의 x,y,z의 최대최소를 구한다.
			minX = min(minX, fustumCornersL[j].x);
			maxX = max(maxX, fustumCornersL[j].x);
			minY = min(minY, fustumCornersL[j].y);
			maxY = max(maxY, fustumCornersL[j].y);
			minZ = min(minZ, fustumCornersL[j].z);
			maxZ = max(maxZ, fustumCornersL[j].z);
		}
		//정육면체의 사각형의 센터값을 구해준다.
		XMFLOAT3 center = XMFLOAT3((maxX - minX) / 2.0f + minX, (maxY - minY) / 2.0f + minY, (maxZ - minZ) / 2.0f + minZ);
		float xLength = (maxX - minX) / 2.0f;
		float yLength = (maxY - minY) / 2.0f;
		float zLength = (maxZ - minZ) / 2.0f;

		float radius = xLength;
		radius = max(radius, yLength);
		radius = max(radius, zLength);
		radius = std::ceil(radius);
		radius = m_cascadePos[i + 1];

		XMFLOAT3 ogMin = XMFLOAT3(center.x - radius, center.y - radius, center.z - radius);
		XMFLOAT3 ogMax = XMFLOAT3(center.x + radius, center.y + radius, center.z + radius);

		// Micro Soft Code Shimmering shadow edge 를 방지하기위해서 소수점을 없애준다.
		float texel = radius * 2 / SHADOW_BUFFER_SIZE;

		ogMin.x /= texel;
		ogMin.y /= texel;
		ogMin.z /= texel;

		ogMin = Vector3::Floor(ogMin);

		ogMin.x *= texel;
		ogMin.y *= texel;
		ogMin.z *= texel;

		ogMax.x /= texel;
		ogMax.y /= texel;
		ogMax.z /= texel;

		ogMax = Vector3::Floor(ogMax);

		ogMax.x *= texel;
		ogMax.y *= texel;
		ogMax.z *= texel;
		ogMin.z -= 100;

		m_lightCamera[i].GenerateOrthograhpicsOffCenterMatrix(ogMin.x, ogMax.x, ogMin.y, ogMax.y, ogMin.z, ogMax.z);
		m_lightCamera[i].SetOrthoInfo(center, radius);
	}

}

void Shadow::CreateLightCamera(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList> commandList)
{
	m_cascadePos = new float[m_cascadeNum + 1];
	m_cascadePos[0] = 1.01f;
	m_cascadePos[1] = 15.0f;
	m_cascadePos[2] = 50.0f;
	m_cascadePos[3] = 300.0f;

	XMFLOAT3 direction = m_scene->GetLight()->lights[0].direction;

	for (int i = 0; i < m_cascadeNum; ++i)
	{
		m_lightCamera[i].SetViewport(SHADOW_BUFFER_SIZE * i, 0, SHADOW_BUFFER_SIZE, SHADOW_BUFFER_SIZE);
		m_lightCamera[i].SetScissorRect(SHADOW_BUFFER_SIZE * i + 2, 2, (SHADOW_BUFFER_SIZE * (1 + i)) - 2, SHADOW_BUFFER_SIZE - 2);
		m_lightCamera[i].CreateViewMatrix(XMFLOAT3(0.0f, 0.0f, 0.0f), direction, XMFLOAT3(0.0f, 1.0f, 0.0f));
		m_lightCamera[i].CreateCbBuffer(device, commandList);
	}

}

void Shadow::InitCbInform(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
	m_lightBuffer = ::CreateBufferResource(device, commandList, NULL, sizeof(VS_CB_SHADOW_INFO), D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
	m_lightBuffer->Map(0, NULL, (void**)&m_lightMappedCamera);
}

void Shadow::UpdateCbState(ID3D12GraphicsCommandList* commandList)
{
	for (int i = 0; i < m_cascadeNum; ++i) {
		XMFLOAT4X4 viewProjection;
		XMStoreFloat4x4(&viewProjection, XMMatrixTranspose(XMLoadFloat4x4(&Matrix4x4::Multiply(m_lightCamera[i].GetView(), m_lightCamera[i].GetProj()))));
		::memcpy(&m_lightMappedCamera->viewProjection[i], &viewProjection, sizeof(XMFLOAT4X4));
		m_lightMappedCamera->cascadeLength[i].x = m_cascadePos[i + 1] * m_cascadeBias;
		m_lightMappedCamera->cascadeNum = float(m_cascadeNum);
		m_lightMappedCamera->shadowTexSize = 1.0f / SHADOW_BUFFER_SIZE;
	}
	D3D12_GPU_VIRTUAL_ADDRESS d3dGpuVirtualAddress = m_lightBuffer->GetGPUVirtualAddress();
	commandList->SetGraphicsRootConstantBufferView(GraphicsRootLightCamera, d3dGpuVirtualAddress);

}

void Shadow::SetMainCamera(Camera* mainCamera)
{
	m_mainCamera = mainCamera;
}

Camera* Shadow::GetLightCamera(int cIdx)
{
	return &m_lightCamera[cIdx];
}
