#pragma once
#include "Mesh.h"
#include "SkinnedMesh.h"
#include "StandardMesh.h"

struct objManageMent {
	int idx;
	bool isVisable;
	XMFLOAT4X4 transform;
	float grassNumber;

};

class Texture {
public:

	~Texture();

	ID3D12Resource** m_textures = NULL;
	ID3D12Resource** m_texturesUploadBuffer = NULL;
	int m_textureNum;

	Texture(int texturesNum, UINT textureType, int samplers) {
		this->m_textureNum = texturesNum;
		m_texturesUploadBuffer = new ID3D12Resource * [this->m_textureNum];
		m_textures = new ID3D12Resource * [this->m_textureNum];
	}

	Texture(ID3D12Resource** textures, ID3D12Resource** texturesUploadBuffer, int textureNum) {
		m_textures = textures;
		m_texturesUploadBuffer = texturesUploadBuffer;
		m_textureNum = textureNum;
	}
	void LoadTextureFromFile(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, wchar_t* fileName, UINT nIndex, bool isDDSFile = true);
	void ReleaseUploadBuffer();


};

struct VS_VB_INSTANCE_OBJECT
{
	XMFLOAT4X4 transform;
};
struct VS_GB_INSTANCE_OBJECT
{
	XMFLOAT4X4 transform;
	float moveGrass;
};

class Shader {
private:

protected:
	vector<objManageMent> m_objVec;

	ComPtr<ID3D12PipelineState> m_bpipelineState;
	ComPtr<ID3D12PipelineState> m_ShadowPipeLine;

	ID3DBlob* m_vsBlob = NULL;
	ID3DBlob* m_psBlob = NULL;
	ID3DBlob* m_csBlob = NULL;
	ID3DBlob* m_gsBlob = NULL;
public:
	Shader();
	virtual ~Shader();
	virtual void CreateRenderPipeLineState(ComPtr<ID3D12Device>& device, ComPtr<ID3D12RootSignature>& rootsignature);
	virtual void CreatebPipeLineState(ComPtr<ID3D12Device>& device, ComPtr<ID3D12RootSignature>& rootsignature);
	void CreateLinePipeLineState(ComPtr<ID3D12Device>& device, ComPtr<ID3D12RootSignature>& rootsignature);
	virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();
	virtual D3D12_RASTERIZER_DESC CreateRasterizerState();
	virtual D3D12_BLEND_DESC CreateBlendState();
	virtual D3D12_DEPTH_STENCIL_DESC CreateDepthStencilState();
	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
	virtual D3D12_SHADER_BYTECODE CreatePixelShader();
	virtual D3D12_SHADER_BYTECODE CreateGeoMetryShader();

	virtual D3D12_SHADER_BYTECODE CreateShadowVertexShader();
	virtual D3D12_SHADER_BYTECODE CreateShadowPixelShader();
	virtual D3D12_SHADER_BYTECODE CreateShadowGeoMetryShader();

	virtual D3D12_SHADER_BYTECODE CreateComputeShader() { return D3D12_SHADER_BYTECODE(); }
	virtual void PrePareRender(ComPtr<ID3D12GraphicsCommandList>& commandList);
	virtual void SetPipeLine(ID3D12GraphicsCommandList* commandList, int idx) {}
	virtual void SetShadowPipe(ID3D12GraphicsCommandList* commandList, int idx);
	virtual int GetShaderNumber();
	
	vector<objManageMent>& GetObjVec();
	void ReleaseUploadBuffer();
	D3D12_SHADER_BYTECODE CompileShaderFromFile(WCHAR* pszFileName, LPCSTR pszShaderName, LPCSTR pszShaderProfile, ID3DBlob** ppd3dShaderBlob);
};

class TerrainShader : public Shader {
public:
	virtual int GetShaderNumber();
	virtual void CreatebPipeLineState(ComPtr<ID3D12Device>& device, ComPtr<ID3D12RootSignature>& rootsignature);
	virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();
	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
	virtual D3D12_SHADER_BYTECODE CreatePixelShader();
	virtual D3D12_SHADER_BYTECODE CreateShadowVertexShader();
	virtual D3D12_SHADER_BYTECODE CreateShadowPixelShader();
};

class UIShader : public Shader {
public:
	virtual int GetShaderNumber();

	virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();
	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
	virtual D3D12_SHADER_BYTECODE CreatePixelShader();
};

class LightShader : public UIShader {
public:
	virtual void CreateRenderPipeLineState(ComPtr<ID3D12Device>& device, ComPtr<ID3D12RootSignature>& rootsignature);
	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
	virtual D3D12_SHADER_BYTECODE CreatePixelShader();
	virtual D3D12_DEPTH_STENCIL_DESC CreateDepthStencilState();
};

class PostShader : public UIShader {

public:
	virtual void CreateRenderPipeLineState(ComPtr<ID3D12Device>& device, ComPtr<ID3D12RootSignature>& rootsignature);
	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
	virtual D3D12_SHADER_BYTECODE CreatePixelShader();
};

class RSShader : public Shader {
private:
	ComPtr<ID3D12Resource> m_isResource;
	VS_GB_INSTANCE_OBJECT* m_MappedisObjects;
	D3D12_VERTEX_BUFFER_VIEW m_objectBufferView;
public:
	~RSShader();
	virtual int GetShaderNumber();
	virtual void CreatebPipeLineState(ComPtr<ID3D12Device>& device, ComPtr<ID3D12RootSignature>& rootsignature);
	virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();
	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
	virtual D3D12_SHADER_BYTECODE CreatePixelShader();
	virtual D3D12_SHADER_BYTECODE CreateGeoMetryShader();

	virtual D3D12_SHADER_BYTECODE CreateShadowVertexShader();
	virtual D3D12_SHADER_BYTECODE CreateShadowPixelShader();
	virtual D3D12_SHADER_BYTECODE CreateShadowGeoMetryShader();

	virtual void CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	virtual void SetPipeLine(ID3D12GraphicsCommandList* commandList, int idx);
	virtual void SetShadowPipe(ID3D12GraphicsCommandList* commandList, int idx);
	VS_GB_INSTANCE_OBJECT* GetMappedObject();
	D3D12_VERTEX_BUFFER_VIEW* GetObjectBufferView();
};