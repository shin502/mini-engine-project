#include "Scene.h"

int	Scene::rootArgumentCurrentIdx = 0;

Scene::~Scene()
{
    if (m_materials)
        delete m_materials;


    for (int i = 0; i < m_GameObjects.size(); ++i) {
        for (int j = 0; j < m_GameObjects[i].size(); ++j) {
            delete m_GameObjects[i][j];
        }
    }

    for (int i = 0; i < m_UIObjects.size(); ++i) {
        for (int j = 0; j < m_UIObjects[i].size(); ++j) {
            delete m_UIObjects[i][j];
        }
    }

    for (int i = 0; i < m_LightObjects.size(); ++i) {
        for (int j = 0; j < m_LightObjects[i].size(); ++j) {
            delete m_LightObjects[i][j];
        }
    }

    //release scene textures
    for (int i = 0; i < m_textures.size(); ++i) {
        if (m_textures[i]->m_textures) {
            for (int j = 0; j < m_textures[i]->m_textureNum; ++j) {
                m_textures[i]->m_textures[j]->Release();
            }
            delete[] m_textures[i]->m_textures;
            m_textures[i]->m_textures = NULL;
            delete m_textures[i];
            m_textures[i] = NULL;

        }
    }

    for (int i = 0; i < m_Modelvec.size(); ++i) {
        m_Modelvec[i]->modelRootObject->ReleaseObj();
        //delete m_Modelvec[i]->animationSets;
        delete m_Modelvec[i];
        m_Modelvec[i] = NULL;
    }

    for (int i = 0; i < m_Objects.size(); ++i) {
        m_Objects[i]->ReleaseObj();
        m_Objects[i] = NULL;
    }
    m_Modelvec.clear();
    m_textures.clear();
    QuadTree* tmp = m_QuadRoot;
    DelQuadTree(tmp);
}

D3D12_ROOT_PARAMETER Scene::CreateRootDesCriptor(D3D12_ROOT_PARAMETER_TYPE pt, UINT sgn, UINT rs, D3D12_SHADER_VISIBILITY sv)
{
    D3D12_ROOT_PARAMETER rootParameters;
    rootParameters.ParameterType = pt;
    rootParameters.Descriptor.ShaderRegister = sgn;
    rootParameters.Descriptor.RegisterSpace = rs;
    rootParameters.ShaderVisibility = sv;
    return rootParameters;
}

D3D12_ROOT_PARAMETER Scene::CreateRootTable(D3D12_ROOT_PARAMETER_TYPE pt, UINT ndr, D3D12_DESCRIPTOR_RANGE* dr, D3D12_SHADER_VISIBILITY sv)
{
    D3D12_ROOT_PARAMETER rootParameters;;
    rootParameters.ParameterType = pt;
    rootParameters.DescriptorTable.NumDescriptorRanges = ndr;
    rootParameters.DescriptorTable.pDescriptorRanges = dr;
    rootParameters.ShaderVisibility = sv;

    return rootParameters;
}

D3D12_ROOT_PARAMETER Scene::CreateRoot32Constants(D3D12_ROOT_PARAMETER_TYPE pt, UINT bv, UINT sg, UINT rs, D3D12_SHADER_VISIBILITY sv)
{
    D3D12_ROOT_PARAMETER rootParameters;
    rootParameters.ParameterType = pt;
    rootParameters.Constants.Num32BitValues = bv;
    rootParameters.Constants.ShaderRegister = sg;
    rootParameters.Constants.RegisterSpace = rs;
    rootParameters.ShaderVisibility = sv;

    return rootParameters;
}



D3D12_DESCRIPTOR_RANGE Scene::CreateDesCriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE rt, UINT ndr, UINT bsr, UINT rs, UINT oid)
{
    D3D12_DESCRIPTOR_RANGE tdr;
    tdr.RangeType = rt;
    tdr.NumDescriptors = ndr;
    tdr.BaseShaderRegister = bsr;
    tdr.RegisterSpace = rs;
    tdr.OffsetInDescriptorsFromTableStart = oid;
    return tdr;
}

D3D12_STATIC_SAMPLER_DESC Scene::CraeteSamplerDesc(D3D12_FILTER fillter, D3D12_TEXTURE_ADDRESS_MODE u, D3D12_TEXTURE_ADDRESS_MODE v, D3D12_TEXTURE_ADDRESS_MODE w, FLOAT Mip, UINT mat, D3D12_COMPARISON_FUNC cpf, D3D12_STATIC_BORDER_COLOR bc, FLOAT minLOD, FLOAT maxLOD, UINT sr, UINT rs, D3D12_SHADER_VISIBILITY svb)
{
    D3D12_STATIC_SAMPLER_DESC spDesc;
    spDesc.Filter = fillter;
    spDesc.AddressU = u;
    spDesc.AddressV = v;
    spDesc.AddressW = w;
    spDesc.MipLODBias = Mip;
    spDesc.MaxAnisotropy = mat;
    spDesc.ComparisonFunc = cpf;
    spDesc.MinLOD = minLOD;
    spDesc.MaxLOD = maxLOD;
    spDesc.ShaderRegister = sr;
    spDesc.RegisterSpace = rs;
    spDesc.ShaderVisibility = svb;

    return spDesc;
}

void Scene::UpdateObjectPosition()
{
    //update object height -> to terrain height 
    TerrainMesh* tmesh = (TerrainMesh*)m_GameObjects[E_TERRAINOBJ][0]->GetMesh(0);

    for (int i = 0; i < m_GameObjects.size(); ++i) {
        if (i != E_TERRAINOBJ) {
            if (i == E_BILLBOARDOBJ || i == E_SKYBOXOBJ || i == E_NODEOBJ)
                continue;

            for (int j = 0; j < m_GameObjects[i].size(); ++j) {
                if (m_GameObjects[i][j]->GetisPlayer() == true) // already do it player.cpp
                    continue;
                XMFLOAT3 pos = m_GameObjects[i][j]->GetPosition();
                XMFLOAT3 tPos = Vector3::TransformCoord(pos, Matrix4x4::Inverse(m_GameObjects[E_TERRAINOBJ][1]->GetWorld()));
                float height = tmesh->GetHeightBylerp(tPos.x, tPos.z);
                if (i == E_GRASSOBJ)
                    height += 1.0f;

                int lightIdx = m_GameObjects[i][j]->GetLightIdx();
                if (lightIdx != FAILNUMBER) {
                    if (i == E_LAMPOBJ && j != 0) {
                        m_lights.lights[lightIdx].position = pos;
                        m_lights.lights[lightIdx].position.y = height + 8.0f;
                    }
                }
                m_GameObjects[i][j]->SetTransformHeight(height);
            }
        }
    }
}

void Scene::LoadandCreateTextureResource(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
    //detailTex 3
    int texCnt = 0;
    ID3D12Resource** textures = new ID3D12Resource * [TerrainDetailTextureNum];
    ID3D12Resource** texturesUploadBuffer = new ID3D12Resource * [TerrainDetailTextureNum];
    for (int i = 0; i < TerrainDetailTextureNum; ++i) {
        wstring fileName = L"Assets/Image/Detail" + to_wstring(i) + L".dds";
        wchar_t* tmpwc = const_cast<wchar_t*>(fileName.c_str());
        textures[i] = ::CreateTextureResourceFromFile(device, commandList, tmpwc, &texturesUploadBuffer[i], D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
    }
    CreateShaderResourceViews(device, commandList, textures, GraphicsRootTerrainDetail, TerrainDetailTextureNum, ResourceTexture2D);
    Texture* tmptex = new Texture(textures, texturesUploadBuffer, TerrainDetailTextureNum);
    m_textures.emplace_back(tmptex);
    m_RootArguIdx.emplace_back(texCnt);
    texCnt += TerrainDetailTextureNum;


    //terrain detail normal
    ID3D12Resource** dntextures = new ID3D12Resource * [TerrainNormalTextureNum];
    ID3D12Resource** dntexturesUploadBuffer = new ID3D12Resource * [TerrainNormalTextureNum];
    for (int i = 0; i < TerrainNormalTextureNum; ++i) {
        wstring fileName = L"Assets/Image/Detail" + to_wstring(i) + L"_n.dds";
        wchar_t* tmpwc = const_cast<wchar_t*>(fileName.c_str());
        dntextures[i] = ::CreateTextureResourceFromFile(device, commandList, tmpwc, &dntexturesUploadBuffer[i], D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
    }
    CreateShaderResourceViews(device, commandList, dntextures, GraphicsRootTerrainNormal, TerrainNormalTextureNum, ResourceTexture2D);
    Texture* tmptex0 = new Texture(dntextures, dntexturesUploadBuffer, TerrainNormalTextureNum);
    m_textures.emplace_back(tmptex0);
    m_RootArguIdx.emplace_back(texCnt);
    texCnt += TerrainNormalTextureNum;

    //defferedTexture
    CreateShaderResourceViews(device, commandList, m_defferedBuffer , GraphicsRootDeferred, DefferredCount, ResourceTexture2D);
    m_RootArguIdx.emplace_back(texCnt);
    texCnt += DefferredCount;

    D3D12_SHADER_RESOURCE_VIEW_DESC depthSrvDesc = {};
    depthSrvDesc.Format = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
    depthSrvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
    depthSrvDesc.Texture2D.MipLevels = 1;
    depthSrvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;

    //shadowTexture
    CreateShaderResourceViews(device, commandList, m_shadowBuffer, GraphicsRootShadow, ShadowCount, ResourceTexture2D, &depthSrvDesc);
    m_RootArguIdx.emplace_back(texCnt);
    texCnt += ShadowCount;

    CreateShaderResourceViews(device, commandList, m_CasCadeBuffer, GraphicsRootCasCade, NUM_CASCADES, ResourceTexture2D);
    m_RootArguIdx.emplace_back(texCnt);
    texCnt += NUM_CASCADES;

    //grass Texture
    ID3D12Resource** gntextures = new ID3D12Resource * [SceneTextureNum];
    ID3D12Resource** gntexturesUploadBuffer = new ID3D12Resource * [SceneTextureNum];
    for (int i = 0; i < SceneTextureNum; ++i) {
        wstring fileName = L"Assets/Image/scene" + to_wstring(i) + L".dds";
        wchar_t* tmpwc = const_cast<wchar_t*>(fileName.c_str());
        gntextures[i] = ::CreateTextureResourceFromFile(device, commandList, tmpwc, &gntexturesUploadBuffer[i], D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
    }
    CreateShaderResourceViews(device, commandList, gntextures, GraphicsRootSceneTexture, SceneTextureNum, ResourceTexture2D);
    Texture* tmptex1 = new Texture(gntextures, gntexturesUploadBuffer, SceneTextureNum);
    m_textures.emplace_back(tmptex1);
    m_RootArguIdx.emplace_back(texCnt);
    texCnt += SceneTextureNum;

    //particleTexture
    ID3D12Resource** pntextures = new ID3D12Resource*[ParticleTextureCount];
    ID3D12Resource** pntexturesUploadBuffer = new ID3D12Resource*[ParticleTextureCount];
    for (int i = 0; i < ParticleTextureCount; ++i) {
        wstring fileName = L"Assets/Image/Particle/Particle" + to_wstring(i) + L".dds";
        wchar_t* tmpwc = const_cast<wchar_t*>(fileName.c_str());
        pntextures[i] = ::CreateTextureResourceFromFile(device, commandList, tmpwc, &pntexturesUploadBuffer[i], D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
    }
    CreateShaderResourceViews(device, commandList, pntextures, GraphicsRootParticleTextures, ParticleTextureCount, ResourceTexture2D);
    Texture* tmptex2 = new Texture(pntextures, pntexturesUploadBuffer, ParticleTextureCount);
    m_textures.emplace_back(tmptex2);
    m_RootArguIdx.emplace_back(texCnt);
    texCnt += ParticleTextureCount;

    //skyBoxTexture
    ID3D12Resource** skytextures = new ID3D12Resource * [SkyTextureCount];
    ID3D12Resource** skytexturesUploadBuffer = new ID3D12Resource * [SkyTextureCount];
    for (int i = 0; i < SkyTextureCount; ++i) {
        wstring fileName = L"Assets/Image/skyBox" + to_wstring(i) + L".dds";
        wchar_t* tmpwc = const_cast<wchar_t*>(fileName.c_str());
        skytextures[i] = ::CreateTextureResourceFromFile(device, commandList, tmpwc, &skytexturesUploadBuffer[i], D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
    }
    CreateShaderResourceViews(device, commandList, skytextures, GraphicsSkyMapTexture, SkyTextureCount, ResourceTextureCube);
    Texture* tmptex3 = new Texture(skytextures, skytexturesUploadBuffer, SkyTextureCount);
    m_textures.emplace_back(tmptex3);
    m_RootArguIdx.emplace_back(texCnt);
    texCnt += SkyTextureCount;

    //depthtexture
    CreateShaderResourceViews(device, commandList, m_depthBuffer, GraphicsDepthTexture, DepthTexture, ResourceTexture2D, &depthSrvDesc);
    m_RootArguIdx.emplace_back(texCnt);
    texCnt += DepthTexture;

    //posteffectTexture
    CreateShaderResourceViews(device, commandList, m_posteffectBuffer, GraphicsPostEffectTexture, postRenderCount, ResourceTexture2D);
    m_RootArguIdx.emplace_back(texCnt);
    texCnt += postRenderCount;
}

void Scene::LoadModelResource(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
    for (int i = 0; i < MODELCOUNT; ++i) {
        string s = "Assets/Model/model" + to_string(i) + ".bin";
        char* tmpc = (char*)s.c_str();
        m_Modelvec.emplace_back(HierarchyObj::LoadGeometryAndAnimationFromFile(device, commandList, tmpc, "ALL"));
        m_Modelvec[i]->modelRootObject->GetHierarchyMaterial(m_materialReflection, m_materialNum);
        m_materialNum++;
    }
    BuildMaterialAndLight();
    InitCbInform(device, commandList);
}

void Scene::LoadObjectResource(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
    for (int i = 0; i < OBJECTMODELCOUNT; ++i) {
        string s = "Assets/Object/Object" + to_string(i) + ".bin";
        char* tmpc = (char*)s.c_str();
        m_Objects.emplace_back(HierarchyObj::LoadGeometryFromFile(device, commandList, tmpc, "ALL"));
        m_Objects[i]->GetHierarchyMaterial(m_materialReflection, m_materialNum);
        m_materialNum++;
    }

}

void Scene::CreateCbvSrvUavDescriptorHeap(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, int nConstantBufferViews, int nShaderResourceViews, int nUnorderedAccessViews)
{
    D3D12_DESCRIPTOR_HEAP_DESC d3dDescriptorHeapDesc;
    d3dDescriptorHeapDesc.NumDescriptors = nConstantBufferViews + nShaderResourceViews + nUnorderedAccessViews; //CBVs + SRVs 
    d3dDescriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
    d3dDescriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
    d3dDescriptorHeapDesc.NodeMask = 0;
    device->CreateDescriptorHeap(&d3dDescriptorHeapDesc, IID_PPV_ARGS(&m_DescriptorHeap));

    cbvCPUDescNextHandle = cbvCPUDescStartHandle = m_DescriptorHeap.Get()->GetCPUDescriptorHandleForHeapStart();
    cbvGPUDescNextHandle = cbvGPUDescStartHandle = m_DescriptorHeap.Get()->GetGPUDescriptorHandleForHeapStart();
    srvCPUDescNextHandle.ptr = srvCPUDescStartHandle.ptr = cbvCPUDescStartHandle.ptr + (m_gCbvSrvDescIncrementSize * nConstantBufferViews);
    srvGPUDescNextHandle.ptr = srvGPUDescStartHandle.ptr = cbvGPUDescStartHandle.ptr + (m_gCbvSrvDescIncrementSize * nConstantBufferViews);
    uavCPUDescNextHandle.ptr = uavCPUDescStartHandle.ptr = srvCPUDescStartHandle.ptr + (m_gCbvSrvDescIncrementSize * nShaderResourceViews);
    uavGPUDescNextHandle.ptr = uavGPUDescStartHandle.ptr = srvGPUDescStartHandle.ptr + (m_gCbvSrvDescIncrementSize * nShaderResourceViews);
}

void Scene::BuildTextures(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList)
{
    int terrainTexture = 30;
    int objectTexture = 10;
    int textureNum = terrainTexture + (objectTexture * 10);
    CreateCbvSrvUavDescriptorHeap(device.Get(), commandList.Get(), 0, textureNum, 0);
    LoadandCreateTextureResource(device.Get(), commandList.Get());
}

int Scene::CreateShaderResourceViews(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12Resource** textures, UINT rootParaStartIdx, int textureNum, UINT textureType , D3D12_SHADER_RESOURCE_VIEW_DESC* desc)
{
    int startIdx = rootArgumentCurrentIdx;

    for (int i = 0; i < textureNum; i++)
    {
        ID3D12Resource* pShaderResource = textures[i];
        D3D12_RESOURCE_DESC	d3dResourceDesc = pShaderResource->GetDesc();
        D3D12_SHADER_RESOURCE_VIEW_DESC d3dShaderResourceViewDesc;
        if (desc)
            d3dShaderResourceViewDesc = *desc;
        else
            d3dShaderResourceViewDesc = GetShaderResourceViewDesc(d3dResourceDesc, textureType);

        device->CreateShaderResourceView(pShaderResource, &d3dShaderResourceViewDesc, srvCPUDescNextHandle);
        SRVROOTARGUMENTINFO temp(rootParaStartIdx, srvGPUDescNextHandle);
        m_rootArgumentInfos.emplace_back(temp);
        rootArgumentCurrentIdx++;
        srvCPUDescNextHandle.ptr += m_gCbvSrvDescIncrementSize;
        srvGPUDescNextHandle.ptr += m_gCbvSrvDescIncrementSize;

    }

    return startIdx;
}

void Scene::InitCbInform(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
    UINT cbMaterialBytes = ((sizeof(MATERIALS) + 255) & ~255);
    m_cbMaterials = ::CreateBufferResource(device, commandList, m_materials, cbMaterialBytes, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &m_materialUploadBuffer);//struct 재질 정보를 위한 constant버퍼를 만듬

    UINT cbElementBytes = ((sizeof(LIGHTS) + 255) & ~255);
	m_cbLights = ::CreateBufferResource(device, commandList, NULL, cbElementBytes, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
	m_cbLights->Map(0, NULL, (void**)& m_cbMappedLights);
}

void Scene::CreateComputeRootSignature(ComPtr<ID3D12Device>& device)
{
    D3D12_ROOT_PARAMETER rootParameters[ComputeRootParametersNum];
    rootParameters[ComputeRootTimeInfo] = CreateRoot32Constants(D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS, 2, 0, 0, D3D12_SHADER_VISIBILITY_ALL); //b0
    rootParameters[ComputeRootBoneOffset] = CreateRootDesCriptor(D3D12_ROOT_PARAMETER_TYPE_CBV, 1, 0, D3D12_SHADER_VISIBILITY_ALL); //b1
    rootParameters[ComputeRootBoneTransform] = CreateRootDesCriptor(D3D12_ROOT_PARAMETER_TYPE_SRV, 0, 0, D3D12_SHADER_VISIBILITY_ALL); //t0
    rootParameters[ComputeRootSkinnedPosition] = CreateRootDesCriptor(D3D12_ROOT_PARAMETER_TYPE_SRV, 1, 0, D3D12_SHADER_VISIBILITY_ALL); //t1
    rootParameters[ComputeRootSkinnedNormal] = CreateRootDesCriptor(D3D12_ROOT_PARAMETER_TYPE_SRV, 2, 0, D3D12_SHADER_VISIBILITY_ALL); //t2
    rootParameters[ComputeRootSkinnedTangent] = CreateRootDesCriptor(D3D12_ROOT_PARAMETER_TYPE_SRV, 3, 0, D3D12_SHADER_VISIBILITY_ALL); //t3
    rootParameters[ComputeRootSkinnedBiTangent] = CreateRootDesCriptor(D3D12_ROOT_PARAMETER_TYPE_SRV, 4, 0, D3D12_SHADER_VISIBILITY_ALL); //t4
    rootParameters[ComputeRootSkinnedBoneIndex] = CreateRootDesCriptor(D3D12_ROOT_PARAMETER_TYPE_SRV, 5, 0, D3D12_SHADER_VISIBILITY_ALL); //t5
    rootParameters[ComputeRootSkinnedBoneWeight] = CreateRootDesCriptor(D3D12_ROOT_PARAMETER_TYPE_SRV, 6, 0, D3D12_SHADER_VISIBILITY_ALL); //t6
    rootParameters[ComputeRootSkinnedPositionWorld] = CreateRootDesCriptor(D3D12_ROOT_PARAMETER_TYPE_UAV, 0, 0, D3D12_SHADER_VISIBILITY_ALL); //u0
    rootParameters[ComputeRootSkinnedNormalWorld] = CreateRootDesCriptor(D3D12_ROOT_PARAMETER_TYPE_UAV, 1, 0, D3D12_SHADER_VISIBILITY_ALL); //u1
    rootParameters[ComputeRootSkinnedTangentWorld] = CreateRootDesCriptor(D3D12_ROOT_PARAMETER_TYPE_UAV, 2, 0, D3D12_SHADER_VISIBILITY_ALL); //u2
    rootParameters[ComputeRootSkinnedBiTangentWorld] = CreateRootDesCriptor(D3D12_ROOT_PARAMETER_TYPE_UAV, 3, 0, D3D12_SHADER_VISIBILITY_ALL); //u3
    rootParameters[ComputeRootSkinnedVerticesNum] = CreateRoot32Constants(D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS, 1, 2, 0, D3D12_SHADER_VISIBILITY_ALL); //b2
    rootParameters[ComputeRootParticleType] = CreateRoot32Constants(D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS, 2, 3, 0, D3D12_SHADER_VISIBILITY_ALL);
    //b3
    rootParameters[ComputeRootParticleInfoSRV] = CreateRootDesCriptor(D3D12_ROOT_PARAMETER_TYPE_SRV, 7, 0, D3D12_SHADER_VISIBILITY_ALL); //t7
    rootParameters[ComputeRootParticleCopySRV] = CreateRootDesCriptor(D3D12_ROOT_PARAMETER_TYPE_SRV, 8, 0, D3D12_SHADER_VISIBILITY_ALL); //t8
    rootParameters[ComputeRootParticleInfoUAV] = CreateRootDesCriptor(D3D12_ROOT_PARAMETER_TYPE_UAV, 4, 0, D3D12_SHADER_VISIBILITY_ALL); //u4
    rootParameters[ComputeRootParticleCameraPos] = CreateRoot32Constants(D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS, 3, 4, 0, D3D12_SHADER_VISIBILITY_ALL);

    D3D12_ROOT_SIGNATURE_DESC rootSignatureDesc;
    ::ZeroMemory(&rootSignatureDesc, sizeof(D3D12_ROOT_SIGNATURE_DESC));
    rootSignatureDesc.NumParameters = _countof(rootParameters);
    rootSignatureDesc.pParameters = rootParameters;
    rootSignatureDesc.NumStaticSamplers = 0;
    rootSignatureDesc.pStaticSamplers = nullptr;
    rootSignatureDesc.Flags = D3D12_ROOT_SIGNATURE_FLAG_NONE;

    ID3DBlob* signatureBlob = NULL;
    ID3DBlob* errorBlob = NULL;
    HRESULT hResult = D3D12SerializeRootSignature(&rootSignatureDesc, D3D_ROOT_SIGNATURE_VERSION_1, &signatureBlob, &errorBlob);
    ThrowIfFailed(device->CreateRootSignature(0, signatureBlob->GetBufferPointer(), signatureBlob->GetBufferSize(), IID_PPV_ARGS(&m_ComputeRootsignature)));
    if (signatureBlob) signatureBlob->Release();
    if (errorBlob) errorBlob->Release();
}

void Scene::CreateGrapicsRootSignature(ComPtr<ID3D12Device>& device)
{
    {
        vector<D3D12_DESCRIPTOR_RANGE> descriptorRanges;
        descriptorRanges.emplace_back(CreateDesCriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, TerrainDetailTextureNum, 0, 0, D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND)); //t0 ~ t2
        for (int i = 0; i < 6; ++i) // diffuse , normal , specualr , metaillic , billboard , particle
            descriptorRanges.emplace_back(CreateDesCriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, MaterialTextureNum, 7 + (10 * i), 0, D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND)); //t7 ~ t16 , t17 ~ t26  , t27 ~ t36 , t37 ~ t46 , t47 ~ t56 , t57 ~ t66
        descriptorRanges.emplace_back(CreateDesCriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, TerrainNormalTextureNum, 67, 0, D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND)); //t67 ~ t69
        descriptorRanges.emplace_back(CreateDesCriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, DefferredCount, 70, 0, D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND)); //t70 ~ 73
        descriptorRanges.emplace_back(CreateDesCriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, ShadowCount, 74, 0, D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND)); //t74
        descriptorRanges.emplace_back(CreateDesCriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, NUM_CASCADES, 75, 0, D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND)); //t75 ~ 77
        descriptorRanges.emplace_back(CreateDesCriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, SceneTextureCount, 78, 0, D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND)); //t78~ 87
        descriptorRanges.emplace_back(CreateDesCriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, SkyTextureCount, 89, 0, D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND));//t89
        descriptorRanges.emplace_back(CreateDesCriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, DepthTexture, 90, 0, D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND));//t90
        descriptorRanges.emplace_back(CreateDesCriptorRange(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, PostEffectTexture, 91, 0, D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND));//t91 ~ 90

        int tablecnt = 0;
        int buffcnt = 0;
        CD3DX12_ROOT_SIGNATURE_DESC rootSignatureDesc;
        rootSignatureDesc.Init(0, nullptr, 0, nullptr, D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT);
        D3D12_ROOT_PARAMETER rootParameters[GraphicsRootParametersNum];
        rootParameters[GraphicsRootCamera] = CreateRootDesCriptor(D3D12_ROOT_PARAMETER_TYPE_CBV, buffcnt++, 0, D3D12_SHADER_VISIBILITY_ALL); //b0
        rootParameters[GraphicsRootNoInstancingObj] = CreateRootDesCriptor(D3D12_ROOT_PARAMETER_TYPE_CBV, buffcnt++, 0, D3D12_SHADER_VISIBILITY_ALL);//b1
        rootParameters[GraphicsRootTerrainDetail] = CreateRootTable(D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE, 1, &descriptorRanges[tablecnt++], D3D12_SHADER_VISIBILITY_PIXEL); // t0 ~ t2 
        rootParameters[GraphicsRootSkinnedPositionWorld] = CreateRootDesCriptor(D3D12_ROOT_PARAMETER_TYPE_SRV, 3, 0, D3D12_SHADER_VISIBILITY_ALL); //t3
        rootParameters[GraphicsRootSkinnedNormalWorld] = CreateRootDesCriptor(D3D12_ROOT_PARAMETER_TYPE_SRV, 4, 0, D3D12_SHADER_VISIBILITY_ALL); //t4
        rootParameters[GraphicsRootSkinnedTangentWorld] = CreateRootDesCriptor(D3D12_ROOT_PARAMETER_TYPE_SRV, 5, 0, D3D12_SHADER_VISIBILITY_ALL); //t5
        rootParameters[GraphicsRootSkinnedBiTangentWorld] = CreateRootDesCriptor(D3D12_ROOT_PARAMETER_TYPE_SRV, 6, 0, D3D12_SHADER_VISIBILITY_ALL); //t6
        rootParameters[GraphicsRootSkinnedVerticesNum] = CreateRoot32Constants(D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS, 1, buffcnt++, 0, D3D12_SHADER_VISIBILITY_ALL); //b2
        rootParameters[GraphicsRootMaterialInfo] = CreateRoot32Constants(D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS, 3, buffcnt++, 0, D3D12_SHADER_VISIBILITY_ALL); //b3
        for (int i = 0; i < 6; ++i)
            rootParameters[GraphicsRootDiffuseTextures + i] = CreateRootTable(D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE, 1, &descriptorRanges[tablecnt++], D3D12_SHADER_VISIBILITY_PIXEL); //t7 ~ t16 , t17 ~ t26  , t27 ~ t36 , t37 ~ t46 , t47 ~ t56 , t57 ~ t66
        rootParameters[GraphicsRootMaterials] = CreateRootDesCriptor(D3D12_ROOT_PARAMETER_TYPE_CBV, buffcnt++, 0, D3D12_SHADER_VISIBILITY_ALL);//b4
        rootParameters[GraphicsRootLights] = CreateRootDesCriptor(D3D12_ROOT_PARAMETER_TYPE_CBV, buffcnt++, 0, D3D12_SHADER_VISIBILITY_ALL);//b5
        rootParameters[GraphicsRootTerrainNormal] = CreateRootTable(D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE, 1, &descriptorRanges[tablecnt++], D3D12_SHADER_VISIBILITY_PIXEL); // 
        rootParameters[GraphicsRootDeferred] = CreateRootTable(D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE, 1, &descriptorRanges[tablecnt++], D3D12_SHADER_VISIBILITY_PIXEL); 
        rootParameters[GraphicsRootShadow] = CreateRootTable(D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE, 1, &descriptorRanges[tablecnt++], D3D12_SHADER_VISIBILITY_PIXEL);
        rootParameters[GraphicsRootLightCamera] = CreateRootDesCriptor(D3D12_ROOT_PARAMETER_TYPE_CBV, buffcnt++, 0, D3D12_SHADER_VISIBILITY_ALL); //b6
        rootParameters[GraphicsRootCasCade] = CreateRootTable(D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE, 1, &descriptorRanges[tablecnt++], D3D12_SHADER_VISIBILITY_PIXEL);
        rootParameters[GraphicsRootCameraNumber] = CreateRoot32Constants(D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS, 1, buffcnt++, 0, D3D12_SHADER_VISIBILITY_ALL); //b7
        rootParameters[GraphicsRootSceneTexture] = CreateRootTable(D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE, 1, &descriptorRanges[tablecnt++], D3D12_SHADER_VISIBILITY_PIXEL);
        rootParameters[GraphicsRootTimer] = CreateRoot32Constants(D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS, 2, buffcnt++, 0, D3D12_SHADER_VISIBILITY_ALL); //b8
        rootParameters[GraphicsParticleData] = CreateRootDesCriptor(D3D12_ROOT_PARAMETER_TYPE_SRV, 88, 0, D3D12_SHADER_VISIBILITY_ALL); //t88
        rootParameters[GraphicsSkyMapTexture] = CreateRootTable(D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE, 1, &descriptorRanges[tablecnt++], D3D12_SHADER_VISIBILITY_PIXEL); //t89
        rootParameters[GraphicsDepthTexture] = CreateRootTable(D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE, 1, &descriptorRanges[tablecnt++], D3D12_SHADER_VISIBILITY_PIXEL); //t90
        rootParameters[GraphicsSunPosData] = CreateRoot32Constants(D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS, 4, buffcnt++, 0, D3D12_SHADER_VISIBILITY_PIXEL);//b9
        rootParameters[GraphicsPostEffectTexture] = CreateRootTable(D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE, 1, &descriptorRanges[tablecnt++], D3D12_SHADER_VISIBILITY_PIXEL); //t91

        {
            //sampler
            D3D12_STATIC_SAMPLER_DESC samplerDeses[4];
            samplerDeses[0] = CraeteSamplerDesc(D3D12_FILTER_ANISOTROPIC, D3D12_TEXTURE_ADDRESS_MODE_WRAP, D3D12_TEXTURE_ADDRESS_MODE_WRAP, D3D12_TEXTURE_ADDRESS_MODE_WRAP, 0, 1, D3D12_COMPARISON_FUNC_GREATER_EQUAL, D3D12_STATIC_BORDER_COLOR_TRANSPARENT_BLACK, 0, D3D12_FLOAT32_MAX, 0, 0, D3D12_SHADER_VISIBILITY_PIXEL);
            samplerDeses[1] = CraeteSamplerDesc(D3D12_FILTER_MIN_MAG_MIP_LINEAR, D3D12_TEXTURE_ADDRESS_MODE_WRAP, D3D12_TEXTURE_ADDRESS_MODE_WRAP, D3D12_TEXTURE_ADDRESS_MODE_WRAP, 0, 1, D3D12_COMPARISON_FUNC_ALWAYS, D3D12_STATIC_BORDER_COLOR_TRANSPARENT_BLACK, 0, D3D12_FLOAT32_MAX, 1, 0, D3D12_SHADER_VISIBILITY_PIXEL);
            samplerDeses[2]= CraeteSamplerDesc(D3D12_FILTER_COMPARISON_MIN_MAG_LINEAR_MIP_POINT, D3D12_TEXTURE_ADDRESS_MODE_CLAMP, D3D12_TEXTURE_ADDRESS_MODE_CLAMP, D3D12_TEXTURE_ADDRESS_MODE_CLAMP, 0, 1, D3D12_COMPARISON_FUNC_GREATER_EQUAL, D3D12_STATIC_BORDER_COLOR_TRANSPARENT_BLACK, 0, D3D12_FLOAT32_MAX, 2, 0, D3D12_SHADER_VISIBILITY_PIXEL);
            samplerDeses[3] = CraeteSamplerDesc(D3D12_FILTER_MIN_MAG_MIP_LINEAR, D3D12_TEXTURE_ADDRESS_MODE_CLAMP, D3D12_TEXTURE_ADDRESS_MODE_CLAMP, D3D12_TEXTURE_ADDRESS_MODE_CLAMP, 0, 1, D3D12_COMPARISON_FUNC_ALWAYS, D3D12_STATIC_BORDER_COLOR_TRANSPARENT_BLACK, 0, D3D12_FLOAT32_MAX, 3, 0, D3D12_SHADER_VISIBILITY_PIXEL);

            rootSignatureDesc.NumParameters = _countof(rootParameters);
            rootSignatureDesc.pParameters = rootParameters;
            rootSignatureDesc.NumStaticSamplers = _countof(samplerDeses);
            rootSignatureDesc.pStaticSamplers = samplerDeses;

            ComPtr<ID3DBlob> signature;
            ComPtr<ID3DBlob> error;
            ThrowIfFailed(D3D12SerializeRootSignature(&rootSignatureDesc, D3D_ROOT_SIGNATURE_VERSION_1, &signature, &error));
            ThrowIfFailed(device->CreateRootSignature(0, signature->GetBufferPointer(), signature->GetBufferSize(), IID_PPV_ARGS(&m_rootSignature)));
        }
    }
}

void Scene::BuildMaterialAndLight()
{
    m_materials = new MATERIALS;
    ::ZeroMemory(m_materials, sizeof(MATERIALS));
    m_materials->reflections[0] = { XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(0.2f, 0.2f, 0.2f, 0.3f), XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f) };
    m_materials->reflections[1] = { XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f), XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f), XMFLOAT4(0.2f, 0.2f, 0.2f, 0.3f), XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f) };
    for (int i = 0; i < m_materialReflection.size(); ++i)
    {
        m_materials->reflections[BASIC_REFLECTION + i] = m_materialReflection[i];
    }
    ::ZeroMemory(&m_lights, sizeof(LIGHTS));
    m_lights.lights[curLightidx].enable = true;
    m_lights.lights[curLightidx].type = DIRECTIONAL_LIGHT;
    m_lights.lights[curLightidx].ambient = XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f);
    m_lights.lights[curLightidx].diffuse = XMFLOAT4(0.3f, 0.3f, 0.3f, 1.0f);
    m_lights.lights[curLightidx].specular = XMFLOAT4(0.1f, 0.1f, 0.1f, 0.1f);
    m_lights.lights[curLightidx].direction = XMFLOAT3(1.0f, -1.0f, 0.0f);
    m_lights.lights[curLightidx].position = XMFLOAT3(0.0f, 0.0f, 0.0f);
    m_lights.globalAmbient = XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f);
    m_rtDir = m_lights.lights[curLightidx].direction;
    curLightidx++;

    m_lights.lights[curLightidx].enable = false;
    m_lights.lights[curLightidx].type = POINT_LIGHT;
    m_lights.lights[curLightidx].range = 50.0f;
    m_lights.lights[curLightidx].ambient = XMFLOAT4(0.1f, 0.1f, 0.0f, 1.0f);
    m_lights.lights[curLightidx].diffuse = XMFLOAT4(0.1f, 0.1f, 0.0f, 1.0f);
    m_lights.lights[curLightidx].specular = XMFLOAT4(0.5f, 0.5f, 0.0f, 0.0f);
    m_lights.lights[curLightidx].position = XMFLOAT3(130.0f, 300.0f, 30.0f);
    m_lights.lights[curLightidx].direction = XMFLOAT3(0.0f, 0.0f, 0.0f);
    m_lights.lights[curLightidx].attenuation = XMFLOAT3(1.0f, 0.001f, 0.01f);
    curLightidx++;

    m_lights.lights[curLightidx].enable = true;
    m_lights.lights[curLightidx].type = SPOT_LIGHT;
    m_lights.lights[curLightidx].range = 10.0f;
    m_lights.lights[curLightidx].ambient = XMFLOAT4(0.1f, 0.1f, 0.1f, 1.0f);
    m_lights.lights[curLightidx].diffuse = XMFLOAT4(0.1f, 0.1f, 0.1f, 1.0f);
    m_lights.lights[curLightidx].specular = XMFLOAT4(0.2f, 0.2f, 0.2f, 0.0f);
    m_lights.lights[curLightidx].position = XMFLOAT3(-50.0f, 20.0f, -5.0f);
    m_lights.lights[curLightidx].direction = XMFLOAT3(0.0f, 0.0f, 1.0f);
    m_lights.lights[curLightidx].attenuation = XMFLOAT3(1.0f, 0.01f, 0.0001f);
    m_lights.lights[curLightidx].falloff = 2.0f;
    m_lights.lights[curLightidx].phi = (float)cos(XMConvertToRadians(40.0f));
    m_lights.lights[curLightidx].theta = (float)cos(XMConvertToRadians(20.0f));
    curLightidx++;
}

void Scene::BuildCubeObjects(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList)
{
    vector<GameObject*> cubevec;
    int CubeNum = 1;
    float x = 0.0f;

    //GameObject -> mesh 
    //GameObject -> material -> shader -> setShader 
    //mesh
    DiffuseCubeMesh* cubeMesh = new DiffuseCubeMesh(device.Get(), commandList.Get(), 5.0f, 5.0f, 5.0f);
    CubeObj* dummy = new CubeObj;
    dummy->SetMesh(cubeMesh);
    dummy->SetConstantNumber(E_CUBECONSTANTNUMBER);

    //Shader
    Shader* shader = new Shader;
    shader->CreatebPipeLineState(device, m_rootSignature);
    //material
    Material* material = new Material();
    material->SetShader(shader);
    dummy->InsertMaterial(material);

    //set Dummy
    cubevec.emplace_back(dummy);

    for (int i = 0; i < CubeNum; ++i) {
        CubeObj* cube = new CubeObj;
        cube->SetTransFormPos(x + i * 10.0f, 0.0f, 0.0f);
        cubevec.emplace_back(cube);
    }
    m_GameObjects.emplace_back(cubevec);

    InitCbInform(device, commandList, m_GameObjects.size() - 1);
}

void Scene::BuildInstancingSkinnedObjects(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList, int& idx)
{
        //Create Skinning intancing object
        vector<GameObject*> tmpObj;
        int objectNum = 1;
        AnimationObj* dummy = new AnimationObj(device.Get(), commandList.Get(), m_Modelvec[RIFLEMODEL + idx], 1, PlayerJobRifle + idx);

        MODELINFO tmpModelInfo;
        tmpModelInfo.framseNum = m_Modelvec[RIFLEMODEL + idx]->modelRootObject->GetFrameNum();
        tmpModelInfo.skinnedMeshNum = m_Modelvec[RIFLEMODEL + idx]->skinnedMeshNum;
        tmpModelInfo.standardMeshNum = m_Modelvec[RIFLEMODEL + idx]->standardMeshNum;
        tmpModelInfo.skinnedMeshes = m_Modelvec[RIFLEMODEL + idx]->skinnedMeshes;
        //shader
        SkinnedInstancingShader* tshader = new SkinnedInstancingShader();
        tshader->SetModelInfo(tmpModelInfo);
        tshader->CreatebPipeLineState(device, m_rootSignature, m_ComputeRootsignature);
        tshader->CreateShaderVariables(device.Get(), commandList.Get());

        //material
        Material* material = new Material;
        dummy->InsertMaterial(material);
        dummy->GetMaterial()[0]->SetShader(tshader);

        dummy->SetTransFormPos(0.0f, 20.0f, 0.0f);
        //set Texture; -> gameObject 
        dummy->SetInstancingTexture(this, device.Get(), commandList.Get(), m_Modelvec[RIFLEMODEL + idx]); //load and create shaderResourceView 
        //save texture idx and to set 

        //animation
        dummy->skinnedAnimationController->SetTrackAnimationSet(0, 0);
        dummy->skinnedAnimationController->SetBeforeTransform(0);
        dummy->skinnedAnimationController->SetTrackSpeed(0, 1);
        dummy->skinnedAnimationController->SetTrackPosition(0.0f, 0.0f);
        tmpObj.emplace_back(dummy);

        if (idx == SWORDMODEL) {
            default_random_engine dre;
            uniform_real_distribution<float> fuid(-256.0f, 256.0f);


            for (int i = 0; i < SwordCharacterNum; ++i) {
                AnimationObj* obj = new AnimationObj(device.Get(), commandList.Get(), m_Modelvec[RIFLEMODEL + idx], 1, PlayerJobRifle + idx);
                obj->skinnedAnimationController->SetTrackAnimationSet(0, (SwordCharacterIdle + i) % (SwordCharacterDeath + 1), (SwordCharacterIdle + i) % (SwordCharacterDeath + 1));
                obj->skinnedAnimationController->SetBeforeTransform(0);
                obj->skinnedAnimationController->SetTrackSpeed(0, 1);
                obj->skinnedAnimationController->SetTrackPosition(0.0f, 0.0f);
                float sx = float(fuid(dre));
                float sz = float(fuid(dre));

                obj->SetTransFormPos(sx, 0.0f, sz);
                obj->Rotate(0.0f, i * 0.0f, 0.0f);
                XMFLOAT3 tlook = obj->GetTransFormLook();
                tmpObj.emplace_back(obj);
            }
        }
        m_GameObjects.emplace_back((tmpObj));
}

void Scene::BuildInstancingObjects(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList, int& idx)
{

    m_lights.lights[curLightidx].enable = true;
    m_lights.lights[curLightidx].type = POINT_LIGHT;
    m_lights.lights[curLightidx].range = 20.0f;
    m_lights.lights[curLightidx].ambient = XMFLOAT4(0.1f, 0.1f, 0.0f, 1.0f);
    m_lights.lights[curLightidx].diffuse = XMFLOAT4(0.1f, 0.1f, 0.0f, 1.0f);
    m_lights.lights[curLightidx].specular = XMFLOAT4(0.5f, 0.5f, 0.0f, 0.0f);
    m_lights.lights[curLightidx].position = XMFLOAT3(130.0f, 300.0f, 30.0f);
    m_lights.lights[curLightidx].direction = XMFLOAT3(0.0f, 0.0f, 0.0f);
    m_lights.lights[curLightidx].attenuation = XMFLOAT3(1.0f, 0.001f, 0.0001f);
    curLightidx++;


    for (int j = 0; j < OBJECTMODELCOUNT; ++j) {
        vector<GameObject*> tmpObj;
        int objectNum = 1;
        HierarchyObj* dummy = new HierarchyObj();
        dummy->SetChild(m_Objects[REDCARMODEL + j]);

        //shader
        InstancingShader* ishader = new InstancingShader();
        MODELINFO mdifo;
        mdifo.framseNum = m_Objects[REDCARMODEL + j]->GetFrameNum() + 1;
        ishader->SetModelInfo(mdifo);
        ishader->CreatebPipeLineState(device, m_rootSignature, m_ComputeRootsignature);
        ishader->CreateShaderVariables(device.Get(), commandList.Get());

        //material
        Material* material = new Material;
        dummy->InsertMaterial(material);
        dummy->GetMaterial()[0]->SetShader(ishader);
        dummy->SetInstancingTexture(device.Get(), commandList.Get(), this);
        if (REDCARMODEL + j == REDCARMODEL) {
            dummy->SetObjNum(3);
        }
        else if (REDCARMODEL + j == LAMPMODEL) {
            dummy->SetObjNum(5);
        }

        tmpObj.emplace_back(dummy);
        for (int i = 0; i < dummy->GetObjNum(); ++i)
        {
            HierarchyObj* gameobject = new HierarchyObj();
            gameobject->SetChild(m_Objects[REDCARMODEL + j]);
            if (REDCARMODEL + j == REDCARMODEL) {
                gameobject->SetTransFormPos(10.0f + i * 10.0f, 0.0f, 105.0f);
            }
            else if (REDCARMODEL + j == LAMPMODEL) {
                m_lights.lights[curLightidx] = m_lights.lights[curLightidx - 1];
                gameobject->SetTransFormPos(-140.0f + (i % 10) * 40.0f, 0.0f, 20.0f + ((i / 10) * 40.0f));
                gameobject->SetLightIdx(curLightidx);
                curLightidx++;
            }

            gameobject->CreateAABBBox(gameobject->GetTransForm());
            BoundingBox& aabbBox = gameobject->GetBounding();
            QuadTree* node = InsertQuadObject(aabbBox);
            if (node == NULL) {

            }
            else {
                node->gVec.emplace_back(gameobject);
            }
            tmpObj.emplace_back(gameobject);
        }
        m_GameObjects.emplace_back(tmpObj);
    }
}

void Scene::BuildEffectObjects(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList)
{
    vector<GameObject*> tmpObj;
    GameObject* dummy = new GameObject;
    Material* material = new Material;

    WeaponTrailerShader* wShader = new WeaponTrailerShader;
    wShader->CreateRenderPipeLineState(device , m_rootSignature);
    wShader->CreateShaderVariables(device.Get(), commandList.Get());
    material->SetShader(wShader);
    dummy->InsertMaterial(material);
    tmpObj.emplace_back(dummy);
    m_GameObjects.emplace_back(tmpObj);
}

void Scene::BuildUIObjects(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList)
{
    vector<GameObject*> tmpObj;
    GameObject* uiobject = new GameObject;
    UIRectMesh* uimesh = new UIRectMesh(device.Get(), commandList.Get(), UITextureNum);

    //mesh
    uiobject->SetMesh(uimesh);
    
    //shader 
    UIShader* uiShader = new UIShader;
    uiShader->CreateRenderPipeLineState(device , m_rootSignature);

    //material
    Material* material = new Material;
    uiobject->InsertMaterial(material);
    uiobject->GetMaterial()[0]->SetShader(uiShader);

    tmpObj.emplace_back(uiobject);
    m_UIObjects.emplace_back(tmpObj);
}

void Scene::BuildLightObjects(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList) //deffered 계산 
{
    vector<GameObject*> tmpObj;
    GameObject* lightobject = new GameObject;
    UIRectMesh* lightmesh = new UIRectMesh(device.Get(), commandList.Get());

    //mesh
    lightobject->SetMesh(lightmesh);

    //shader 
    LightShader* lightShader = new LightShader;
    lightShader->CreateRenderPipeLineState(device, m_rootSignature);

    //material
    Material* material = new Material;
    lightobject->InsertMaterial(material);
    lightobject->GetMaterial()[0]->SetShader(lightShader);

    tmpObj.emplace_back(lightobject);
    m_LightObjects.emplace_back(tmpObj);
}

void Scene::BuildRealGrassObjects(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList)
{
    vector<GameObject*> realgrassvec;
    int grassNum = 10;
    float grassSize = 2.0f;
    //mesh
    RealGrassMesh* realgrassMesh = new RealGrassMesh(device.Get(), commandList.Get());
    GameObject* dummy = new GameObject;
    dummy->SetMesh(realgrassMesh);
    RSShader* rsShader = new RSShader;
    rsShader->CreateShaderVariables(device.Get(), commandList.Get());
    rsShader->CreatebPipeLineState(device, m_rootSignature);
    
    Material* material = new Material();
    material->SetShader(rsShader);
    dummy->InsertMaterial(material);
    //set dummy 
    realgrassvec.emplace_back(dummy);
    
    int hang = 10;
    uniform_real_distribution<float> uid(-256.0f,256.0f);
    uniform_real_distribution<float> gid(-4.0f, 4.0f);
    default_random_engine dre;
    uid(dre);

    for (int i = 0; i < grassNum; ++i) {
        GameObject* grassObj = new GameObject;
        //grassObj->SetTransFormPos(20.0f + ((grassSize + 2.0f) * i), 0.0f, -10.0f + (i % hang));
        float posX = uid(dre);
        float posZ = uid(dre);

        grassObj->SetGrassNumber(gid(dre));
        grassObj->SetTransFormPos(posX, 0.0f, posZ);
        grassObj->CreateAABBBox(grassObj->GetTransForm());

        BoundingBox& aabbBox = grassObj->GetBounding();
        QuadTree* node = InsertQuadObject(aabbBox);
        node->gVec.emplace_back(grassObj);

        realgrassvec.emplace_back(grassObj);    
    }

    vector<objManageMent>& objVec = rsShader->GetObjVec();
    objVec.resize(realgrassvec.size());
    m_GameObjects.emplace_back(realgrassvec);

}

void Scene::BuildParticleObjects(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList)
{
    vector<GameObject*> particleObjects;
    GameObject* dummy = new GameObject;
    ParticleShader* particleShader = new ParticleShader;
    particleShader->CreatebPipeLineState(device, m_rootSignature, m_ComputeRootsignature.Get());
    particleShader->BuildShaderParticle(device.Get(), commandList.Get());
    

    Material* material = new Material();
    material->SetShader(particleShader);
    dummy->InsertMaterial(material);
    particleObjects.emplace_back(dummy);
    m_GameObjects.emplace_back(particleObjects);
}

void Scene::BuildBillBoardObjects(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList)
{
    vector<GameObject*> billBoardObjects;
    SunMoonObj* dummy = new SunMoonObj;
    //mesh
    BillBoardMesh* billMesh0 = new BillBoardMesh(device.Get(), commandList.Get(), 1);
    BillBoardMesh* billMesh1 = new BillBoardMesh(device.Get(), commandList.Get(), 2);
    dummy->SetMesh(billMesh0);
    dummy->SetMesh(billMesh1);


    BillBoardShader* billShader = new BillBoardShader;
    billShader->CreatebPipeLineState(device, m_rootSignature);
    
    Material* material = new Material();
    material->SetShader(billShader);
    dummy->InsertMaterial(material);
    dummy->SetConstantNumber(E_BILLCONSTANTNUMBER);
    
    billBoardObjects.emplace_back(dummy);
    
    for (int i = 0; i < 2; ++i) { // 해 , 달 
        SunMoonObj* tmpObj = new SunMoonObj;
        if (i == 0) {
            //XMFLOAT3 sunPos = Vector3::ScalarProduct(Vector3::Normalize(m_rtDir), -SUNDISTANCE, false);
            //tmpObj->SetTransFormPos(sunPos.x, sunPos.y, sunPos.z);
        }


        if (i == 1) {
            //XMFLOAT3 moonPos = Vector3::ScalarProduct(Vector3::Normalize(m_rtDir), SUNDISTANCE, false);
            //tmpObj->SetTransFormPos(moonPos.x, moonPos.y, moonPos.z);
        }
        billBoardObjects.emplace_back(tmpObj);
    }
    m_GameObjects.emplace_back(billBoardObjects);
    InitCbInform(device, commandList, m_GameObjects.size() - 1);
}

void Scene::BuildSkyBoxObjects(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList)
{
    vector<GameObject*> SkyBoxObjects;
    GameObject* dummy = new GameObject;

    //mesh
    SkyBoxMesh* mesh = new SkyBoxMesh(device.Get(),commandList.Get(),10.0f,10.0f,10.0f);
    dummy->SetMesh(mesh);

    //shader
    SkyBoxShader* sShader = new SkyBoxShader;
    sShader->CreatebPipeLineState(device, m_rootSignature); 
    dummy->SetConstantNumber(E_SKYBOXCONSTANTNUMBER);
    //material
    Material* material = new Material();
    material->SetShader(sShader);
    dummy->InsertMaterial(material);
   
    GameObject* tmpObj = new GameObject;
    tmpObj->SetTransFormPos(0.0f, 0.0f, 0.0f);

    SkyBoxObjects.emplace_back(dummy);
    SkyBoxObjects.emplace_back(tmpObj);

    m_GameObjects.emplace_back(SkyBoxObjects);
    InitCbInform(device, commandList, m_GameObjects.size() - 1);
}

void Scene::BuildPostObjects(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList)
{
    vector<GameObject*> tmpObj;
    GameObject* posobject = new GameObject;
    UIRectMesh* postmesh = new UIRectMesh(device.Get(), commandList.Get());
    //mesh
    posobject->SetMesh(postmesh);

    //shader 
    PostShader* postShader = new PostShader;
    postShader->CreateRenderPipeLineState(device, m_rootSignature);

    //material
    Material* material = new Material;
    posobject->InsertMaterial(material);
    posobject->GetMaterial()[0]->SetShader(postShader);

    tmpObj.emplace_back(posobject);
    m_LightObjects.emplace_back(tmpObj);

}

void Scene::BuildAstarNode(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList)
{
    vector<GameObject*> nodeObjects;
    GameObject* dummy = new GameObject;

    //x z -256 ~ 256 // 16간격으로 32X32개의 노드 생성 -> 1024개의 노드 
    m_AstarNodes.resize(AstarLineNode + 1, vector<Node>(AstarLineNode + 1));

    float startx = -256.0f;
    float startz = -256.0f;
    TerrainMesh* tmesh = (TerrainMesh*)this->GetGameObjects()[E_TERRAINOBJ][0]->GetMesh(0);
    for (int i = 0; i < AstarLineNode + 1; ++i) {
        for (int j = 0; j < AstarLineNode + 1; ++j) {
            float height = tmesh->GetHeightByCorrectLerp(startx + AstarInterval * j, startz + AstarInterval * i , 256.0f , 256.0f);
            m_AstarNodes[i][j] = Node(startx + AstarInterval * j, height,startz + AstarInterval * i, (i * (AstarLineNode + 1)) + j);
        }
    }
    
    Shader* nodeshader = new Shader;
    nodeshader->CreateLinePipeLineState(device, m_rootSignature);
    NodeMesh* nodeMesh = new NodeMesh(device.Get(), commandList.Get(), m_AstarNodes, AstarInterval , tmesh);
    
    dummy->SetMesh(nodeMesh);
    dummy->SetConstantNumber(E_NODECONSTANTNUMBER);

    Material* material = new Material();
    material->SetShader(nodeshader);
    dummy->InsertMaterial(material);

    GameObject* tmpObj = new GameObject;
    tmpObj->SetTransFormPos(0.0f, 0.0f, 0.0f);
    nodeObjects.emplace_back(dummy);
    nodeObjects.emplace_back(tmpObj);

    m_GameObjects.emplace_back(nodeObjects);
    InitCbInform(device, commandList, m_GameObjects.size() - 1);
}

void Scene::BuildTerrainObjcet(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList)
{
    vector<GameObject*> terrainvec;
    int terrainNum = 1;

    //mesh
    int width = 257;
    int length = 257;
    XMFLOAT3 scale = { 2.0f, 2.0f, 2.0f };
    TerrainMesh* terrainMesh = new TerrainMesh(device, commandList, width, length, scale);
    TerrainObj* dummy = new TerrainObj;
    dummy->SetMesh(terrainMesh);
    dummy->SetConstantNumber(E_TERRAINCONSTANTNUMBER);

    //terrain Shader
    Shader* shader = new TerrainShader;
    shader->CreatebPipeLineState(device, m_rootSignature);

    //terrain material
    Material* material = new Material();
    material->SetShader(shader);
    dummy->InsertMaterial(material);

    //set Dummy
    terrainvec.emplace_back(dummy);

    for (int i = 0; i < terrainNum; ++i) {
        TerrainObj* terrain = new TerrainObj;
        terrain->SetTransFormPos(((-(float)(width - 1) / 2) * scale.x), 0.0f, (-(float)(length - 1) / 2) * scale.z);
        terrainvec.emplace_back(terrain);
    }
    m_GameObjects.emplace_back(terrainvec);

    InitCbInform(device, commandList, m_GameObjects.size() - 1);


    //make QuadTree
    CreateQuadTree(&m_QuadRoot, E_ROOT, ((-(float)(width - 1) / 2) * scale.x), (-(float)(length - 1) / 2) * scale.z, float(width - 1) * scale.x, float(length - 1) * scale.z);
}

void Scene::CreateQuadTree(QuadTree** parent, int number, float startX, float startZ, float width, float length)
{
    if (width < 30.0f) {
        return;
    }

    *parent = new QuadTree(startX, startZ, width, length);

    float cWidth = width / 2;
    float cLength = length / 2;

    CreateQuadTree(&(*parent)->Node[E_BL], E_BL, startX, startZ, cWidth, cLength);
    CreateQuadTree(&(*parent)->Node[E_BR], E_BR, startX + cWidth, startZ, cWidth, cLength);
    CreateQuadTree(&(*parent)->Node[E_UL], E_UL, startX, startZ + cLength, cWidth, cLength);
    CreateQuadTree(&(*parent)->Node[E_UR], E_UR, startX + cWidth, startZ + cLength, cWidth, cLength);
}

void Scene::ProcessPickingQuadTree(XMFLOAT3 rayOrigin, XMFLOAT3 rayDir , vector<QuadTree*>& nodes , QuadTree* node)
{
    XMVECTOR tempOrigin = XMLoadFloat3(&rayOrigin);
    XMVECTOR tempDirection = XMLoadFloat3(&rayDir);
    if (node->Node[0] != NULL)
    {
        for (int i = 0; i < 4; ++i) {
            float distance = FLT_MAX;
            if (node->Node[i]->aabbBox.Intersects(tempOrigin, tempDirection, distance)) {
                QuadTree* temp = node->Node[i];
                ProcessPickingQuadTree(rayOrigin, rayDir, nodes, temp);
            }
        }
        return;
    }
    else {
        nodes.emplace_back(node);
        return;
    }
}

QuadTree* Scene::ProcessOBBQuadTree(BoundingOrientedBox obbBox)
{
    QuadTree* temp = m_QuadRoot;
    if (temp->aabbBox.Intersects(obbBox)) {
        while (true) {
            if (temp->Node[0] != NULL) {
                for (int i = 0; i < 4; ++i) {
                    
                    if (temp->Node[i]->aabbBox.Intersects(obbBox)) {
                        temp = temp->Node[i];
                        break;
                    }
                }
            }
            else
                return temp;
        }
    }
    else {
        return NULL;
    }
}

QuadTree* Scene::InsertQuadObject(BoundingBox aabbBox)
{
    QuadTree* temp = m_QuadRoot;
    if (temp->aabbBox.Intersects(aabbBox)) {
        while (true) {
            if (temp->Node[0] != NULL) {
                for (int i = 0; i < 4; ++i) {

                    if (temp->Node[i]->aabbBox.Intersects(aabbBox)) {
                        temp = temp->Node[i];
                        break;
                    }
                }
            }
            else
                return temp;
        }
    }
    else {
        return NULL;
    }
}

void Scene::ProcessCheckVisable(BoundingFrustum& frustum, QuadTree* node)
{
    if (node->Node[0] != NULL)
    {
        for (int i = 0; i < 4; ++i) {
            float distance = FLT_MAX;
            if (node->Node[i]->aabbBox.Intersects(frustum)) {
                QuadTree* temp = node->Node[i];
                ProcessCheckVisable(frustum, temp);
            }
        }
        return;
    }
    else {
        for (int i = 0; i < node->gVec.size(); ++i) {
            node->gVec[i]->SetIsVisable(true);
        }
    }
}

void Scene::DelQuadTree(QuadTree* parent)
{
    if (parent->Node[0] == NULL) {
        delete parent;
        parent = NULL;
        return;
    }
    DelQuadTree(parent->Node[E_BL]);
    DelQuadTree(parent->Node[E_BR]);
    DelQuadTree(parent->Node[E_UL]);
    DelQuadTree(parent->Node[E_UR]);

    delete parent;
    parent = NULL;

}

vector<int> Scene::RunAstaralgorithm(GameObject* Obj ,Node* fNode , float mapWorld)
{
    int dx[4] = { 0,0,-1,1 };
    int dz[4] = { 1,-1,0,0 };

    unordered_map<int, Node*> openList;//NodeNumber, NODE
    unordered_map<int, Node*> closeList; //NodeNumber,NODE

    //obj에서 가장 가까운 노드를 closeList에 포함시키고 openList에 그 노드 주변 노드를 넣는다.
    XMFLOAT3 pos = Obj->GetPosition(); //x , z -> -256 ~ 256 -> 0 ~ 512
    pos.x += mapWorld;
    pos.z += mapWorld;
    int xidx = int(pos.x) / AstarInterval;
    int zidx = int(pos.z) / AstarInterval;
    int size = m_AstarNodes.size();

    if (xidx < 0 || zidx < 0 || xidx >= size || zidx >= size)
        return vector<int>();
    
    Node* startPoint = &m_AstarNodes[zidx][xidx];
    startPoint->ParentNode = NULL;
    closeList[startPoint->number] = startPoint;

    for (int i = 0; i < 4; ++i) {
        int fx = xidx + dx[i];
        int fz = zidx + dz[i];

        if (fx < 0 || fz < 0 || fx >= size || fz >= size) 
            continue;
        Node* node = &m_AstarNodes[fz][fx];
        node->G = Vector3::Length(Vector3::Subtract(startPoint->pos, node->pos)); //인접 노드와의 거리
        node->H = Vector3::Length(Vector3::Subtract(node->pos, fNode->pos)); //최종 노드와의 거리
        node->F = node->G + node->H;
        node->ParentNode = startPoint;
        openList[node->number] = node;
    }
    //이제 본격적으로 Astar를 돌림
    
    //openList에서 F가 가장 큰값을 찾는다.
    while (true) {
        float minvalue = FLT_MAX;
        int idx = -1;
        for (auto& a : openList) {
            if (minvalue > a.second->F) {
                minvalue = a.second->F;
                idx = a.second->number;
            }
        }
        if (idx == -1) {
            idx = -2;//error
        }

        Node* newNode = openList[idx];
        openList.erase(newNode->number);
        closeList[newNode->number] = newNode;
        if (newNode->number == fNode->number) {
            break;
        }


        XMFLOAT3 newPos = newNode->pos;

        newPos.x += mapWorld;
        newPos.z += mapWorld;
        int cx = int(newPos.x) / AstarInterval;
        int cz = int(newPos.z) / AstarInterval;

        for (int i = 0; i < 4; ++i) {
            int fx = cx + dx[i];
            int fz = cz + dz[i];

            if ((fx < 0 || fz < 0 || fx >= size || fz >= size))
                continue;
            Node* node = &m_AstarNodes[fz][fx];
            
            if (closeList.count(node->number))
                continue;                         //closeList를 조사해서 중복되는 값인지 확인
            
            float G = Vector3::Length(Vector3::Subtract(newNode->pos, node->pos)); //인접 노드와의 거리
            float H = Vector3::Length(Vector3::Subtract(node->pos, fNode->pos)); //최종 노드와의 거리
            float F = G + H;
            Node* ParentNode = newNode;
            if (openList.count(node->number)) {
                if (openList[node->number]->F > F) {
                    openList[node->number]->G = G;
                    openList[node->number]->H = H;
                    openList[node->number]->F = F;
                    openList[node->number]->ParentNode = ParentNode;
                }
            }
            else {
                node->G = G;
                node->H = H;
                node->F = F;
                node->ParentNode = ParentNode;
                openList[node->number] = node;
            }
        }


    }

    Node* n = fNode;
    vector<int> tmp;
    while (n != NULL) {
        tmp.emplace_back(n->number);
        n = n->ParentNode;
    }
    return tmp;
}

void Scene::PrePareCompute(ID3D12GraphicsCommandList* commandList)
{
    //프레임들의 월드변환과 점들의 위치, 노말 , 탄젠트 , 바이탄젠트 들을 컴퓨트 쉐이더에서 계산 하도록 한다.
    for (int i = 0; i < m_GameObjects.size(); ++i) {
        auto dummy = m_GameObjects[i][0];
        if (dummy->GetMaterial()[0]->GetShader()->GetShaderNumber() == E_SKINNEDINSTANCINGSHADER) {
            SkinnedInstancingShader* tshader = (SkinnedInstancingShader*)dummy->GetMaterial()[0]->GetShader();
            MODELINFO* tmodel = &tshader->GetModelInfo();
            tshader->SetComputePipelineState(commandList);
            for (int j = 0; j < tmodel->skinnedMeshNum; ++j) {
                tmodel->skinnedMeshes[j]->skinningBoneTransforms = tshader->GetSkinnedBuffer()[j];
                tmodel->skinnedMeshes[j]->mappedSkinningBoneTransforms = tshader->GetMappedSkinnedBuffer()[j];
                ID3D12Resource* buffers[4] = { tshader->m_PositionW[j].Get(), tshader->m_NormalW[j].Get(), tshader->m_TangentW[j].Get(), tshader->m_BiTangentW[j].Get() };
                tmodel->skinnedMeshes[j]->OnPreRender(commandList, m_GameObjects[i].size(), buffers);
            }
        }
        if (dummy->GetMaterial()[0]->GetShader()->GetShaderNumber() == E_PARTICLESHADER) {
            ParticleShader* pshader = (ParticleShader*)dummy->GetMaterial()[0]->GetShader();
            pshader->PrePareCompute(commandList);
        }

    }
}
void Scene::PrePareRender(ComPtr<ID3D12GraphicsCommandList>& commandList)
{
    ID3D12DescriptorHeap* ppHeaps[] = { m_DescriptorHeap.Get() };
    commandList.Get()->SetDescriptorHeaps(_countof(ppHeaps), ppHeaps);
    for (int i = 0; i < m_RootArguIdx.size(); ++i) {
        commandList.Get()->SetGraphicsRootDescriptorTable(m_rootArgumentInfos[m_RootArguIdx[i]].rootParameterIndex, m_rootArgumentInfos[m_RootArguIdx[i]].srvGpuDescriptorHandle);//scene에서 load해서 읽어온 이미지들을 set해준다.
    }

    //material
    auto cbMaterialGpuAddr = m_cbMaterials.Get()->GetGPUVirtualAddress();
    commandList->SetGraphicsRootConstantBufferView(GraphicsRootMaterials, cbMaterialGpuAddr);

    //light
    memcpy(m_cbMappedLights, &m_lights, sizeof(LIGHTS));
    D3D12_GPU_VIRTUAL_ADDRESS d3dcbLightsGpuVirtualAddress = m_cbLights->GetGPUVirtualAddress();
    commandList->SetGraphicsRootConstantBufferView(GraphicsRootLights, d3dcbLightsGpuVirtualAddress);

}

void Scene::OnRender(ComPtr<ID3D12GraphicsCommandList>& commandList)
{
    for (int i = 0; i < m_GameObjects.size(); ++i)
    {
        if (i == E_CUBEOBJ)
            continue;
        GameObject* dummy = m_GameObjects[i][0];//->set pipeLine and Mesh 
        int startNumber = 1;
        if (dummy->GetisPlayer() == true) // is player ? -> yes -> rendering dummy
            startNumber = 0;
        if (dummy->GetConstantNumber() != FAILNUMBER) {
            vector<Material*> mt = dummy->GetMaterial();
            for (int j = 0; j < mt.size(); ++j) {
                mt[j]->GetShader()->PrePareRender(commandList);//setpipeline
            }
            UpdateCbInform(commandList, dummy->GetConstantNumber(), startNumber , i);

            int idx = dummy->GetConstantNumber();
            UINT ncbElementBytes = ((sizeof(VS_CB_OBJ_INFO) + 255) & ~255);
            D3D12_GPU_VIRTUAL_ADDRESS d3dGpuVirtualAddress = m_GameObjCsi[idx].m_cbGameObjects->GetGPUVirtualAddress();

            for (int j = startNumber; j < m_GameObjects[i].size(); j++)
            {
                commandList->SetGraphicsRootConstantBufferView(GraphicsRootNoInstancingObj, d3dGpuVirtualAddress + (ncbElementBytes * j));
                //dummy have a mesh 
                if (i == E_BILLBOARDOBJ) {//sun and moon
                    if (j == 0)
                        continue;
                    m_GameObjects[i][0]->BasicRenderIdx(commandList, j - 1);
                }
                else {
                    m_GameObjects[i][0]->BasicRender(commandList);
                }
            }

        }
        else { //instancing objects -> dont have constant number == -1 == FAILNUMBER
            //isplayer -> no -> except dummy
            GameObject* dummy = m_GameObjects[i][0];
           
            if (E_SKINNEDINSTANCINGSHADER == dummy->GetMaterial()[0]->GetShader()->GetShaderNumber()) {
                dummy->OnPrePareInstancingRender(commandList.Get(), this);  //  set texture
                SkinnedInstancingShader* tshader = (SkinnedInstancingShader*)dummy->GetMaterial()[0]->GetShader();
                ComPtr<ID3D12Resource>* buffers[4] = { tshader->m_PositionW, tshader->m_NormalW, tshader->m_TangentW, tshader->m_BiTangentW };
                dummy->InstancingAnimateRender(commandList.Get(), m_GameObjects[i].size(), tshader->m_StandardBufferView, tshader->GetObjectIdxBufferView(), buffers , tshader, false);
            }
            else if (E_INSTANCESHADER == dummy->GetMaterial()[0]->GetShader()->GetShaderNumber()) {
                dummy->OnPrePareInstancingRender(commandList.Get(), this);  //  set texture
                InstancingShader* ishader = (InstancingShader*)dummy->GetMaterial()[0]->GetShader();
                ishader->SetPipeLine(commandList.Get(), 0);
                dummy->InstancingRender(commandList.Get() , dummy->GetInstanceNum(), ishader->m_ObjectinstancBufferView, false);
            }
            else if(E_RSSHADER == dummy->GetMaterial()[0]->GetShader()->GetShaderNumber()){
                RSShader* ishader = (RSShader*)dummy->GetMaterial()[0]->GetShader();
                ishader->SetPipeLine(commandList.Get(), 0);
                dummy->InstancingRender(commandList.Get(), dummy->GetInstanceNum(), *ishader->GetObjectBufferView());
            }
        }
    }
}

void Scene::OnShadwRender(ComPtr<ID3D12GraphicsCommandList>& commandList)
{
    if (shadowRender == true) {
        for (int i = 0; i < m_GameObjects.size(); ++i)
        {
            if (i == E_CUBEOBJ || i == E_TERRAINOBJ || i == E_BILLBOARDOBJ || i == E_SKYBOXOBJ || i == E_PARTICLEOBJ || i == E_EFFECTOBJ || i == E_NODEOBJ)
                continue;
            GameObject* dummy = m_GameObjects[i][0];//->set pipeLine and Mesh 
            int startNumber = 1;
            if (dummy->GetisPlayer() == true) // is player ? -> yes -> rendering dummy
                startNumber = 0;
            if (dummy->GetConstantNumber() != FAILNUMBER) {
                vector<Material*> mt = dummy->GetMaterial();
                for (int j = 0; j < mt.size(); ++j) {
                    mt[j]->GetShader()->SetShadowPipe(commandList.Get(), 0);//setpipeline
                }
                UpdateCbInform(commandList, dummy->GetConstantNumber(), startNumber, i);

                int idx = dummy->GetConstantNumber();
                UINT ncbElementBytes = ((sizeof(VS_CB_OBJ_INFO) + 255) & ~255);
                D3D12_GPU_VIRTUAL_ADDRESS d3dGpuVirtualAddress = m_GameObjCsi[idx].m_cbGameObjects->GetGPUVirtualAddress();

                for (int j = startNumber; j < m_GameObjects[i].size(); j++)
                {
                    commandList->SetGraphicsRootConstantBufferView(GraphicsRootNoInstancingObj, d3dGpuVirtualAddress + (ncbElementBytes * j));
                    //dummy have a mesh 
                    m_GameObjects[i][0]->BasicRender(commandList);
                }
            }
            else { //instancing objects -> dont have constant number == -1 == FAILNUMBER
                //isplayer -> no -> except dummy
                GameObject* dummy = m_GameObjects[i][0];

                if (E_SKINNEDINSTANCINGSHADER == dummy->GetMaterial()[0]->GetShader()->GetShaderNumber()) {
                    dummy->OnPrePareInstancingRender(commandList.Get(), this);  //  set texture
                    SkinnedInstancingShader* tshader = (SkinnedInstancingShader*)dummy->GetMaterial()[0]->GetShader();
                    ComPtr<ID3D12Resource>* buffers[4] = { tshader->m_PositionW, tshader->m_NormalW, tshader->m_TangentW, tshader->m_BiTangentW };
                    dummy->InstancingAnimateRender(commandList.Get(), m_GameObjects[i].size(), tshader->m_StandardBufferView, tshader->GetObjectIdxBufferView(), buffers, tshader, true);
                }
                else if (E_INSTANCESHADER == dummy->GetMaterial()[0]->GetShader()->GetShaderNumber()) {
                    dummy->OnPrePareInstancingRender(commandList.Get(), this);  //  set texture
                    InstancingShader* ishader = (InstancingShader*)dummy->GetMaterial()[0]->GetShader();
                    ishader->SetShadowPipe(commandList.Get(), 0);
                    dummy->InstancingRender(commandList.Get(), dummy->GetInstanceNum(), ishader->m_ObjectinstancBufferView, true);
                }
                else if (E_RSSHADER == dummy->GetMaterial()[0]->GetShader()->GetShaderNumber()) {
                    RSShader* ishader = (RSShader*)dummy->GetMaterial()[0]->GetShader();
                    ishader->SetShadowPipe(commandList.Get(), 0);
                    dummy->InstancingRender(commandList.Get(), dummy->GetInstanceNum(), *ishader->GetObjectBufferView());
                }
            }
        }
    }
}

void Scene::OnEffectRender(ComPtr<ID3D12GraphicsCommandList>& commandList)
{
    GameObject* dummy = m_GameObjects[E_EFFECTOBJ][0];//->set pipeLine and Mesh 
    WeaponTrailerShader* wshader = (WeaponTrailerShader*)dummy->GetMaterial()[0]->GetShader();
    wshader->Render(commandList.Get());
}

void Scene::OnParticleRender(ComPtr<ID3D12GraphicsCommandList>& commandList , XMFLOAT3 cPos)
{
    GameObject* dummy = m_GameObjects[E_PARTICLEOBJ][0];//->set pipeLine and Mesh 
    ParticleShader* ptshader = (ParticleShader*)dummy->GetMaterial()[0]->GetShader();
    ptshader->Render(commandList.Get(), cPos);
}

void Scene::OnNodeRender(ComPtr<ID3D12GraphicsCommandList>& commandList)
{
    GameObject* dummy = m_GameObjects[E_NODEOBJ][0];//->set pipeLine and Mesh 
    int startNumber = 1;
    if (dummy->GetisPlayer() == true) // is player ? -> yes -> rendering dummy
        startNumber = 0;
    if (dummy->GetConstantNumber() != FAILNUMBER) {
        vector<Material*> mt = dummy->GetMaterial();
        for (int i = 0; i < mt.size(); ++i) {
            mt[i]->GetShader()->PrePareRender(commandList);//setpipeline
        }
        UpdateCbInform(commandList, dummy->GetConstantNumber(), startNumber, E_NODEOBJ);

        int idx = dummy->GetConstantNumber();
        UINT ncbElementBytes = ((sizeof(VS_CB_OBJ_INFO) + 255) & ~255);
        D3D12_GPU_VIRTUAL_ADDRESS d3dGpuVirtualAddress = m_GameObjCsi[idx].m_cbGameObjects->GetGPUVirtualAddress();

        for (int j = startNumber; j < m_GameObjects[E_NODEOBJ].size(); j++)
        {
            commandList->SetGraphicsRootConstantBufferView(GraphicsRootNoInstancingObj, d3dGpuVirtualAddress + (ncbElementBytes * j));
            //dummy have a mesh 
            m_GameObjects[E_NODEOBJ][0]->BasicRender(commandList);
        }
    }

}

void Scene::OnUIRender(ComPtr<ID3D12GraphicsCommandList>& commandList)
{
    for (int i = 0; i < m_UIObjects.size(); ++i)
    {
        m_UIObjects[i][0]->GetMaterial()[0]->GetShader()->PrePareRender(commandList);
        for (int j = 0; j < m_UIObjects[i].size(); ++j) {
            m_UIObjects[i][j]->BasicRender(commandList);
        }
    }

}

void Scene::OnLightRender(ComPtr<ID3D12GraphicsCommandList>& commandList)
{
    m_LightObjects[E_LIGHTBOJ][0]->GetMaterial()[0]->GetShader()->PrePareRender(commandList);
    for (int j = 0; j < m_LightObjects[E_LIGHTBOJ].size(); ++j) {
        m_LightObjects[E_LIGHTBOJ][j]->BasicRender(commandList);
    }
}

void Scene::OnPostRender(ComPtr<ID3D12GraphicsCommandList>& commandList)
{
    m_LightObjects[E_POSTOBJ][0]->GetMaterial()[0]->GetShader()->PrePareRender(commandList);
    for (int j = 0; j < m_LightObjects[E_POSTOBJ].size(); ++j) {
        m_LightObjects[E_POSTOBJ][j]->BasicRender(commandList);
    }
}

void Scene::ReleaseUploadBuffer()
{
    m_materialUploadBuffer->Release();

    for (int i = 0; i < m_GameObjects.size(); ++i) {
        if (m_GameObjects[i][0]->GetMaterial()[0]->GetShader()->GetShaderNumber() == E_PARTICLESHADER) {
            ParticleShader* pshader = (ParticleShader*)m_GameObjects[i][0]->GetMaterial()[0]->GetShader();
            pshader->ReleaseUploadBuffer();
        }
        else
            m_GameObjects[i][0]->ReleaseUploadBuffer();
    }

    //model
    for (int i = 0; i < m_Modelvec.size(); ++i) {
        m_Modelvec[i]->modelRootObject->ReleaseUploadBuffer();
        for (int j = 0; j < m_Modelvec[i]->skinnedMeshNum; ++j) {
            m_Modelvec[i]->skinnedMeshes[j]->ReleaseUploadBuffer();
        }
    }
    //objects
    for (int i = 0; i < m_Modelvec.size(); ++i) {
        m_Objects[i]->ReleaseUploadBuffer();
    }


    for (int i = 0; i < m_textures.size(); ++i) {
        if (m_textures[i]->m_texturesUploadBuffer) {
            for (int j = 0; j < m_textures[i]->m_textureNum; ++j) {
                m_textures[i]->m_texturesUploadBuffer[j]->Release();
            }
            delete[] m_textures[i]->m_texturesUploadBuffer;
        }
    }

    for (int i = 0; i < m_GameObjects.size(); ++i) {
        vector<Texture*> texVec = m_GameObjects[i][0]->GetTextures();

        for (int j = 0; j < texVec.size(); ++j) {
            for (int k = 0; k < texVec[j]->m_textureNum; ++k) {
                texVec[j]->m_texturesUploadBuffer[k]->Release();
            }
            delete[] texVec[j]->m_texturesUploadBuffer;
        }

    }
}

void Scene::InitCbInform(ComPtr<ID3D12Device>& device, ComPtr<ID3D12GraphicsCommandList>& commandList, int idx)
{
    ConstantInform Ctemp;
    UINT ncbElementBytes = ((sizeof(VS_CB_OBJ_INFO) + 255) & ~255); //256의 배수
    Ctemp.m_cbGameObjects = ::CreateBufferResource(device.Get(), commandList.Get(), NULL, ncbElementBytes * m_GameObjects[idx].size(), D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
    Ctemp.m_cbGameObjects->Map(0, NULL, (void**)&Ctemp.m_cbMappedGameObjects);
    m_GameObjCsi.emplace_back(Ctemp);
}

void Scene::UpdateCbInform(ComPtr<ID3D12GraphicsCommandList>& commandList, int idx, int startNumber, int objNum)
{
    UINT ncbElementBytes = ((sizeof(VS_CB_OBJ_INFO) + 255) & ~255);
    XMFLOAT4X4 xmf4x4World;
    XMFLOAT3 normal = XMFLOAT3(0.0f, 1.0f, 0.0f);
    for (int j = startNumber; j < m_GameObjects[objNum].size(); j++)
    {
        XMStoreFloat4x4(&xmf4x4World, XMMatrixTranspose(XMLoadFloat4x4(&m_GameObjects[objNum][j]->GetWorld())));
        VS_CB_OBJ_INFO* pbMappedcbGameObject = (VS_CB_OBJ_INFO*)(m_GameObjCsi[idx].m_cbMappedGameObjects + (j * ncbElementBytes));
        ::memcpy(&pbMappedcbGameObject->world, &xmf4x4World, sizeof(XMFLOAT4X4));
    }
}

void Scene::UpdateInstanceObjInform(float timeElapsed, XMFLOAT3 cameraPos , XMFLOAT3 playerPos)
{
    for (int i = 0; i < m_GameObjects.size(); ++i) {

        auto mt = m_GameObjects[i][0]->GetMaterial();
        for (int j = 0; j < mt.size(); ++j) {

            if (m_GameObjects[i][0]->GetisPlayer() != true) {
                m_GameObjects[i][0]->SetTransFormPos(DUMMY_WAITING, DUMMY_WAITING, DUMMY_WAITING);
            }

            auto shaderNumber = mt[j]->GetShader()->GetShaderNumber();
            if (shaderNumber == E_SKYBOXSHADER) {
                SkyBoxShader* sShader = (SkyBoxShader*)mt[j]->GetShader();
                for (int k = 0; k < m_GameObjects[i].size(); ++k) {
                    m_GameObjects[i][k]->SetTransFormPos(cameraPos.x, cameraPos.y, cameraPos.z);
                }
            }

            if (shaderNumber == E_INSTANCESHADER) {
                InstancingShader* tshader = ((InstancingShader*)mt[j]->GetShader());
                for (int k = 0; k < m_GameObjects[i].size(); ++k) {
                    ((HierarchyObj*)m_GameObjects[i][k])->UpdateTransform(tshader->GetMappedObject(), k, NULL);
                }
            }
            else if (shaderNumber == E_SKINNEDINSTANCINGSHADER) {

                //길찾기 수행
                for (int k = 1; k < m_GameObjects[i].size(); ++k) {
                    if (m_GameObjects[i][k]->GetState() == E_IDLE) {
                        XMFLOAT3 pos = playerPos;
                        pos.x += TerrainWorld;
                        pos.z += TerrainWorld;
                        int xidx = int(pos.x) / AstarInterval;
                        int zidx = int(pos.z) / AstarInterval;
                        int size = m_AstarNodes.size();
                        if (xidx < 0 || zidx < 0 || xidx >= size || zidx >= size) //-> 범위에 벗어난 것은 conintue
                            continue;
                        Node* pNode = &m_AstarNodes[zidx][xidx];

                        vector<int> tmpvec = RunAstaralgorithm(m_GameObjects[i][k], pNode, TerrainWorld);
                        if (tmpvec.size() > 0) {
                            m_GameObjects[i][k]->SetRouteList(tmpvec , m_AstarNodes);
                            m_GameObjects[i][k]->SetState(E_MOVING);
                        }
                    }
                    else if (m_GameObjects[i][k]->GetState() == E_MOVING) { //움직이게 하기
                        m_GameObjects[i][k]->MovingObjects(m_AstarNodes, timeElapsed);
                    }
                }

                SkinnedInstancingShader* tshader = ((SkinnedInstancingShader*)mt[j]->GetShader());
                tshader->UpdateShaderVariables(m_GameObjects[i].size());
                for (int k = 0; k < m_GameObjects[i].size(); ++k) {
                    m_GameObjects[i][k]->Animate(timeElapsed, NULL); // update frames transform
                    tshader->UpdateShaderVariable(k);
                    WeaponPoint temp = (((HierarchyObj*)m_GameObjects[i][k])->UpdateTransformOnlyStandardMesh(tshader->GetMappedStandard(), k, tshader->GetModelInfo().standardMeshNum, 1, NULL, NULL));
                    if (k == 0 && m_GameObjects[i][k]->GetisPlayer() == true)
                        pWeaponPoint = temp;
                }

                
            }
            else if (shaderNumber == E_RSSHADER) {
                RSShader* rshader = ((RSShader*)mt[j]->GetShader());
                for (int k = 0; k < m_GameObjects[i].size(); ++k) {
                    m_GameObjects[i][k]->UpdateGrassTransForm(k, rshader->GetMappedObject());
                }
            }
            else if (shaderNumber == E_PARTICLESHADER) {
                ParticleShader* ptShader = ((ParticleShader*)mt[j]->GetShader());
                ptShader->AnimateAndUpdate(timeElapsed, cameraPos);
            }
            else if (shaderNumber == E_WEAPONTRAILSHADER) {
                WeaponTrailerShader* wtShader((WeaponTrailerShader*)mt[j]->GetShader());
                wtShader->AnimateObjects(timeElapsed);
            }
            else if (shaderNumber == E_BILLBOARDSHADER) //sun and moon
            {
                for (int k = 0; k < m_GameObjects[i].size(); ++k) {

                    if (k == 1) { //sun
                        m_sunPos = Vector3::ScalarProduct(Vector3::Normalize(m_rtDir), -SUNDISTANCE, false);
                        m_GameObjects[i][k]->SetTransFormPos(m_sunPos.x, m_sunPos.y, m_sunPos.z);
                    }

                    if (k == 2) { //moon
                        XMFLOAT3 moonPos = Vector3::ScalarProduct(Vector3::Normalize(m_rtDir), SUNDISTANCE, false);
                        m_GameObjects[i][k]->SetTransFormPos(moonPos.x, moonPos.y, moonPos.z);
                    }
                    m_GameObjects[i][k]->UpdateTransform();
                }
            }
            else {
                //no instancing constant 
                for (int k = 0; k < m_GameObjects[i].size(); ++k) {
                    m_GameObjects[i][k]->UpdateTransform();
                }
            }
        }
    }
}

void Scene::UpdateDerationalLight(float timeElpased)
{
    shadowRender = true;
    XMFLOAT3 Axis = XMFLOAT3(0.0f, 0.0f, 1.0f);
    float rSpeed = 1.0f;
}

void Scene::UpdateFrustumCulling(BoundingFrustum& frustum)
{
    for (int i = 0; i < m_GameObjects.size(); ++i) {
        for (int k = 0; k < m_GameObjects[i].size(); ++k) {
            m_GameObjects[i][k]->SetIsVisable(false);
        }
    }


    ProcessCheckVisable(frustum, m_QuadRoot); // check visable by QuadTree
    for (int i = 0; i < m_GameObjects.size(); ++i) {
        GameObject* dummy = m_GameObjects[i][0];
        Shader* tshader = dummy->GetMaterial()[0]->GetShader();
        int instanceNum = m_GameObjects[i].size();
        if (m_GameObjects[i].size() > 0) {
            instanceNum = 0;
            for (int j = 0; j < m_GameObjects[i].size(); ++j) {
                if (m_GameObjects[i][j]->GetIsVisable())
                    instanceNum++;
            }
            
            sort(m_GameObjects[i].begin() + 1, m_GameObjects[i].end(), [](GameObject* a, GameObject* b) {
                return a->GetIsVisable() > b->GetIsVisable(); //sort GameObject
            });
            dummy->SetInstanceNum(instanceNum + 1);
        }
    }
}


void Scene::SetCbvSrvDescIncrementSize(UINT incrementSize)
{
    m_gCbvSrvDescIncrementSize = incrementSize;
}

void Scene::SetShadowBuffer(ID3D12Resource* shadowResource)
{
    for (int i = 0; i < ShadowCount; ++i) 
        m_shadowBuffer[i] = shadowResource;
}

void Scene::SetCasCadeBuffer(ID3D12Resource** CascadeResource)
{
    for (int i = 0; i < NUM_CASCADES; ++i) {
        m_CasCadeBuffer[i] = CascadeResource[i];
    }
}

void Scene::SetPostEffectBuffer(ID3D12Resource** PosteffectResource)
{
    for (int i = 0; i < postRenderCount; ++i) {
        m_posteffectBuffer[i] = PosteffectResource[i];
    }
}

void Scene::SetDepthBuffer(ID3D12Resource* depthResource)
{
    for (int i = 0; i < DepthTexture; ++i) {
        m_depthBuffer[i] = depthResource;
    }
}

void Scene::SetParticlePosition(ID3D12GraphicsCommandList* commandList)
{
    ParticleShader* pshader = (ParticleShader*)m_GameObjects[E_PARTICLEOBJ][0]->GetMaterial()[0]->GetShader();
    vector<ParticleInfoInCPU> pinfo = pshader->GetParticleCPU();
    int pNum = pshader->GetParticleNum();

    //modifyParticle
    int rcNum = m_GameObjects[E_REDCAROBJ].size();
    ParticleBlobModify blob;
    blob.velocityMin = XMFLOAT3(-0.01f, 2.0f, -0.01f);
    blob.velocityMax = XMFLOAT3(-0.01f, 5.0f, 0.01f);

    blob.positionMin = XMFLOAT3(-1.0f, -1.0f, -1.0f);
    blob.positionMax = XMFLOAT3(1.0f, 1.0f, 1.0f);

    blob.ageMin = 2.0f;
    blob.ageMax = 10.0f;
    blob.texid = E_SMOKE1PARTICLE;

    for (int i = 0; i < rcNum; ++i) {
        pshader->particles[i].position = m_GameObjects[E_REDCAROBJ][i]->GetTransFormPos();
        pshader->particles[i].position.y -= 18.0f;
        //pinfo[i].position.y -= 40.0f;
        pshader->ModifyParticle(commandList, i, i, blob);
    }
    blob.texid = E_DUSTPARTICLE;
    pshader->particles[3].texid = 3;
    pshader->ModifyParticle(commandList, 3, 3, blob);
}

vector<vector<GameObject*>>& Scene::GetGameObjects()
{
    return m_GameObjects;
}

vector<LoadedModelInfo*>& Scene::GetModelVec()
{
    return m_Modelvec;
}


QuadTree* Scene::GetQuad()
{
    return m_QuadRoot;
}

LIGHTS* Scene::GetLight()
{
    return &m_lights;
}

XMFLOAT3 Scene::GetSunPos()
{
    return m_sunPos;
}

WeaponPoint Scene::GetpWeaponPoint()
{
    return pWeaponPoint;
}