#include"WeaponTrail.hlsl"

struct VS_SHADOW_OUTPUT
{
    float4 position : SV_POSITION;
};


struct VS_GRASS_SHADOW_OUTPUT
{
    float3 position : POSITION;
    float4x4 transform : WORLDMATRIX;
    float gNumber : GRASSNUMBER;
};

struct GS_GRASS_SHADOW_OUTPUT
{
    float4 position : SV_POSITION;
    float2 uv0 : TEXCOORD;
};


struct VS_INSTANCING_SHADOW_INPUT
{
    float3 position : POSITION;
    float4x4 transform : WORLDMATRIX;
};

struct VS_SKINNED_INSTANCING_SHADOW_INPUT
{
    float3 position : POSITION;
    uint objIdx : OBJIDX;
    uint vertexID : SV_VertexID;
};




//�׸��ڸ� ���� hlsl 
VS_GRASS_SHADOW_OUTPUT VS_SHADOW_REAL_GRASS(VS_REAL_GRASS_INPUT input)
{
    VS_GRASS_SHADOW_OUTPUT output;
    output.position = input.position;
    output.transform = input.transform;
    output.gNumber = input.gNumber;
    return (output);
}


[maxvertexcount(12)]
void GS_SHADOW_REAL_GRASS(point VS_GRASS_SHADOW_OUTPUT input[1], inout TriangleStream<GS_GRASS_SHADOW_OUTPUT> outStream)
{
    float quadSize = 2.0f;
    //quad 0
    float3 c = input[0].position;
    
    [unroll]
    for (int j = 0; j < 3; ++j)
    {
        float3 pVertices[4];
        if (j == 0)
        {
            pVertices[0] = float3(c.x - quadSize, c.y + quadSize, 0.0f); // -w +h
            pVertices[1] = float3(c.x + quadSize, c.y + quadSize, 0.0f); // +w +h
            pVertices[2] = float3(c.x - quadSize, c.y - quadSize, 0.0f); // -w +h
            pVertices[3] = float3(c.x + quadSize, c.y - quadSize, 0.0f); // -w +h
            
        }
        else if (j == 1)
        {
            pVertices[0] = float3(c.x - quadSize, c.y + quadSize, c.z + quadSize); // -w +h +z
            pVertices[1] = float3(c.x + quadSize, c.y + quadSize, c.z - quadSize); // -w +h
            pVertices[2] = float3(c.x - quadSize, c.y - quadSize, c.z + quadSize); // -w +h
            pVertices[3] = float3(c.x + quadSize, c.y - quadSize, c.z - quadSize); // -w +h
        }
        else if (j == 2)
        {
            pVertices[0] = float3(c.x - quadSize, c.y + quadSize, c.z - quadSize); // -w +h +z
            pVertices[1] = float3(c.x + quadSize, c.y + quadSize, c.z + quadSize); // -w +h
            pVertices[2] = float3(c.x - quadSize, c.y - quadSize, c.z - quadSize); // -w +h
            pVertices[3] = float3(c.x + quadSize, c.y - quadSize, c.z + quadSize); // -w +h
        }
       
        float2 pUV[4] = { float2(0.0f, 0.0f), float2(1.0f, 0.0f), float2(0.0f, 1.0f), float2(1.0f, 1.0f) };
        float3 normalW[4] = { float3(0.0f, 1.0f, 0.0f), float3(0.0f, 1.0f, 0.0f), float3(0.0f, -1.0f, 0.0f), float3(0.0f, -1.0f, 0.0f) };
        GS_GRASS_SHADOW_OUTPUT output;
        for (int i = 0; i < 4; ++i)
        {
            float4 positionW = mul(float4(pVertices[i], 1.0f), input[0].transform);
            if (normalW[i].y > 0.9f)
            {
                positionW.x += cos(gTotalTime + input[0].gNumber);
            }
            output.position = mul(positionW, gmtxLightViewProjection[lightCameraNum]);
            output.uv0 = pUV[i];
            outStream.Append(output);
        }
        outStream.RestartStrip();
    }
}


//particleShadow

[maxvertexcount(4)]
void GS_SHADOW_PARTICLE(point GS_PARTICLE_INPUT input[1], inout TriangleStream<GS_PARTICLE_OUTPUT> outStream)
{
    if (input[0].age < 0 || input[0].age > 4.0f)
        return;
    
    float3 vUp = float3(0.0f, 1.0f, 0.0f);
    float3 vLook = gvCameraPosition.xyz - input[0].position;
    vLook = normalize(vLook);
    float3 vRight = cross(vUp, vLook);
    float fHalfW = input[0].size * 0.5f;
    float fHalfH = input[0].size * 0.5f;
    float4 pVertices[4];
    float4x4 worldMat = { float4(vRight, 0.0f), float4(vUp, 0.0f), float4(vLook, 0.0f), float4(input[0].position, 1.0f) };
    
    pVertices[0] = float4(input[0].position + fHalfW * vRight - fHalfH * vUp, 1.0f);
    pVertices[1] = float4(input[0].position + fHalfW * vRight + fHalfH * vUp, 1.0f);
    pVertices[2] = float4(input[0].position - fHalfW * vRight - fHalfH * vUp, 1.0f);
    pVertices[3] = float4(input[0].position - fHalfW * vRight + fHalfH * vUp, 1.0f);

    float2 pUVs[4] = { float2(0.0f, 1.0f), float2(0.0f, 0.0f), float2(1.0f, 1.0f), float2(1.0f, 0.0f) };
    GS_PARTICLE_OUTPUT output;
    float3 normalW[4] = { float3(0.0f, -1.0f, 0.0f), float3(0.0f, 1.0f, 0.0f), float3(0.0f, -1.0f, 0.0f), float3(0.0f, 1.0f, 0.0f) };
    
    
    for (int j = 0; j < 4; ++j)
    {
        output.position = mul(pVertices[j], gmtxLightViewProjection[lightCameraNum]);
        output.positionW = pVertices[j];
        output.uv = pUVs[j];
        output.texid = input[0].texid;
        output.normal = normalW[j];
        output.age = input[0].age;
        outStream.Append(output);
    }
}



VS_SHADOW_OUTPUT VS_SHADOW_TERRAIN(VS_TERRAIN_INPUT input)
{
    VS_SHADOW_OUTPUT output;
    output.position = mul(mul(float4(input.position, 1.0f), gmtxWorld), gmtxLightViewProjection[lightCameraNum]);
    return (output);
}


VS_SHADOW_OUTPUT VS_SHADOW_INSTANCING(VS_INSTANCING_SHADOW_INPUT input)
{
    VS_SHADOW_OUTPUT output;
    output.position = mul(mul(float4(input.position, 1.0f), input.transform), gmtxLightViewProjection[lightCameraNum]);
    return (output);
}

VS_SHADOW_OUTPUT VS_SHADOW_SKINNED_INSTANCING(VS_SKINNED_INSTANCING_SHADOW_INPUT input)
{
    VS_SHADOW_OUTPUT output;
    output.position = mul(float4(gsbSkinnedPosition[input.objIdx * gnVerticesNum + input.vertexID], 1.0f), gmtxLightViewProjection[lightCameraNum]);
    return (output);
}




float4 PS_SHADOW(VS_SHADOW_OUTPUT input) : SV_Target
{
    float depth = input.position.z;
    return float4(depth, depth, depth, 1.0f);
}

float4 PS_GRASS_SHADOW(GS_GRASS_SHADOW_OUTPUT input) : SV_Target
{
    float4 diffuseColor = gtxtSceneTexture[GRASS_TEXTURE].Sample(gspWarp, input.uv0);
    clip(diffuseColor.a - 0.1);
    
    float depth = input.position.z;
    return float4(depth, depth, depth, 1.0f);
}

float4 PS_SHADOW_PARTICLE(GS_PARTICLE_OUTPUT input) : SV_TARGET
{
    
    float4 normalTex = gtxtParticleTexture[input.texid].Sample(gspWarp, input.uv);
    clip(normalTex.a - 0.1);
    float depth = input.position.z;
    return float4(depth, depth, depth, 1.0f);
}