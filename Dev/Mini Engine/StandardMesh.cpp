#include "StandardMesh.h"
StandardMesh::~StandardMesh()
{
	if (positions) {
		delete[] positions;
		positions = NULL;
	}

	if (colors) {
		delete[] colors;
		colors = NULL;
	}
	if (normals) {
		delete[] normals;
		normals = NULL;
	}
	if (tangents) {
		delete[] tangents;
		tangents = NULL;
	}
	if (biTangents) {
		delete[] biTangents;
		biTangents = NULL;
	}
	if (textureCoords0) {
		delete[] textureCoords0;
		textureCoords0 = NULL;
	}
	if (textureCoords1) {
		delete[] textureCoords1;
		textureCoords1 = NULL;
	}

	if (subMeshesNum > 0)
	{
		for (int i = 0; i < subMeshesNum; i++)
		{
			if (subSetIndexBuffers[i]) {
				subSetIndexBuffers[i]->Release();
			}
			if (subSetIndices[i]) {
				delete[] subSetIndices[i];
				subSetIndices[i] = NULL;
			}
		}
		if (subSetIndexBuffers) {
			delete[] subSetIndexBuffers;
			subSetIndexBuffers = NULL;
		}
		if (subSetIndexBufferViews) {
			delete[] subSetIndexBufferViews;
			subSetIndexBufferViews = NULL;
		}

		if (subSetIndicesNum) {
			delete[] subSetIndicesNum;
			subSetIndicesNum = NULL;
		}
		if (subSetIndices) {
			delete[] subSetIndices;
			subSetIndices = NULL;
		}
		subMeshesNum = 0;
	}

}

void StandardMesh::ReleaseUploadBuffer()
{
	Mesh::ReleaseUploadBuffer();

	if (positionUploadBuffer) {
		positionUploadBuffer->Release();
		positionUploadBuffer = NULL;
	}

	if (textureCoord0UploadBuffer) {
		textureCoord0UploadBuffer->Release();
		textureCoord0UploadBuffer = NULL;
	}

	if (textureCoord1UploadBuffer) {
		textureCoord1UploadBuffer->Release();
		textureCoord1UploadBuffer = NULL;
	}

	if (normalUploadBuffer) {
		normalUploadBuffer->Release();
		normalUploadBuffer = NULL;
	}

	if (tangentUploadBuffer) {
		tangentUploadBuffer->Release();
		tangentUploadBuffer = NULL;
	}

	if (biTangentUploadBuffer) {
		biTangentUploadBuffer->Release();
		biTangentUploadBuffer = NULL;
	}

	if (subMeshesNum > 0 && subSetIndexUploadBuffers)
	{
		for (int i = 0; i < subMeshesNum; i++)
		{
			if (subSetIndicesNum[i] > 0)
				if (subSetIndexUploadBuffers[i]) subSetIndexUploadBuffers[i]->Release();
		}
	

		if (subSetIndexUploadBuffers) {
			delete[] subSetIndexUploadBuffers;
			subSetIndexUploadBuffers = NULL;
		}
	}


}

void StandardMesh::LoadMeshFromFile(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, FILE* inFile)
{
	char token[64] = { '\0' };
	BYTE strLength = 0;

	int nPositions = 0, nColors = 0, nNormals = 0, nTangents = 0, nBiTangents = 0, nTextureCoords = 0, nIndices = 0, nSubMeshes = 0, nSubIndices = 0;

	UINT nReads = (UINT)::fread(&verticesNum, sizeof(int), 1, inFile);
	nReads = (UINT)::fread(&strLength, sizeof(BYTE), 1, inFile);
	nReads = (UINT)::fread(meshName, sizeof(char), strLength, inFile);
	meshName[strLength] = '\0';

	for (; ; )
	{
		nReads = (UINT)::fread(&strLength, sizeof(BYTE), 1, inFile);
		nReads = (UINT)::fread(token, sizeof(char), strLength, inFile);
		token[strLength] = '\0';

		if (!strcmp(token, "<Bounds>:"))
		{
			nReads = (UINT)::fread(&boundingBoxAABBCenter, sizeof(XMFLOAT3), 1, inFile);
			nReads = (UINT)::fread(&boundingBoxAABBExtents, sizeof(XMFLOAT3), 1, inFile);
		}
		else if (!strcmp(token, "<Positions>:"))
		{
			nReads = (UINT)::fread(&nPositions, sizeof(int), 1, inFile);
			if (nPositions > 0)
			{
				type |= VertexPosition;
				positions = new XMFLOAT3[nPositions];
				nReads = (UINT)::fread(positions, sizeof(XMFLOAT3), nPositions, inFile);

				positionBuffer = ::CreateBufferResource(device, commandList, positions, sizeof(XMFLOAT3) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &positionUploadBuffer);

				positionBufferView.BufferLocation = positionBuffer->GetGPUVirtualAddress();
				positionBufferView.StrideInBytes = sizeof(XMFLOAT3);
				positionBufferView.SizeInBytes = sizeof(XMFLOAT3) * verticesNum;
				vertexSize++;
			}
		}
		else if (!strcmp(token, "<Colors>:"))
		{
			nReads = (UINT)::fread(&nColors, sizeof(int), 1, inFile);
			if (nColors > 0)
			{
				type |= VertexColor;
				colors = new XMFLOAT4[nColors];
				nReads = (UINT)::fread(colors, sizeof(XMFLOAT4), nColors, inFile);
			}
		}
		else if (!strcmp(token, "<TextureCoords0>:"))
		{
			nReads = (UINT)::fread(&nTextureCoords, sizeof(int), 1, inFile);
			if (nTextureCoords > 0)
			{
				type |= VertexTexCoord0;
				textureCoords0 = new XMFLOAT2[nTextureCoords];
				nReads = (UINT)::fread(textureCoords0, sizeof(XMFLOAT2), nTextureCoords, inFile);

				textureCoord0Buffer = ::CreateBufferResource(device, commandList, textureCoords0, sizeof(XMFLOAT2) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &textureCoord0UploadBuffer);

				textureCoord0BufferView.BufferLocation = textureCoord0Buffer->GetGPUVirtualAddress();
				textureCoord0BufferView.StrideInBytes = sizeof(XMFLOAT2);
				textureCoord0BufferView.SizeInBytes = sizeof(XMFLOAT2) * verticesNum;
				vertexSize++;
			}
		}
		else if (!strcmp(token, "<TextureCoords1>:"))
		{
			nReads = (UINT)::fread(&nTextureCoords, sizeof(int), 1, inFile);
			if (nTextureCoords > 0)
			{
				type |= VertexTexCoord1;
				textureCoords1 = new XMFLOAT2[nTextureCoords];
				nReads = (UINT)::fread(textureCoords1, sizeof(XMFLOAT2), nTextureCoords, inFile);

				textureCoord1Buffer = ::CreateBufferResource(device, commandList, textureCoords1, sizeof(XMFLOAT2) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &textureCoord1UploadBuffer);

				textureCoord1BufferView.BufferLocation = textureCoord1Buffer->GetGPUVirtualAddress();
				textureCoord1BufferView.StrideInBytes = sizeof(XMFLOAT2);
				textureCoord1BufferView.SizeInBytes = sizeof(XMFLOAT2) * verticesNum;
				vertexSize++;
			}
		}
		else if (!strcmp(token, "<Normals>:"))
		{
			nReads = (UINT)::fread(&nNormals, sizeof(int), 1, inFile);
			if (nNormals > 0)
			{
				type |= VertexNormal;
				normals = new XMFLOAT3[nNormals];
				nReads = (UINT)::fread(normals, sizeof(XMFLOAT3), nNormals, inFile);

				normalBuffer = ::CreateBufferResource(device, commandList, normals, sizeof(XMFLOAT3) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &normalUploadBuffer);

				normalBufferView.BufferLocation = normalBuffer->GetGPUVirtualAddress();
				normalBufferView.StrideInBytes = sizeof(XMFLOAT3);
				normalBufferView.SizeInBytes = sizeof(XMFLOAT3) * verticesNum;
				vertexSize++;
			}
		}
		else if (!strcmp(token, "<Tangents>:"))
		{
			nReads = (UINT)::fread(&nTangents, sizeof(int), 1, inFile);
			if (nTangents > 0)
			{
				type |= VertexTangent;
				tangents = new XMFLOAT3[nTangents];
				nReads = (UINT)::fread(tangents, sizeof(XMFLOAT3), nTangents, inFile);

				tangentBuffer = ::CreateBufferResource(device, commandList, tangents, sizeof(XMFLOAT3) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &tangentUploadBuffer);

				tangentBufferView.BufferLocation = tangentBuffer->GetGPUVirtualAddress();
				tangentBufferView.StrideInBytes = sizeof(XMFLOAT3);
				tangentBufferView.SizeInBytes = sizeof(XMFLOAT3) * verticesNum;
				vertexSize++;
			}
		}
		else if (!strcmp(token, "<BiTangents>:"))
		{
			nReads = (UINT)::fread(&nBiTangents, sizeof(int), 1, inFile);
			if (nBiTangents > 0)
			{
				biTangents = new XMFLOAT3[nBiTangents];
				nReads = (UINT)::fread(biTangents, sizeof(XMFLOAT3), nBiTangents, inFile);

				biTangentBuffer = ::CreateBufferResource(device, commandList, biTangents, sizeof(XMFLOAT3) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &biTangentUploadBuffer);

				biTangentBufferView.BufferLocation = biTangentBuffer->GetGPUVirtualAddress();
				biTangentBufferView.StrideInBytes = sizeof(XMFLOAT3);
				biTangentBufferView.SizeInBytes = sizeof(XMFLOAT3) * verticesNum;
				vertexSize++;
			}
		}
		else if (!strcmp(token, "<SubMeshes>:"))
		{
			nReads = (UINT)::fread(&(subMeshesNum), sizeof(int), 1, inFile);
			if (subMeshesNum > 0)
			{
				subSetIndicesNum = new int[subMeshesNum];
				subSetIndices = new UINT * [subMeshesNum];

				subSetIndexBuffers = new ComPtr<ID3D12Resource>[subMeshesNum];
				subSetIndexUploadBuffers = new ID3D12Resource * [subMeshesNum];
				subSetIndexBufferViews = new D3D12_INDEX_BUFFER_VIEW[subMeshesNum];

				for (int i = 0; i < subMeshesNum; i++)
				{
					nReads = (UINT)::fread(&strLength, sizeof(BYTE), 1, inFile);
					nReads = (UINT)::fread(token, sizeof(char), strLength, inFile);
					token[strLength] = '\0';
					if (!strcmp(token, "<SubMesh>:"))
					{
						int nIndex = 0;
						nReads = (UINT)::fread(&nIndex, sizeof(int), 1, inFile);
						nReads = (UINT)::fread(&(subSetIndicesNum[i]), sizeof(int), 1, inFile);
						if (subSetIndicesNum[i] > 0)
						{
							subSetIndices[i] = new UINT[subSetIndicesNum[i]];
							nReads = (UINT)::fread(subSetIndices[i], sizeof(UINT) * subSetIndicesNum[i], 1, inFile);

							subSetIndexBuffers[i] = ::CreateBufferResource(device, commandList, subSetIndices[i], sizeof(UINT) * subSetIndicesNum[i], D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_INDEX_BUFFER, &subSetIndexUploadBuffers[i]);

							subSetIndexBufferViews[i].BufferLocation = subSetIndexBuffers[i]->GetGPUVirtualAddress();
							subSetIndexBufferViews[i].Format = DXGI_FORMAT_R32_UINT;
							subSetIndexBufferViews[i].SizeInBytes = sizeof(UINT) * subSetIndicesNum[i];
						}
					}
				}
			}
		}
		else if (!strcmp(token, "</Mesh>"))
		{
			break;
		}
	}
}

void StandardMesh::Render(ID3D12GraphicsCommandList* commandList, int subSet)
{
	commandList->IASetPrimitiveTopology(primitiveTopology);

	D3D12_VERTEX_BUFFER_VIEW pVertexBufferViews[5] = { positionBufferView, textureCoord0BufferView, normalBufferView, tangentBufferView, biTangentBufferView };
	commandList->IASetVertexBuffers(slot, 5, pVertexBufferViews);

	if ((subMeshesNum > 0) && (subSet < subMeshesNum))
	{
		commandList->IASetIndexBuffer(&(subSetIndexBufferViews[subSet]));
		commandList->DrawIndexedInstanced(subSetIndicesNum[subSet], 1, 0, 0, 0);
	}
	else
	{
		commandList->DrawInstanced(verticesNum, 1, offset, 0);
	}
}

void StandardMesh::Render(ID3D12GraphicsCommandList* commandList, int nSubSet, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingBufferViews)
{
	commandList->IASetPrimitiveTopology(primitiveTopology);
	D3D12_VERTEX_BUFFER_VIEW vertexBufferViews[] = {
	 positionBufferView,
	 textureCoord0BufferView,
	 normalBufferView,
	 tangentBufferView,
	 biTangentBufferView,
	 instancingBufferViews
	};

	commandList->IASetVertexBuffers(slot,6, vertexBufferViews);

	if ((subMeshesNum > 0) && (nSubSet < subMeshesNum))
	{
		commandList->IASetIndexBuffer(&(subSetIndexBufferViews[nSubSet]));
		commandList->DrawIndexedInstanced(subSetIndicesNum[nSubSet], instancesNum, 0, 0, 0);
	}
	else
	{
		commandList->DrawInstanced(verticesNum, instancesNum, offset, 0);
	}
}

void StandardMesh::Render(ID3D12GraphicsCommandList* commandList, int nSubSet, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingBufferViews, int weaponNum)
{
	if (weaponNum == this->meshnum)
	{
		commandList->IASetPrimitiveTopology(primitiveTopology);
		D3D12_VERTEX_BUFFER_VIEW vertexBufferViews[] = {
			 positionBufferView,
			 textureCoord0BufferView,
			 normalBufferView,
			 tangentBufferView,
			 biTangentBufferView,
			 instancingBufferViews
		};

		commandList->IASetVertexBuffers(slot, 6, vertexBufferViews);

		if ((subMeshesNum > 0) && (nSubSet < subMeshesNum))
		{
			commandList->IASetIndexBuffer(&(subSetIndexBufferViews[nSubSet]));
			commandList->DrawIndexedInstanced(subSetIndicesNum[nSubSet], instancesNum, 0, 0, 0);
		}
		else
		{
			commandList->DrawInstanced(verticesNum, instancesNum, offset, 0);
		}
	}
}

void StandardMesh::ShadowRender(ID3D12GraphicsCommandList* commandList, int nSubSet, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingBufferViews)
{
	commandList->IASetPrimitiveTopology(primitiveTopology);
	D3D12_VERTEX_BUFFER_VIEW vertexBufferViews[] = {
	 positionBufferView,
	 instancingBufferViews
	};
	commandList->IASetVertexBuffers(slot, 2, vertexBufferViews);
	if ((subMeshesNum > 0) && (nSubSet < subMeshesNum))
	{
		commandList->IASetIndexBuffer(&(subSetIndexBufferViews[nSubSet]));
		commandList->DrawIndexedInstanced(subSetIndicesNum[nSubSet], instancesNum, 0, 0, 0);
	}
	else
	{
		commandList->DrawInstanced(verticesNum, instancesNum, offset, 0);
	}
}
