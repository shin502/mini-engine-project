#pragma once
#include "StandardSkinnedAnimationInstanceShader.h"
#define SKINNEDINSTANCINGPIPECOUNT 3
#define SKINNEDSHADOWPIPECOUNT 2
class SkinnedInstancingShader : public StandardSkinnedAnimationInstanceShader
{
private:
	ComPtr<ID3D12PipelineState> m_pipes[SKINNEDINSTANCINGPIPECOUNT];
	ComPtr<ID3D12PipelineState> m_ComputePipes;
	ComPtr<ID3D12PipelineState> m_ShadowPipes[SKINNEDSHADOWPIPECOUNT];

protected:

	UINT m_skinnedStride = 0;
	ComPtr<ID3D12Resource>* m_SkinnedResource = NULL;
	XMFLOAT4X4** m_MappedSkinned = NULL;

	UINT objIdxStride = 0;
	ComPtr<ID3D12Resource> m_objIdx = NULL;
	UINT* m_MappedObjIdx = NULL;
	D3D12_VERTEX_BUFFER_VIEW m_ObjIdxBufferView;


	UINT m_standardStride = 0;
	ComPtr<ID3D12Resource>* m_StandardResource = NULL;
	VS_VB_INSTANCE_OBJECT** m_MappedStandard = NULL;
	
public:
	D3D12_VERTEX_BUFFER_VIEW* m_StandardBufferView = NULL;

	ComPtr<ID3D12Resource>* m_PositionW = NULL;
	ComPtr<ID3D12Resource>* m_NormalW = NULL;
	ComPtr<ID3D12Resource>* m_TangentW = NULL;
	ComPtr<ID3D12Resource>* m_BiTangentW = NULL;

	SkinnedInstancingShader() {}
	virtual ~SkinnedInstancingShader();

	virtual void CreatebPipeLineState(ComPtr<ID3D12Device>& device, ComPtr<ID3D12RootSignature>& rootsignature, ComPtr<ID3D12RootSignature>& computeRootSignature);
	virtual void CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	virtual void UpdateShaderVariable(int objIdx);
	virtual void UpdateShaderVariables(int objNum);
	virtual D3D12_SHADER_BYTECODE CreateComputeShader();
	virtual int GetShaderNumber();
	virtual void SetPipeLine(ID3D12GraphicsCommandList* commandList, int idx);
	virtual void SetShadowPipe(ID3D12GraphicsCommandList* commandList, int idx);
	ComPtr<ID3D12Resource>* GetSkinnedBuffer() { return m_SkinnedResource; }
	XMFLOAT4X4** GetMappedSkinnedBuffer() { return m_MappedSkinned; }

	VS_VB_INSTANCE_OBJECT** GetMappedStandard() { return m_MappedStandard; };
	MODELINFO GetModelInfo() { return m_modelInfo; }
	void SetModelInfo(MODELINFO modelinfo) { m_modelInfo = modelinfo; }
	virtual void SetComputePipelineState(ID3D12GraphicsCommandList* commandList);
	D3D12_VERTEX_BUFFER_VIEW& GetObjectIdxBufferView() { return m_ObjIdxBufferView; }
	D3D12_VERTEX_BUFFER_VIEW* GetStandardBufferView() { return m_StandardBufferView; }
};

